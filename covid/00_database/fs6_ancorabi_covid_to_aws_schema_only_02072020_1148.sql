--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: covid19; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA covid19;


ALTER SCHEMA covid19 OWNER TO postgres;

--
-- Name: covid19_etl; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA covid19_etl;


ALTER SCHEMA covid19_etl OWNER TO postgres;

--
-- Name: covid19_report; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA covid19_report;


ALTER SCHEMA covid19_report OWNER TO postgres;

--
-- Name: covid19_tmp; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA covid19_tmp;


ALTER SCHEMA covid19_tmp OWNER TO postgres;

--
-- Name: protocolos_omi_covid; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA protocolos_omi_covid;


ALTER SCHEMA protocolos_omi_covid OWNER TO postgres;

--
-- Name: protocolos_omi_covid_base; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA protocolos_omi_covid_base;


ALTER SCHEMA protocolos_omi_covid_base OWNER TO postgres;

--
-- Name: protocolos_omi_covid_etl; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA protocolos_omi_covid_etl;


ALTER SCHEMA protocolos_omi_covid_etl OWNER TO postgres;

--
-- Name: mapro_sp_creatbl(integer); Type: FUNCTION; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE FUNCTION protocolos_omi_covid.mapro_sp_creatbl(n integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
declare
	_maxnum int4:=0;
	_nomtbl text;
	micodi text;
begin 
	if (n=0) then
	return 0;
	end if;
	_nomtbl:=(select 'pr_'||protocolo||'_'||nomtbl from protocolos_omi_covid.mapro_nomtbl where wproto=n);
	_nomtbl:='protocolos_omi_covid.'||_nomtbl;
	_maxnum:=(select max(id_num) from protocolos_omi_covid.mapro_nomvar where wproto=n);
	micodi:='DROP TABLE if exists '||_nomtbl||';
CREATE TABLE '||_nomtbl||' (
	nif int not null
	, nombre_paciente varchar(100)
	, sexo varchar(1)
	, nacimiento date
	, meses int
	, edad int
	, centro varchar(5)
	, fecha date not null
	, aym varchar(8)
	, estamento varchar(50)
	, cod_estamento varchar(50)
	, med_codigo varchar(50)
	, med_nombre varchar(50)
	, ciap_codigo varchar(100)
	, descrip_ciap varchar(100)
	, especiali varchar not NULL
	, fechr timestamp
	, medico_id int4
)
WITH (
	OIDS=FALSE
) ;

INSERT INTO '||_nomtbl||'
(nif, especiali, fecha, fechr, medico_id)
select distinct a.nif2, a.especiali, a.fecha2, a.fecha, a.medico_id from admomi.iddcop as a where a.wproto='||n||' and a.nif2 is not null and a.fecha is not null and a.especiali is not null;
select distinct a.nif as nif2, a.especiali, a.fecha2, a.fecha, b.id as medico_id 
  from admomi.iddcor as a,admomi.iddmed as b
 where  a.wproto='||n||'
   and a.numero=1 and a.wproto>=1 and a.medico=b.medico
   and a.nif is not null 
   and a.fecha is not null 
   and a.especiali is not null;

select * from protocolos_omi_covid.mapro_sp_permisos('''||_nomtbl||''');

UPDATE '||_nomtbl||$$ as a
SET (nombre_paciente, sexo, nacimiento, centro)=((b.nombre||' '||b.apellido1||' '||b.apellido2), b.sexo, b.nacimiento, b.centro)
from admomi.iddpacpa3 as b where a.nif=b.nif2
;
UPDATE $$||_nomtbl||$$ as a
SET (edad, aym, meses)=(
							date_part('year', age(fecha, nacimiento))
							, to_char(fecha, 'yyyy-mm')
							, (date_part('year', age(fecha, nacimiento))*12)+date_part('month', age(fecha, nacimiento))
						)
;
UPDATE $$||_nomtbl||' SET (estamento, cod_estamento, med_codigo, med_nombre)=(b.med_estamento_desc, b.med_estamento_cod, b.med_codigo, b.med_nombre) from admomi.iddmed as b where medico_id=b.id;
UPDATE '||_nomtbl||' as a SET descrip_ciap=b.descripcio,ciap_codigo=b.ciap from admomi.iddncu as b where a.nif=b.nif2 and a.especiali=b.especiali
;

select * from protocolos_omi_covid.mapro_sp_insertcol('||n||','||_maxnum||');

select * from protocolos_omi_covid.mapro_sp_updatecol('||n||','||_maxnum||')';

if (_nomtbl<>'' and _maxnum>=1) then
	execute micodi;
	end if;
	return 1;
end
$_$;


ALTER FUNCTION protocolos_omi_covid.mapro_sp_creatbl(n integer) OWNER TO postgres;

--
-- Name: mapro_sp_creatbl(integer, integer); Type: FUNCTION; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE FUNCTION protocolos_omi_covid.mapro_sp_creatbl(n1 integer, n2 integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
declare
	i integer:=n1-1;
	_wproto int4:=0;
	_maxnum int4:=0;
	_nomtbl text;
	micodi text;
begin 
	if (n2=0) then
	return 0;
	end if;
	loop
	exit when i=n2;
	i:=i+1;
	_nomtbl:=(select 'pr_'||protocolo||'_'||nomtbl from protocolos_omi_covid.mapro_nomtbl where wproto=i);
	_nomtbl:='protocolos_omi_covid.'||_nomtbl;
	_wproto:=i; --(select wproto from protocolos_omi_covid.mapro_nomtbl where id=i);
	_maxnum:=(select max(id_num) from protocolos_omi_covid.mapro_nomvar where wproto=_wproto);
	micodi:='DROP TABLE if exists '||_nomtbl||';
CREATE TABLE '||_nomtbl||' (
	nif int not null
	, nombre_paciente varchar(100)
	, sexo varchar(1)
	, nacimiento date
	, meses int
	, edad int
	, centro varchar(5)
	, fecha date not null
	, aym varchar(8)
	, estamento varchar(50)
	, cod_estamento varchar(50)
	, med_codigo varchar(50)
	, med_nombre varchar(50)
	, ciap_codigo varchar(100)
	, descrip_ciap varchar(100)
	, especiali varchar not NULL
	, fechr timestamp
	--, fechr varchar(20) 
	, medico_id int4
)
WITH (
	OIDS=FALSE
) ;

--select * from admomi.iddncu
select * from protocolos_omi_covid.mapro_sp_permisos('''||_nomtbl||''');

INSERT INTO '||_nomtbl||'
(nif, especiali, fecha, fechr, medico_id)
select distinct a.nif2, a.especiali, a.fecha2, a.fecha, a.medico_id
from admomi.iddcop as a where a.wproto='||_wproto||' and a.nif2 is not null and a.fecha is not null and a.especiali is not null;

UPDATE '||_nomtbl||' as a
SET (nombre_paciente, sexo, nacimiento, centro)=(b.nombre||'' ''||b.apellido1||'' ''||b.apellido2, b.sexo, b.nacimiento, b.centro)
from admomi.iddpacpa3 as b where a.nif=b.nif2
;
UPDATE '||_nomtbl||' as a
SET (edad, aym, meses)=(date_part(''year'', age(fecha, nacimiento)), to_char(fecha, ''yyyy-mm''), (date_part(''year'', age(fecha, nacimiento))*12)+date_part(''month'', age(fecha, nacimiento)))
;
UPDATE '||_nomtbl||' SET (estamento, cod_estamento, med_codigo, med_nombre)=(b.med_estamento_desc, b.med_estamento_cod, b.med_codigo, b.med_nombre) from admomi.iddmed as b where medico_id=b.id;
UPDATE '||_nomtbl||' as a SET descrip_ciap=b.descripcio,ciap_codigo=b.ciap from admomi.iddncu as b where a.nif=b.nif2 a.especiali=b.especiali
;
select * from protocolos_omi_covid.mapro_sp_insertcol('||_wproto||','||_maxnum||');

select * from protocolos_omi_covid.mapro_sp_updatecol('||_wproto||','||_maxnum||')';
if (_nomtbl<>'' and _maxnum>=1) then
	execute micodi;
	end if;
	end LOOP;
	return 1;
end
$$;


ALTER FUNCTION protocolos_omi_covid.mapro_sp_creatbl(n1 integer, n2 integer) OWNER TO postgres;

--
-- Name: mapro_sp_insertcol(integer, integer); Type: FUNCTION; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE FUNCTION protocolos_omi_covid.mapro_sp_insertcol(n1 integer, n2 integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
declare
	i int:=0;
	_wnumero int4:=0;
	_nomvar text;
	_nomtbl text;
	_tipdat text;
	micod text;
begin 
	if (n2=0) then
		return 0;
	end if;
	loop
		exit when i=n2;
		i:=i+1;
		_nomvar:=(select vareva||'_wn_'||substring('0000'||wnumero from '....$')::char(4)||'_t_'||lower(wtipo) from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i);
		_nomtbl:=(select 'pr_'||protocolo||'_'||nomtbl from protocolos_omi_covid.mapro_nomtbl where wproto=n1);
		_nomtbl:='protocolos_omi_covid.'||_nomtbl;
		--_wnumero:=(select wnumero from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i);
		if (select wtipo from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i)='F' then
		_tipdat:=' date';
		elsif (select wtipo from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i) in ('T','C', 'X') then
		_tipdat:=' text';
		else
		_tipdat:=' numeric';
		end if;
		micod:='ALTER TABLE '||_nomtbl||' ADD COLUMN "'||_nomvar||'" '||_tipdat; 
		execute micod;
	end LOOP;
	return 1;
end
$_$;


ALTER FUNCTION protocolos_omi_covid.mapro_sp_insertcol(n1 integer, n2 integer) OWNER TO postgres;

--
-- Name: mapro_sp_insertnomtbl(integer); Type: FUNCTION; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE FUNCTION protocolos_omi_covid.mapro_sp_insertnomtbl(n integer) RETURNS TABLE(id bigint, nomtbl text, wproto integer, wprotocolo character varying, protocolo text)
    LANGUAGE plpgsql
    AS $_$
declare
	i int:=0;
begin 
	if (n=0) then
	return query
	select * from protocolos_omi_covid.mapro_nomtbl order by 3 desc limit 0;
	end if;
	loop
	exit when i= n;
	i:=i+1;
	INSERT INTO protocolos_omi_covid.mapro_nomtbl
	(id, nomtbl, wproto, wprotocolo, protocolo)
	select max(b.id)+1 as id, 'wp_'::char(3)||substring('00000'||a.wproto from '......$')::char(6) as nomtbl, min(a.wproto), a.wprotocolo, regexp_replace(
		regexp_replace(
			regexp_replace(
				regexp_replace(
					regexp_replace(
						translate(lower(a.wprotocolo),'áéíóúñ', 'aeioun')
						,' ','_','g')
					,'\(','_','g')
				,']','_','g')
			,'[^a-z0-9+_]','','g')
		,'__','_','g') as protocolo
	from admomi.iddco5 as a, protocolos_omi_covid.mapro_nomtbl as b where a.wproto not in (select c.wproto from protocolos_omi_covid.mapro_nomtbl as c) group by 2,4 order by 3 limit 1;
	end LOOP;
	return QUERY
	select * from protocolos_omi_covid.mapro_nomtbl order by 3 desc limit n;
end
$_$;


ALTER FUNCTION protocolos_omi_covid.mapro_sp_insertnomtbl(n integer) OWNER TO postgres;

--
-- Name: mapro_sp_insertnomvar(integer); Type: FUNCTION; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE FUNCTION protocolos_omi_covid.mapro_sp_insertnomvar(n integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
declare
	i int:=0;
	_proto int;
begin 
	if (n=0) then
	return 0;
	end if;
	truncate table protocolos_omi_covid.mapro_nomvar;
	loop
	exit when i= n;
	i:=i+1;
	_proto:=(select wproto from protocolos_omi_covid.mapro_nomtbl where id=i)::int;
	INSERT INTO protocolos_omi_covid.mapro_nomvar
		(wproto, id_num, nomvar, wnumero, wcarpeta, wcolumna, wlinea, wcomentari, wtipo, wtabla, wcodigodgp, vareva)
		select a.wproto, row_number() over (partition by b.wproto order by a.wproto, a.wcarpeta, a.wcolumna, a.wlinea) as id_num
		,'var_'::char(4)||wnumero::char(4) as nomvar
		, a.wnumero, a.wcarpeta, a.wcolumna, a.wlinea, a.wcomentari, a.wtipo, a.wtabla, a.wcodigodgp, regexp_replace(
		regexp_replace(
			regexp_replace(
				regexp_replace(
					regexp_replace(
						regexp_replace(
							translate(lower(wcomentari),'áéíóúñ', 'aeioun')
							,' ','_','g')
						,'\/','_','g')
					,'\(','_','g')
				,']','_','g')
			,'[^a-z0-9+_]','','g')
		,'__','_','g') as vareva
		from admomi.iddcol as a left join protocolos_omi_covid.mapro_nomtbl as b on a.wproto=b.wproto where b.wproto=_proto and a.wtipo in ('T','C','S','N','X','V','F','P','Z','L','O', '+') order by 2;
	end LOOP;
	return (select count(*) from protocolos_omi_covid.mapro_nomvar)::integer;
end
$$;


ALTER FUNCTION protocolos_omi_covid.mapro_sp_insertnomvar(n integer) OWNER TO postgres;

--
-- Name: mapro_sp_permisos(character varying); Type: FUNCTION; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE FUNCTION protocolos_omi_covid.mapro_sp_permisos(n character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
declare
	micodi text;
begin 
	if (n='') then
	return 0;
	end if;
	micodi:='
		GRANT SELECT ON TABLE '||n||' TO jesoto;
		GRANT SELECT ON TABLE '||n||' TO ugina;
		GRANT SELECT ON TABLE '||n||' TO ehenriquez;
		GRANT SELECT ON TABLE '||n||' TO consulta;
		GRANT SELECT ON TABLE '||n||' TO consultaomi;
';

if (n<>'') then
	execute micodi;
	end if;
	return 1;
end
$$;


ALTER FUNCTION protocolos_omi_covid.mapro_sp_permisos(n character varying) OWNER TO postgres;

--
-- Name: mapro_sp_updatecol(integer, integer); Type: FUNCTION; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE FUNCTION protocolos_omi_covid.mapro_sp_updatecol(n1 integer, n2 integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
declare
	i int:=0;
	_wnumero int4:=0;
	_nomvar text;
	_nomtbl text;
	micod text;
begin 
	if (n2=0) then
		return 0;
	end if;
	loop
		exit when i=n2;
		i:=i+1;
		_nomvar:=(select vareva||'_wn_'||substring('0000'||wnumero from '....$')::char(4)||'_t_'||lower(wtipo) from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i);
		_nomtbl:=(select 'pr_'||protocolo||'_'||nomtbl from protocolos_omi_covid.mapro_nomtbl where wproto=n1);
		_nomtbl:='protocolos_omi_covid.'||_nomtbl;
		_wnumero:=(select wnumero from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i);
		IF (select wtipo from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i)='F' then
		micod:='UPDATE '||_nomtbl||' as a SET "'||_nomvar||'"= to_date(regexp_replace(b.campo,''/'',''-'',''g''), ''DD MM YYYY'')
			from admomi.iddcop as b 
			where a.nif=b.nif2 and a.especiali=b.especiali and a.fechr=b.fecha and a.medico_id=b.medico_id and b.wproto='||n1||'and b.wnumero='||_wnumero;
		ELSIF (select wtipo from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i)='T' then
		micod:='UPDATE '||_nomtbl||' as a SET "'||_nomvar||'"= b.campo::text from admomi.iddcoe as b 
			where a.nif=b.nif2 and a.especiali=b.especiali and a.fechr=b.fecha and a.medico_id=b.medico_id and b.wproto='||n1||'and b.wnumero='||_wnumero;
		ELSIF (select wtipo from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i)='C' then
		micod:='UPDATE '||_nomtbl||' as a SET "'||_nomvar||'"= b.campo::text from admomi.iddcot as b 
			where a.nif=b.nif2 and a.especiali=b.especiali and a.fechr=b.fecha and a.medico_id=b.medico_id and b.wproto='||n1||'and b.wnumero='||_wnumero;
		elsif (select wtipo from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i)='X' then
		micod:='UPDATE '||_nomtbl||' as a SET "'||_nomvar||'"=(select x.contenido_desc from protocolos_omi_covid.dim_tablas as x where x.id=(select c.id from protocolos_omi_covid.dim_tblvar as c where c.wproto=b.wproto and c.wnumero=b.wnumero and c.wcampo=b.campo)::int4)::text
			from admomi.iddcop as b 
			where a.nif=b.nif2 and a.especiali=b.especiali and a.fechr=b.fecha and a.medico_id=b.medico_id and b.wproto='||n1||'and b.wnumero='||_wnumero; 
		ELSE
		micod:='UPDATE '||_nomtbl||' as a SET "'||_nomvar||'"=case'||$$
			when b.wtipo in ('O','Z','P','L','N','V') and char_length(regexp_replace(b.campo,'[^0-9]','','g'))=0 then 0
			when b.wtipo in ('O','Z','P','L','N','V') then to_number(b.campo, '999999G999999D99999')
			when b.wtipo='S' and b.campo='S' then 1
			when b.wtipo='S' and b.campo='N' then 0
			when b.wtipo='+' and b.campo='+' then 1
			when b.wtipo='+' and b.campo='-' then 0
			end from admomi.iddcop as b 
			where a.nif=b.nif2 and a.especiali=b.especiali and a.fechr=b.fecha and a.medico_id=b.medico_id and b.wproto=$$||n1||'and b.wnumero='||_wnumero; 
		end if;
		execute micod;
	end LOOP;
	return 1;
end
$_$;


ALTER FUNCTION protocolos_omi_covid.mapro_sp_updatecol(n1 integer, n2 integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: consultas_de_morbilidad; Type: TABLE; Schema: covid19; Owner: postgres
--

CREATE TABLE covid19.consultas_de_morbilidad (
    nif integer,
    medico character varying(3),
    fecha2 date,
    hora double precision,
    respiratorias smallint
);


ALTER TABLE covid19.consultas_de_morbilidad OWNER TO postgres;

--
-- Name: episodios; Type: TABLE; Schema: covid19; Owner: postgres
--

CREATE TABLE covid19.episodios (
    nif integer,
    medico character varying(3),
    fecha2 date,
    hora double precision,
    respiratorias smallint,
    covid_sospecha smallint,
    covid_confirmado smallint,
    covid_caso_probable smallint
);


ALTER TABLE covid19.episodios OWNER TO postgres;

--
-- Name: episodios_cierre_covid; Type: TABLE; Schema: covid19; Owner: postgres
--

CREATE TABLE covid19.episodios_cierre_covid (
    nif integer,
    medico character varying(3),
    fecha2 date,
    hora double precision,
    covid_sospecha_cerrada smallint,
    covid_confirmado_cerrada smallint,
    covid_confirmado_probalble_cerrada smallint
);


ALTER TABLE covid19.episodios_cierre_covid OWNER TO postgres;

--
-- Name: hechos; Type: TABLE; Schema: covid19; Owner: postgres
--

CREATE TABLE covid19.hechos (
    nif integer,
    fecha2 date,
    hora double precision,
    medico character varying
);


ALTER TABLE covid19.hechos OWNER TO postgres;

--
-- Name: hechos2; Type: TABLE; Schema: covid19; Owner: postgres
--

CREATE TABLE covid19.hechos2 (
    nif integer,
    fecha2 date,
    hora double precision,
    medico character varying,
    morbilidad_respiratoria smallint,
    episodios_respiratoria smallint,
    vacuna_influenza bigint,
    consultas_controles_procedimientos bigint,
    despacho_medicamento bigint,
    covid_seguimiento bigint
);


ALTER TABLE covid19.hechos2 OWNER TO postgres;

--
-- Name: hechos4; Type: TABLE; Schema: covid19; Owner: postgres
--

CREATE TABLE covid19.hechos4 (
    id text,
    nif text,
    nif2 text,
    fecha2 date,
    hora text,
    hora2 double precision,
    medico text,
    morbilidad_respiratoria text,
    morbilidad_respiratoria2 smallint,
    episodios_respiratoria text,
    episodios_respiratoria2 smallint,
    consultas_controles_procedimientos text,
    consultas_controles_procedimientos2 smallint,
    despacho_medicamento text,
    despacho_medicamento2 smallint,
    vacuna_influenza text,
    vacuna_influenza2 smallint,
    consultas_respiratorias_totales text,
    consultas_respiratorias_totales2 smallint,
    covid_sospecha text,
    covid_sospecha2 smallint,
    covid_confirmado text,
    covid_confirmado2 smallint,
    covid_caso_probable text,
    covid_caso_probable2 smallint,
    estamento character varying(100),
    es_medico text,
    es_medico2 smallint,
    inscrito_validado text,
    inscrito_validado2 smallint,
    descripcion_exa text,
    resultado text,
    pac_centro character varying(8),
    covid_seguimiento text,
    covid_seguimiento2 smallint,
    pac_centro2 integer,
    med_centro2 integer,
    med_centro character varying(8),
    latitude numeric,
    longitude numeric,
    q smallint,
    edad_en_agnios integer
);


ALTER TABLE covid19.hechos4 OWNER TO postgres;

--
-- Name: hechos5; Type: TABLE; Schema: covid19; Owner: postgres
--

CREATE TABLE covid19.hechos5 (
    id text,
    nif text,
    nif2 text,
    fecha2 date,
    hora text,
    hora2 double precision,
    medico text,
    morbilidad_respiratoria text,
    morbilidad_respiratoria2 smallint,
    episodios_respiratoria text,
    episodios_respiratoria2 smallint,
    consultas_controles_procedimientos text,
    consultas_controles_procedimientos2 smallint,
    despacho_medicamento text,
    despacho_medicamento2 smallint,
    vacuna_influenza text,
    vacuna_influenza2 smallint,
    consultas_respiratorias_totales text,
    consultas_respiratorias_totales2 smallint,
    covid_sospecha text,
    covid_sospecha2 smallint,
    covid_confirmado text,
    covid_confirmado2 smallint,
    covid_caso_probable text,
    covid_caso_probable2 smallint,
    estamento character varying(100),
    es_medico text,
    es_medico2 smallint,
    inscrito_validado text,
    inscrito_validado2 smallint,
    descripcion_exa text,
    resultado text,
    pac_centro character varying(8),
    covid_seguimiento text,
    covid_seguimiento2 smallint,
    pac_centro2 integer,
    med_centro2 integer,
    med_centro character varying(8),
    latitude numeric,
    longitude numeric,
    q smallint,
    edad_en_agnios integer,
    wproto2 integer,
    protocolo text,
    id_paciente integer,
    clinico_run bigint,
    clinico_nombre character varying(100)
);


ALTER TABLE covid19.hechos5 OWNER TO postgres;

--
-- Name: hechos_examenes_pcr; Type: TABLE; Schema: covid19; Owner: postgres
--

CREATE TABLE covid19.hechos_examenes_pcr (
    latencia1_kpi integer,
    centro character varying(8),
    nif2 integer,
    rut character varying(1000),
    fecha_solicitud date,
    fecha_recepcion date,
    es_confirmado integer,
    fecha_apertura_episodio_confirmacion date,
    fecha_nutificacion date,
    fecha_identificacion_contactos date,
    resultado_exacto character varying(1000),
    resultado character varying,
    q integer,
    latencia1 integer
);


ALTER TABLE covid19.hechos_examenes_pcr OWNER TO postgres;

--
-- Name: kpi_examenes_pcr; Type: TABLE; Schema: covid19; Owner: postgres
--

CREATE TABLE covid19.kpi_examenes_pcr (
    rut character varying(1000),
    fecha_solicitud date,
    resultado_exacto character varying(1000),
    resultado character varying,
    q integer
);


ALTER TABLE covid19.kpi_examenes_pcr OWNER TO postgres;

--
-- Name: rangos_edad_01; Type: TABLE; Schema: covid19; Owner: postgres
--

CREATE TABLE covid19.rangos_edad_01 (
    fechai timestamp without time zone,
    fechaf timestamp without time zone
);


ALTER TABLE covid19.rangos_edad_01 OWNER TO postgres;

--
-- Name: resultado_examen; Type: TABLE; Schema: covid19; Owner: postgres
--

CREATE TABLE covid19.resultado_examen (
    nif2 integer,
    fecha timestamp without time zone,
    fecha2 date,
    hora double precision,
    descripcion_exa text,
    medico character varying(3),
    resultado text
);


ALTER TABLE covid19.resultado_examen OWNER TO postgres;

--
-- Name: todos_los_protocolos; Type: TABLE; Schema: covid19; Owner: postgres
--

CREATE TABLE covid19.todos_los_protocolos (
    nif2 integer,
    fecha2 date,
    hora2 double precision,
    medico character varying(3),
    wproto2 integer
);


ALTER TABLE covid19.todos_los_protocolos OWNER TO postgres;

--
-- Name: hechos01; Type: TABLE; Schema: covid19_report; Owner: postgres
--

CREATE TABLE covid19_report.hechos01 (
    nif2 integer,
    paciente_rut character varying,
    fecha date
);


ALTER TABLE covid19_report.hechos01 OWNER TO postgres;

--
-- Name: hechos02; Type: TABLE; Schema: covid19_report; Owner: postgres
--

CREATE TABLE covid19_report.hechos02 (
    nif2 integer,
    fecha date
);


ALTER TABLE covid19_report.hechos02 OWNER TO postgres;

--
-- Name: listado_casos_confirmados_seremi; Type: TABLE; Schema: covid19_report; Owner: postgres
--

CREATE TABLE covid19_report.listado_casos_confirmados_seremi (
    nif2 integer,
    correlativo bigint,
    fecha date,
    nombre character varying(25),
    apellido1 character varying(25),
    apellido2 character varying(25),
    rut character varying(16),
    edad integer,
    sexo character varying(1),
    factor_riesgo_embarazo bigint,
    domicilio text,
    telefono text,
    comuna character varying(100),
    centro character varying(8),
    seguimiento_antecetente_epidemioligico text,
    fecha_confirmacion date,
    seguimiento_condicion_clinica text,
    seguimiento_conducta_hospitalizacion bigint,
    hospital_derivacion text,
    alta text,
    fecha_alta date,
    responsable_del_seguimiento text,
    numero_total_de_contactos bigint,
    numero_de_contactos_que_requieren_pcr bigint
);


ALTER TABLE covid19_report.listado_casos_confirmados_seremi OWNER TO postgres;

--
-- Name: listado_casos_seremi; Type: TABLE; Schema: covid19_report; Owner: postgres
--

CREATE TABLE covid19_report.listado_casos_seremi (
    nif2 integer,
    correlativo_de_caso bigint,
    fecha_caso_indice_confirmado date,
    nombre_del_caso_positivo character varying(25),
    apellido_paterno_del_caso_positivo character varying(25),
    apellido_materno_del_caso_positivo character varying(25),
    rut_del_caso_positivo character varying(16),
    fecha_de_cada_seguimiento_realizado date,
    nombre_del_contacto character varying(25),
    apellido_paterno_del_contacto character varying(25),
    apellido_materno_del_contacto character varying(25),
    rut_del_contacto text,
    edad_del_contacto integer,
    sexo_del_contacto character varying(1),
    domicilio_del_contacto text,
    telefono text,
    comuna_del_contacto character varying(100),
    fecha_de_inicio_cuarentena date,
    fecha_termino_de_cuarentena date,
    responsable_del_seguimiento text
);


ALTER TABLE covid19_report.listado_casos_seremi OWNER TO postgres;

--
-- Name: examenes; Type: TABLE; Schema: covid19_tmp; Owner: postgres
--

CREATE TABLE covid19_tmp.examenes (
    numorden integer,
    codilab character varying(15),
    nif character varying(10),
    fechap integer,
    horap integer,
    examen character varying(60),
    resultado character varying(80),
    comentario character varying(3000),
    fecha_actualizacion timestamp without time zone
);


ALTER TABLE covid19_tmp.examenes OWNER TO postgres;

--
-- Name: examenes01; Type: TABLE; Schema: covid19_tmp; Owner: postgres
--

CREATE TABLE covid19_tmp.examenes01 (
    nif2 integer,
    paciente_rut character varying,
    fecha date,
    resultado text
);


ALTER TABLE covid19_tmp.examenes01 OWNER TO postgres;

--
-- Name: dim_tablas; Type: TABLE; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE TABLE protocolos_omi_covid.dim_tablas (
    id integer NOT NULL,
    tabla character varying(8),
    tabla_desc character varying(30),
    contenido_cod character varying(8),
    contenido_desc character varying(50)
);


ALTER TABLE protocolos_omi_covid.dim_tablas OWNER TO postgres;

--
-- Name: dim_tablas_id_seq; Type: SEQUENCE; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE SEQUENCE protocolos_omi_covid.dim_tablas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE protocolos_omi_covid.dim_tablas_id_seq OWNER TO postgres;

--
-- Name: dim_tablas_id_seq; Type: SEQUENCE OWNED BY; Schema: protocolos_omi_covid; Owner: postgres
--

ALTER SEQUENCE protocolos_omi_covid.dim_tablas_id_seq OWNED BY protocolos_omi_covid.dim_tablas.id;


--
-- Name: dim_tblvar; Type: TABLE; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE TABLE protocolos_omi_covid.dim_tblvar (
    id integer NOT NULL,
    wproto integer,
    wnumero integer,
    wcampo character varying
);


ALTER TABLE protocolos_omi_covid.dim_tblvar OWNER TO postgres;

--
-- Name: estado_de_seguimiento02; Type: TABLE; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE TABLE protocolos_omi_covid.estado_de_seguimiento02 (
    centro character varying(8),
    nif2 integer,
    rut character varying(16),
    pac_nombre_completo text,
    fecha_confirmacion date,
    fecha_episodio_cierre date,
    tipo_caso text,
    fecha_confirmacion_mas_14dias date,
    caso_repetido bigint,
    riesgo text,
    riesgo_x_pcr text,
    riesgo_calculado text,
    fecha_seguimiento timestamp without time zone,
    latencia_seguimiento integer,
    fecha_hoy date,
    usuario_egresa text,
    usuario_egresa_fecha timestamp without time zone,
    usuario_egresa_causa text,
    tiempo_proximo_seguimiento interval,
    tiempo_proximo_seguimiento_calculado interval,
    clinico character varying(50),
    clinico_estamento character varying(50),
    fecha_proximo_seguimiento date,
    estado_seguimiento text,
    diferencia_fecha_actual_prox_seguimiento integer,
    estado_de_seguimiento text
);


ALTER TABLE protocolos_omi_covid.estado_de_seguimiento02 OWNER TO postgres;

--
-- Name: estado_de_seguimiento03; Type: TABLE; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE TABLE protocolos_omi_covid.estado_de_seguimiento03 (
    centro character varying(8),
    nif2 integer,
    rut character varying(16),
    pac_nombre_completo text,
    fecha_confirmacion date,
    fecha_episodio_cierre date,
    tipo_caso text,
    fecha_confirmacion_mas_25dias date,
    caso_repetido bigint,
    riesgo text,
    riesgo_x_pcr text,
    riesgo_calculado text,
    fecha_seguimiento timestamp without time zone,
    latencia_seguimiento integer,
    fecha_hoy date,
    usuario_egresa text,
    usuario_egresa_fecha timestamp without time zone,
    usuario_egresa_causa text,
    tiempo_proximo_seguimiento text,
    tiempo_proximo_seguimiento_calculado text,
    clinico character varying(50),
    clinico_estamento character varying(50),
    fecha_proximo_seguimiento date,
    estado_seguimiento text,
    diferencia_fecha_actual_prox_seguimiento integer,
    estado_de_seguimiento text
);


ALTER TABLE protocolos_omi_covid.estado_de_seguimiento03 OWNER TO postgres;

--
-- Name: listado_contactos_seremi; Type: TABLE; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE TABLE protocolos_omi_covid.listado_contactos_seremi (
    centro character varying(8),
    nif2 integer,
    fecha2 date,
    caso_positivo_rut character varying(16),
    caso_positivo_nomnre character varying(25),
    caso_positivo_apellido1 character varying(25),
    caso_positivo_apellido2 character varying(25),
    caso_positivo_domicilio text,
    caso_positivo_comuna character varying(100),
    fecha_seguimiento date,
    num_caso text,
    contacto_nombre text,
    contacto_rut text,
    contacto_tiene_sintomas text,
    contacto_edad text,
    contacto_sexo text,
    contacto_embarazado text,
    contacto_telefono text,
    fecha_inicio_cuarentena date,
    fecha_termino_cuarentena date,
    responsable_del_seguimiento text,
    cumple_perfil_caso_sospechoso text,
    fecha_toma_de_muestra text,
    resultado_del_examen text,
    fecha_confirmacion text,
    lugar_de_derivacion_si_se_confirma text,
    caso_contacto_domicilio text,
    caso_contacto_comuna character varying(100)
);


ALTER TABLE protocolos_omi_covid.listado_contactos_seremi OWNER TO postgres;

--
-- Name: mapro_nomtbl; Type: TABLE; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE TABLE protocolos_omi_covid.mapro_nomtbl (
    id bigint,
    nomtbl text,
    wproto integer,
    wprotocolo character varying(40),
    protocolo text
);


ALTER TABLE protocolos_omi_covid.mapro_nomtbl OWNER TO postgres;

--
-- Name: mapro_nomvar; Type: TABLE; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE TABLE protocolos_omi_covid.mapro_nomvar (
    wproto integer,
    id_num bigint,
    nomvar text,
    wnumero integer,
    wcarpeta smallint,
    wcolumna numeric(18,3),
    wlinea numeric(18,3),
    wcomentari character varying(40),
    wtipo character varying(1),
    wtabla character varying(8),
    wcodigodgp character varying(8),
    vareva text
);


ALTER TABLE protocolos_omi_covid.mapro_nomvar OWNER TO postgres;

--
-- Name: pr__covid_seguimiento_casos_y_contacto_wp_000409; Type: TABLE; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE TABLE protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 (
    nif integer NOT NULL,
    nombre_paciente character varying(100),
    sexo character varying(1),
    nacimiento date,
    meses integer,
    edad integer,
    centro character varying(5),
    fecha date NOT NULL,
    aym character varying(8),
    estamento character varying(50),
    cod_estamento character varying(50),
    med_codigo character varying(50),
    med_nombre character varying(50),
    ciap_codigo character varying(100),
    descrip_ciap character varying(100),
    especiali character varying NOT NULL,
    fechr timestamp without time zone,
    medico_id integer,
    comentarios_al_seguimiento_wn_0048_t_t text,
    antecedentes_epidemiologicos_wn_0308_t_x text,
    modalidad_de_contacto_wn_0189_t_x text,
    via_de_contacto_wn_0191_t_x text,
    motivo_de_seguimiento_wn_0193_t_x text,
    contactorealizadosn_wn_0043_t_s numeric,
    dia_de_seguimiento_wn_0188_t_n numeric,
    telefono_de_contacto_seguimiento_wn_0601_t_n numeric,
    fechainiciosintomas_wn_0085_t_f date,
    refiere_cumplimiento_de_cuarentena_wn_0046_t_s numeric,
    fecha_probable_fin_de_cuarentena_wn_0528_t_f date,
    factores_de_riesgo_para_complicaciones_wn_0310_t_s numeric,
    estado_notificacion_resultado_wn_0532_t_x text,
    resutaldocovid19fr_wn_0056_t_t text,
    resultadocovid19sfr_wn_0058_t_t text,
    resultado_serologico_igm_wn_0312_t_x text,
    resultado_serologico_igg_wn_0314_t_x text,
    paciente_ingreso_al_proyecto_test_rapido_wn_0481_t_s numeric,
    infant_decaido_irritable_somnoliento_wn_0145_t_s numeric,
    infant_fiebre_mayor_38c_axilar_wn_0147_t_s numeric,
    infant_tos_ha_aumentado_wn_0149_t_s numeric,
    infant_respiracion_rapida_irregular_wn_0151_t_s numeric,
    infant_undimiento_costillas_al_respirar_wn_0154_t_s numeric,
    infant_pecho_silba_al_respirar_wn_0156_t_s numeric,
    infant_pausa_al_respirar_mas10seg_wn_0157_t_s numeric,
    infant_coloracion_violacea_u_otros_wn_0160_t_s numeric,
    infat_vomitos_dificulta_alimentacion_wn_0162_t_s numeric,
    infant_rechazo_alimentos_wn_0163_t_s numeric,
    infant_diuresis_wn_0165_t_s numeric,
    infant_diarrea_wn_0168_t_s numeric,
    infant_en_contacto_con_toda_la_familia_wn_0171_t_s numeric,
    infant_objetos_q_utiliza_los_comparte_wn_0173_t_s numeric,
    infant_ha_recibido_visitas_wn_0174_t_s numeric,
    infant_mas_7dias_fiebre_mas38_gradosc_wn_0178_t_s numeric,
    infant_tos_superior_a_14dias_wn_0181_t_s numeric,
    inf_cambio_condicion_aislamiento_wn_0182_t_s numeric,
    comentarios_wn_0499_t_t text,
    embarazo_wn_0470_t_v numeric,
    embarazo_sangrado_wn_0473_t_s numeric,
    embarazo_flujo_wn_0475_t_s numeric,
    embarazo_movimientos_fetales_wn_0477_t_s numeric,
    embarazo_contracciones_uterinas_wn_0479_t_s numeric,
    sintomas_sensacion_febril_wn_0290_t_v numeric,
    sintomas_fiebre_wn_0291_t_v numeric,
    sintomas_coriza_wn_0294_t_v numeric,
    sintomas_odinofagia_wn_0300_t_v numeric,
    sintomas_tos_productiva_wn_0296_t_v numeric,
    sintomas_tos_seca_wn_0295_t_v numeric,
    sintomas_disnea_wn_0297_t_v numeric,
    sintomas_fatiga_wn_0301_t_v numeric,
    sintomas_dolor_toracico_wn_0299_t_v numeric,
    sintomas_cefalea_wn_0298_t_v numeric,
    sintomas_anosmia_wn_0302_t_v numeric,
    sintomas_disgeusia_wn_0305_t_v numeric,
    sintomas_mialgias_wn_0303_t_v numeric,
    sintomas_diarrea_wn_0304_t_v numeric,
    sintomas_otros_wn_0306_t_v numeric,
    fiebre_temperatura_maxima_wn_0293_t_n numeric,
    tos_persistente_que_le_dificulte_aliment_wn_0502_t_v numeric,
    hemoptisis_wn_0508_t_v numeric,
    se_fatiga_al_levantarse_empezar_a_camina_wn_0503_t_v numeric,
    duracion_dificultad_respiratoria_nograve_wn_0505_t_v numeric,
    taquipnea_habla_entrecortado_o_dificulta_wn_0506_t_v numeric,
    incapacidad_hacer_cualquier_act_fisica_wn_0509_t_v numeric,
    dolor_costal_dificulta_la_respiracion_wn_0510_t_v numeric,
    se_encuentra_insconsiente_desorientado_wn_0512_t_v numeric,
    le_cuesta_responder_las_preguntas_wn_0513_t_v numeric,
    vomitos_incoercibles_impiden_alimentacio_wn_0514_t_v numeric,
    diarrea_abundante_wn_0515_t_v numeric,
    duracion_fiebre_38_durante_7_dias_wn_0500_t_v numeric,
    duracion_de_la_tos_superior_a_14_dias_wn_0501_t_v numeric,
    han_cambiado_condiciones_psicosociales_wn_0516_t_v numeric,
    respecto_contacto_anterior_wn_0288_t_x text,
    score_crb_65_riesgo_neumonia_sin_riesgo_wn_0518_t_v numeric,
    score_crb_65_riesgo_neumonia_riesgo_inte_wn_0519_t_v numeric,
    score_crb_65_riesgo_neumonia_riesgo_alto_wn_0520_t_v numeric,
    temperatura_wn_0320_t_n numeric,
    frecuencia_cardiaca_wn_0322_t_n numeric,
    frecuencia_respiratoria_wn_0328_t_n numeric,
    saturacion_oxigeno_wn_0330_t_n numeric,
    faringe_wn_0332_t_x text,
    pulmonar_wn_0334_t_x text,
    presion_arterial_sistolica_wn_0324_t_n numeric,
    presion_arterial_diastolica_wn_0325_t_n numeric,
    estrategias_de_afrontamiento_wn_0570_t_t text,
    riesgo_suicida_wn_0555_t_v numeric,
    ausencia_de_red_de_apoyo_wn_0556_t_v numeric,
    probelmas_economicos_wn_0557_t_v numeric,
    consumo_wn_0558_t_v numeric,
    patologia_previa_en_salud_mental_wn_0559_t_v numeric,
    violencia_intrafamiliar_wn_0564_t_v numeric,
    tipo_de_intervencion_wn_0543_t_x text,
    sufre_estigma_social_wn_0566_t_s numeric,
    sintomas_animicos_wn_0547_t_x text,
    sintomas_ansiosos_wn_0549_t_x text,
    descontrol_de_impulsos_wn_0551_t_x text,
    otros_sintomas_wn_0553_t_t text,
    especificar_diagnostico_wn_0561_t_t text,
    adherencia_tratamiento_farmacologico_wn_0563_t_s numeric,
    comentarios_a_la_evaluacion_wn_0579_t_t text,
    intervencion_en_crisis_wn_0591_t_v numeric,
    derivacion_interna_wn_0592_t_v numeric,
    derivacion_o_gestion_con_red_externa_wn_0595_t_v numeric,
    suena_regular_wn_0572_t_v numeric,
    ejercicio_fisico_wn_0573_t_v numeric,
    alimentacion_saludable_wn_0574_t_v numeric,
    recreacion_wn_0575_t_v numeric,
    objetivos_acuerdos_1_wn_0583_t_t text,
    objetivos_acuerdos_2_wn_0584_t_t text,
    objetivos_acuerdos_3_wn_0585_t_t text,
    se_acuerda_wn_0590_t_t text,
    derivacion_interna_especificar_wn_0594_t_t text,
    responsable_1_wn_0586_t_t text,
    responsable_2_wn_0587_t_t text,
    responsable_3_wn_0588_t_t text,
    fecha_acordada_proximo_seguimiento_wn_0599_t_f date,
    derivacion_o_gestion_con_red_externa_esp_wn_0597_t_t text,
    cuenta_condiciones_mantener_aislamiento_wn_0577_t_s numeric,
    indicaciones_wn_0218_t_t text,
    usuario_egresa_wn_0219_t_v numeric,
    comentarios_wn_0343_t_t text,
    indicacionesconducta_wn_0061_t_x text,
    condicion_clinica_wn_0337_t_x text,
    causaegresosegcovid19_wn_0064_t_x text,
    categorizacion_de_riesgo_wn_0534_t_x text,
    tiemposeguimientocovid19_wn_0054_t_x text,
    derivacion_nombre_hospital_wn_0530_t_x text,
    ninguno_wn_0507_t_v numeric,
    hemograma_wn_0491_t_v numeric,
    radigrafia_torax_pa_y_lateral_wn_0492_t_v numeric,
    tac_torax_wn_0493_t_v numeric,
    resultado_serologico_igm_seguim_ii_wn_0486_t_x text,
    resultado_serologico_igg_seguim_ii_wn_0495_t_x text,
    resultado_serologico_igm_seguim_iii_wn_0489_t_x text,
    resultado_serologico_igg_seguim_iii_wn_0496_t_x text,
    algunos_de_contactos_no_cumple_cuarenten_wn_0469_t_v numeric,
    nombre_contacto_01_wn_0221_t_t text,
    nombre_contacto_02_wn_0222_t_t text,
    nombre_contacto_03_wn_0223_t_t text,
    nombre_contacto_04_wn_0224_t_t text,
    nombre_contacto_05_wn_0225_t_t text,
    nombre_contacto_06_wn_0226_t_t text,
    nombre_contacto_07_wn_0227_t_t text,
    nombre_contacto_08_wn_0228_t_t text,
    rut_contacto_04_wn_0235_t_t text,
    rut_contacto_01_wn_0232_t_t text,
    rut_contacto_02_wn_0233_t_t text,
    rut_contacto_03_wn_0234_t_t text,
    rut_contacto_05_wn_0236_t_t text,
    rut_contacto_06_wn_0237_t_t text,
    rut_contacto_07_wn_0238_t_t text,
    rut_contacto_08_wn_0239_t_t text,
    debloqueacampos_wn_0060_t_v numeric,
    telefono_contacto_06_wn_0249_t_t text,
    telefono_contacto_07_wn_0250_t_t text,
    telefono_contacto_08_wn_0251_t_t text,
    telefono_contacto_01_wn_0243_t_t text,
    telefono_contacto_02_wn_0245_t_t text,
    telefono_contacto_03_wn_0246_t_t text,
    telefono_contacto_04_wn_0247_t_t text,
    telefono_contacto_05_wn_0248_t_t text,
    sexo_contacto_01_wn_0345_t_x text,
    sexo_contacto_02_wn_0346_t_x text,
    sexo_contacto_03_wn_0347_t_x text,
    sexo_contacto_04_wn_0348_t_x text,
    sexo_contacto_05_wn_0349_t_x text,
    sexo_contacto_06_wn_0350_t_x text,
    sexo_contacto_07_wn_0351_t_x text,
    sexo_contacto_08_wn_0352_t_x text,
    otros_comentarios_wn_0447_t_t text,
    edad_contacto_01_wn_0356_t_n numeric,
    edad_contacto_02_wn_0357_t_n numeric,
    edad_contacto_03_wn_0358_t_n numeric,
    edad_contacto_04_wn_0359_t_n numeric,
    edad_contacto_05_wn_0360_t_n numeric,
    edad_contacto_06_wn_0361_t_n numeric,
    edad_contacto_07_wn_0362_t_n numeric,
    edad_contacto_08_wn_0363_t_n numeric,
    fr_contacto_01_wn_0366_t_v numeric,
    fr_contacto_02_wn_0367_t_v numeric,
    fr_contacto_03_wn_0368_t_v numeric,
    fr_contacto_04_wn_0369_t_v numeric,
    fr_contacto_05_wn_0370_t_v numeric,
    fr_contacto_06_wn_0371_t_v numeric,
    fr_contacto_07_wn_0372_t_v numeric,
    fr_contacto_08_wn_0373_t_v numeric,
    embarazo_contacto_01_wn_0399_t_x text,
    embarazo_contacto_02_wn_0400_t_x text,
    embarazo_contacto_03_wn_0401_t_x text,
    embarazo_contacto_04_wn_0402_t_x text,
    embarazo_contacto_05_wn_0403_t_x text,
    embarazo_contacto_06_wn_0404_t_x text,
    embarazo_contacto_07_wn_0405_t_x text,
    embarazo_contacto_08_wn_0406_t_x text,
    tiene_sisntomas_contacto_01_wn_0409_t_x text,
    tiene_sisntomas_contacto_02_wn_0410_t_x text,
    tiene_sisntomas_contacto_03_wn_0411_t_x text,
    tiene_sisntomas_contacto_04_wn_0412_t_x text,
    tiene_sisntomas_contacto_05_wn_0413_t_x text,
    tiene_sisntomas_contacto_06_wn_0414_t_x text,
    tiene_sisntomas_contacto_07_wn_0415_t_x text,
    tiene_sisntomas_contacto_08_wn_0416_t_x text,
    tomar_examen_covid10_contacto_01_wn_0421_t_v numeric,
    tomar_examen_covid10_contacto_02_wn_0422_t_v numeric,
    tomar_examen_covid10_contacto_03_wn_0423_t_v numeric,
    tomar_examen_covid10_contacto_04_wn_0424_t_v numeric,
    tomar_examen_covid10_contacto_05_wn_0425_t_v numeric,
    tomar_examen_covid10_contacto_06_wn_0426_t_v numeric,
    tomar_examen_covid10_contacto_07_wn_0427_t_v numeric,
    tomar_examen_covid10_contacto_08_wn_0428_t_v numeric,
    plan_indicaciones_contacto_02_wn_0434_t_x text,
    plan_indicaciones_contacto_03_wn_0435_t_x text,
    plan_indicaciones_contacto_04_wn_0436_t_x text,
    plan_indicaciones_contacto_05_wn_0437_t_x text,
    plan_indicaciones_contacto_06_wn_0438_t_x text,
    plan_indicaciones_contacto_07_wn_0439_t_x text,
    plan_indicaciones_contacto_08_wn_0440_t_x text,
    plan_indicaciones_contacto_01_wn_0433_t_x text
);


ALTER TABLE protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 OWNER TO postgres;

--
-- Name: pr_morbilidad_aguda_covid19_wp_000412; Type: TABLE; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE TABLE protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 (
    nif integer NOT NULL,
    nombre_paciente character varying(100),
    sexo character varying(1),
    nacimiento date,
    meses integer,
    edad integer,
    centro character varying(5),
    fecha date NOT NULL,
    aym character varying(8),
    estamento character varying(50),
    cod_estamento character varying(50),
    med_codigo character varying(50),
    med_nombre character varying(50),
    ciap_codigo character varying(100),
    descrip_ciap character varying(100),
    especiali character varying NOT NULL,
    fechr timestamp without time zone,
    medico_id integer,
    anamnesis_breve_wn_0004_t_t text,
    sensacion_febril_wn_0162_t_v numeric,
    fiebre_wn_0163_t_v numeric,
    coriza_wn_0164_t_v numeric,
    tos_seca_wn_0165_t_v numeric,
    tos_productiva_wn_0166_t_v numeric,
    disnea_wn_0167_t_v numeric,
    cefalea_wn_0168_t_v numeric,
    dolor_toraccico_wn_0169_t_v numeric,
    motivo_de_consulta_wn_0070_t_x text,
    centro_de_salud_wn_0088_t_x text,
    motivo_de_consulta_otra_wn_0124_t_t text,
    fecha_primer_sintoma_wn_0179_t_f date,
    fiebre_temperatura_maxima_wn_0276_t_n numeric,
    odinofagia_wn_0170_t_v numeric,
    fatiga_wn_0171_t_v numeric,
    congestion_nasal_wn_0172_t_v numeric,
    anosmia_wn_0173_t_v numeric,
    mialgias_wn_0174_t_v numeric,
    diarrea_wn_0175_t_v numeric,
    disgeusia_wn_0176_t_v numeric,
    otros_sintomas_wn_0177_t_v numeric,
    trabaja_wn_0310_t_s numeric,
    tuvo_contacto_con_personas_enfermas_wn_0204_t_s numeric,
    tipo_de_contacto_wn_0183_t_x text,
    fecha_contacto_wn_0185_t_f date,
    lugar_de_trabajo_wn_0314_t_t text,
    correo_electronico_wn_0199_t_t text,
    domicilio_wn_0201_t_t text,
    fecha_ultimo_dia_trabajo_wn_0312_t_f date,
    contacto_conocido_covid19_positivo_wn_0181_t_s numeric,
    telefono_de_contacto_seguimiento_1_wn_0328_t_n numeric,
    telefono_de_contacto_seguimiento_2_wn_0334_t_n numeric,
    contacto_realizado_wn_0207_t_s numeric,
    modalidad_de_contacto_wn_0209_t_x text,
    via_de_contacto_wn_0211_t_x text,
    embarazo_wn_0225_t_v numeric,
    hipertension_cronica_wn_0214_t_v numeric,
    diabetes_mellitus_ii_wn_0215_t_v numeric,
    obesidad_morbida_wn_0216_t_v numeric,
    enfermedad_coronaria_wn_0217_t_v numeric,
    accidente_cerebrovascular_wn_0218_t_v numeric,
    enfermedad_pulmonar_obstructiva_cronica_wn_0219_t_v numeric,
    asma_wn_0220_t_v numeric,
    limitacion_cronica_flujo_aereo_wn_0221_t_v numeric,
    enfermedad_renal_cronica_wn_0222_t_v numeric,
    cancer_wn_0223_t_v numeric,
    inmunodeficiencia_wn_0224_t_v numeric,
    tabaquismo_wn_0302_t_v numeric,
    cigarros_por_dia_wn_0190_t_n numeric,
    categorizacion_riesgo_covid_wn_0320_t_x text,
    ex_fumador_wn_0191_t_v numeric,
    nunca_ha_fumado_wn_0192_t_v numeric,
    medicamentos_ara_ii_wn_0229_t_v numeric,
    medicamentos_ieca_wn_0230_t_v numeric,
    medicamentos_aines_cox1_wn_0231_t_v numeric,
    medicamentos_aines_cox2_wn_0232_t_v numeric,
    corticoides_inhalatorios_wn_0233_t_v numeric,
    medicamentos_saba_laba_wn_0234_t_v numeric,
    medicamentos_sama_lama_wn_0235_t_v numeric,
    corticoides_sistemicos_wn_0236_t_v numeric,
    inmunomoduladores_wn_0237_t_v numeric,
    terapia_biologica_wn_0238_t_v numeric,
    presenta_factores_de_riesgo_complicacion_wn_0227_t_s numeric,
    comentarios_wn_0316_t_t text,
    prematurez_wn_0240_t_v numeric,
    displasia_broncopulmonar_wn_0242_t_v numeric,
    asma_sindrome_bronquial_obstructivo_wn_0243_t_v numeric,
    hospitalizacion_reciente_wn_0244_t_v numeric,
    patologias_cronicas_alteracion_wn_0246_t_v numeric,
    padres_adolescentes_wn_0248_t_v numeric,
    patologia_psicosocial_importante_wn_0249_t_v numeric,
    hacinamiento_wn_0250_t_v numeric,
    exposicion_a_tabaco_intradomiciliario_wn_0251_t_v numeric,
    mala_adherencia_al_tratamiento_cronico_wn_0252_t_v numeric,
    mal_incremento_ponderal_wn_0254_t_v numeric,
    ausencia_la_lactancia_materna_wn_0255_t_v numeric,
    frecuencia_cardiaca_wn_0082_t_n numeric,
    temperatura_wn_0084_t_n numeric,
    frecuencia_respiratoria_wn_0083_t_n numeric,
    peso_ninos_wn_0078_t_p numeric,
    talla_ninos_wn_0079_t_l numeric,
    saturacion_de_oxigeno_wn_0091_t_n numeric,
    presion_arterial_sistolica_wn_0085_t_n numeric,
    presion_arterial_diastolica_wn_0086_t_n numeric,
    comentarios_de_la_exploracion_wn_0150_t_t text,
    examen_fisico_piel_mucosas_wn_0126_t_x text,
    examen_fisico_ojos_wn_0128_t_x text,
    examen_fisico_oidos_wn_0130_t_x text,
    examen_fisico_faringe_wn_0132_t_x text,
    examen_fisico_abdomen_wn_0144_t_x text,
    examen_fisico_genitales_wn_0146_t_x text,
    examen_fisico_extremidades_wn_0148_t_x text,
    examen_fisico_adenopatias_lugar_wn_0137_t_x text,
    examen_fisico_torax_auscultacion_cardiac_wn_0140_t_x text,
    examen_fisico_torax_auscultacion_pulmona_wn_0142_t_x text,
    examen_fisico_cuello_adenopatias_wn_0135_t_s numeric,
    derivacion_nombre_hospital_wn_0308_t_x text,
    indicadores_generales_wn_0116_t_t text,
    conducta_wn_0258_t_x text,
    reposo_licencia_wn_0115_t_v numeric,
    proximo_control_con_quien_wn_0158_t_x text,
    proximo_control_cuando_aprox_wn_0160_t_f date,
    dias_de_reposo_wn_0155_t_n numeric,
    solicitar_pcr_wn_0263_t_v numeric,
    serologico_igm_wn_0264_t_v numeric,
    serologico_igg_wn_0265_t_v numeric,
    tes_de_antigeno_wn_0317_t_v numeric,
    serologico_igm_wn_0266_t_x text,
    serologico_igg_wn_0267_t_x text,
    tes_de_antigeno_wn_0318_t_x text,
    funcionario_equipo_de_salud_wn_0305_t_v numeric,
    gendarme_wn_0306_t_v numeric,
    consentimiento_informado_wn_0277_t_v numeric,
    criterio_de_ingreso_wn_0282_t_x text,
    ingreso_al_proyecto_wn_0271_t_s numeric,
    numero_de_telefono_1_wn_0195_t_t text,
    numero_de_telefono_2_wn_0197_t_t text
);


ALTER TABLE protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 OWNER TO postgres;

--
-- Name: pr_morbilidad_aguda_wp_000397; Type: TABLE; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE TABLE protocolos_omi_covid.pr_morbilidad_aguda_wp_000397 (
    nif integer NOT NULL,
    nombre_paciente character varying(100),
    sexo character varying(1),
    nacimiento date,
    meses integer,
    edad integer,
    centro character varying(5),
    fecha date NOT NULL,
    aym character varying(8),
    estamento character varying(50),
    cod_estamento character varying(50),
    med_codigo character varying(50),
    med_nombre character varying(50),
    ciap_codigo character varying(100),
    descrip_ciap character varying(100),
    especiali character varying NOT NULL,
    fechr timestamp without time zone,
    medico_id integer,
    anamnesis_breve_wn_0004_t_t text,
    motivo_de_consulta_wn_0070_t_x text,
    centro_de_salud_wn_0088_t_x text,
    frecuencia_cardiaca_wn_0082_t_n numeric,
    temperatura_wn_0084_t_n numeric,
    frecuencia_respiratoria_wn_0083_t_n numeric,
    peso_ninos_wn_0078_t_p numeric,
    talla_ninos_wn_0079_t_l numeric,
    motivo_de_consulta_otra_wn_0124_t_t text,
    saturacion_de_oxigeno_wn_0091_t_n numeric,
    presion_arterial_sistolica_wn_0085_t_n numeric,
    presion_arterial_diastolica_wn_0086_t_n numeric,
    espisodio_familia_abierto_wn_0120_t_s numeric,
    comentarios_de_la_exploracion_wn_0150_t_t text,
    pido_baciloscopia_wn_0095_t_s numeric,
    examen_fisico_piel_mucosas_wn_0126_t_x text,
    examen_fisico_ojos_wn_0128_t_x text,
    examen_fisico_oidos_wn_0130_t_x text,
    examen_fisico_faringe_wn_0132_t_x text,
    examen_fisico_abdomen_wn_0144_t_x text,
    examen_fisico_genitales_wn_0146_t_x text,
    examen_fisico_extremidades_wn_0148_t_x text,
    examen_fisico_adenopatias_lugar_wn_0137_t_x text,
    examen_fisico_torax_auscultacion_cardiac_wn_0140_t_x text,
    examen_fisico_torax_auscultacion_pulmona_wn_0142_t_x text,
    examen_fisico_cuello_adenopatias_wn_0135_t_s numeric,
    mayor_15_con_tos_expector_wn_0093_t_s numeric,
    indicadores_generales_wn_0116_t_t text,
    reposo_licencia_wn_0115_t_v numeric,
    consejeria_actividad_fisica_wn_0105_t_v numeric,
    consejeria_alimentacion_saludable_wn_0106_t_v numeric,
    consejeria_tabaquismo_wn_0107_t_v numeric,
    consejeria_consumo_drogas_wn_0108_t_v numeric,
    consejeria_salud_sexual_reproductiva_wn_0109_t_v numeric,
    consejeria_regulacion_fertilidad_wn_0110_t_v numeric,
    consejeria_prevencion_vih_trans_sexual_wn_0111_t_v numeric,
    consejeria_entrega_preservativo_adolesce_wn_0114_t_v numeric,
    consejeria_otras_areas_wn_0112_t_v numeric,
    destino_wn_0153_t_x text,
    empa_al_dia_wn_0097_t_s numeric,
    pap_al_dia_wn_0099_t_s numeric,
    proximo_control_con_quien_wn_0158_t_x text,
    proximo_control_cuando_aprox_wn_0160_t_f date,
    dias_de_reposo_wn_0155_t_n numeric,
    vacunas_al_dia_wn_0104_t_s numeric
);


ALTER TABLE protocolos_omi_covid.pr_morbilidad_aguda_wp_000397 OWNER TO postgres;

--
-- Name: seguimiento_covid_hechos03; Type: TABLE; Schema: protocolos_omi_covid; Owner: postgres
--

CREATE TABLE protocolos_omi_covid.seguimiento_covid_hechos03 (
    centro character varying(8),
    nif2 integer,
    fecha2 date,
    caso_positivo_rut character varying(16),
    caso_positivo_nomnre character varying(25),
    caso_positivo_apellido1 character varying(25),
    caso_positivo_apellido2 character varying(25),
    caso_positivo_domicilio text,
    caso_positivo_comuna character varying(100),
    fecha_seguimiento date,
    num_caso text,
    contacto_nombre text,
    contacto_rut text,
    contacto_tiene_sintomas text,
    contacto_edad text,
    contacto_sexo text,
    contacto_embarazado text,
    contacto_telefono text,
    fecha_inicio_cuarentena date,
    fecha_termino_cuarentena date,
    responsable_del_seguimiento text,
    cumple_perfil_caso_sospechoso text,
    fecha_toma_de_muestra text,
    resultado_del_examen text,
    fecha_confirmacion text,
    lugar_de_derivacion_si_se_confirma text
);


ALTER TABLE protocolos_omi_covid.seguimiento_covid_hechos03 OWNER TO postgres;

--
-- Name: datos_contactos01; Type: TABLE; Schema: protocolos_omi_covid_base; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_base.datos_contactos01 (
    nif integer,
    fecha date,
    especiali character varying,
    num_caso text,
    dato text,
    valor text
);


ALTER TABLE protocolos_omi_covid_base.datos_contactos01 OWNER TO postgres;

--
-- Name: examenes_pcr_omi; Type: TABLE; Schema: protocolos_omi_covid_base; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_base.examenes_pcr_omi (
    numorden integer,
    codilab text,
    nif text,
    nif2 integer,
    fecha_solicitud timestamp without time zone,
    fecha_cierre timestamp without time zone,
    resultado text
);


ALTER TABLE protocolos_omi_covid_base.examenes_pcr_omi OWNER TO postgres;

--
-- Name: hisa_iddcoe; Type: TABLE; Schema: protocolos_omi_covid_base; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_base.hisa_iddcoe (
    fecha timestamp without time zone,
    fecha2 date,
    usuario text,
    tipo_operacion text,
    id_hisa_iddcoe integer,
    nif text,
    nif2 integer,
    especiali text,
    secuen numeric(21,3),
    wproto integer,
    wnumero integer,
    wptipo text,
    campo text,
    tabla text
);


ALTER TABLE protocolos_omi_covid_base.hisa_iddcoe OWNER TO postgres;

--
-- Name: hisa_iddcop; Type: TABLE; Schema: protocolos_omi_covid_base; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_base.hisa_iddcop (
    fecha timestamp without time zone,
    fecha2 date,
    usuario text,
    tipo_operacion text,
    id_hisa_iddcop integer,
    nif text,
    nif2 integer,
    especiali text,
    secuen numeric(21,3),
    wproto integer,
    wnumero integer,
    wptipo text,
    campo text,
    tabla text
);


ALTER TABLE protocolos_omi_covid_base.hisa_iddcop OWNER TO postgres;

--
-- Name: hisa_iddcor; Type: TABLE; Schema: protocolos_omi_covid_base; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_base.hisa_iddcor (
    stk_fecha timestamp without time zone,
    stk_fecha2 date,
    stk_usuario text,
    stk_tipo_operacion text,
    id_hisa_iddcor integer,
    nif text,
    nif2 integer,
    especiali text,
    secuen numeric(21,3),
    numero integer,
    fecha date,
    fecha2 date,
    curso text,
    tipolin text,
    visitam text,
    origen text,
    tipocor smallint,
    wproto integer,
    medico text
);


ALTER TABLE protocolos_omi_covid_base.hisa_iddcor OWNER TO postgres;

--
-- Name: hisa_iddncu; Type: TABLE; Schema: protocolos_omi_covid_base; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_base.hisa_iddncu (
    stk_fecha timestamp without time zone,
    stk_fecha2 date,
    stk_usuario text,
    stk_tipo_operacion text,
    nif text,
    nif2 integer,
    ciap text,
    cipap text,
    prescrip text,
    archivo text,
    tipoepi text,
    visitam text,
    origen text,
    descripci2 text,
    histo text,
    especiali text,
    fecha timestamp without time zone,
    fecha2 date,
    descripcio text,
    medico text,
    fechalta date,
    fechalta2 date,
    curobser text,
    subjetiva text,
    aparato text,
    especiali2 text,
    medicocap text,
    codigoesp text,
    tipus text,
    t smallint,
    n smallint,
    m smallint,
    destino text,
    cie text,
    fechaing timestamp without time zone,
    horaing integer,
    usuarioing text,
    lateralidad text,
    esp text
);


ALTER TABLE protocolos_omi_covid_base.hisa_iddncu OWNER TO postgres;

--
-- Name: hisa_iddome; Type: TABLE; Schema: protocolos_omi_covid_base; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_base.hisa_iddome (
    stk_fecha timestamp without time zone,
    stk_usuario character varying(3),
    stk_tipo_operacion character varying(1),
    id_hisa_iddome integer,
    comentario character varying(1000),
    respuestainter character varying(1000),
    nif character varying(10),
    nif2 integer,
    prueba character varying(8),
    fecha date,
    centro character varying(8),
    fechar timestamp without time zone,
    prioridad character varying(1),
    usuario character varying(3),
    observacio character varying(59),
    numorden integer,
    numage numeric(21,3),
    patologica character varying(1),
    fecharea date,
    aproxmes numeric(21,3),
    aproxdias numeric(21,3),
    tiporea character varying(1),
    tiposol character varying(2),
    otraspru character varying(59),
    clasepru character varying(1),
    solicita character varying(3),
    lugar character varying(1),
    numero1 numeric(21,3),
    numero2 numeric(21,3),
    numero3 numeric(21,3),
    numero4 numeric(21,3),
    numero5 numeric(21,3),
    numero6 numeric(21,3),
    numero7 numeric(21,3),
    numero8 numeric(21,3),
    tubo1 character varying(10),
    tubo2 character varying(10),
    tubo3 character varying(10),
    tubo4 character varying(10),
    tubo5 character varying(10),
    tubo6 character varying(10),
    tubo7 character varying(10),
    tubo8 character varying(10),
    nombrem character varying(30),
    especia character varying(3),
    especiali character varying(10),
    secuen numeric(21,3),
    visto character varying(1),
    fechav timestamp without time zone,
    fechac timestamp without time zone,
    fechap timestamp without time zone,
    fechare timestamp without time zone,
    total integer,
    realizadas integer,
    codilab character varying(15),
    lab smallint,
    fecharev timestamp without time zone,
    yo character varying(1),
    liniacor character varying(54),
    medext character varying(3),
    usuome character varying(3),
    medico character varying(3),
    conformelab smallint,
    usuarioconforme character varying(8),
    fechaconforme timestamp without time zone,
    numerofichero integer,
    usuextraccion character varying(3),
    usucierre character varying(3),
    usuvisto character varying(3),
    cie9 character varying(9),
    especialista character varying(3),
    numordenrel integer,
    diagnostico character varying(100),
    numagrusol integer,
    secuenproto numeric(21,3),
    tiporadio character varying(1),
    fechainicita date,
    fechafincita date,
    dirweb character varying(255),
    actuesalus smallint,
    fechacitaext timestamp without time zone,
    tipopic smallint
);


ALTER TABLE protocolos_omi_covid_base.hisa_iddome OWNER TO postgres;

--
-- Name: iddcoe; Type: TABLE; Schema: protocolos_omi_covid_base; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_base.iddcoe (
    nif text,
    nif2 integer,
    especiali text,
    secuen numeric(21,3),
    wproto integer,
    wnumero integer,
    wtipo text,
    campo text,
    medico text,
    fecha timestamp without time zone,
    fecha2 date,
    tabla text,
    medico_id bigint
);


ALTER TABLE protocolos_omi_covid_base.iddcoe OWNER TO postgres;

--
-- Name: iddcop; Type: TABLE; Schema: protocolos_omi_covid_base; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_base.iddcop (
    nif text,
    nif2 integer,
    especiali text,
    secuen numeric(21,3),
    wproto integer,
    wnumero integer,
    wtipo text,
    campo text,
    medico text,
    fecha timestamp without time zone,
    fecha2 date,
    tabla character varying(6),
    medico_id bigint
);


ALTER TABLE protocolos_omi_covid_base.iddcop OWNER TO postgres;

--
-- Name: iddcor; Type: TABLE; Schema: protocolos_omi_covid_base; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_base.iddcor (
    nif text,
    nif2 integer,
    especiali text,
    secuen numeric(18,3),
    numero integer,
    fecha timestamp without time zone,
    fecha2 date,
    curso text,
    tipolin text,
    visitam text,
    origen text,
    tipocor smallint,
    wproto integer,
    medico text,
    clinico_id integer
);


ALTER TABLE protocolos_omi_covid_base.iddcor OWNER TO postgres;

--
-- Name: iddncu; Type: TABLE; Schema: protocolos_omi_covid_base; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_base.iddncu (
    nif text,
    nif2 integer,
    ciap text,
    cipap text,
    prescrip text,
    archivo text,
    tipoepi text,
    visitam text,
    origen text,
    descripci2 text,
    histo text,
    especiali text,
    fecha timestamp without time zone,
    fecha2 date,
    descripcio text,
    medico text,
    fechalta date,
    fechalta2 date,
    curobser text,
    subjetiva text,
    aparato text,
    especiali2 text,
    medicocap text,
    codigoesp text,
    tipus text,
    t smallint,
    n smallint,
    m smallint,
    destino text,
    cie text,
    fechaing timestamp without time zone,
    horaing integer,
    usuarioing text,
    lateralidad text,
    esp text
);


ALTER TABLE protocolos_omi_covid_base.iddncu OWNER TO postgres;

--
-- Name: iddpacpa3; Type: TABLE; Schema: protocolos_omi_covid_base; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_base.iddpacpa3 (
    zona character varying(4),
    gerencia character varying(4),
    centro character varying(8),
    consultorio character varying(100),
    nif character varying(10),
    apellido1 character varying(25),
    apellido2 character varying(25),
    nombre character varying(25),
    enviado character varying(25),
    domicilio character varying(50),
    poblacion character varying(25),
    postal character varying(5),
    provincia character varying(25),
    pais character varying(3),
    sexo character varying(1),
    estado character varying(1),
    nacimiento date,
    coberturap character varying(8),
    ga character varying(3),
    empresa character varying(2),
    telpart character varying(12),
    teldesp character varying(12),
    fechalta timestamp without time zone,
    fechaultvi timestamp without time zone,
    paconflic character varying(1),
    medicoha character varying(8),
    enfermera character varying(8),
    observac character varying(255),
    opcional character varying(20),
    idioma character varying(2),
    profesion character varying(25),
    grupo character varying(2),
    subgrupo character varying(1),
    observacio character varying(64),
    dni character varying(10),
    situacion character varying(1),
    subtis character varying(2),
    tipopac character varying(1),
    tis character varying(16),
    papps1 character varying(1),
    papps2 character varying(1),
    papps3 character varying(1),
    papps4 character varying(1),
    tipofili character varying(1),
    regimen character varying(2),
    nsp character varying(3),
    clasepac character varying(1),
    recrec character varying(1),
    intereco integer,
    interfecha integer,
    hmunresi character varying(3),
    hcalresi character varying(7),
    hnportal character varying(3),
    hnsubporta character varying(1),
    hbis character varying(1),
    hbloque character varying(3),
    hpiso character varying(2),
    hmano character varying(1),
    factual integer,
    fmedico character varying(4),
    ftransfer character varying(1),
    grupfam character varying(30),
    causa character varying(2),
    nsegsoc character varying(13),
    parentes character varying(1),
    femitar integer,
    emitarsit character varying(1),
    causacmed character varying(2),
    caduca character varying(6),
    tiscab character varying(15),
    tisnom character varying(20),
    tisapel1 character varying(20),
    tisapel2 character varying(20),
    tisotras character varying(24),
    fechapre timestamp without time zone,
    observac2 character varying(255),
    caserio character varying(20),
    tipousu character varying(3),
    visitas integer,
    interno character varying(20),
    autononaci character varying(3),
    paisnacimiento character varying(3),
    entasegu character varying(4),
    entconcer character varying(4),
    fechactis timestamp without time zone,
    uba character varying(10),
    pacocu character varying(1),
    exitus timestamp without time zone,
    teobsobs character varying(200),
    vism integer,
    vise integer,
    visx integer,
    email character varying(30),
    nfinal character varying(5),
    ninicial character varying(5),
    km character varying(5),
    tipusdoc character varying(1),
    conjuncio character varying(1),
    padroind character varying(1),
    interno2 integer,
    prox_tartrec integer,
    residencia character varying(4),
    cipregional character varying(16),
    tabla text,
    estado2 text,
    nif2 integer,
    region character varying(100),
    comuna character varying(100),
    ciudad character varying(100),
    run integer,
    dv character varying(1),
    centro_deis smallint,
    pac_nombre_completo text
);


ALTER TABLE protocolos_omi_covid_base.iddpacpa3 OWNER TO postgres;

--
-- Name: examenes_pcr_omi; Type: TABLE; Schema: protocolos_omi_covid_etl; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_etl.examenes_pcr_omi (
    numorden integer,
    codilab character varying(15),
    nif character varying(10),
    solicitud_fecha integer,
    solicitud_hora integer,
    cierre_fecha integer,
    cierre_hora integer,
    resultado character varying(80)
);


ALTER TABLE protocolos_omi_covid_etl.examenes_pcr_omi OWNER TO postgres;

--
-- Name: hisa_iddcoe; Type: TABLE; Schema: protocolos_omi_covid_etl; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_etl.hisa_iddcoe (
    fecha timestamp without time zone,
    usuario character varying(3),
    tipo_operacion character varying(1),
    id_hisa_iddcoe integer,
    nif character varying(10),
    especiali character varying(10),
    secuen numeric(21,3),
    wproto integer,
    wnumero integer,
    wtipo character varying(1),
    campo character varying(1000),
    tabla character varying(6)
);


ALTER TABLE protocolos_omi_covid_etl.hisa_iddcoe OWNER TO postgres;

--
-- Name: hisa_iddcop; Type: TABLE; Schema: protocolos_omi_covid_etl; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_etl.hisa_iddcop (
    fecha timestamp without time zone,
    usuario character varying(3),
    tipo_operacion character varying(1),
    id_hisa_iddcop integer,
    nif character varying(10),
    especiali character varying(10),
    secuen numeric(21,3),
    wproto integer,
    wnumero integer,
    wtipo character varying(1),
    campo character varying(1000),
    tabla character varying(6)
);


ALTER TABLE protocolos_omi_covid_etl.hisa_iddcop OWNER TO postgres;

--
-- Name: hisa_iddcor; Type: TABLE; Schema: protocolos_omi_covid_etl; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_etl.hisa_iddcor (
    fecha timestamp without time zone,
    usuario character varying(3),
    tipo_operacion character varying(1),
    id_hisa_iddcor integer,
    nhc character varying(10),
    especiali character varying(10),
    secuen numeric(21,3),
    numero integer,
    fecha_1 integer,
    curso character varying(54),
    tipolin character varying(124),
    visitam character varying(8),
    origen character varying(2),
    tipocor smallint,
    wproto integer,
    medico character varying(3)
);


ALTER TABLE protocolos_omi_covid_etl.hisa_iddcor OWNER TO postgres;

--
-- Name: hisa_iddncu; Type: TABLE; Schema: protocolos_omi_covid_etl; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_etl.hisa_iddncu (
    stk_fecha timestamp without time zone,
    stk_usuario character varying(3),
    stk_tipo_operacion character varying(1),
    id_hisa_iddncu integer,
    nif character varying(10),
    ciap character varying(60),
    cipap character varying(3),
    prescrip character varying(1),
    archivo character varying(1),
    tipoepi character varying(1),
    visitam character varying(8),
    origen character varying(2),
    descripci2 character varying(55),
    histo character varying(1),
    especiali character varying(10),
    fecha integer,
    descripcio character varying(150),
    medico character varying(3),
    fechalta integer,
    curobser character varying(56),
    subjetiva character varying(1),
    aparato character varying(1),
    especiali2 character varying(10),
    medicocap character varying(30),
    codigoesp character varying(3),
    tipus character varying(2),
    t smallint,
    n smallint,
    m smallint,
    destino character varying(2),
    cie character varying(9),
    fechaing integer,
    horaing integer,
    usuarioing character varying(8),
    lateralidad character varying(1),
    esp character varying(3)
);


ALTER TABLE protocolos_omi_covid_etl.hisa_iddncu OWNER TO postgres;

--
-- Name: hisa_iddome; Type: TABLE; Schema: protocolos_omi_covid_etl; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_etl.hisa_iddome (
    stk_fecha timestamp without time zone,
    stk_usuario character varying(3),
    stk_tipo_operacion character varying(1),
    id_hisa_iddome integer,
    comentario character varying(1000),
    respuestainter character varying(1000),
    nif character varying(10),
    prueba character varying(8),
    fecha integer,
    centro character varying(8),
    fechar integer,
    prioridad character varying(1),
    usuario character varying(3),
    observacio character varying(59),
    numorden integer,
    numage numeric(21,3),
    patologica character varying(1),
    fecharea integer,
    aproxmes numeric(21,3),
    aproxdias numeric(21,3),
    tiporea character varying(1),
    tiposol character varying(2),
    otraspru character varying(59),
    clasepru character varying(1),
    solicita character varying(3),
    lugar character varying(1),
    numero1 numeric(21,3),
    numero2 numeric(21,3),
    numero3 numeric(21,3),
    numero4 numeric(21,3),
    numero5 numeric(21,3),
    numero6 numeric(21,3),
    numero7 numeric(21,3),
    numero8 numeric(21,3),
    tubo1 character varying(10),
    tubo2 character varying(10),
    tubo3 character varying(10),
    tubo4 character varying(10),
    tubo5 character varying(10),
    tubo6 character varying(10),
    tubo7 character varying(10),
    tubo8 character varying(10),
    nombrem character varying(30),
    especia character varying(3),
    especiali character varying(10),
    secuen numeric(21,3),
    visto character varying(1),
    fechav integer,
    horav integer,
    fechac integer,
    horac integer,
    fechap integer,
    horap integer,
    fechare integer,
    horare integer,
    total integer,
    realizadas integer,
    codilab character varying(15),
    lab smallint,
    fecharev integer,
    horarev integer,
    yo character varying(1),
    liniacor character varying(54),
    medext character varying(3),
    usuome character varying(3),
    medico character varying(3),
    conformelab smallint,
    usuarioconforme character varying(8),
    fechaconforme integer,
    horaconforme integer,
    numerofichero integer,
    horar integer,
    usuextraccion character varying(3),
    usucierre character varying(3),
    usuvisto character varying(3),
    cie9 character varying(9),
    especialista character varying(3),
    numordenrel integer,
    diagnostico character varying(100),
    numagrusol integer,
    secuenproto numeric(21,3),
    tiporadio character varying(1),
    fechainicita integer,
    fechafincita integer,
    dirweb character varying(255),
    actuesalus smallint,
    fechacitaext integer,
    horacitaext integer,
    tipopic smallint,
    tabla character varying(6)
);


ALTER TABLE protocolos_omi_covid_etl.hisa_iddome OWNER TO postgres;

--
-- Name: iddcoe; Type: TABLE; Schema: protocolos_omi_covid_etl; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_etl.iddcoe (
    nif character varying(10),
    especiali character varying(10),
    secuen numeric(21,3),
    wproto integer,
    wnumero integer,
    wtipo character varying(1),
    campo character varying(1000)
);


ALTER TABLE protocolos_omi_covid_etl.iddcoe OWNER TO postgres;

--
-- Name: iddcop; Type: TABLE; Schema: protocolos_omi_covid_etl; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_etl.iddcop (
    nif character varying(10),
    especiali character varying(10),
    secuen numeric(21,3),
    wproto integer,
    wnumero integer,
    wtipo character varying(1),
    campo character varying(1000),
    tabla character varying(6)
);


ALTER TABLE protocolos_omi_covid_etl.iddcop OWNER TO postgres;

--
-- Name: iddcor; Type: TABLE; Schema: protocolos_omi_covid_etl; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_etl.iddcor (
    nhc character varying(10),
    especiali character varying(10),
    secuen numeric(18,3),
    numero integer,
    fecha integer,
    curso character varying(1000),
    tipolin character varying(124),
    visitam character varying(8),
    origen character varying(2),
    tipocor smallint,
    wproto integer,
    medico character varying(3)
);


ALTER TABLE protocolos_omi_covid_etl.iddcor OWNER TO postgres;

--
-- Name: iddncu; Type: TABLE; Schema: protocolos_omi_covid_etl; Owner: postgres
--

CREATE TABLE protocolos_omi_covid_etl.iddncu (
    nif character varying(10),
    ciap character varying(60),
    cipap character varying(3),
    prescrip character varying(1),
    archivo character varying(1),
    tipoepi character varying(1),
    visitam character varying(8),
    origen character varying(2),
    descripci2 character varying(55),
    histo character varying(1),
    especiali character varying(10),
    fecha integer,
    descripcio character varying(150),
    medico character varying(3),
    fechalta integer,
    curobser character varying(56),
    subjetiva character varying(1),
    aparato character varying(1),
    especiali2 character varying(10),
    medicocap character varying(30),
    codigoesp character varying(3),
    tipus character varying(2),
    t smallint,
    n smallint,
    m smallint,
    destino character varying(2),
    cie character varying(9),
    fechaing integer,
    horaing integer,
    usuarioing character varying(8),
    lateralidad character varying(1),
    esp character varying(3)
);


ALTER TABLE protocolos_omi_covid_etl.iddncu OWNER TO postgres;

--
-- Name: dim_tablas id; Type: DEFAULT; Schema: protocolos_omi_covid; Owner: postgres
--

ALTER TABLE ONLY protocolos_omi_covid.dim_tablas ALTER COLUMN id SET DEFAULT nextval('protocolos_omi_covid.dim_tablas_id_seq'::regclass);


SET default_tablespace = tb_index;

--
-- Name: resultado_examen_i_01; Type: INDEX; Schema: covid19; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX resultado_examen_i_01 ON covid19.resultado_examen USING btree (nif2, fecha2, hora);


--
-- Name: todos_los_protocolos_i_01; Type: INDEX; Schema: covid19; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX todos_los_protocolos_i_01 ON covid19.todos_los_protocolos USING btree (medico, wproto2);


--
-- Name: dim_tblvar_i_04; Type: INDEX; Schema: protocolos_omi_covid; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX dim_tblvar_i_04 ON protocolos_omi_covid.dim_tblvar USING btree (wproto, wnumero, wcampo);


--
-- Name: dim_tblvar_wcampo_idx; Type: INDEX; Schema: protocolos_omi_covid; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX dim_tblvar_wcampo_idx ON protocolos_omi_covid.dim_tblvar USING btree (wcampo);


--
-- Name: dim_tblvar_wnumero_idx; Type: INDEX; Schema: protocolos_omi_covid; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX dim_tblvar_wnumero_idx ON protocolos_omi_covid.dim_tblvar USING btree (wnumero);


--
-- Name: dim_tblvar_wproto_idx; Type: INDEX; Schema: protocolos_omi_covid; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX dim_tblvar_wproto_idx ON protocolos_omi_covid.dim_tblvar USING btree (wproto);


--
-- Name: mapro_nomtbl_i_01; Type: INDEX; Schema: protocolos_omi_covid; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX mapro_nomtbl_i_01 ON protocolos_omi_covid.mapro_nomtbl USING btree (wproto, wprotocolo, nomtbl);


--
-- Name: mapro_nomtbl_nomtbl_idx; Type: INDEX; Schema: protocolos_omi_covid; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX mapro_nomtbl_nomtbl_idx ON protocolos_omi_covid.mapro_nomtbl USING btree (nomtbl);


--
-- Name: mapro_nomtbl_wproto_idx; Type: INDEX; Schema: protocolos_omi_covid; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX mapro_nomtbl_wproto_idx ON protocolos_omi_covid.mapro_nomtbl USING btree (wproto);


--
-- Name: mapro_nomtbl_wprotocolo_idx; Type: INDEX; Schema: protocolos_omi_covid; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX mapro_nomtbl_wprotocolo_idx ON protocolos_omi_covid.mapro_nomtbl USING btree (wprotocolo);


--
-- Name: mapro_nomvar_i_05; Type: INDEX; Schema: protocolos_omi_covid; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX mapro_nomvar_i_05 ON protocolos_omi_covid.mapro_nomvar USING btree (wproto, id_num, nomvar, wnumero);


--
-- Name: mapro_nomvar_id_num_idx; Type: INDEX; Schema: protocolos_omi_covid; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX mapro_nomvar_id_num_idx ON protocolos_omi_covid.mapro_nomvar USING btree (id_num);


--
-- Name: mapro_nomvar_nomvar_idx; Type: INDEX; Schema: protocolos_omi_covid; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX mapro_nomvar_nomvar_idx ON protocolos_omi_covid.mapro_nomvar USING btree (nomvar);


--
-- Name: mapro_nomvar_wnumero_idx; Type: INDEX; Schema: protocolos_omi_covid; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX mapro_nomvar_wnumero_idx ON protocolos_omi_covid.mapro_nomvar USING btree (wnumero);


--
-- Name: mapro_nomvar_wproto_idx; Type: INDEX; Schema: protocolos_omi_covid; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX mapro_nomvar_wproto_idx ON protocolos_omi_covid.mapro_nomvar USING btree (wproto);


--
-- Name: datos_contactos01_i_01; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX datos_contactos01_i_01 ON protocolos_omi_covid_base.datos_contactos01 USING btree (nif);


--
-- Name: datos_contactos01_i_02; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX datos_contactos01_i_02 ON protocolos_omi_covid_base.datos_contactos01 USING btree (fecha);


--
-- Name: datos_contactos01_i_03; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX datos_contactos01_i_03 ON protocolos_omi_covid_base.datos_contactos01 USING btree (num_caso);


--
-- Name: datos_contactos01_i_04; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX datos_contactos01_i_04 ON protocolos_omi_covid_base.datos_contactos01 USING btree (dato);


--
-- Name: datos_contactos01_i_05; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX datos_contactos01_i_05 ON protocolos_omi_covid_base.datos_contactos01 USING btree (dato);


--
-- Name: datos_contactos01_i_06; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX datos_contactos01_i_06 ON protocolos_omi_covid_base.datos_contactos01 USING btree (valor);


--
-- Name: datos_contactos01_i_07; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX datos_contactos01_i_07 ON protocolos_omi_covid_base.datos_contactos01 USING btree (nif, num_caso);


--
-- Name: datos_contactos01_i_08; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX datos_contactos01_i_08 ON protocolos_omi_covid_base.datos_contactos01 USING btree (nif, fecha, num_caso);


--
-- Name: hisa_iddcor_i_01; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX hisa_iddcor_i_01 ON protocolos_omi_covid_base.hisa_iddcor USING btree (numero, stk_tipo_operacion, wproto);


--
-- Name: hisa_iddome_i_01; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX hisa_iddome_i_01 ON protocolos_omi_covid_base.hisa_iddome USING btree (id_hisa_iddome);


--
-- Name: hisa_iddome_i_02; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX hisa_iddome_i_02 ON protocolos_omi_covid_base.hisa_iddome USING btree (nif2);


--
-- Name: hisa_iddome_i_03; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX hisa_iddome_i_03 ON protocolos_omi_covid_base.hisa_iddome USING btree (nif);


--
-- Name: hisa_iddome_i_04; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX hisa_iddome_i_04 ON protocolos_omi_covid_base.hisa_iddome USING btree (numorden);


--
-- Name: hisa_iddome_i_05; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX hisa_iddome_i_05 ON protocolos_omi_covid_base.hisa_iddome USING btree (stk_fecha);


--
-- Name: hisa_iddome_i_06; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX hisa_iddome_i_06 ON protocolos_omi_covid_base.hisa_iddome USING btree (stk_tipo_operacion);


--
-- Name: iddcoe_i_01; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_01 ON protocolos_omi_covid_base.iddcoe USING btree (nif, especiali, secuen, wproto);


--
-- Name: iddcoe_i_02; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_02 ON protocolos_omi_covid_base.iddcoe USING btree (nif);


--
-- Name: iddcoe_i_03; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_03 ON protocolos_omi_covid_base.iddcoe USING btree (especiali);


--
-- Name: iddcoe_i_04; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_04 ON protocolos_omi_covid_base.iddcoe USING btree (secuen);


--
-- Name: iddcoe_i_05; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_05 ON protocolos_omi_covid_base.iddcoe USING btree (wproto);


--
-- Name: iddcoe_i_06; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_06 ON protocolos_omi_covid_base.iddcoe USING btree (wnumero);


--
-- Name: iddcoe_i_07; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_07 ON protocolos_omi_covid_base.iddcoe USING btree (wtipo);


--
-- Name: iddcoe_i_08; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_08 ON protocolos_omi_covid_base.iddcoe USING btree (wproto, wnumero);


--
-- Name: iddcoe_i_09; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_09 ON protocolos_omi_covid_base.iddcoe USING btree (fecha);


--
-- Name: iddcoe_i_10; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_10 ON protocolos_omi_covid_base.iddcoe USING btree (medico);


--
-- Name: iddcoe_i_11; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_11 ON protocolos_omi_covid_base.iddcoe USING btree (nif, especiali, secuen, wproto, fecha, medico);


--
-- Name: iddcoe_i_12; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_12 ON protocolos_omi_covid_base.iddcoe USING btree (wproto, wnumero, fecha, campo, nif, especiali, secuen);


--
-- Name: iddcoe_i_13; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_13 ON protocolos_omi_covid_base.iddcoe USING btree (fecha2);


--
-- Name: iddcoe_i_14; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_14 ON protocolos_omi_covid_base.iddcoe USING btree (wproto, wnumero, fecha2);


--
-- Name: iddcoe_i_15; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_15 ON protocolos_omi_covid_base.iddcoe USING btree (wproto, wnumero, fecha);


--
-- Name: iddcoe_i_16; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_16 ON protocolos_omi_covid_base.iddcoe USING btree (nif2, especiali, secuen, wproto, fecha2, medico);


--
-- Name: iddcoe_i_17; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_17 ON protocolos_omi_covid_base.iddcoe USING btree (wproto, wnumero, fecha2, campo, nif2, especiali, secuen);


--
-- Name: iddcoe_i_18; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_18 ON protocolos_omi_covid_base.iddcoe USING btree (wproto, wnumero, campo);


--
-- Name: iddcoe_i_19; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_19 ON protocolos_omi_covid_base.iddcoe USING btree (nif2);


--
-- Name: iddcoe_i_21; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_21 ON protocolos_omi_covid_base.iddcoe USING btree (medico_id);


--
-- Name: iddcoe_i_22; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_22 ON protocolos_omi_covid_base.iddcoe USING btree (nif2, fecha2, medico_id);


--
-- Name: iddcoe_i_23; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_23 ON protocolos_omi_covid_base.iddcoe USING btree (nif2, fecha2, medico_id, especiali, secuen);


--
-- Name: iddcoe_i_24; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_24 ON protocolos_omi_covid_base.iddcoe USING btree (nif2, fecha2, medico_id, especiali, secuen, wproto);


--
-- Name: iddcoe_i_25; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_25 ON protocolos_omi_covid_base.iddcoe USING btree (nif2, fecha2, medico_id, especiali, secuen, wproto, wnumero);


--
-- Name: iddcoe_i_26; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_26 ON protocolos_omi_covid_base.iddcoe USING btree (nif2, fecha2, medico_id, wproto, wnumero);


--
-- Name: iddcoe_i_27; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_27 ON protocolos_omi_covid_base.iddcoe USING btree (nif2, fecha2, medico_id, wproto);


--
-- Name: iddcoe_i_28; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcoe_i_28 ON protocolos_omi_covid_base.iddcoe USING btree (nif2, fecha2, medico_id, especiali);


--
-- Name: iddcop_i_01; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_01 ON protocolos_omi_covid_base.iddcop USING btree (nif, especiali, secuen, wproto);


--
-- Name: iddcop_i_02; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_02 ON protocolos_omi_covid_base.iddcop USING btree (nif);


--
-- Name: iddcop_i_03; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_03 ON protocolos_omi_covid_base.iddcop USING btree (especiali);


--
-- Name: iddcop_i_04; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_04 ON protocolos_omi_covid_base.iddcop USING btree (secuen);


--
-- Name: iddcop_i_05; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_05 ON protocolos_omi_covid_base.iddcop USING btree (wproto);


--
-- Name: iddcop_i_06; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_06 ON protocolos_omi_covid_base.iddcop USING btree (wnumero);


--
-- Name: iddcop_i_07; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_07 ON protocolos_omi_covid_base.iddcop USING btree (wtipo);


--
-- Name: iddcop_i_08; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_08 ON protocolos_omi_covid_base.iddcop USING btree (wproto, wnumero);


--
-- Name: iddcop_i_09; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_09 ON protocolos_omi_covid_base.iddcop USING btree (fecha);


--
-- Name: iddcop_i_10; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_10 ON protocolos_omi_covid_base.iddcop USING btree (medico);


--
-- Name: iddcop_i_11; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_11 ON protocolos_omi_covid_base.iddcop USING btree (nif, especiali, secuen, wproto, fecha, medico);


--
-- Name: iddcop_i_12; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_12 ON protocolos_omi_covid_base.iddcop USING btree (wproto, wnumero, fecha, campo, nif, especiali, secuen);


--
-- Name: iddcop_i_13; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_13 ON protocolos_omi_covid_base.iddcop USING btree (fecha2);


--
-- Name: iddcop_i_14; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_14 ON protocolos_omi_covid_base.iddcop USING btree (wproto, wnumero, fecha2);


--
-- Name: iddcop_i_15; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_15 ON protocolos_omi_covid_base.iddcop USING btree (wproto, wnumero, fecha);


--
-- Name: iddcop_i_16; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_16 ON protocolos_omi_covid_base.iddcop USING btree (nif2, especiali, secuen, wproto, fecha2, medico);


--
-- Name: iddcop_i_17; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_17 ON protocolos_omi_covid_base.iddcop USING btree (wproto, wnumero, fecha2, campo, nif2, especiali, secuen);


--
-- Name: iddcop_i_18; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_18 ON protocolos_omi_covid_base.iddcop USING btree (wproto, wnumero, campo);


--
-- Name: iddcop_i_19; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_19 ON protocolos_omi_covid_base.iddcop USING btree (nif2);


--
-- Name: iddcop_i_21; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_21 ON protocolos_omi_covid_base.iddcop USING btree (medico_id);


--
-- Name: iddcop_i_22; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_22 ON protocolos_omi_covid_base.iddcop USING btree (nif2, fecha2, medico_id);


--
-- Name: iddcop_i_23; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_23 ON protocolos_omi_covid_base.iddcop USING btree (nif2, fecha2, medico_id, especiali, secuen);


--
-- Name: iddcop_i_24; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_24 ON protocolos_omi_covid_base.iddcop USING btree (nif2, fecha2, medico_id, especiali, secuen, wproto);


--
-- Name: iddcop_i_25; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_25 ON protocolos_omi_covid_base.iddcop USING btree (nif2, fecha2, medico_id, especiali, secuen, wproto, wnumero);


--
-- Name: iddcop_i_26; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_26 ON protocolos_omi_covid_base.iddcop USING btree (nif2, fecha2, medico_id, wproto, wnumero);


--
-- Name: iddcop_i_27; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_27 ON protocolos_omi_covid_base.iddcop USING btree (nif2, fecha2, medico_id, wproto);


--
-- Name: iddcop_i_28; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcop_i_28 ON protocolos_omi_covid_base.iddcop USING btree (nif2, fecha2, medico_id, especiali);


--
-- Name: iddcor_i_01; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_01 ON protocolos_omi_covid_base.iddcor USING btree (nif);


--
-- Name: iddcor_i_02; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_02 ON protocolos_omi_covid_base.iddcor USING btree (fecha);


--
-- Name: iddcor_i_03; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_03 ON protocolos_omi_covid_base.iddcor USING btree (medico);


--
-- Name: iddcor_i_04; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_04 ON protocolos_omi_covid_base.iddcor USING btree (wproto);


--
-- Name: iddcor_i_05; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_05 ON protocolos_omi_covid_base.iddcor USING btree (numero);


--
-- Name: iddcor_i_06; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_06 ON protocolos_omi_covid_base.iddcor USING btree (nif, especiali, secuen, numero, wproto);


--
-- Name: iddcor_i_07; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_07 ON protocolos_omi_covid_base.iddcor USING btree (nif, fecha, medico);


--
-- Name: iddcor_i_09; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_09 ON protocolos_omi_covid_base.iddcor USING btree (nif, fecha);


--
-- Name: iddcor_i_10; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_10 ON protocolos_omi_covid_base.iddcor USING btree (nif, fecha, wproto);


--
-- Name: iddcor_i_11; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_11 ON protocolos_omi_covid_base.iddcor USING btree (especiali);


--
-- Name: iddcor_i_12; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_12 ON protocolos_omi_covid_base.iddcor USING btree (tipocor);


--
-- Name: iddcor_i_13; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_13 ON protocolos_omi_covid_base.iddcor USING btree (secuen);


--
-- Name: iddcor_i_14; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_14 ON protocolos_omi_covid_base.iddcor USING btree (nif2);


--
-- Name: iddcor_i_15; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_15 ON protocolos_omi_covid_base.iddcor USING btree (fecha2);


--
-- Name: iddcor_i_16; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_16 ON protocolos_omi_covid_base.iddcor USING btree (clinico_id);


--
-- Name: iddcor_i_17; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_17 ON protocolos_omi_covid_base.iddcor USING btree (nif2, especiali, secuen, numero, wproto);


--
-- Name: iddcor_i_18; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_18 ON protocolos_omi_covid_base.iddcor USING btree (nif2, fecha2, clinico_id);


--
-- Name: iddcor_i_19; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_19 ON protocolos_omi_covid_base.iddcor USING btree (wproto, numero, nif, fecha);


--
-- Name: iddcor_i_21; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_21 ON protocolos_omi_covid_base.iddcor USING btree (nif2, fecha2);


--
-- Name: iddcor_i_22; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_22 ON protocolos_omi_covid_base.iddcor USING btree (nif2, fecha2, wproto);


--
-- Name: iddcor_i_23; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_23 ON protocolos_omi_covid_base.iddcor USING btree (wproto, numero, medico, nif, fecha, especiali);


--
-- Name: iddcor_i_24; Type: INDEX; Schema: protocolos_omi_covid_base; Owner: postgres; Tablespace: tb_index
--

CREATE INDEX iddcor_i_24 ON protocolos_omi_covid_base.iddcor USING btree (wproto, numero, medico, nif2, fecha, especiali);


--
-- Name: SCHEMA protocolos_omi_covid; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA protocolos_omi_covid TO jesoto;
GRANT USAGE ON SCHEMA protocolos_omi_covid TO ehenriquez;
GRANT USAGE ON SCHEMA protocolos_omi_covid TO ugina;
GRANT USAGE ON SCHEMA protocolos_omi_covid TO consultaomi;
GRANT USAGE ON SCHEMA protocolos_omi_covid TO consulta;


--
-- Name: TABLE consultas_de_morbilidad; Type: ACL; Schema: covid19; Owner: postgres
--

GRANT SELECT ON TABLE covid19.consultas_de_morbilidad TO consulta;
GRANT SELECT ON TABLE covid19.consultas_de_morbilidad TO ugina;


--
-- Name: TABLE episodios; Type: ACL; Schema: covid19; Owner: postgres
--

GRANT SELECT ON TABLE covid19.episodios TO consulta;
GRANT SELECT ON TABLE covid19.episodios TO ugina;


--
-- Name: TABLE episodios_cierre_covid; Type: ACL; Schema: covid19; Owner: postgres
--

GRANT SELECT ON TABLE covid19.episodios_cierre_covid TO consulta;
GRANT SELECT ON TABLE covid19.episodios_cierre_covid TO ugina;


--
-- Name: TABLE hechos; Type: ACL; Schema: covid19; Owner: postgres
--

GRANT SELECT ON TABLE covid19.hechos TO consulta;
GRANT SELECT ON TABLE covid19.hechos TO ugina;


--
-- Name: TABLE hechos2; Type: ACL; Schema: covid19; Owner: postgres
--

GRANT SELECT ON TABLE covid19.hechos2 TO consulta;
GRANT SELECT ON TABLE covid19.hechos2 TO ugina;


--
-- Name: TABLE hechos4; Type: ACL; Schema: covid19; Owner: postgres
--

GRANT SELECT ON TABLE covid19.hechos4 TO consulta;
GRANT SELECT ON TABLE covid19.hechos4 TO ugina;


--
-- Name: TABLE hechos5; Type: ACL; Schema: covid19; Owner: postgres
--

GRANT SELECT ON TABLE covid19.hechos5 TO consulta;
GRANT SELECT ON TABLE covid19.hechos5 TO ugina;


--
-- Name: TABLE hechos_examenes_pcr; Type: ACL; Schema: covid19; Owner: postgres
--

GRANT SELECT ON TABLE covid19.hechos_examenes_pcr TO consulta;
GRANT SELECT ON TABLE covid19.hechos_examenes_pcr TO ugina;


--
-- Name: TABLE kpi_examenes_pcr; Type: ACL; Schema: covid19; Owner: postgres
--

GRANT SELECT ON TABLE covid19.kpi_examenes_pcr TO consulta;
GRANT SELECT ON TABLE covid19.kpi_examenes_pcr TO ugina;


--
-- Name: TABLE rangos_edad_01; Type: ACL; Schema: covid19; Owner: postgres
--

GRANT SELECT ON TABLE covid19.rangos_edad_01 TO consulta;
GRANT SELECT ON TABLE covid19.rangos_edad_01 TO ugina;


--
-- Name: TABLE resultado_examen; Type: ACL; Schema: covid19; Owner: postgres
--

GRANT SELECT ON TABLE covid19.resultado_examen TO consulta;
GRANT SELECT ON TABLE covid19.resultado_examen TO ugina;


--
-- Name: TABLE todos_los_protocolos; Type: ACL; Schema: covid19; Owner: postgres
--

GRANT SELECT ON TABLE covid19.todos_los_protocolos TO consulta;
GRANT SELECT ON TABLE covid19.todos_los_protocolos TO ugina;


--
-- Name: TABLE hechos01; Type: ACL; Schema: covid19_report; Owner: postgres
--

GRANT SELECT ON TABLE covid19_report.hechos01 TO consulta;
GRANT SELECT ON TABLE covid19_report.hechos01 TO ugina;


--
-- Name: TABLE hechos02; Type: ACL; Schema: covid19_report; Owner: postgres
--

GRANT SELECT ON TABLE covid19_report.hechos02 TO consulta;
GRANT SELECT ON TABLE covid19_report.hechos02 TO ugina;


--
-- Name: TABLE listado_casos_confirmados_seremi; Type: ACL; Schema: covid19_report; Owner: postgres
--

GRANT SELECT ON TABLE covid19_report.listado_casos_confirmados_seremi TO consulta;
GRANT SELECT ON TABLE covid19_report.listado_casos_confirmados_seremi TO ugina;


--
-- Name: TABLE listado_casos_seremi; Type: ACL; Schema: covid19_report; Owner: postgres
--

GRANT SELECT ON TABLE covid19_report.listado_casos_seremi TO consulta;
GRANT SELECT ON TABLE covid19_report.listado_casos_seremi TO ugina;


--
-- Name: TABLE examenes; Type: ACL; Schema: covid19_tmp; Owner: postgres
--

GRANT SELECT ON TABLE covid19_tmp.examenes TO consulta;
GRANT SELECT ON TABLE covid19_tmp.examenes TO ugina;


--
-- Name: TABLE examenes01; Type: ACL; Schema: covid19_tmp; Owner: postgres
--

GRANT SELECT ON TABLE covid19_tmp.examenes01 TO consulta;
GRANT SELECT ON TABLE covid19_tmp.examenes01 TO ugina;


--
-- Name: TABLE dim_tablas; Type: ACL; Schema: protocolos_omi_covid; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid.dim_tablas TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.dim_tablas TO ugina;
GRANT SELECT ON TABLE protocolos_omi_covid.dim_tablas TO jesoto;
GRANT SELECT ON TABLE protocolos_omi_covid.dim_tablas TO ehenriquez;
GRANT SELECT ON TABLE protocolos_omi_covid.dim_tablas TO consultaomi;


--
-- Name: TABLE dim_tblvar; Type: ACL; Schema: protocolos_omi_covid; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid.dim_tblvar TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.dim_tblvar TO ugina;
GRANT SELECT ON TABLE protocolos_omi_covid.dim_tblvar TO jesoto;
GRANT SELECT ON TABLE protocolos_omi_covid.dim_tblvar TO ehenriquez;
GRANT SELECT ON TABLE protocolos_omi_covid.dim_tblvar TO consultaomi;


--
-- Name: TABLE estado_de_seguimiento02; Type: ACL; Schema: protocolos_omi_covid; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO ugina;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO consultaomi;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO ehenriquez;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO jesoto;


--
-- Name: TABLE estado_de_seguimiento03; Type: ACL; Schema: protocolos_omi_covid; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 TO ugina;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 TO consultaomi;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 TO ehenriquez;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 TO jesoto;


--
-- Name: TABLE listado_contactos_seremi; Type: ACL; Schema: protocolos_omi_covid; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid.listado_contactos_seremi TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.listado_contactos_seremi TO ugina;
GRANT SELECT ON TABLE protocolos_omi_covid.listado_contactos_seremi TO consultaomi;


--
-- Name: TABLE mapro_nomtbl; Type: ACL; Schema: protocolos_omi_covid; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid.mapro_nomtbl TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.mapro_nomtbl TO ugina;
GRANT SELECT ON TABLE protocolos_omi_covid.mapro_nomtbl TO jesoto;
GRANT SELECT ON TABLE protocolos_omi_covid.mapro_nomtbl TO ehenriquez;
GRANT SELECT ON TABLE protocolos_omi_covid.mapro_nomtbl TO consultaomi;


--
-- Name: TABLE mapro_nomvar; Type: ACL; Schema: protocolos_omi_covid; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid.mapro_nomvar TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.mapro_nomvar TO ugina;
GRANT SELECT ON TABLE protocolos_omi_covid.mapro_nomvar TO jesoto;
GRANT SELECT ON TABLE protocolos_omi_covid.mapro_nomvar TO ehenriquez;
GRANT SELECT ON TABLE protocolos_omi_covid.mapro_nomvar TO consultaomi;


--
-- Name: TABLE pr__covid_seguimiento_casos_y_contacto_wp_000409; Type: ACL; Schema: protocolos_omi_covid; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 TO ugina;
GRANT SELECT ON TABLE protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 TO jesoto;
GRANT SELECT ON TABLE protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 TO ehenriquez;
GRANT SELECT ON TABLE protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 TO consultaomi;


--
-- Name: TABLE pr_morbilidad_aguda_covid19_wp_000412; Type: ACL; Schema: protocolos_omi_covid; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 TO ugina;
GRANT SELECT ON TABLE protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 TO jesoto;
GRANT SELECT ON TABLE protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 TO ehenriquez;
GRANT SELECT ON TABLE protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 TO consultaomi;


--
-- Name: TABLE pr_morbilidad_aguda_wp_000397; Type: ACL; Schema: protocolos_omi_covid; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid.pr_morbilidad_aguda_wp_000397 TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.pr_morbilidad_aguda_wp_000397 TO ugina;
GRANT SELECT ON TABLE protocolos_omi_covid.pr_morbilidad_aguda_wp_000397 TO jesoto;
GRANT SELECT ON TABLE protocolos_omi_covid.pr_morbilidad_aguda_wp_000397 TO ehenriquez;
GRANT SELECT ON TABLE protocolos_omi_covid.pr_morbilidad_aguda_wp_000397 TO consultaomi;


--
-- Name: TABLE seguimiento_covid_hechos03; Type: ACL; Schema: protocolos_omi_covid; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid.seguimiento_covid_hechos03 TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.seguimiento_covid_hechos03 TO ugina;


--
-- Name: TABLE datos_contactos01; Type: ACL; Schema: protocolos_omi_covid_base; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_base.datos_contactos01 TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_base.datos_contactos01 TO ugina;


--
-- Name: TABLE examenes_pcr_omi; Type: ACL; Schema: protocolos_omi_covid_base; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_base.examenes_pcr_omi TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_base.examenes_pcr_omi TO ugina;


--
-- Name: TABLE hisa_iddcoe; Type: ACL; Schema: protocolos_omi_covid_base; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_base.hisa_iddcoe TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_base.hisa_iddcoe TO ugina;


--
-- Name: TABLE hisa_iddcop; Type: ACL; Schema: protocolos_omi_covid_base; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_base.hisa_iddcop TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_base.hisa_iddcop TO ugina;


--
-- Name: TABLE hisa_iddcor; Type: ACL; Schema: protocolos_omi_covid_base; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_base.hisa_iddcor TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_base.hisa_iddcor TO ugina;


--
-- Name: TABLE hisa_iddncu; Type: ACL; Schema: protocolos_omi_covid_base; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_base.hisa_iddncu TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_base.hisa_iddncu TO ugina;


--
-- Name: TABLE hisa_iddome; Type: ACL; Schema: protocolos_omi_covid_base; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_base.hisa_iddome TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_base.hisa_iddome TO ugina;


--
-- Name: TABLE iddcoe; Type: ACL; Schema: protocolos_omi_covid_base; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_base.iddcoe TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_base.iddcoe TO ugina;


--
-- Name: TABLE iddcop; Type: ACL; Schema: protocolos_omi_covid_base; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_base.iddcop TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_base.iddcop TO ugina;


--
-- Name: TABLE iddcor; Type: ACL; Schema: protocolos_omi_covid_base; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_base.iddcor TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_base.iddcor TO ugina;


--
-- Name: TABLE iddncu; Type: ACL; Schema: protocolos_omi_covid_base; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_base.iddncu TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_base.iddncu TO ugina;


--
-- Name: TABLE iddpacpa3; Type: ACL; Schema: protocolos_omi_covid_base; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_base.iddpacpa3 TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_base.iddpacpa3 TO ugina;


--
-- Name: TABLE examenes_pcr_omi; Type: ACL; Schema: protocolos_omi_covid_etl; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_etl.examenes_pcr_omi TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_etl.examenes_pcr_omi TO ugina;


--
-- Name: TABLE hisa_iddcoe; Type: ACL; Schema: protocolos_omi_covid_etl; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_etl.hisa_iddcoe TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_etl.hisa_iddcoe TO ugina;


--
-- Name: TABLE hisa_iddcop; Type: ACL; Schema: protocolos_omi_covid_etl; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_etl.hisa_iddcop TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_etl.hisa_iddcop TO ugina;


--
-- Name: TABLE hisa_iddcor; Type: ACL; Schema: protocolos_omi_covid_etl; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_etl.hisa_iddcor TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_etl.hisa_iddcor TO ugina;


--
-- Name: TABLE hisa_iddncu; Type: ACL; Schema: protocolos_omi_covid_etl; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_etl.hisa_iddncu TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_etl.hisa_iddncu TO ugina;


--
-- Name: TABLE hisa_iddome; Type: ACL; Schema: protocolos_omi_covid_etl; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_etl.hisa_iddome TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_etl.hisa_iddome TO ugina;


--
-- Name: TABLE iddcoe; Type: ACL; Schema: protocolos_omi_covid_etl; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_etl.iddcoe TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_etl.iddcoe TO ugina;


--
-- Name: TABLE iddcop; Type: ACL; Schema: protocolos_omi_covid_etl; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_etl.iddcop TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_etl.iddcop TO ugina;


--
-- Name: TABLE iddcor; Type: ACL; Schema: protocolos_omi_covid_etl; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_etl.iddcor TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_etl.iddcor TO ugina;


--
-- Name: TABLE iddncu; Type: ACL; Schema: protocolos_omi_covid_etl; Owner: postgres
--

GRANT SELECT ON TABLE protocolos_omi_covid_etl.iddncu TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid_etl.iddncu TO ugina;


--
-- PostgreSQL database dump complete
--

