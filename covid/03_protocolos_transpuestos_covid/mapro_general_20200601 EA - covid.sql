DROP SCHEMA IF EXISTS protocolos_omi_covid CASCADE;

CREATE SCHEMA protocolos_omi_covid
  AUTHORIZATION postgres;

GRANT ALL ON SCHEMA protocolos_omi_covid TO postgres;
GRANT USAGE ON SCHEMA protocolos_omi_covid TO jesoto;
GRANT USAGE ON SCHEMA protocolos_omi_covid TO ehenriquez;
GRANT USAGE ON SCHEMA protocolos_omi_covid TO ugina;
GRANT USAGE ON SCHEMA protocolos_omi_covid TO consultaomi;
GRANT USAGE ON SCHEMA protocolos_omi_covid TO consulta; 


----------------------------------------------------------------Crear funcion permisos para las tablas-----------------------------------------------------------------

CREATE OR replace FUNCTION protocolos_omi_covid.mapro_sp_permisos (n varchar) 
 RETURNS integer AS $permisos$
declare
	micodi text;
begin 
	if (n='') then
	return 0;
	end if;
	micodi:='
		GRANT SELECT ON TABLE '||n||' TO jesoto;
		GRANT SELECT ON TABLE '||n||' TO ugina;
		GRANT SELECT ON TABLE '||n||' TO ehenriquez;
		GRANT SELECT ON TABLE '||n||' TO consulta;
		GRANT SELECT ON TABLE '||n||' TO consultaomi;
';

if (n<>'') then
	execute micodi;
	end if;
	return 1;
end
$permisos$ LANGUAGE plpgsql;

-------------------------------------------------------------Crea Listado Nombre de Tablas------------------------------------------------------------------------

DROP TABLE if exists protocolos_omi_covid.mapro_nomtbl;
create table protocolos_omi_covid.mapro_nomtbl as
select row_number() over (order by wproto) as id, 'wp_'::char(3)||substring('00000'||wproto from '......$')::char(6) as nomtbl, wproto, wprotocolo, regexp_replace(
regexp_replace(
	regexp_replace(
		regexp_replace(
			regexp_replace(
				translate(lower(wprotocolo),'áéíóúñ', 'aeioun')
				,' ','_','g')
			,'\(','_','g')
		,']','_','g')
	,'[^a-z0-9+_]','','g')
,'__','_','g') as protocolo from admomi.iddco5 where wproto<380;

CREATE INDEX mapro_nomtbl_wproto_idx 	 ON protocolos_omi_covid.mapro_nomtbl (wproto) tablespace tb_index;
CREATE INDEX mapro_nomtbl_wprotocolo_idx ON protocolos_omi_covid.mapro_nomtbl (wprotocolo) tablespace tb_index;
CREATE INDEX mapro_nomtbl_nomtbl_idx 	 ON protocolos_omi_covid.mapro_nomtbl (nomtbl) tablespace tb_index;
CREATE INDEX mapro_nomtbl_i_01 			 ON protocolos_omi_covid.mapro_nomtbl (wproto,wprotocolo,nomtbl) tablespace tb_index;

select * from protocolos_omi_covid.mapro_sp_permisos('protocolos_omi_covid.mapro_nomtbl');

-------------------------------------------------------------Crea Listado Nombre de Variables------------------------------------------------------------------------

DROP TABLE if exists protocolos_omi_covid.mapro_nomvar;
create table protocolos_omi_covid.mapro_nomvar as
select wproto, row_number() over (order by wproto, wcarpeta, wcolumna, wlinea) as id_num
,'var_'::char(4)||wnumero::char(4) as nomvar
, wnumero, wcarpeta, wcolumna, wlinea, wcomentari, wtipo, wtabla, wcodigodgp, upper(wcomentari) as vareva from admomi.iddcol order by 1 limit 0;

CREATE INDEX mapro_nomvar_wproto_idx  ON protocolos_omi_covid.mapro_nomvar (wproto) tablespace tb_index;
CREATE INDEX mapro_nomvar_id_num_idx  ON protocolos_omi_covid.mapro_nomvar (id_num) tablespace tb_index;
CREATE INDEX mapro_nomvar_nomvar_idx  ON protocolos_omi_covid.mapro_nomvar (nomvar) tablespace tb_index;
CREATE INDEX mapro_nomvar_wnumero_idx ON protocolos_omi_covid.mapro_nomvar (wnumero) tablespace tb_index;
CREATE INDEX mapro_nomvar_i_05 		  ON protocolos_omi_covid.mapro_nomvar (wproto,id_num,nomvar,wnumero) tablespace tb_index;

select * from protocolos_omi_covid.mapro_sp_permisos('protocolos_omi_covid.mapro_nomvar');

-------------------------------------------------------------Inserta protocolos nuevos------------------------------------------------------------------------

CREATE OR replace FUNCTION protocolos_omi_covid.mapro_sp_insertnomtbl (n INTEGER) 
 RETURNS table(id bigint, nomtbl text, wproto integer, wprotocolo varchar(40), protocolo text) AS $insertproto$
declare
	i int:=0;
begin 
	if (n=0) then
	return query
	select * from protocolos_omi_covid.mapro_nomtbl order by 3 desc limit 0;
	end if;
	loop
	exit when i= n;
	i:=i+1;
	INSERT INTO protocolos_omi_covid.mapro_nomtbl
	(id, nomtbl, wproto, wprotocolo, protocolo)
	select max(b.id)+1 as id, 'wp_'::char(3)||substring('00000'||a.wproto from '......$')::char(6) as nomtbl, min(a.wproto), a.wprotocolo, regexp_replace(
		regexp_replace(
			regexp_replace(
				regexp_replace(
					regexp_replace(
						translate(lower(a.wprotocolo),'áéíóúñ', 'aeioun')
						,' ','_','g')
					,'\(','_','g')
				,']','_','g')
			,'[^a-z0-9+_]','','g')
		,'__','_','g') as protocolo
	from admomi.iddco5 as a, protocolos_omi_covid.mapro_nomtbl as b where a.wproto not in (select c.wproto from protocolos_omi_covid.mapro_nomtbl as c) group by 2,4 order by 3 limit 1;
	end LOOP;
	return QUERY
	select * from protocolos_omi_covid.mapro_nomtbl order by 3 desc limit n;
end
$insertproto$ LANGUAGE plpgsql;

-------------------------------------------------------------Ejecucion Inserta protocolos nuevos------------------------------------------------------------------

select * 
  from protocolos_omi_covid.mapro_sp_insertnomtbl((select count(*) 
					       from admomi.iddco5 as a 
					      where a.wproto not in (select c.wproto 
					                               from protocolos_omi_covid.mapro_nomtbl as c))::integer);

--select * from protocolos_omi_covid.mapro_nomtbl order by 1 desc;

----------------------------------------------------------- Inserta nombres de variables----------------------------------------------------------------

CREATE OR REPLACE FUNCTION protocolos_omi_covid.mapro_sp_insertnomvar(n integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
declare
	i int:=0;
	_proto int;
begin 
	if (n=0) then
	return 0;
	end if;
	truncate table protocolos_omi_covid.mapro_nomvar;
	loop
	exit when i= n;
	i:=i+1;
	_proto:=(select wproto from protocolos_omi_covid.mapro_nomtbl where id=i)::int;
	INSERT INTO protocolos_omi_covid.mapro_nomvar
		(wproto, id_num, nomvar, wnumero, wcarpeta, wcolumna, wlinea, wcomentari, wtipo, wtabla, wcodigodgp, vareva)
		select a.wproto, row_number() over (partition by b.wproto order by a.wproto, a.wcarpeta, a.wcolumna, a.wlinea) as id_num
		,'var_'::char(4)||wnumero::char(4) as nomvar
		, a.wnumero, a.wcarpeta, a.wcolumna, a.wlinea, a.wcomentari, a.wtipo, a.wtabla, a.wcodigodgp, regexp_replace(
		regexp_replace(
			regexp_replace(
				regexp_replace(
					regexp_replace(
						regexp_replace(
							translate(lower(wcomentari),'áéíóúñ', 'aeioun')
							,' ','_','g')
						,'\/','_','g')
					,'\(','_','g')
				,']','_','g')
			,'[^a-z0-9+_]','','g')
		,'__','_','g') as vareva
		from admomi.iddcol as a left join protocolos_omi_covid.mapro_nomtbl as b on a.wproto=b.wproto where b.wproto=_proto and a.wtipo in ('T','C','S','N','X','V','F','P','Z','L','O', '+') order by 2;
	end LOOP;
	return (select count(*) from protocolos_omi_covid.mapro_nomvar)::integer;
end
$function$
;

--------------------------------------------------------Ejecucion Inserta nombres de variables-------------------------------------------------------------

select * from protocolos_omi_covid.mapro_sp_insertnomvar((select max(wproto) from protocolos_omi_covid.mapro_nomtbl)::integer);
--select * from protocolos_omi_covid.mapro_sp_insertnomvar(385::integer);


------------------------------------------------------Crea e Inserta nombres de Tablas y de variables----------------------------------------------------------

DROP TABLE if exists protocolos_omi_covid.dim_tablas;

CREATE TABLE protocolos_omi_covid.dim_tablas
(
  id serial NOT NULL,
  tabla character varying(8),
  tabla_desc character varying(30),
  contenido_cod character varying(8),
  contenido_desc character varying(50)
)
WITH (
  OIDS=FALSE
);


insert into protocolos_omi_covid.dim_tablas 
select 
	nextval('protocolos_omi_covid.dim_tablas_id_seq')
	,a.tabla
	,(select b.descripcio tabla_desc from admomi.iddta2 as b where a.tabla=b.codigo limit 1) 
	,a.codigo contenido_cod	
	,a.descripcio contenido_desc
  from admomi.iddtau as a
  where a.tabla not in (select c.tabla from protocolos_omi_covid.dim_tablas as c); --where a.tabla='TCSECREC'
  
  select * from protocolos_omi_covid.mapro_sp_permisos('protocolos_omi_covid.dim_tablas');
  
 --select * from protocolos_omi_covid.dim_tablas;

DROP TABLE if exists protocolos_omi_covid.dim_tblvar;
CREATE TABLE protocolos_omi_covid.dim_tblvar
(
  id int4 NOT null
  , wproto int4
  , wnumero int4
  , wcampo varchar
)
WITH (
  OIDS=FALSE
);
insert into protocolos_omi_covid.dim_tblvar
select a.id, b.wproto, b.wnumero, a.contenido_cod from protocolos_omi_covid.dim_tablas as a inner join protocolos_omi_covid.mapro_nomvar as b on a.tabla=b.wtabla order by 2,3,1;

CREATE INDEX dim_tblvar_wproto_idx ON protocolos_omi_covid.dim_tblvar (wproto) tablespace tb_index;
CREATE INDEX dim_tblvar_wnumero_idx ON protocolos_omi_covid.dim_tblvar (wnumero) tablespace tb_index;
CREATE INDEX dim_tblvar_wcampo_idx ON protocolos_omi_covid.dim_tblvar (wcampo) tablespace tb_index;
CREATE INDEX dim_tblvar_i_04 ON protocolos_omi_covid.dim_tblvar (wproto,wnumero,wcampo) tablespace tb_index;

select * from protocolos_omi_covid.mapro_sp_permisos('protocolos_omi_covid.dim_tblvar');

--select * from protocolos_omi_covid.dim_tblvar

-------------------------------------------------------------Genera Columnas--------------------------------------------------------------

CREATE OR replace FUNCTION protocolos_omi_covid.mapro_sp_insertcol (n1 INTEGER, n2 INTEGER) 
RETURNS integer AS $insertcol$
declare
	i int:=0;
	_wnumero int4:=0;
	_nomvar text;
	_nomtbl text;
	_tipdat text;
	micod text;
begin 
	if (n2=0) then
		return 0;
	end if;
	loop
		exit when i=n2;
		i:=i+1;
		_nomvar:=(select vareva||'_wn_'||substring('0000'||wnumero from '....$')::char(4)||'_t_'||lower(wtipo) from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i);
		_nomtbl:=(select 'pr_'||protocolo||'_'||nomtbl from protocolos_omi_covid.mapro_nomtbl where wproto=n1);
		_nomtbl:='protocolos_omi_covid.'||_nomtbl;
		--_wnumero:=(select wnumero from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i);
		if (select wtipo from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i)='F' then
		_tipdat:=' date';
		elsif (select wtipo from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i) in ('T','C', 'X') then
		_tipdat:=' text';
		else
		_tipdat:=' numeric';
		end if;
		micod:='ALTER TABLE '||_nomtbl||' ADD COLUMN "'||_nomvar||'" '||_tipdat; 
		execute micod;
	end LOOP;
	return 1;
end
$insertcol$ LANGUAGE plpgsql;

-------------------------------------------------------------Rellena Columnas--------------------------------------------------------------

CREATE OR replace FUNCTION protocolos_omi_covid.mapro_sp_updatecol (n1 INTEGER, n2 INTEGER) 
RETURNS integer AS $updatecol$
declare
	i int:=0;
	_wnumero int4:=0;
	_nomvar text;
	_nomtbl text;
	micod text;
begin 
	if (n2=0) then
		return 0;
	end if;
	loop
		exit when i=n2;
		i:=i+1;
		_nomvar:=(select vareva||'_wn_'||substring('0000'||wnumero from '....$')::char(4)||'_t_'||lower(wtipo) from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i);
		_nomtbl:=(select 'pr_'||protocolo||'_'||nomtbl from protocolos_omi_covid.mapro_nomtbl where wproto=n1);
		_nomtbl:='protocolos_omi_covid.'||_nomtbl;
		_wnumero:=(select wnumero from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i);
		IF (select wtipo from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i)='F' then
		micod:='UPDATE '||_nomtbl||' as a SET "'||_nomvar||'"= to_date(regexp_replace(b.campo,''/'',''-'',''g''), ''DD MM YYYY'')
			from admomi.iddcop as b 
			where a.nif=b.nif2 and a.especiali=b.especiali and a.fechr=b.fecha and a.medico_id=b.medico_id and b.wproto='||n1||'and b.wnumero='||_wnumero;
		ELSIF (select wtipo from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i)='T' then
		micod:='UPDATE '||_nomtbl||' as a SET "'||_nomvar||'"= b.campo::text from admomi.iddcoe as b 
			where a.nif=b.nif2 and a.especiali=b.especiali and a.fechr=b.fecha and a.medico_id=b.medico_id and b.wproto='||n1||'and b.wnumero='||_wnumero;
		ELSIF (select wtipo from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i)='C' then
		micod:='UPDATE '||_nomtbl||' as a SET "'||_nomvar||'"= b.campo::text from admomi.iddcot as b 
			where a.nif=b.nif2 and a.especiali=b.especiali and a.fechr=b.fecha and a.medico_id=b.medico_id and b.wproto='||n1||'and b.wnumero='||_wnumero;
		elsif (select wtipo from protocolos_omi_covid.mapro_nomvar where wproto=n1 and id_num=i)='X' then
		micod:='UPDATE '||_nomtbl||' as a SET "'||_nomvar||'"=(select x.contenido_desc from protocolos_omi_covid.dim_tablas as x where x.id=(select c.id from protocolos_omi_covid.dim_tblvar as c where c.wproto=b.wproto and c.wnumero=b.wnumero and c.wcampo=b.campo)::int4)::text
			from admomi.iddcop as b 
			where a.nif=b.nif2 and a.especiali=b.especiali and a.fechr=b.fecha and a.medico_id=b.medico_id and b.wproto='||n1||'and b.wnumero='||_wnumero; 
		ELSE
		micod:='UPDATE '||_nomtbl||' as a SET "'||_nomvar||'"=case'||$$
			when b.wtipo in ('O','Z','P','L','N','V') and char_length(regexp_replace(b.campo,'[^0-9]','','g'))=0 then 0
			when b.wtipo in ('O','Z','P','L','N','V') then to_number(b.campo, '999999G999999D99999')
			when b.wtipo='S' and b.campo='S' then 1
			when b.wtipo='S' and b.campo='N' then 0
			when b.wtipo='+' and b.campo='+' then 1
			when b.wtipo='+' and b.campo='-' then 0
			end from admomi.iddcop as b 
			where a.nif=b.nif2 and a.especiali=b.especiali and a.fechr=b.fecha and a.medico_id=b.medico_id and b.wproto=$$||n1||'and b.wnumero='||_wnumero; 
		end if;
		execute micod;
	end LOOP;
	return 1;
end
$updatecol$ LANGUAGE plpgsql;
-------------------------------------------------crear tablas con identificador de protocolo-----------------------------------------------------------

CREATE OR replace FUNCTION protocolos_omi_covid.mapro_sp_creatbl (n INTEGER) 
 RETURNS integer AS $creatablas$
declare
	_maxnum int4:=0;
	_nomtbl text;
	micodi text;
begin 
	if (n=0) then
	return 0;
	end if;
	_nomtbl:=(select 'pr_'||protocolo||'_'||nomtbl from protocolos_omi_covid.mapro_nomtbl where wproto=n);
	_nomtbl:='protocolos_omi_covid.'||_nomtbl;
	_maxnum:=(select max(id_num) from protocolos_omi_covid.mapro_nomvar where wproto=n);
	micodi:='DROP TABLE if exists '||_nomtbl||';
CREATE TABLE '||_nomtbl||' (
	nif int not null
	, nombre_paciente varchar(100)
	, sexo varchar(1)
	, nacimiento date
	, meses int
	, edad int
	, centro varchar(5)
	, fecha date not null
	, aym varchar(8)
	, estamento varchar(50)
	, cod_estamento varchar(50)
	, med_codigo varchar(50)
	, med_nombre varchar(50)
	, ciap_codigo varchar(100)
	, descrip_ciap varchar(100)
	, especiali varchar not NULL
	, fechr timestamp
	, medico_id int4
)
WITH (
	OIDS=FALSE
) ;

INSERT INTO '||_nomtbl||'
(nif, especiali, fecha, fechr, medico_id)
select distinct a.nif2, a.especiali, a.fecha2, a.fecha, a.medico_id from admomi.iddcop as a where a.wproto='||n||' and a.nif2 is not null and a.fecha is not null and a.especiali is not null;
select distinct a.nif as nif2, a.especiali, a.fecha2, a.fecha, b.id as medico_id 
  from admomi.iddcor as a,admomi.iddmed as b
 where  a.wproto='||n||'
   and a.numero=1 and a.wproto>=1 and a.medico=b.medico
   and a.nif is not null 
   and a.fecha is not null 
   and a.especiali is not null;

select * from protocolos_omi_covid.mapro_sp_permisos('''||_nomtbl||''');

UPDATE '||_nomtbl||$$ as a
SET (nombre_paciente, sexo, nacimiento, centro)=((b.nombre||' '||b.apellido1||' '||b.apellido2), b.sexo, b.nacimiento, b.centro)
from admomi.iddpacpa3 as b where a.nif=b.nif2
;
UPDATE $$||_nomtbl||$$ as a
SET (edad, aym, meses)=(
							date_part('year', age(fecha, nacimiento))
							, to_char(fecha, 'yyyy-mm')
							, (date_part('year', age(fecha, nacimiento))*12)+date_part('month', age(fecha, nacimiento))
						)
;
UPDATE $$||_nomtbl||' SET (estamento, cod_estamento, med_codigo, med_nombre)=(b.med_estamento_desc, b.med_estamento_cod, b.med_codigo, b.med_nombre) from admomi.iddmed as b where medico_id=b.id;
UPDATE '||_nomtbl||' as a SET descrip_ciap=b.descripcio,ciap_codigo=b.ciap from admomi.iddncu as b where a.nif=b.nif2 and a.especiali=b.especiali
;

select * from protocolos_omi_covid.mapro_sp_insertcol('||n||','||_maxnum||');

select * from protocolos_omi_covid.mapro_sp_updatecol('||n||','||_maxnum||')';

if (_nomtbl<>'' and _maxnum>=1) then
	execute micodi;
	end if;
	return 1;
end
$creatablas$ LANGUAGE plpgsql;

-------------------------------------------------crear tablas con identificador de protocolo V2-----------------------------------------------------------

CREATE OR replace FUNCTION protocolos_omi_covid.mapro_sp_creatbl (n1 INTEGER, n2 INTEGER) 
 RETURNS integer AS $creatablas$
declare
	i integer:=n1-1;
	_wproto int4:=0;
	_maxnum int4:=0;
	_nomtbl text;
	micodi text;
begin 
	if (n2=0) then
	return 0;
	end if;
	loop
	exit when i=n2;
	i:=i+1;
	_nomtbl:=(select 'pr_'||protocolo||'_'||nomtbl from protocolos_omi_covid.mapro_nomtbl where wproto=i);
	_nomtbl:='protocolos_omi_covid.'||_nomtbl;
	_wproto:=i; --(select wproto from protocolos_omi_covid.mapro_nomtbl where id=i);
	_maxnum:=(select max(id_num) from protocolos_omi_covid.mapro_nomvar where wproto=_wproto);
	micodi:='DROP TABLE if exists '||_nomtbl||';
CREATE TABLE '||_nomtbl||' (
	nif int not null
	, nombre_paciente varchar(100)
	, sexo varchar(1)
	, nacimiento date
	, meses int
	, edad int
	, centro varchar(5)
	, fecha date not null
	, aym varchar(8)
	, estamento varchar(50)
	, cod_estamento varchar(50)
	, med_codigo varchar(50)
	, med_nombre varchar(50)
	, ciap_codigo varchar(100)
	, descrip_ciap varchar(100)
	, especiali varchar not NULL
	, fechr timestamp
	--, fechr varchar(20) 
	, medico_id int4
)
WITH (
	OIDS=FALSE
) ;

--select * from admomi.iddncu
select * from protocolos_omi_covid.mapro_sp_permisos('''||_nomtbl||''');

INSERT INTO '||_nomtbl||'
(nif, especiali, fecha, fechr, medico_id)
select distinct a.nif2, a.especiali, a.fecha2, a.fecha, a.medico_id
from admomi.iddcop as a where a.wproto='||_wproto||' and a.nif2 is not null and a.fecha is not null and a.especiali is not null;

UPDATE '||_nomtbl||' as a
SET (nombre_paciente, sexo, nacimiento, centro)=(b.nombre||'' ''||b.apellido1||'' ''||b.apellido2, b.sexo, b.nacimiento, b.centro)
from admomi.iddpacpa3 as b where a.nif=b.nif2
;
UPDATE '||_nomtbl||' as a
SET (edad, aym, meses)=(date_part(''year'', age(fecha, nacimiento)), to_char(fecha, ''yyyy-mm''), (date_part(''year'', age(fecha, nacimiento))*12)+date_part(''month'', age(fecha, nacimiento)))
;
UPDATE '||_nomtbl||' SET (estamento, cod_estamento, med_codigo, med_nombre)=(b.med_estamento_desc, b.med_estamento_cod, b.med_codigo, b.med_nombre) from admomi.iddmed as b where medico_id=b.id;
UPDATE '||_nomtbl||' as a SET descrip_ciap=b.descripcio,ciap_codigo=b.ciap from admomi.iddncu as b where a.nif=b.nif2 a.especiali=b.especiali
;
select * from protocolos_omi_covid.mapro_sp_insertcol('||_wproto||','||_maxnum||');

select * from protocolos_omi_covid.mapro_sp_updatecol('||_wproto||','||_maxnum||')';
if (_nomtbl<>'' and _maxnum>=1) then
	execute micodi;
	end if;
	end LOOP;
	return 1;
end
$creatablas$ LANGUAGE plpgsql;


-------------------------------------------------Ejecuta crear tablas con identificador de protocolo V1 y V2-----------------------------------------------------------

--select * from protocolos_omi_covid.mapro_sp_creatbl(1,100); -- crea tablas de los protocolos entre los numeros ingresados.
--select * from protocolos_omi_covid.mapro_sp_creatbl(398); -- crea tablas del protocolo ingresado.

drop table if exists protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409;
drop table if exists protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412;
select * from protocolos_omi_covid.mapro_sp_creatbl(412);
select * from protocolos_omi_covid.mapro_sp_creatbl(409);


--select * from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 where nombre_PACIENTE like '%ARSENIO%';


/*
select row_number() over (partition by nif) 
		,case when med_nombre like '%LEON%' then 1 end as PATTY
		,*
 from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412;
/*

/* 
 * 
 * *************************************************Informacion Tablas y columnas*************************************************

select * from information_schema.columns where lower(table_name) like 'mcp_%'
and lower(column_name) like '%fisica%'
order by 4

 * *******************************************************************************************************************************
 * 
 * **********************************************Cantidad de Registros por Protocolo**********************************************
 * 
select wproto, count(*) from admomi.iddcop
where nif2 is not null and wproto>250 and fecha is not null and medico_id is not null and campo is not null group by 1 order by 2 desc;
 * 
 * *******************************************************************************************************************************
 * 
 * 
select * from admomi.iddcoe where nif is not null and campo is not null limit 100;
 * 
select * from admomi.iddcop where nif='252' and wnumero='36' and campo is not null limit 100;
 * 
 * 
 */.