drop table if exists covid19.rangos_edad_01;create table covid19.rangos_edad_01 as
select '20190101 00:00:00'::timestamp as  fechai,(('now'::date-'1 day'::interval)::date::Text||' 23:59:59')::timestamp as fechaf;

	drop table if exists covid19.consultas_de_morbilidad; --select count(*) from cons_morbilidad 8780
	create table    covid19.consultas_de_morbilidad as
	select nif2 as nif,medico,fecha2,extract(hour from fecha) as hora
			,case when 	(
				 		(wproto=397 and wnumero=70  and campo in ('01','02','03','04','05','06') )
				 		or
				 		(wproto=298 and wnumero=242 and campo in ('01','02','03','04','05','06') )
				 		or
				 		(wproto=412 and wnumero=70  and campo in ('01','02','03','04','05','06') )
				 		or
				 		(wproto=373 and wnumero=226  and campo is not null )
				 		)
				 		then 1::smallint end respiratorias  
	  from admomi.iddcop as a
	 where a.wproto in (397,298,412,373)
	   and a.fecha between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)   
	   group by 1,2,3,4,5;
	  
	  

	--consultas episodios R%
	drop table if exists covid19.episodios;
	create table 		 covid19.episodios as
	  select a.nif,a.medico
	  		,a.fecha2,extract(hour from a.fecha) as hora
	  		,case when b.ciap like 'R%'  then 1::smallint 		end respiratorias
	  		,case when b.ciap in ('G10') then 1::smallint 		end covid_sospecha
	  		,case when b.ciap in ('G12','G13') then 1::smallint end covid_confirmado
	  		,case when b.ciap in ('G13') then 1::smallint 		end covid_caso_probable
	  from admomi.iddcor as a,admomi.iddncu as b
	 where a.numero=1 
	   and a.fecha2 between (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)   
	   and a.nif=b.nif2
	   and a.especiali=b.especiali
	   group by 1,2,3,4,5,6,7,8; --635236

	  /*
	   select nif,medico,fecha2,hora from covid19.consultas_de_morbilidad where respiratorias=1 union
	   select nif,medico,fecha2,hora from covid19.episodios where respiratorias=1 or covid_sospecha=1 or covid_confirmado=1
	    */
	  
	drop table if exists covid19.episodios_cierre_covid;
	create table 		 covid19.episodios_cierre_covid as
	select	 a.nif
			,a.medico
			,B.fechalta::date as fecha2
			,extract(hour from b.fechalta) as hora
	  		,case when b.ciap in ('G10') then 1::smallint end covid_sospecha_cerrada
	  		,case when b.ciap in ('G12','G13') then 1::smallint end covid_confirmado_cerrada
	  		,case when b.ciap in ('G13') then 1::smallint end covid_confirmado_probalble_cerrada
	  from admomi.iddcor as a,admomi.iddncu as b
	 where a.numero=1 
	   and b.fechalta::date between (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)   
	   and a.nif=b.nif2
	   and a.especiali=b.especiali
	   and b.fechalta is not null
	   and b.ciap in ('G10','G12','G13')
	   group by 1,2,3,4,5,6,7;
	  
	  
drop table if exists covid19.todos_los_protocolos;  --select * from covid19.todos_los_protocolos
create table 		 covid19.todos_los_protocolos as
select   distinct nif as nif2
		,stk_fecha::date as fecha2
		,extract(hour from stk_fecha) as hora2
		,medico
		,wproto as wproto2
  from admomi.hisa_iddcor 
 where stk_fecha between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)
   and numero=1
   and wproto>=1
   and stk_tipo_operacion='A'
   and nif is not null;
create index todos_los_protocolos_i_01 on covid19.todos_los_protocolos(medico,wproto2) tablespace tb_index;

drop table if exists 	covid19.resultado_examen;
create table 			covid19.resultado_examen as
select a.nif2 --,a.fecha_solicitud
		,a02.stk_fecha as fecha--a01.fecha as fecha
		,a02.stk_fecha::date as fecha2 --a01.fechap::Date as fecha2
		,extract(hour from a02.stk_fecha) as hora --,extract(hour from fechar) as hora
		,a.descripcion_exa
		,a.solicita as medico
		,case 
			when btrim(upper(a.resultado)) is null or btrim(upper(a.resultado))='' then 'PENDIENTE' 
			else btrim(upper(a.resultado)) 
			 end as resultado
  from report.baciloscopias a --select * from report.baciloscopias a --select * from admomi.hisa_iddome
  left join 
  			(
  			select nif,numorden,fecha,fechar,fechap,row_number() over (partition by nif,fechap::date order by fechap) as rownum
  			  from admomi.iddome 
  			  where nif<>'' and nif is not null
  			) as a01
  			  on a01.nif		= a.nif
			 and a01.numorden	= a.numorden 
			 and a01.rownum		= 1
  left join	(
  			select nif::integer as nif2,numorden,stk_fecha
  					,row_number() over (partition by nif,numorden order by stk_fecha) as rownum
  			  from admomi.hisa_iddome 
  			 where stk_tipo_operacion='A' 
  			   and stk_fecha>='20200101 00:00:00'
  			) as a02
  			  on a02.numorden=a01.numorden
  			 and a02.nif2=a.nif2 
  			 and a02.rownum=1
 where a.descripcion_exa like 'Coronavirus%'
;
create index resultado_examen_i_01 on covid19.resultado_examen(nif2,fecha2,hora) tablespace tb_index;

--select * from report.baciloscopias b limit 10
--select fecha,fechap,* from admomi.iddome order by fechap desc
;

  --hechos central
	drop table if exists 	covid19.hechos; --select * from hechos
	create table	    	covid19.hechos as  
	select nif,fecha2,hora,medico from covid19.consultas_de_morbilidad 
	union
	select nif,fecha2,hora,medico from covid19.episodios
	union
	select nif,fecha2,hora,medico from covid19.episodios_cierre_covid
	union
	select pac_nif::integer,vac_fecharegistro::Date,extract(hour from vac_fecharegistro) as hora, upper(empleado_codigo)  	from vacunas.cubo_vacunas where vac_fecharegistro between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01) and (vac_codigo like 'AIH%' or vac_codigo like 'AIF%')--select * from vacunas.cubo_vacunas order by vac_fecharegistro
	union
	select nif::integer as nif,fecha::date fecha,extract(hour from fecha) as hora,medico									from admomi.iddage 			where FECHA between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01) AND ASIGNADA='S' and vino<>'N' 
	union 
	select nif2,fecha_servida::date,extract(hour from fecha_servida) as hora,upper(med_codigo)  							from farmacos.cubo_prescripciones_servidas 	where fecha_servida between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)
	union --1178659
	select nif2,fecha2,hora2,medico from covid19.todos_los_protocolos
	union
	select nif2,fecha2,hora,medico from covid19.resultado_examen;
--select * from report.baciloscopias b limit 10
--select fecha,fechap,* from admomi.iddome order by fechap desc
	;
	  
	drop table if exists 	covid19.hechos2;
	create table    		covid19.hechos2 as  
	select 
			a.*
			,a01.respiratorias as morbilidad_respiratoria
			,a02.respiratorias  as episodios_respiratoria
			,a03.rownum as vacuna_influenza
			,a04.rownum as consultas_controles_procedimientos
			,a05.rownum as despacho_medicamento
			,a06.rownum as covid_seguimiento
			
	  from covid19.hechos a
	  left join 
	  			(
	  			/*
	  			select nif,fecha2,hora,respiratorias,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.consultas_de_morbilidad
	  			 where respiratorias=1
	  			 */
	  			 /*asumiento que todo registro en R y episodios covid son respiratorias*/
				select aa.nif,aa.fecha2,aa.hora
						,1::smallint as respiratorias,row_number() over (partition by aa.nif,aa.fecha2,aa.hora) rownum
				  from 
						(
			  			select nif,fecha2,hora 
			  			  from covid19.consultas_de_morbilidad
			  			 where respiratorias=1
			  			 union
			  			select nif,fecha2,hora from covid19.episodios where respiratorias=1 or covid_sospecha=1 or covid_confirmado=1
						) as aa	  			 
	  			) as a01 
	  			  on a01.nif	= a.nif 
	  			 and a01.fecha2	= a.fecha2
	  			 and a01.hora	= a.hora
	  			 and a01.rownum	= 1
	  left join 
	  			(
	  			select nif,fecha2,hora,respiratorias,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.episodios
	  			) as a02 
	  			  on a02.nif	= a.nif 
	  			 and a02.fecha2	= a.fecha2
	  			 and a02.hora	= a.hora
	  			 and a02.rownum	= 1
	  left join 
	  			(
	  			select pac_nif::integer as nif
	  					,vac_fecha::Date as fecha2
	  					,extract(hour from  vac_fecharegistro) hora
	  					,upper(empleado_codigo) as clinico
	  					,row_number() over (partition by pac_nif,vac_fecharegistro::Date,extract(hour from  vac_fecharegistro)) rownum
				  from vacunas.cubo_vacunas
				 where vac_fecharegistro between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)
				   and (vac_codigo like 'AIH%' or vac_codigo like 'AIF%' or vac_nombre like 'A-INFLUENZA%' or vac_nombre like 'ANTIINFLUENZA%')
	  			) as a03
	  			  on a03.nif	= a.nif
	  			 and a03.fecha2	= a.fecha2
	  			 and a03.hora	= a.hora
	  			 and a03.rownum	= 1
	  left join
	  			(
	  			select nif::integer as nif
	  					,fecha::date fecha2
	  					,extract(hour from fecha) as hora
	  					,medico
	  					,row_number() over (partition by nif,fecha::Date,extract(hour from fecha)) rownum
				  from admomi.iddage
				 where FECHA between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)
				   AND ASIGNADA='S' 
				   and vino<>'N'
	  			) as a04
	  			  on a04.nif	= a.nif
	  			 and a04.fecha2	= a.fecha2
	  			 and a04.hora	= a.hora
	  			 and a04.rownum	= 1
	left join
				(
				select nif2 as nif
						,fecha_servida::date as fecha2
						,extract(hour from fecha_servida) as hora
						,upper(med_codigo)
						,row_number() over (partition by nif2,fecha_servida::Date,extract(hour from fecha_servida)) rownum
				  from farmacos.cubo_prescripciones_servidas
				 where fecha_servida between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)
				) as a05
				  on a05.nif	= a.nif
				 and a05.fecha2	= a.fecha2
				 and a05.hora	= a.hora 
				 and a05.rownum	= 1
	left join	(
				select distinct nif2,fecha2,hora2,medico
						,row_number() over (partition by nif2,fecha2,hora2 order by fecha2,hora2) rownum
	   			  from covid19.todos_los_protocolos 
	   			 where wproto2 = 409
				) as a06
				  on a06.nif2	= a.nif
				 and a06.fecha2	= a.fecha2
				 and a06.hora2	= a.hora 
				 and a06.rownum	= 1
		
	  			 ; --1316583
	  			 
drop table if exists covid19.hechos4; --select * from covid19.hechos4
create table covid19.hechos4 as   			
select 		distinct
			 a.nif as nif2--,md5(a.nif::text||'-'||a.nif::text) as nif  --a.nif::text as nif
			--,md5(a.nif::text||'-'||a.nif::text) as nif2 --a.nif as nif2			--md5(a.nif::text||'-'||a.nif::text) as nif			,a.fecha2
			,a.fecha2::Date fecha2
			--,a.hora::text as hora
			,a.hora as hora2
			,a01.id as medico --md5(a01.id::text||'-'||a01.id::text) as medico
			--,case when a.morbilidad_respiratoria=1 then 'SI'::text else 'NO' 									end as morbilidad_respiratoria
			,case when a.morbilidad_respiratoria=1 then 1::smallint else 0::smallint 							end as morbilidad_respiratoria2
			--,case when a.episodios_respiratoria=1 then 'SI'::text else 'NO' 									end as episodios_respiratoria
			,case when a.episodios_respiratoria=1 then 1::smallint else 0::smallint 							end as episodios_respiratoria2
			--,case when a.consultas_controles_procedimientos=1 then 'SI'::text else 'NO' 						end as consultas_controles_procedimientos
			,case when a.consultas_controles_procedimientos=1 then 1::smallint else 0::smallint 				end as consultas_controles_procedimientos2
			--,case when despacho_medicamento=1 then 'SI'::text else 'NO' 										end as despacho_medicamento
			,case when despacho_medicamento=1 then 1::smallint else 0::smallint 								end as despacho_medicamento2
			--,case when vacuna_influenza=1 then 'SI'::text else 'NO' 											end as vacuna_influenza
			,case when vacuna_influenza=1 then 1::smallint else 0::smallint 									end as vacuna_influenza2
			--,case when morbilidad_respiratoria=1 or episodios_respiratoria=1 then 'SI' else 'NO'::text 			end as consultas_respiratorias_totales
			,case when morbilidad_respiratoria=1 or episodios_respiratoria=1 then 1::smallint else 0::smallint 	end as consultas_respiratorias_totales2	
			--,case when a04.rownum=1 then 'SI' else 'NO'::text 													end as covid_sospecha
			,case when a04.rownum=1 then 1::smallint else 0::smallint 											end as covid_sospecha2
			--,case when a05.rownum=1 then 'SI' else 'NO'::text 													end as covid_confirmado
			,case when a05.rownum=1 then 1::smallint else 0::smallint 											end as covid_confirmado2
			,case when a08.rownum=1 then 1::smallint else 0::smallint 											end as covid_caso_probable2
			--,case when a01.med_estamento_desc in ('MEDICO','PSIQUIATRIA','GASTROENTEROLOGIA','GINECOLOGIA') then 'SI'::text else 'NO'::text end as es_medico
			,case when a01.med_estamento_desc in ('MEDICO','PSIQUIATRIA','GASTROENTEROLOGIA','GINECOLOGIA') then 1::smallint else 0::smallint end as es_medico2
			--,case when a06.inscrito_validado2=1 then 'SI' else 'NO'::text 													end as inscrito_validado
			,case when a06.inscrito_validado2=1 then 1::smallint else 0::smallint 								end as inscrito_validado2
			,case when a.covid_seguimiento=1 then 1::smallint else 0::smallint 									end as covid_seguimiento2
/*
			,a02.centro as pac_centro
			,case 
				when a02.centro = 'MTC'  then 1
				when a02.centro = 'JPII' then 2
				when a02.centro = 'SAH'  then 3
				end as pac_centro2
			,case
				when a01.centro = 'MTC'  then 1
				when a01.centro = 'JPII' then 2
				when a01.centro = 'SAH'  then 3
				end as med_centro2
			,a01.centro as med_centro
			,CASE 
				WHEN a03.latitude IS NOT NULL THEN a03.latitude
				WHEN a03.latitude IS NULL AND a02.centro='JPII' THEN -33.59495818 --29708 --select distinct nif,latitude,longitude from covid19.hechos3 where nif in (29708,5543,46775)
				WHEN a03.latitude IS NULL AND a02.centro='MTC'  THEN -33.61403557 --5543
				WHEN a03.latitude IS NULL AND a02.centro='SAH'  THEN -33.59637181 --46775
				END AS   latitude
			,CASE
				WHEN a03.longitude IS NOT NULL THEN a03.longitude
				WHEN a03.longitude IS NULL AND a02.centro='JPII' THEN -70.62199003
				WHEN a03.longitude IS NULL AND a02.centro='MTC'  THEN -70.55252455
				WHEN a03.longitude IS NULL AND a02.centro='SAH'  THEN -70.5563369
				END AS   longitude
				*/
			--,1::smallint as q
			--,a07.edad_en_agnios 
	  from covid19.hechos2 as a --select * from covid19.hechos2 as a
	 left join 
				(
				select distinct centro,medico,id,med_estamento_desc
			      from admomi.iddmed 
			    ) as a01 on a01.medico=a.medico
	 left join admomi.iddpacpa3 as a02 on a02.nif2=a.nif
	 left join geo.ancora_geo as a03 on a03.nifs=a.nif
	 left join 
	 			(
	  			select nif
	  					,fecha2
	  					,hora
	  					,covid_sospecha
	  					,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.episodios
	  			 where covid_sospecha=1
	  			) as a04 
	  			  on a04.nif	= a.nif 
	  			 and a04.fecha2	= a.fecha2
	  			 and a04.hora	= a.hora
	  			 and a04.rownum	= 1
	left join 
	 			(
	  			select nif
	  					,fecha2
	  					,hora
	  					,covid_sospecha
	  					,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.episodios --select * from covid19.episodios
	  			 where covid_confirmado=1
	  			) as a05 
	  			  on a05.nif	= a.nif 
	  			 and a05.fecha2	= a.fecha2
	  			 and a05.hora	= a.hora
	  			 and a05.rownum	= 1
	left join 
	  			(
	  			select distinct nif,1::smallint inscrito_validado2 
	  			  from fonasa.inscritos_validados_2020_corte_20190831 
	  			 where nif is not null and autorizado='X'
	  			) as a06 
	  			  on a06.nif=a.nif
	left join  
  	  			(
  	  			select nif2,edad_en_agnios(now(),nacimiento) as edad_en_agnios
  	  			  from admomi.iddpacpa3 
  	  			) as a07 on a07.nif2=a.nif	  			 
	left join 
	 			(
	  			select nif
	  					,fecha2
	  					,hora
	  					,covid_sospecha
	  					,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.episodios --select * from covid19.episodios
	  			 where covid_caso_probable=1
	  			) as a08 
	  			  on a08.nif	= a.nif 
	  			 and a08.fecha2	= a.fecha2
	  			 and a08.hora	= a.hora
	  			 and a08.rownum	= 1
  	  			;

drop table if exists covid19.hechos4; 
create table covid19.hechos4 as   			
select 		distinct
			 md5(a.nif::text||'-'||a.fecha2::date::text) as id
			,md5(a.nif::text||'-'||a.nif::text) as nif
			,md5(a.nif::text||'-'||a.nif::text) as nif2--a.nif::text as nif
			--,md5(a.nif::text||'-'||a.nif::text) as nif2 --a.nif as nif2			--md5(a.nif::text||'-'||a.nif::text) as nif			,a.fecha2
			,a.fecha2::Date fecha2
			,a.hora::TEXT as hora
			,a.hora as hora2
			,md5(a01.id::text||'-'||a01.id::text) as medico
			,case when a.morbilidad_respiratoria=1 then 'SI'::text else 'NO' 									end as morbilidad_respiratoria
			,case when a.morbilidad_respiratoria=1 then 1::smallint else 0::smallint 							end as morbilidad_respiratoria2
			,case when a.episodios_respiratoria=1 then 'SI'::text else 'NO' 									end as episodios_respiratoria
			,case when a.episodios_respiratoria=1 then 1::smallint else 0::smallint 							end as episodios_respiratoria2
			,case when a.consultas_controles_procedimientos=1 then 'SI'::text else 'NO' 						end as consultas_controles_procedimientos
			,case when a.consultas_controles_procedimientos=1 then 1::smallint else 0::smallint 				end as consultas_controles_procedimientos2
			,case when despacho_medicamento=1 then 'SI'::text else 'NO' 										end as despacho_medicamento
			,case when despacho_medicamento=1 then 1::smallint else 0::smallint 								end as despacho_medicamento2
			,case when vacuna_influenza=1 then 'SI'::text else 'NO' 											end as vacuna_influenza
			,case when vacuna_influenza=1 then 1::smallint else 0::smallint 									end as vacuna_influenza2
			,case when morbilidad_respiratoria=1 or episodios_respiratoria=1 then 'SI' else 'NO'::text 			end as consultas_respiratorias_totales
			,case when morbilidad_respiratoria=1 or episodios_respiratoria=1 then 1::smallint else 0::smallint 	end as consultas_respiratorias_totales2	
			,case when a04.rownum=1 then 'SI' else 'NO'::text 													end as covid_sospecha
			,case when a04.rownum=1 then 1::smallint else 0::smallint 											end as covid_sospecha2
			,case when a05.rownum=1 then 'SI' else 'NO'::text 													end as covid_confirmado
			,case when a05.rownum=1 then 1::smallint else 0::smallint 											end as covid_confirmado2
			,case when a09.rownum=1 then 'SI' else 'NO'::text 													end as covid_caso_probable
			,case when a09.rownum=1 then 1::smallint else 0::smallint 											end as covid_caso_probable2
			,a01.med_estamento_desc																					as estamento
			,case when a01.med_estamento_desc in ('MEDICO','PSIQUIATRIA','GASTROENTEROLOGIA','GINECOLOGIA') then 'SI'::text else 'NO'::text end as es_medico
			,case when a01.med_estamento_desc in ('MEDICO','PSIQUIATRIA','GASTROENTEROLOGIA','GINECOLOGIA') then 1::smallint else 0::smallint end as es_medico2
			,case when a06.inscrito_validado2=1 then 'SI' else 'NO'::text 													end as inscrito_validado
			,case when a06.inscrito_validado2=1 then 1::smallint else 0::smallint 											end as inscrito_validado2
			,a08.descripcion_exa
			,a08.resultado
			,a02.centro as pac_centro
			,case when a.covid_seguimiento=1 then 'SI'::text else 'NO' 														end as covid_seguimiento
			,case when a.covid_seguimiento=1 then 1::smallint else 0::smallint 												end as covid_seguimiento2

			,case 
				when a02.centro = 'MTC'  then 1
				when a02.centro = 'JPII' then 2
				when a02.centro = 'SAH'  then 3
				end as pac_centro2
			,case
				when a01.centro = 'MTC'  then 1
				when a01.centro = 'JPII' then 2
				when a01.centro = 'SAH'  then 3
				end as med_centro2
			,a01.centro as med_centro
			,CASE 
				WHEN a03.latitude IS NOT NULL THEN a03.latitude
				WHEN a03.latitude IS NULL AND a02.centro='JPII' THEN -33.59495818 --29708 --select distinct nif,latitude,longitude from covid19.hechos3 where nif in (29708,5543,46775)
				WHEN a03.latitude IS NULL AND a02.centro='MTC'  THEN -33.61403557 --5543
				WHEN a03.latitude IS NULL AND a02.centro='SAH'  THEN -33.59637181 --46775
				END AS   latitude
			,CASE
				WHEN a03.longitude IS NOT NULL THEN a03.longitude
				WHEN a03.longitude IS NULL AND a02.centro='JPII' THEN -70.62199003
				WHEN a03.longitude IS NULL AND a02.centro='MTC'  THEN -70.55252455
				WHEN a03.longitude IS NULL AND a02.centro='SAH'  THEN -70.5563369
				END AS   longitude
			,1::smallint as q
			,a07.edad_en_agnios
	  from covid19.hechos2 as a
	 left join 
				(
				select distinct centro,medico,id,med_estamento_desc,row_number() over (partition by id order by id,profesional) rownum
			      from admomi.iddmed order by 3
			    ) as a01 
			      on a01.medico=a.medico
			     and a01.rownum=1
	 left join 	(
	 			select nif2,centro,row_number() over (partition by nif2 order by fechalta) rownum
	 			from admomi.iddpacpa3 
	 			) as a02 
	 			  on a02.nif2=a.nif
	 			 and a02.rownum=1 
	 left join 
	 			(
	 			 select *,row_number() over (partition by nifs order by nifp) rownum 
	 			   from geo.ancora_geo order by 1,2
	 			) as a03 
	 			  on a03.nifs=a.nif 
	 			 and a03.rownum=1
	 left join 
	 			(
	  			select nif
	  					,fecha2
	  					,hora
	  					,covid_sospecha
	  					,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.episodios
	  			 where covid_sospecha=1
	  			) as a04 
	  			  on a04.nif	= a.nif 
	  			 and a04.fecha2	= a.fecha2
	  			 and a04.hora	= a.hora
	  			 and a04.rownum	= 1
	left join 
	 			(
	  			select nif
	  					,fecha2
	  					,hora
	  					,covid_sospecha
	  					,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.episodios
	  			 where covid_confirmado=1
	  			) as a05 
	  			  on a05.nif	= a.nif 
	  			 and a05.fecha2	= a.fecha2
	  			 and a05.hora	= a.hora
	  			 and a05.rownum	= 1
	left join 
	  			(
	  			select distinct nif,1::smallint inscrito_validado2 
	  			  from fonasa.inscritos_validados_2020_corte_20190831 
	  			 where nif is not null and autorizado='X'
	  			) as a06 
	  			  on a06.nif=a.nif
	left join  
  	  			(
  	  			select nif2,edad_en_agnios(now(),nacimiento) as edad_en_agnios
  	  					,row_number() over (partition by nif2 order by fechalta) rownum
  	  			  from admomi.iddpacpa3 
  	  			) as a07 
  	  			  on a07.nif2=a.nif	  			 
  	  			 and a07.rownum=1
	left join	(
				select nif2,fecha2,hora,RESULTADO,descripcion_exa from covid19.resultado_examen
				) as a08
				  on a08.nif2	= a.nif 
				 and a08.fecha2	= a.fecha2
				 and a08.hora	= a.hora
	left join 
	 			(
	  			select nif
	  					,fecha2
	  					,hora
	  					,covid_sospecha
	  					,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.episodios
	  			 where covid_caso_probable=1
	  			) as a09 
	  			  on a09.nif	= a.nif 
	  			 and a09.fecha2	= a.fecha2
	  			 and a09.hora	= a.hora
	  			 and a09.rownum	= 1
; 

drop table if exists covid19.hechos5; 
create table covid19.hechos5 as   			 			  
with consulta as (
select a.*,a08.wproto2,a08.protocolo,a08.nif2 as id_paciente,a08.clinico as clinico_run,a08.clinico_nombre 
  from covid19.hechos4 as a 
 left join
  	 			(
  	 			select   md5(aa.nif2::text||'-'||aa.nif2::text) as nif,aa.nif2
  	 					,aa.fecha2
  	 					,aa.hora2
  	 					,md5(bb.id::text||'-'||bb.id::text) as medico,bb.id as clinico,bb.med_nombre as clinico_nombre
  	 					,aa.wproto2
  	 					,replace(cc.wprotocolo,'.','') as protocolo
  	 					,row_number() over (partition by aa.nif2,aa.fecha2,aa.hora2,bb.id order by aa.wproto2) as rownum
  	 			  from covid19.todos_los_protocolos as aa,admomi.iddmed as bb,admomi.iddco5 as cc
  	 			  where aa.medico=bb.medico
  	 			    and aa.wproto2=cc.wproto
  	 			) as a08 
  	 			  on a08.nif 	= a.nif
  	 			 and a08.fecha2	= a.fecha2
  	 			 and a08.hora2	= a.hora2
  	 			 and a08.medico	= a.medico
  	 			 and a08.rownum	= 1
  	where a.pac_centro2 in (1,2,3)
) select distinct a.* from consulta as a;

----------------------------------------
-- COVID-19 HECHOS EXAMENES PCR
----------------------------------------
drop table if exists covid19.hechos_examenes_pcr;create table covid19.hechos_examenes_pcr as
with consulta as	(
 select distinct a.*
		,case 
			when a.fecha_apertura_episodio_confirmacion<a.fecha_identificacion_contactos 
				then (a.fecha_apertura_episodio_confirmacion-fecha_recepcion)::integer 
				else (a.fecha_identificacion_contactos-fecha_recepcion)::integer
				end as latencia1
  from
		(
		select   distinct 
		         a01.centro
				,a01.nif2
				,a.patient_rut as rut
				,a.orderdate as fecha_solicitud
				,a.receptiondate as fecha_recepcion
				,min(a04.q) over (partition by a04.tis) as es_confirmado
				,min(a02.fecha2) over (partition by a02.tis,a.receptiondate) as fecha_apertura_episodio_confirmacion --as positiva --2977
				,min(a02.fecha2) over (partition by a02.tis,a.receptiondate) as fecha_nutificacion --as positiva --2977
				,case 
					when  a03.fecha2 is not null 	then min(a03.fecha2) over (partition by a03.tis,a.receptiondate) 
													else min(a02.fecha2) over (partition by a02.tis,a.receptiondate)
				end as fecha_identificacion_contactos
				,a.interpretedresult as resultado_exacto
				,case 
					when lower(a.interpretedresult) like '%indeterminado%' 	then 'Positivo' 
					when lower(a.interpretedresult) like '%Pendiente%' 		then 'Positivo'
					else a.interpretedresult end as resultado
				,1::integer as q
		  		FROM 
		  				(
		  				select distinct patient_rut,orderdate::Date orderdate,receptiondate::date receptiondate,interpretedresult 
		  				from examenes_etl.mg_vw_ancora2
		  				where profilecode = '2798H'
		  				) as a
		  left join (
		  			select centro,tis,nif2 
		  					,row_number() over (partition by tis order by fechalta) rownum
		  			  from admomi.iddpacpa3 where tis like '%-%' and estado2 is not null
		  			) as a01 on a01.tis=a.patient_rut and a01.rownum =1 --2920
		  left join 
		  			( --confirmación
		  			select pac.tis,ncu.fecha2,ncu.ciap
		  					,row_number() over (partition by pac.tis) rownum
		  			  from admomi.iddncu as ncu,admomi.iddpacpa3 as pac
		  			 where ncu.ciap in ('G12','G13')
		  			   and ncu.nif2=pac.nif2 
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null union
		  			select pac.tis,ncu.fechalta2,ncu.ciap
		 				   ,row_number() over (partition by pac.tis) rownum
		  			  from admomi.iddncu as ncu,admomi.iddpacpa3 as pac
		  			 where ncu.ciap in ('G10')
		  			   and ncu.nif2=pac.nif2 and ncu.fechalta2 is not null
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null
		  			) as a02 
		  			  on a02.tis=a.patient_rut 
		  			 and a02.rownum=1  
		  			 and a02.fecha2>=a.orderdate
		  left join 
		  			( --seguimiento
		  			select pac.tis
		  					,seg.fecha as fecha2
		  					,row_number() over (partition by pac.tis) rownum 
		  			  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 seg
		  			  		,admomi.iddpacpa3 as pac
		  			 where seg.nif=pac.nif2
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null
		  			) as a03
		  			  on a03.tis=a.patient_rut
		  			 and a03.rownum=1
		  			 and a03.fecha2>=a.orderdate
		    left join 
		  			( --es_confirmación
		  			select pac.tis,ncu.fecha2,ncu.ciap,1::integer as q
		  					,row_number() over (partition by pac.tis) rownum
		  			  from admomi.iddncu as ncu,admomi.iddpacpa3 as pac
		  			 where ncu.ciap in ('G12')
		  			   and ncu.nif2=pac.nif2 
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null
		  			) as a04 
		  			  on a04.tis=a.patient_rut 
		  			 and a04.rownum=1  
		  			 and a04.fecha2>=a.orderdate
		  order by a.orderdate desc nulls last
		 ) as a --where es_confirmado=1
		)
	select distinct case when latencia1<=2 then 1 else 0 end as latencia1_kpi,* --((r_quantile(array_accum(latencia1),0.75) - r_quantile(array_accum(latencia1),0.25) )*1.5)+(r_quantile(array_accum(latencia1),0.75)) as iqr2
      from consulta;



