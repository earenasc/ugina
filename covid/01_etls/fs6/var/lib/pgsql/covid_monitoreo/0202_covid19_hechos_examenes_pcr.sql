drop table if exists covid19.hechos_examenes_pcr;create table covid19.hechos_examenes_pcr as
with consulta as	(
 select distinct a.*
		,case 
			when a.fecha_apertura_episodio_confirmacion<a.fecha_identificacion_contactos 
				then (a.fecha_apertura_episodio_confirmacion-fecha_recepcion)::integer 
				else (a.fecha_identificacion_contactos-fecha_recepcion)::integer
				end as latencia1
  from
		(
		select   distinct 
		         a01.centro
				,a01.nif2
				,a.patient_rut as rut
				,a.orderdate as fecha_solicitud
				,a.receptiondate as fecha_recepcion
				,min(a04.q) over (partition by a04.tis) as es_confirmado
				,min(a02.fecha2) over (partition by a02.tis,a.receptiondate) as fecha_apertura_episodio_confirmacion --as positiva --2977
				,min(a02.fecha2) over (partition by a02.tis,a.receptiondate) as fecha_nutificacion --as positiva --2977
				,case 
					when  a03.fecha2 is not null 	then min(a03.fecha2) over (partition by a03.tis,a.receptiondate) 
													else min(a02.fecha2) over (partition by a02.tis,a.receptiondate)
				end as fecha_identificacion_contactos
				,a.interpretedresult as resultado_exacto
				,case 
					when lower(a.interpretedresult) like '%indeterminado%' 	then 'Positivo' 
					when lower(a.interpretedresult) like '%Pendiente%' 		then 'Positivo'
					else a.interpretedresult end as resultado
				,1::integer as q
		  		FROM 
		  				(
		  				select distinct patient_rut,orderdate::Date orderdate,receptiondate::date receptiondate,interpretedresult 
		  				from examenes_etl.mg_vw_ancora2
		  				where profilecode = '2798H'
		  				) as a
		  left join (
		  			select centro,tis,nif2 
		  					,row_number() over (partition by tis order by fechalta) rownum
		  			  from admomi.iddpacpa3 where tis like '%-%' and estado2 is not null
		  			) as a01 on a01.tis=a.patient_rut and a01.rownum =1 --2920
		  left join 
		  			( --confirmación
		  			select pac.tis,ncu.fecha2,ncu.ciap
		  					,row_number() over (partition by pac.tis) rownum
		  			  from admomi.iddncu as ncu,admomi.iddpacpa3 as pac
		  			 where ncu.ciap in ('G12','G13')
		  			   and ncu.nif2=pac.nif2 
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null union
		  			select pac.tis,ncu.fechalta2,ncu.ciap
		 				   ,row_number() over (partition by pac.tis) rownum
		  			  from admomi.iddncu as ncu,admomi.iddpacpa3 as pac
		  			 where ncu.ciap in ('G10')
		  			   and ncu.nif2=pac.nif2 and ncu.fechalta2 is not null
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null
		  			) as a02 
		  			  on a02.tis=a.patient_rut 
		  			 and a02.rownum=1  
		  			 and a02.fecha2>=a.orderdate
		  left join 
		  			( --seguimiento
		  			select pac.tis
		  					,seg.fecha as fecha2
		  					,row_number() over (partition by pac.tis) rownum 
		  			  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 seg
		  			  		,admomi.iddpacpa3 as pac
		  			 where seg.nif=pac.nif2
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null
		  			) as a03
		  			  on a03.tis=a.patient_rut
		  			 and a03.rownum=1
		  			 and a03.fecha2>=a.orderdate
		    left join 
		  			( --es_confirmación
		  			select pac.tis,ncu.fecha2,ncu.ciap,1::integer as q
		  					,row_number() over (partition by pac.tis) rownum
		  			  from admomi.iddncu as ncu,admomi.iddpacpa3 as pac
		  			 where ncu.ciap in ('G12')
		  			   and ncu.nif2=pac.nif2 
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null
		  			) as a04 
		  			  on a04.tis=a.patient_rut 
		  			 and a04.rownum=1  
		  			 and a04.fecha2>=a.orderdate
		  order by a.orderdate desc nulls last
		 ) as a --where es_confirmado=1
		)
	select distinct case when latencia1<=2 then 1 else 0 end as latencia1_kpi,* --((r_quantile(array_accum(latencia1),0.75) - r_quantile(array_accum(latencia1),0.25) )*1.5)+(r_quantile(array_accum(latencia1),0.75)) as iqr2
      from consulta;



