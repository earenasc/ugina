-----------------------------------------------------------------------------------------------------------------------------------------------------------
-- CUBO COVID19 CARGA NEOTEL
-----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists seguimiento_covid_hechos01;
create temp table    seguimiento_covid_hechos01 as
select nif2,fecha2 from  protocolos_omi_covid_base.iddncu where ciap in ('G10','G12','G13') group by 1,2;

drop table if exists protocolos_omi_covid_base.datos_contactos01;
create table    protocolos_omi_covid_base.datos_contactos01 as
select nif as nif2,fecha as fecha2,null::date as fechalta,especiali,'caso01' as num_caso,'NOMBRE CONTACTO' as dato,upper(nombre_contacto_01_wn_0221_t_t) as VALOR		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_01_wn_0221_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_02_wn_0222_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_02_wn_0222_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_03_wn_0223_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_03_wn_0223_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_04_wn_0224_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_04_wn_0224_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_05_wn_0225_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_05_wn_0225_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_06_wn_0226_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_06_wn_0226_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_07_wn_0227_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_07_wn_0227_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_08_wn_0228_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_08_wn_0228_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'RUT CONTACTO',upper(rut_contacto_01_wn_0232_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_01_wn_0232_t_t 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'RUT CONTACTO',upper(rut_contacto_02_wn_0233_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_02_wn_0233_t_t 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'RUT CONTACTO',upper(rut_contacto_03_wn_0234_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_03_wn_0234_t_t						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'RUT CONTACTO',upper(rut_contacto_04_wn_0235_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_04_wn_0235_t_t 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'RUT CONTACTO',upper(rut_contacto_05_wn_0236_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_05_wn_0236_t_t 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'RUT CONTACTO',upper(rut_contacto_06_wn_0237_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_06_wn_0237_t_t 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'RUT CONTACTO',upper(rut_contacto_07_wn_0238_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_07_wn_0238_t_t 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'RUT CONTACTO',upper(rut_contacto_08_wn_0239_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_08_wn_0239_t_t 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_01_wn_0409_t_x)  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_01_wn_0409_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_02_wn_0410_t_x) 	 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_02_wn_0410_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_03_wn_0411_t_x)  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_03_wn_0411_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_04_wn_0412_t_x)	  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_04_wn_0412_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_05_wn_0413_t_x)  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_05_wn_0413_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_06_wn_0414_t_x)  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_06_wn_0414_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_07_wn_0415_t_x)	  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409	where tiene_sisntomas_contacto_07_wn_0415_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_08_wn_0416_t_x)  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_08_wn_0416_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'EDAD CONTACTO',edad_contacto_01_wn_0356_t_n::text as valor 					from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_01_wn_0356_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'EDAD CONTACTO',edad_contacto_02_wn_0357_t_n::text as valor 					from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_02_wn_0357_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'EDAD CONTACTO',edad_contacto_03_wn_0358_t_n::text as valor 					from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_03_wn_0358_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'EDAD CONTACTO',edad_contacto_04_wn_0359_t_n::text as valor 					from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_04_wn_0359_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'EDAD CONTACTO',edad_contacto_05_wn_0360_t_n::text   						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_05_wn_0360_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'EDAD CONTACTO',edad_contacto_06_wn_0361_t_n::text   						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_06_wn_0361_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'EDAD CONTACTO',edad_contacto_07_wn_0362_t_n::text   						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_07_wn_0362_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'EDAD CONTACTO',edad_contacto_08_wn_0363_t_n::text 		  					from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_08_wn_0363_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_01_wn_0345_t_x)  as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_01_wn_0345_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_02_wn_0346_t_x)  as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_02_wn_0346_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_03_wn_0347_t_x)  as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_03_wn_0347_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_04_wn_0348_t_x)  as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_04_wn_0348_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_05_wn_0349_t_x)    						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_05_wn_0349_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_06_wn_0350_t_x)    						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_06_wn_0350_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_07_wn_0351_t_x)    						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_07_wn_0351_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_08_wn_0352_t_x) 	  						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_08_wn_0352_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_01_wn_0399_t_x)  				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_01_wn_0399_t_x 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_02_wn_0400_t_x)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_02_wn_0400_t_x 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_03_wn_0401_t_x)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_03_wn_0401_t_x 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_04_wn_0402_t_x)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_04_wn_0402_t_x 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_05_wn_0403_t_x)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_05_wn_0403_t_x 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_06_wn_0404_t_x)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_06_wn_0404_t_x					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_07_wn_0405_t_x)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_07_wn_0405_t_x 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_08_wn_0406_t_x) 	  				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_08_wn_0406_t_x 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_01_wn_0243_t_t)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_01_wn_0243_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_02_wn_0245_t_t)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_02_wn_0245_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_03_wn_0246_t_t)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_03_wn_0246_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_04_wn_0247_t_t)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_04_wn_0247_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_05_wn_0248_t_t)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_05_wn_0248_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_06_wn_0249_t_t)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_06_wn_0249_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_07_wn_0250_t_t)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_07_wn_0250_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_08_wn_0251_t_t) 	  				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_08_wn_0251_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_01_wn_0433_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_01_wn_0433_t_x			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_02_wn_0434_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_02_wn_0434_t_x			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_03_wn_0435_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_03_wn_0435_t_x 		is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_04_wn_0436_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_04_wn_0436_t_x 		is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_05_wn_0437_t_x)	    	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_05_wn_0437_t_x 		is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_06_wn_0438_t_x) 	   	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_06_wn_0438_t_x 		is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_07_wn_0439_t_x)    		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_07_wn_0439_t_x 		is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_08_wn_0440_t_x) 	  	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_08_wn_0440_t_x 		is not null union
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'MODALIDAD DE CONTACTO',upper(modalidad_de_contacto_wn_0189_t_x) 	  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where modalidad_de_contacto_wn_0189_t_x 				is not null union
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'VIA DE CONTACTO',upper(via_de_contacto_wn_0191_t_x)		 	  				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where via_de_contacto_wn_0191_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'TIENE CONTACTOS ESTRECHOS','NO'							 	  				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where no_tiene_contacto_estrechos_wn_0606_t_v=1						union
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'INDICACIONES CONDUCTA',upper(indicacionesconducta_wn_0061_t_x)				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where indicacionesconducta_wn_0061_t_x					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'NOTIFICACION',upper(estado_notificacion_resultado_wn_0532_t_x)	  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where estado_notificacion_resultado_wn_0532_t_x			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'FECHA INICIO SINTOMAS',upper(fechainiciosintomas_wn_0085_t_f::text)			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where fechainiciosintomas_wn_0085_t_f					is not null union --fecha_probable_fin_de_cuarentena_wn_0528_t_f
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'FECHA FIN',upper(fecha_probable_fin_de_cuarentena_wn_0528_t_f::text)		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where fecha_probable_fin_de_cuarentena_wn_0528_t_f		is not null union --fecha_probable_fin_de_cuarentena_wn_0528_t_f
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'TELEFONO01',upper(numero_de_telefono_1_wn_0195_t_t)		 	  				from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where numero_de_telefono_1_wn_0195_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'TELEFONO02',upper(numero_de_telefono_2_wn_0197_t_t)		 	  				from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where numero_de_telefono_2_wn_0197_t_t 					is not null union --fecha_primer_sintoma_wn_0179_t_f
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'TELEFONO01',upper(telefono_de_contacto_seguimiento_1_wn_0328_t_n::Text)		from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where telefono_de_contacto_seguimiento_1_wn_0328_t_n	is not null union
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'TELEFONO02',upper(telefono_de_contacto_seguimiento_2_wn_0334_t_n::text)		from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where telefono_de_contacto_seguimiento_2_wn_0334_t_n 	is not null union --fecha_primer_sintoma_wn_0179_t_f
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'TELEFONO01',upper(telefono_de_contacto_seguimiento_wn_0601_t_n::text)		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_de_contacto_seguimiento_wn_0601_t_n		is not null union --fecha_probable_fin_de_cuarentena_wn_0528_t_f
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'FECHA INICIO SINTOMAS',upper(fecha_primer_sintoma_wn_0179_t_f::text)		from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where fecha_primer_sintoma_wn_0179_t_f 					is not null union --fecha_primer_sintoma_wn_0179_t_f
select a.nif,a.fecha,null::date as fechalta,a.especiali,'caso00' as num_caso,'RUT CASO',upper(b.tis)
  from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 as a,admomi.iddpacpa3 as b
 where a.nif=b.nif2 union
select a.nif,a.fecha,null::date as fechalta,a.especiali,'caso00' as num_caso,'RUT CASO',upper(b.tis)
  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 as a,admomi.iddpacpa3 as b
 where a.nif=b.nif2 union
select a.nif2,a.fecha2,a.fechalta::date fechalta,a.especiali,'caso00' as num_caso,'RUT CASO',(b.tis)
  from  protocolos_omi_covid_base.iddncu as a,admomi.iddpacpa3 as b
 where a.ciap in ('G12','G13') and a.nif=b.nif union
select a.nif2,a.fecha2,a.fechalta::date fechalta,a.especiali,'caso00' as num_caso,'EPISODIO','SOSPECHA COVID-19' from  protocolos_omi_covid_base.iddncu as a,admomi.iddpacpa3 as b where a.ciap in ('G10') and a.nif=b.nif	union
select a.nif2,a.fecha2,a.fechalta::date fechalta,a.especiali,'caso00' as num_caso,'EPISODIO','CONFIRMADO COVID-19' from  protocolos_omi_covid_base.iddncu as a,admomi.iddpacpa3 as b where a.ciap in ('G12') and a.nif=b.nif	union
select a.nif2,a.fecha2,a.fechalta::date fechalta,a.especiali,'caso00' as num_caso,'EPISODIO','CASO PROBABLE COVID-19' from  protocolos_omi_covid_base.iddncu as a,admomi.iddpacpa3 as b where a.ciap in ('G13') and a.nif=b.nif	union
select a.nif2,a.fecha2,a.fechalta::date fechalta,a.especiali,'caso00' as num_caso,'EPISODIO','BUSQUEDA ACTIVA COVID-19' from  protocolos_omi_covid_base.iddncu as a,admomi.iddpacpa3 as b where a.ciap in ('G14') and a.nif=b.nif
union select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'USUARIO EGRESA',upper(usuario_egresa_wn_0219_t_v::text)			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 		where usuario_egresa_wn_0219_t_v 						is not null --fecha_primer_sintoma_wn_0179_t_f usuario_egresa_wn_0219_t_v
;

alter table protocolos_omi_covid_base.datos_contactos01 add column rut_nif 		text;
alter table protocolos_omi_covid_base.datos_contactos01 add column rut_num_caso text;
alter table protocolos_omi_covid_base.datos_contactos01 add column centro 		text;

update protocolos_omi_covid_base.datos_contactos01 set valor=BTRIM(upper(valor));
-- estado_notificacion_resultado
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'.','')		where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,',','')		where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'--','-')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,':','')		where dato in ('RUT CASO','RUT CONTACTO');
--update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G10T','')	where dato in ('RUT CASO','RUT CONTACTO');
--update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G12T','')	where dato in ('RUT CASO','RUT CONTACTO');
--update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G13T','')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'*','')		where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'/','')		where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'+','')		where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,' ','')		where dato in ('RUT CASO','RUT CONTACTO');

update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'.','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,',','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'--','-')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'-','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,':','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
--update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G10T','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
--update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G12T','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
--update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G13T','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'*','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'/','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'+','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,' ','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');

with consulta as	(
					select centro,nif2,tis
						,row_number() over (partition by nif2 order by fechalta) rownum
				      from admomi.iddpacpa3
				     where tis is not null and tis<>'' and tis like '%-%'
				       and estado2 is not null
					)
update protocolos_omi_covid_base.datos_contactos01
   set rut_nif=consulta.tis,centro=consulta.centro
  from consulta
 where consulta.nif2=protocolos_omi_covid_base.datos_contactos01.nif2
   and consulta.rownum=1;


update protocolos_omi_covid_base.datos_contactos01
   set rut_num_caso=rut_nif
 where num_caso='caso00';

update protocolos_omi_covid_base.datos_contactos01
   set rut_num_caso=valor
 where dato='RUT CONTACTO';

with consulta as 	(
					select distinct nif2,num_caso,valor,fecha2
					  from protocolos_omi_covid_base.datos_contactos01
					 where valor is not null and valor<>''
					   and dato='RUT CONTACTO'
					)
update protocolos_omi_covid_base.datos_contactos01 --el nif=paciente indice, es igual a caso num_caso='caso00'
   set rut_num_caso=consulta.valor
  from consulta
 where protocolos_omi_covid_base.datos_contactos01.num_caso<>'caso00'
   and protocolos_omi_covid_base.datos_contactos01.rut_num_caso is null
   and protocolos_omi_covid_base.datos_contactos01.nif2=consulta.nif2;

delete from protocolos_omi_covid_base.datos_contactos01 where nif2 in (select nif2 from admomi.iddpacpa3 where estado2 is null);

create index datos_contactos01_i_01 on protocolos_omi_covid_base.datos_contactos01(nif2)								tablespace tb_index;
create index datos_contactos01_i_02 on protocolos_omi_covid_base.datos_contactos01(nif2,centro)							tablespace tb_index;
create index datos_contactos01_i_03 on protocolos_omi_covid_base.datos_contactos01(fecha2)								tablespace tb_index;
create index datos_contactos01_i_04 on protocolos_omi_covid_base.datos_contactos01(num_caso)							tablespace tb_index;
create index datos_contactos01_i_05 on protocolos_omi_covid_base.datos_contactos01(dato)								tablespace tb_index;
create index datos_contactos01_i_06 on protocolos_omi_covid_base.datos_contactos01(dato)								tablespace tb_index;
create index datos_contactos01_i_07 on protocolos_omi_covid_base.datos_contactos01(valor)								tablespace tb_index;
create index datos_contactos01_i_08 on protocolos_omi_covid_base.datos_contactos01(nif2,num_caso)						tablespace tb_index;
create index datos_contactos01_i_09 on protocolos_omi_covid_base.datos_contactos01(nif2,fecha2,num_caso)				tablespace tb_index;
create index datos_contactos01_i_10 on protocolos_omi_covid_base.datos_contactos01(nif2,fecha2,num_caso,centro)			tablespace tb_index;
create index datos_contactos01_i_11 on protocolos_omi_covid_base.datos_contactos01(rut_nif)								tablespace tb_index;
create index datos_contactos01_i_12 on protocolos_omi_covid_base.datos_contactos01(rut_num_caso)						tablespace tb_index;
create index datos_contactos01_i_13 on protocolos_omi_covid_base.datos_contactos01(fechalta)							tablespace tb_index;
create index datos_contactos01_i_14 on protocolos_omi_covid_base.datos_contactos01(rut_nif,num_caso)					tablespace tb_index;
create index datos_contactos01_i_15 on protocolos_omi_covid_base.datos_contactos01(rut_nif,fecha2,num_caso)				tablespace tb_index;
create index datos_contactos01_i_16 on protocolos_omi_covid_base.datos_contactos01(rut_num_caso,num_caso)				tablespace tb_index;
create index datos_contactos01_i_17 on protocolos_omi_covid_base.datos_contactos01(rut_num_caso,fecha2,num_caso)		tablespace tb_index;
create index datos_contactos01_i_18 on protocolos_omi_covid_base.datos_contactos01(centro,rut_nif,num_caso)				tablespace tb_index;
create index datos_contactos01_i_19 on protocolos_omi_covid_base.datos_contactos01(centro,rut_nif,fecha2,num_caso)		tablespace tb_index;
create index datos_contactos01_i_20 on protocolos_omi_covid_base.datos_contactos01(centro,rut_num_caso,num_caso)		tablespace tb_index;
create index datos_contactos01_i_21 on protocolos_omi_covid_base.datos_contactos01(centro,rut_num_caso,fecha2,num_caso)	tablespace tb_index;

--select * from protocolos_omi_covid_base.datos_contactos01 where rut_nif='6610653-5'

drop table if exists seguimiento_covid_hechos02;
create temp table    seguimiento_covid_hechos02 as
select distinct
		 a01.centro
		, a.nif2
		,a.fecha2
		,a01.tis		as caso_positivo_rut
		,a01.nombre		as caso_positivo_nomnre
		,a01.apellido1	as caso_positivo_apellido1
		,a01.apellido2	as caso_positivo_apellido2
		,a01.caso_positivo_domicilio
		,a01.caso_positivo_comuna
		,a02.fecha 		as fecha_seguimiento
		,a03.num_caso
  from protocolos_omi_covid_base.datos_contactos01 as a
  left join	(
  			select centro,nif2,tis,nombre,apellido1,apellido2
  					,coalesce(domicilio,' ')||' '||coalesce(tiscab,' ') as caso_positivo_domicilio,comuna as caso_positivo_comuna
  					,row_number() over (partition by nif order by fechalta) rownum
  			  from admomi.iddpacpa3
  			) as a01
  			  on a01.nif2	= a.nif2
  			 and a01.rownum	= 1
 left join 	(
 			select distinct nif,fecha,especiali from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409
 			) as a02
 			  on a02.nif=a.nif2
 left join	(
			select distinct nif2,num_caso from protocolos_omi_covid_base.datos_contactos01
			) as a03
			  on a03.nif2=a.nif2;

drop 	table if exists  protocolos_omi_covid.seguimiento_covid_hechos03;
create	table    		 protocolos_omi_covid.seguimiento_covid_hechos03 as
select a.*
		,a01.contacto_nombre
		,a02.contacto_rut
		,a03.contacto_tiene_sintomas
		,a04.contacto_edad
		,a05.contacto_sexo
		,a06.contacto_embarazado
		,a07.contacto_telefono
		,a08.fecha_inicio_cuarentena
		,(a08.fecha_inicio_cuarentena+'14 days'::interval)::date as fecha_termino_cuarentena
		,'APS '||coalesce(a09.contacto_modalidad,'')||' '||coalesce(a10.contacto_via,'') as responsable_del_seguimiento
		,null::text as CUMPLE_PERFIL_CASO_SOSPECHOSO
		,null::text as FECHA_TOMA_DE_MUESTRA
		,null::text as RESULTADO_DEL_EXAMEN
		,null::text as FECHA_CONFIRMACION
		,null::text as LUGAR_DE_DERIVACION_SI_SE_CONFIRMA
  from seguimiento_covid_hechos02 as a
  left join (
			select nif2,num_caso,fecha2,valor as contacto_nombre
					,row_number() over (partition by nif2,num_caso order by fecha2) rownum
			  from protocolos_omi_covid_base.datos_contactos01
			 where dato='NOMBRE CONTACTO'
			   and valor<>'' and valor is not null
			) as a01
			  on a01.nif2		= a.nif2
			 and a01.num_caso	= a.num_caso
			 and a01.rownum		= 1
  left join (
			select nif2,num_caso,fecha2,valor as contacto_rut
					,row_number() over (partition by nif2,num_caso order by fecha2) rownum
			  from protocolos_omi_covid_base.datos_contactos01
			 where dato='RUT CONTACTO'
			   and valor<>'' and valor is not null
			) as a02
			  on a02.nif2		= a.nif2
			 and a02.num_caso	= a.num_caso
			 and a02.rownum		= 1
  left join (
			select nif2,num_caso,fecha2,valor as contacto_tiene_sintomas
					,row_number() over (partition by nif2,fecha2,num_caso order by fecha2) rownum
			  from protocolos_omi_covid_base.datos_contactos01
			 where dato='SINTOMAS CONTACTO'
			   and valor<>'' and valor is not null
			) as a03
			  on a03.nif2		= a.nif2
			 and a03.num_caso	= a.num_caso
			 and a03.fecha2		= a.fecha2
			 and a03.rownum		= 1
  left join (
			select nif2,num_caso,fecha2,valor as contacto_edad
					,row_number() over (partition by nif2,num_caso order by fecha2) rownum
			  from protocolos_omi_covid_base.datos_contactos01
			 where dato='EDAD CONTACTO'
			   and valor<>'' and valor is not null
			) as a04
			  on a04.nif2		= a.nif2
			 and a04.num_caso	= a.num_caso
			 and a04.rownum		= 1
left join 	(
			select nif2,num_caso,fecha2,valor as contacto_sexo
					,row_number() over (partition by nif2,num_caso order by fecha2) rownum
			  from protocolos_omi_covid_base.datos_contactos01
			 where dato='SEXO CONTACTO'
			   and valor<>'' and valor is not null
			) as a05
			  on a05.nif2		= a.nif2
			 and a05.num_caso	= a.num_caso
			 and a05.rownum		= 1
left join 	(
			select nif2,num_caso,fecha2,valor as contacto_embarazado
					,row_number() over (partition by nif2,num_caso order by fecha2) rownum
			  from protocolos_omi_covid_base.datos_contactos01
			 where dato='EMBARAZO CONTACTO'
			   and valor<>'' and valor is not null
			) as a06
			  on a06.nif2		= a.nif2
			 and a06.num_caso	= a.num_caso
			 and a06.rownum		= 1
left join 	(
			select nif2,num_caso,fecha2,valor as contacto_telefono
					,row_number() over (partition by nif2,fecha2,num_caso order by fecha2) rownum
			  from protocolos_omi_covid_base.datos_contactos01
			 where dato='TELEFONO CONTACTO'
			   and valor<>'' and valor is not null
			) as a07
			  on a07.nif2		= a.nif2
			 and a07.num_caso	= a.num_caso
			 and a07.fecha2		= a.fecha2
			 and a07.rownum		= 1
left join 	(
			select nif2,fecha2 as fecha_inicio_cuarentena
					,row_number() over (partition by nif2,fecha2 order by fecha2) rownum
			  from  protocolos_omi_covid_base.iddncu where ciap in ('G12','G13')
			) as a08
			  on a08.nif2=a.nif2
			 and a08.rownum=1
left join 	(
			select nif2,num_caso,fecha2,valor as contacto_modalidad
					,row_number() over (partition by nif2,fecha2,num_caso order by fecha2) rownum
			  from protocolos_omi_covid_base.datos_contactos01
			 where dato='MODALIDAD DE CONTACTO'
			   and valor<>'' and valor is not null
			) as a09
			  on a09.nif2		= a.nif2
			 and a09.num_caso	= a.num_caso
			 and a09.fecha2		= a.fecha2
			 and a09.rownum		= 1
left join 	(
			select nif2,num_caso,fecha2,valor as contacto_via
					,row_number() over (partition by nif2,fecha2,num_caso order by fecha2) rownum
			  from protocolos_omi_covid_base.datos_contactos01
			 where dato='VIA DE CONTACTO'
			   and valor<>'' and valor is not null
			) as a10
			  on a10.nif2		= a.nif2
			 and a10.num_caso	= a.num_caso
			 and a10.fecha2		= a.fecha2
			 and a10.rownum		= 1
			 ;

drop table if exists seguimiento_covid_hechos04;
create temp table    seguimiento_covid_hechos04 as
select a.*
		,a01.domicilio  as caso_contacto_domicilio
		,a01.comuna		as caso_contacto_comuna
  from  protocolos_omi_covid.seguimiento_covid_hechos03 as a
  left join	(
  			select centro,nif2,tis,nombre,apellido1,apellido2
  					,coalesce(domicilio,' ')||' '||coalesce(tiscab,' ') as domicilio,comuna
  					,row_number() over (partition by nif order by fechalta) rownum
  			  from admomi.iddpacpa3
  			) as a01
  			  on a01.tis	= a.contacto_rut
  			 and a01.rownum	= 1;

drop table if exists protocolos_omi_covid.listado_contactos_seremi;
create table protocolos_omi_covid.listado_contactos_seremi as
select distinct * from seguimiento_covid_hechos04;

grant select on protocolos_omi_covid.listado_contactos_seremi to consultaomi;

--Casos Confirmados Pentaho pentaho
drop table if exists casos_confirmados_pentaho;create table casos_confirmados_pentaho as
 select  distinct
		a01.centro
		,a01.nif2 as nif
		,a01.tis as rut
		,a.ciap as diagnostico_cod
		,a.descripcio diagnostico_descripcion
		,a.fecha ciap_fecha
		,a01.nombre
		,a01.apellido1
		,a01.apellido2
		,a01.sexo
		,edad_en_agnios(now(),a01.nacimiento) as edad
		,(coalesce(a01.domicilio,'')||' '||coalesce(a01.tiscab,'')) as domicilio,a01.comuna
		,a02.telefono
  from  protocolos_omi_covid_base.iddncu as a
  	left join
  				(
  				select distinct nif2,nif,centro,nacimiento,sexo,domicilio,tiscab,tis,nombre,apellido1,apellido2,comuna,
  					row_number() over (partition by nif order by fechalta) rownum
  					from admomi.iddpacpa3
  				) as a01
  				  on a01.nif=a.nif
  				 and a01.rownum=1
	left join
				(
				select aa.nif,dato,replace(replace(replace(replace(telefono,'-',''),'*',''),'/',''),'+','') as telefono
						,row_number() over (partition by nif order by dato) as rownum
				  from
				  		(
						select nif,'telefono1' as dato,upper(numero_de_telefono_1_wn_0195_t_t) as telefono
						  from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412
						 where numero_de_telefono_1_wn_0195_t_t is not null union
						select nif,'telefono2' as dato,upper(numero_de_telefono_2_wn_0197_t_t)
						  from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412
						 where numero_de_telefono_2_wn_0197_t_t is not null
						) as aa where telefono<>'' and telefono is not null
				) as a02
				  on a02.nif=a.nif2
				 and a02.rownum=1
 where a.ciap in ('G10','G12','G13')
   and a.nif not in (select nif from admomi.iddpacpa3 where estado2 is null);

update casos_confirmados_pentaho
   set telefono=NULLIF(regexp_replace(admomi.iddpacpa3.telpart, '\D','','g'), '')::numeric
  from admomi.iddpacpa3
 where casos_confirmados_pentaho.telefono is null
   and casos_confirmados_pentaho.nif=admomi.iddpacpa3.nif2;

update casos_confirmados_pentaho
   set telefono=NULLIF(regexp_replace(admomi.iddpacpa3.teldesp, '\D','','g'), '')::numeric
  from admomi.iddpacpa3
 where casos_confirmados_pentaho.telefono is null
   and casos_confirmados_pentaho.nif=admomi.iddpacpa3.nif2;

--carga NEOTEL
drop table if exists carga_neotel_01;
create table carga_neotel_01 as
select a.nif2,b.num_caso,b.valor as rut_contacto
  from protocolos_omi_covid_base.datos_contactos01 as a
  left join protocolos_omi_covid_base.datos_contactos01 as b on a.nif2=b.nif2 and b.dato in ('RUT CONTACTO','RUT CASO')
 group by 1,2,3 order by 1,2;

drop table if exists carga_neotel_02;
create table carga_neotel_02 as
select distinct
		 a.*
		,a01.telefono1
		,a02.riesgo
		,a00.centro
		,a03.edad
		,a04.nombre
		,a05.contacto_embarazado as embarazada
		,case
			when a06.diagnostico_cod='G10' then 'Sospechoso'
			when a06.diagnostico_cod='G12' then 'Confirmado'
			when a06.diagnostico_cod='G13' then 'Probable'
			when a06.diagnostico_cod='G14' then 'Pesquiza Activa'
			else 'Contacto' end as condicion
		--,a06.ciap_fecha::Date as fecha_inicio
		,a08.notificacion
		,a09.fecha_inicio::Date as fecha_inicio
		,a10.fecha_fin::Date as fecha_fin
  from carga_neotel_01 as a
   left join (
  			select nif2,tis,pac_nombre_completo,centro
  					,row_number() over (partition by nif order by fechalta) as rownum
  			  from admomi.iddpacpa3
  			) as a00
  			  on a00.nif2=a.nif2
  left join
			(
  			select *,NULLIF(regexp_replace(valor, '\D','','g'), '')::numeric as telefono1
  					,row_number() over (partition by nif2 order by fecha2 desc) rownum
  			  from protocolos_omi_covid_base.datos_contactos01 where dato='TELEFONO01'
  			) as a01
 			  on a01.nif2=a.nif2
 			 and a01.rownum=1
 left join 	(
  			select nif2,fecha
  					,case
  						when campo='1' then 'Alto'
  						when campo='2' then 'Medio'
  						when campo='3' then 'Bajo'
  						end as riesgo
  					,row_number() over (partition by nif2 order by fecha desc) rownum
  			  from protocolos_omi_covid_base.iddcop
  			 where wproto=409
  			   and wnumero=534
  			   and campo is not null
  			   and campo<>''
  			) as a02
  			  on a02.nif2=a.nif2
  			 and a02.rownum=1
  left join (
  			select NIF2,num_caso
  					,NULLIF(regexp_replace(valor, '\D','','g'), '')::numeric as edad
  					,row_number() over (partition by nif2,num_caso order by fecha2) rownum
  			  from protocolos_omi_covid_base.datos_contactos01
  			 where dato='EDAD CONTACTO'
  			   and valor<>'' and valor is not null
 			) as a03
 		      on a03.nif2=a.nif2
 		     and a03.num_caso=a.num_caso
 		     and a03.rownum=1
   left join
  			(
  			select nif2,num_caso,valor as nombre
  					,row_number() over (partition by nif2,num_caso order by fecha2) rownum
  			  from protocolos_omi_covid_base.datos_contactos01
  			 where dato='NOMBRE CONTACTO'
  			   and valor<>'' and valor is not null
  			) as a04
  			  on a04.nif2=a.nif2
  			 and a04.num_caso=a.num_caso
  			 and a04.rownum=1
left join 	(
			select nif2,num_caso,fecha2,valor as contacto_embarazado
					,row_number() over (partition by nif2,num_caso order by fecha2 desc) rownum
			  from protocolos_omi_covid_base.datos_contactos01
			 where dato='EMBARAZO CONTACTO'
			   and valor<>'' and valor is not null
			) as a05
			  on a05.nif2		= a.nif2
			 and a05.num_caso	= a.num_caso
			 and a05.rownum		= 1
left join	(
			select *
					,row_number() over (partition by rut order by ciap_fecha desc) as rownum
			  from casos_confirmados_pentaho
			) as a06
			  on a06.rut = a.rut_contacto
			 and a06.rut is not null
			 and a06.rownum=1
left join	(
			select *
					,row_number() over (partition by rut order by ciap_fecha desc) as rownum
			  from casos_confirmados_pentaho
			) as a07
			  on a07.nif = a.nif2
			 and a07.rownum=1
left join 	(
			select nif2,num_caso,fecha2,valor as NOTIFICACION
					,row_number() over (partition by nif2 order by fecha2 desc) rownum
			  from protocolos_omi_covid_base.datos_contactos01
			 where dato='NOTIFICACION'
			   and valor<>'' and valor is not null
			) as a08
			  on a08.nif2		= a.nif2
			 and a08.rownum		= 1
left join 	(
			select nif2,num_caso,fecha2,valor as FECHA_INICIO
					,row_number() over (partition by nif2 order by fecha2 desc) rownum
			  from protocolos_omi_covid_base.datos_contactos01
			 where dato='FECHA INICIO SINTOMAS'
			   and valor<>'' and valor is not null
			) as a09
			  on a09.nif2		= a.nif2
			 and a09.rownum		= 1
left join 	(
			select nif2,num_caso,fecha2,valor as FECHA_FIN
					,row_number() over (partition by nif2 order by fecha2 desc) rownum
			  from protocolos_omi_covid_base.datos_contactos01
			 where dato='FECHA FIN'
			   and valor<>'' and valor is not null
			) as a10
			  on a10.nif2		= a.nif2
			 and a10.rownum		= 1
;

update carga_neotel_02
   set telefono1=NULLIF(regexp_replace(admomi.iddpacpa3.telpart, '\D','','g'), '')::numeric
  from admomi.iddpacpa3
 where carga_neotel_02.telefono1 is null
   and carga_neotel_02.nif2=admomi.iddpacpa3.nif2;

update carga_neotel_02
   set edad=edad_en_agnios(now(),admomi.iddpacpa3.nacimiento)
  from admomi.iddpacpa3
 where carga_neotel_02.edad is null
   and carga_neotel_02.nif2=admomi.iddpacpa3.nif2;

update carga_neotel_02
   set nombre=admomi.iddpacpa3.pac_nombre_completo
  from admomi.iddpacpa3
 where carga_neotel_02.nombre is null
   and carga_neotel_02.nif2=admomi.iddpacpa3.nif2;
--select * from carga_neotel_02

drop table if exists carga_neotel_0301;
create table carga_neotel_0301 as
select distinct
		a.nif2 			as nif_caso_indice
		,a.num_caso
		,a.nombre
		,a.rut_contacto 	as rut
		,a.edad  			as edad
		,'No registrada'	as direccion
		,'correo@correo.cl' as email
		,a.telefono1	   as telefono1
		,a.centro 		as cesfam
		,case when a.riesgo is not null 		then a.riesgo 		else 'No registrado' end as fr
		,case when a.condicion is not null		then a.condicion 	else 'No registrado' end as condicion
		,case when a.notificacion is not null 	then a.notificacion else 'No registrado' end as Notificacion
		,case when a.embarazada is not null 	then a.embarazada 	else 'No registrado' end as embaraza
		,a.fecha_inicio	as Fecha_inicio
		,a.fecha_fin 	as fecha_fin
		,null::date 	as Fecha_Seguimiento
  from carga_neotel_02 as a;

drop index if exists carga_neotel_0301_i_01;create index carga_neotel_0301_i_01 on carga_neotel_0301(nif_caso_indice) 		tablespace tb_index;
drop index if exists carga_neotel_0301_i_02;create index carga_neotel_0301_i_02 on carga_neotel_0301(rut) 					tablespace tb_index;
drop index if exists carga_neotel_0301_i_03;create index carga_neotel_0301_i_03 on carga_neotel_0301(nif_caso_indice,rut) 	tablespace tb_index;

drop table if exists carga_neotel_0302;
create table carga_neotel_0302 as
select distinct a01.fecha_confirmacion,a.*
  from carga_neotel_0301 as a --select * from carga_neotel_0301 as a
  left join
  			(
  			with consulta as 	(
								select distinct bb.nif2 as nif_caso_indice,bb.tis,aa.*
					 			  from admomi.iddncu as aa
					 					,admomi.iddpacpa3 as bb
					 			 where ciap in ('G12','G13')
					 			   and aa.nif=bb.nif
					 			)
					 			select aa.nif_caso_indice,aa.tis as rut,min(cc.stk_fecha) as fecha_confirmacion --,aa.*
					 			  from consulta as aa
					 			left join protocolos_omi_covid_base.hisa_iddncu as cc on aa.nif=cc.nif and aa.especiali=cc.especiali
				 			   --WHERE aa.tis in ('6610653-5')
				 			 group by 1,2
  			) as a01
  			  on a01.nif_caso_indice=a.nif_caso_indice --a01.rut=a.rut
  order by a01.fecha_confirmacion desc nulls last;

with consulta as
				(
				select nif2,min(fecha) fecha_confirmacion
	 			  from protocolos_omi_covid_base.hisa_iddncu
	 			 where ciap in ('G12','G13') and stk_tipo_operacion='A'
	 			 group by 1
	  			)
update carga_neotel_0302
   set fecha_confirmacion = consulta.fecha_confirmacion
  from consulta
 where consulta.nif2=nif_caso_indice
   and carga_neotel_0302.fecha_confirmacion is null;

drop table if exists protocolos_omi_covid_base.carga_neotel_03;
create table protocolos_omi_covid_base.carga_neotel_03 as
select distinct *
  from carga_neotel_0302
 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null);
create index carga_neotel_03_i_01 on protocolos_omi_covid_base.carga_neotel_03(nif_caso_indice) 		tablespace tb_index;
create index carga_neotel_03_i_02 on protocolos_omi_covid_base.carga_neotel_03(rut) 					tablespace tb_index;
create index carga_neotel_03_i_03 on protocolos_omi_covid_base.carga_neotel_03(nif_caso_indice,rut) 	tablespace tb_index;


drop table if exists 	carga_neotel_final_to_pentaho;
create temp table 		carga_neotel_final_to_pentaho as
with consulta as 	(
					select row_number() over (partition by nif_caso_indice order by fecha_confirmacion) rownum
							,('0'::varchar||telefono1::varchar) telefono2,*
					  from protocolos_omi_covid_base.carga_neotel_03
					  /*
					 where nif_caso_indice in 	(
					 							select nif2
					 							  from
					 							  		(
					 							  		select * from protocolos_omi_covid.estado_de_seguimiento03
					 							  		 where centro in ('SAH','JPII','MTC')
					 							  		   and usuario_egresa_fecha is null and fecha_episodio_cierre is null
					 							  		) as a
					 							)	*/
					)
select distinct
		 a01.med_nombre
		 ,a01.medico
		 ,a.fecha_confirmacion::date as fecha_confirmacion2
		,a.rownum
		,a.telefono2
		,a.fecha_confirmacion
		,a.nif_caso_indice
		,a.num_caso
		,a.nombre
		,a.rut
		,a.edad
		,a.direccion
		,a.email
		,a.telefono2 as telefono1
		,a.cesfam
		,a.fr
		,a.condicion
		,a.notificacion
		,a.embaraza as embarazada
		,case when a.fecha_inicio is not null then a.fecha_inicio::date else a.fecha_confirmacion::date 						end as fecha_inicio
		,case when a.fecha_fin is not null then a.fecha_fin::date 		else (a.fecha_confirmacion+'12 days'::interval)::date 	end as fecha_fin
		,a.fecha_seguimiento
  from consulta as a
  left join	(
  			select   NULLIF(regexp_replace(aa.nif, '\D','','g'), '')::numeric as nif
  					,bb.med_nombre
  					,row_number() over (partition by aa.nif order by aa.fecha) rownum
  					,bb.medico
  			  from   admomi.iddage as aa
  			  		,admomi.iddmed as bb
  			  where aa.medico=bb.medico and bb.id>=2000000
  			    and aa.nif<>'' and aa.nif is not null
  			    and aa.fecha>='20200723 00:00:00'
  			    and aa.medico in 	(
  			    					'LZS','AAG','DZO','ICM','JLO','JAV','VEP','VVM','MC7','JG2','MJ2','MLG','VNN','MP6',	--SAH
  			    					'LCV','CDM','CV3','VVG','DCM','MJL','NCM','GSK','JVP','IIH','TLB','VE2','SGU','NSM','LCF','RCF','SIS','AAB','AVP','MB','MBV','LGV','AML','LLS','MV4'	--JPII
  			    					)
  			) as a01
  			  on a01.nif=a.nif_caso_indice
  			 and a01.rownum=1
where a.nif_caso_indice not 	in (select nif2 from admomi.iddpacpa3 where estado2 is null)
  and a.nif_caso_indice 		in (select nif  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409)
  ;--and a.fecha_confirmacion>='20200723 00:00:00' order by 1,6,7;

drop table if exists protocolos_omi_covid.carga_neotel_final_to_pentaho;
create table protocolos_omi_covid.carga_neotel_final_to_pentaho as
--truncate table protocolos_omi_covid.carga_neotel_final_to_pentaho;
--insert into protocolos_omi_covid.carga_neotel_final_to_pentaho
select
		 a.rownum
		,a.med_nombre
		,a.fecha_confirmacion
		,a.nif_caso_indice
		,a.num_caso
		,case when a.medico is not null then (a.medico||' - '||a.nombre) else a.nombre end as nombre
		,a.rut
		,a.edad
		,a.direccion
		,a.email
		,a.telefono2 as telefono1
		,a.cesfam
		,a.fr
		,a.condicion
		,a.notificacion
		,a.embarazada
		,a.fecha_inicio
		,a.fecha_fin
  from carga_neotel_final_to_pentaho as a
 where num_caso||rut not in	( --saca las personas q están repetidas tanto en un grupo con otro caso indice cuando se vuelven probables
			 				select num_caso||rut
							  from carga_neotel_final_to_pentaho
							 where num_caso='caso00'
							   and rut in
							   					(
							   					select rut--,*
												  from carga_neotel_final_to_pentaho
												 where num_caso<>'caso00' and condicion<>'Contacto'
												   and fecha_confirmacion2 between (now()-'14 days'::interval)::date and now()::Date
												 )
							)
order by nombre;

grant select on table protocolos_omi_covid.carga_neotel_final_to_pentaho to consultaomi;
grant select on table protocolos_omi_covid.carga_neotel_final_to_pentaho to consulta;
grant select on table protocolos_omi_covid.carga_neotel_final_to_pentaho to ugina;
grant select on table protocolos_omi_covid.carga_neotel_final_to_pentaho to jesoto;
grant select on table protocolos_omi_covid.carga_neotel_final_to_pentaho to ehenriquez;
