/usr/pgsql-12/bin/psql -h 192.168.150.39 -d ancorabi -p 5432 -U postgres -c \ "
drop table if exists covid19.rangos_edad_01;create table covid19.rangos_edad_01 as
select '20190101 00:00:00'::timestamp as  fechai,(('now'::date-'1 day'::interval)::date::Text||' 23:59:59')::timestamp as fechaf;

	drop table if exists covid19.consultas_de_morbilidad; --select count(*) from cons_morbilidad 8780
	create table    covid19.consultas_de_morbilidad as
	select nif2 as nif,medico,fecha2,extract(hour from fecha) as hora
			,case when 	(
				 		(wproto=397 and wnumero=70  and campo in ('01','02','03','04','05','06') )
				 		or
				 		(wproto=298 and wnumero=242 and campo in ('01','02','03','04','05','06') )
				 		or
				 		(wproto=412 and wnumero=70  and campo in ('01','02','03','04','05','06') )
				 		or
				 		(wproto=373 and wnumero=226  and campo is not null )
				 		)
				 		then 1::smallint end respiratorias  
	  from admomi.iddcop as a
	 where a.wproto in (397,298,412,373)
	   and a.fecha between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)   
	   group by 1,2,3,4,5;
	  
	  

	--consultas episodios R%
	drop table if exists covid19.episodios;
	create table 		 covid19.episodios as
	  select a.nif,a.medico
	  		,a.fecha2,extract(hour from a.fecha) as hora
	  		,case when b.ciap like 'R%'  then 1::smallint 		end respiratorias
	  		,case when b.ciap in ('G10') then 1::smallint 		end covid_sospecha
	  		,case when b.ciap in ('G12','G13') then 1::smallint end covid_confirmado
	  		,case when b.ciap in ('G13') then 1::smallint 		end covid_caso_probable
	  from admomi.iddcor as a,admomi.iddncu as b
	 where a.numero=1 
	   and a.fecha2 between (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)   
	   and a.nif=b.nif2
	   and a.especiali=b.especiali
	   group by 1,2,3,4,5,6,7,8; --635236

	  /*
	   select nif,medico,fecha2,hora from covid19.consultas_de_morbilidad where respiratorias=1 union
	   select nif,medico,fecha2,hora from covid19.episodios where respiratorias=1 or covid_sospecha=1 or covid_confirmado=1
	    */
	  
	drop table if exists covid19.episodios_cierre_covid;
	create table 		 covid19.episodios_cierre_covid as
	select	 a.nif
			,a.medico
			,B.fechalta::date as fecha2
			,extract(hour from b.fechalta) as hora
	  		,case when b.ciap in ('G10') then 1::smallint end covid_sospecha_cerrada
	  		,case when b.ciap in ('G12','G13') then 1::smallint end covid_confirmado_cerrada
	  		,case when b.ciap in ('G13') then 1::smallint end covid_confirmado_probalble_cerrada
	  from admomi.iddcor as a,admomi.iddncu as b
	 where a.numero=1 
	   and b.fechalta::date between (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)   
	   and a.nif=b.nif2
	   and a.especiali=b.especiali
	   and b.fechalta is not null
	   and b.ciap in ('G10','G12','G13')
	   group by 1,2,3,4,5,6,7;
	  
	  
drop table if exists covid19.todos_los_protocolos;  --select * from covid19.todos_los_protocolos
create table 		 covid19.todos_los_protocolos as
select   distinct nif as nif2
		,stk_fecha::date as fecha2
		,extract(hour from stk_fecha) as hora2
		,medico
		,wproto as wproto2
  from admomi.hisa_iddcor 
 where stk_fecha between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)
   and numero=1
   and wproto>=1
   and stk_tipo_operacion='A'
   and nif is not null;
create index todos_los_protocolos_i_01 on covid19.todos_los_protocolos(medico,wproto2) tablespace tb_index;

drop table if exists 	covid19.resultado_examen;
create table 			covid19.resultado_examen as
select a.nif2 --,a.fecha_solicitud
		,a02.stk_fecha as fecha--a01.fecha as fecha
		,a02.stk_fecha::date as fecha2 --a01.fechap::Date as fecha2
		,extract(hour from a02.stk_fecha) as hora --,extract(hour from fechar) as hora
		,a.descripcion_exa
		,a.solicita as medico
		,case 
			when btrim(upper(a.resultado)) is null or btrim(upper(a.resultado))='' then 'PENDIENTE' 
			else btrim(upper(a.resultado)) 
			 end as resultado
  from report.baciloscopias a --select * from report.baciloscopias a --select * from admomi.hisa_iddome
  left join 
  			(
  			select nif,numorden,fecha,fechar,fechap,row_number() over (partition by nif,fechap::date order by fechap) as rownum
  			  from admomi.iddome 
  			  where nif<>'' and nif is not null
  			) as a01
  			  on a01.nif		= a.nif
			 and a01.numorden	= a.numorden 
			 and a01.rownum		= 1
  left join	(
  			select nif::integer as nif2,numorden,stk_fecha
  					,row_number() over (partition by nif,numorden order by stk_fecha) as rownum
  			  from admomi.hisa_iddome 
  			 where stk_tipo_operacion='A' 
  			   and stk_fecha>='20200101 00:00:00'
  			) as a02
  			  on a02.numorden=a01.numorden
  			 and a02.nif2=a.nif2 
  			 and a02.rownum=1
 where a.descripcion_exa like 'Coronavirus%'
;
create index resultado_examen_i_01 on covid19.resultado_examen(nif2,fecha2,hora) tablespace tb_index;

--select * from report.baciloscopias b limit 10
--select fecha,fechap,* from admomi.iddome order by fechap desc
;

  --hechos central
	drop table if exists 	covid19.hechos; --select * from hechos
	create table	    	covid19.hechos as  
	select nif,fecha2,hora,medico from covid19.consultas_de_morbilidad 
	union
	select nif,fecha2,hora,medico from covid19.episodios
	union
	select nif,fecha2,hora,medico from covid19.episodios_cierre_covid
	union
	select pac_nif::integer,vac_fecharegistro::Date,extract(hour from vac_fecharegistro) as hora, upper(empleado_codigo)  	from vacunas.cubo_vacunas where vac_fecharegistro between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01) and (vac_codigo like 'AIH%' or vac_codigo like 'AIF%')--select * from vacunas.cubo_vacunas order by vac_fecharegistro
	union
	select nif::integer as nif,fecha::date fecha,extract(hour from fecha) as hora,medico									from admomi.iddage 			where FECHA between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01) AND ASIGNADA='S' and vino<>'N' 
	union 
	select nif2,fecha_servida::date,extract(hour from fecha_servida) as hora,upper(med_codigo)  							from farmacos.cubo_prescripciones_servidas 	where fecha_servida between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)
	union --1178659
	select nif2,fecha2,hora2,medico from covid19.todos_los_protocolos
	union
	select nif2,fecha2,hora,medico from covid19.resultado_examen;
--select * from report.baciloscopias b limit 10
--select fecha,fechap,* from admomi.iddome order by fechap desc
	;
	  
	drop table if exists 	covid19.hechos2;
	create table    		covid19.hechos2 as  
	select 
			a.*
			,a01.respiratorias as morbilidad_respiratoria
			,a02.respiratorias  as episodios_respiratoria
			,a03.rownum as vacuna_influenza
			,a04.rownum as consultas_controles_procedimientos
			,a05.rownum as despacho_medicamento
			,a06.rownum as covid_seguimiento
			
	  from covid19.hechos a
	  left join 
	  			(
	  			/*
	  			select nif,fecha2,hora,respiratorias,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.consultas_de_morbilidad
	  			 where respiratorias=1
	  			 */
	  			 /*asumiento que todo registro en R y episodios covid son respiratorias*/
				select aa.nif,aa.fecha2,aa.hora
						,1::smallint as respiratorias,row_number() over (partition by aa.nif,aa.fecha2,aa.hora) rownum
				  from 
						(
			  			select nif,fecha2,hora 
			  			  from covid19.consultas_de_morbilidad
			  			 where respiratorias=1
			  			 union
			  			select nif,fecha2,hora from covid19.episodios where respiratorias=1 or covid_sospecha=1 or covid_confirmado=1
						) as aa	  			 
	  			) as a01 
	  			  on a01.nif	= a.nif 
	  			 and a01.fecha2	= a.fecha2
	  			 and a01.hora	= a.hora
	  			 and a01.rownum	= 1
	  left join 
	  			(
	  			select nif,fecha2,hora,respiratorias,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.episodios
	  			) as a02 
	  			  on a02.nif	= a.nif 
	  			 and a02.fecha2	= a.fecha2
	  			 and a02.hora	= a.hora
	  			 and a02.rownum	= 1
	  left join 
	  			(
	  			select pac_nif::integer as nif
	  					,vac_fecha::Date as fecha2
	  					,extract(hour from  vac_fecharegistro) hora
	  					,upper(empleado_codigo) as clinico
	  					,row_number() over (partition by pac_nif,vac_fecharegistro::Date,extract(hour from  vac_fecharegistro)) rownum
				  from vacunas.cubo_vacunas
				 where vac_fecharegistro between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)
				   and (vac_codigo like 'AIH%' or vac_codigo like 'AIF%' or vac_nombre like 'A-INFLUENZA%' or vac_nombre like 'ANTIINFLUENZA%')
	  			) as a03
	  			  on a03.nif	= a.nif
	  			 and a03.fecha2	= a.fecha2
	  			 and a03.hora	= a.hora
	  			 and a03.rownum	= 1
	  left join
	  			(
	  			select nif::integer as nif
	  					,fecha::date fecha2
	  					,extract(hour from fecha) as hora
	  					,medico
	  					,row_number() over (partition by nif,fecha::Date,extract(hour from fecha)) rownum
				  from admomi.iddage
				 where FECHA between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)
				   AND ASIGNADA='S' 
				   and vino<>'N'
	  			) as a04
	  			  on a04.nif	= a.nif
	  			 and a04.fecha2	= a.fecha2
	  			 and a04.hora	= a.hora
	  			 and a04.rownum	= 1
	left join
				(
				select nif2 as nif
						,fecha_servida::date as fecha2
						,extract(hour from fecha_servida) as hora
						,upper(med_codigo)
						,row_number() over (partition by nif2,fecha_servida::Date,extract(hour from fecha_servida)) rownum
				  from farmacos.cubo_prescripciones_servidas
				 where fecha_servida between  (select fechai from covid19.rangos_edad_01)  and  (select fechaf from covid19.rangos_edad_01)
				) as a05
				  on a05.nif	= a.nif
				 and a05.fecha2	= a.fecha2
				 and a05.hora	= a.hora 
				 and a05.rownum	= 1
	left join	(
				select distinct nif2,fecha2,hora2,medico
						,row_number() over (partition by nif2,fecha2,hora2 order by fecha2,hora2) rownum
	   			  from covid19.todos_los_protocolos 
	   			 where wproto2 = 409
				) as a06
				  on a06.nif2	= a.nif
				 and a06.fecha2	= a.fecha2
				 and a06.hora2	= a.hora 
				 and a06.rownum	= 1
		
	  			 ; --1316583
	  			 
drop table if exists covid19.hechos4; --select * from covid19.hechos4
create table covid19.hechos4 as   			
select 		distinct
			 a.nif as nif2--,md5(a.nif::text||'-'||a.nif::text) as nif  --a.nif::text as nif
			--,md5(a.nif::text||'-'||a.nif::text) as nif2 --a.nif as nif2			--md5(a.nif::text||'-'||a.nif::text) as nif			,a.fecha2
			,a.fecha2::Date fecha2
			--,a.hora::text as hora
			,a.hora as hora2
			,a01.id as medico --md5(a01.id::text||'-'||a01.id::text) as medico
			--,case when a.morbilidad_respiratoria=1 then 'SI'::text else 'NO' 									end as morbilidad_respiratoria
			,case when a.morbilidad_respiratoria=1 then 1::smallint else 0::smallint 							end as morbilidad_respiratoria2
			--,case when a.episodios_respiratoria=1 then 'SI'::text else 'NO' 									end as episodios_respiratoria
			,case when a.episodios_respiratoria=1 then 1::smallint else 0::smallint 							end as episodios_respiratoria2
			--,case when a.consultas_controles_procedimientos=1 then 'SI'::text else 'NO' 						end as consultas_controles_procedimientos
			,case when a.consultas_controles_procedimientos=1 then 1::smallint else 0::smallint 				end as consultas_controles_procedimientos2
			--,case when despacho_medicamento=1 then 'SI'::text else 'NO' 										end as despacho_medicamento
			,case when despacho_medicamento=1 then 1::smallint else 0::smallint 								end as despacho_medicamento2
			--,case when vacuna_influenza=1 then 'SI'::text else 'NO' 											end as vacuna_influenza
			,case when vacuna_influenza=1 then 1::smallint else 0::smallint 									end as vacuna_influenza2
			--,case when morbilidad_respiratoria=1 or episodios_respiratoria=1 then 'SI' else 'NO'::text 			end as consultas_respiratorias_totales
			,case when morbilidad_respiratoria=1 or episodios_respiratoria=1 then 1::smallint else 0::smallint 	end as consultas_respiratorias_totales2	
			--,case when a04.rownum=1 then 'SI' else 'NO'::text 													end as covid_sospecha
			,case when a04.rownum=1 then 1::smallint else 0::smallint 											end as covid_sospecha2
			--,case when a05.rownum=1 then 'SI' else 'NO'::text 													end as covid_confirmado
			,case when a05.rownum=1 then 1::smallint else 0::smallint 											end as covid_confirmado2
			,case when a08.rownum=1 then 1::smallint else 0::smallint 											end as covid_caso_probable2
			--,case when a01.med_estamento_desc in ('MEDICO','PSIQUIATRIA','GASTROENTEROLOGIA','GINECOLOGIA') then 'SI'::text else 'NO'::text end as es_medico
			,case when a01.med_estamento_desc in ('MEDICO','PSIQUIATRIA','GASTROENTEROLOGIA','GINECOLOGIA') then 1::smallint else 0::smallint end as es_medico2
			--,case when a06.inscrito_validado2=1 then 'SI' else 'NO'::text 													end as inscrito_validado
			,case when a06.inscrito_validado2=1 then 1::smallint else 0::smallint 								end as inscrito_validado2
			,case when a.covid_seguimiento=1 then 1::smallint else 0::smallint 									end as covid_seguimiento2
/*
			,a02.centro as pac_centro
			,case 
				when a02.centro = 'MTC'  then 1
				when a02.centro = 'JPII' then 2
				when a02.centro = 'SAH'  then 3
				end as pac_centro2
			,case
				when a01.centro = 'MTC'  then 1
				when a01.centro = 'JPII' then 2
				when a01.centro = 'SAH'  then 3
				end as med_centro2
			,a01.centro as med_centro
			,CASE 
				WHEN a03.latitude IS NOT NULL THEN a03.latitude
				WHEN a03.latitude IS NULL AND a02.centro='JPII' THEN -33.59495818 --29708 --select distinct nif,latitude,longitude from covid19.hechos3 where nif in (29708,5543,46775)
				WHEN a03.latitude IS NULL AND a02.centro='MTC'  THEN -33.61403557 --5543
				WHEN a03.latitude IS NULL AND a02.centro='SAH'  THEN -33.59637181 --46775
				END AS   latitude
			,CASE
				WHEN a03.longitude IS NOT NULL THEN a03.longitude
				WHEN a03.longitude IS NULL AND a02.centro='JPII' THEN -70.62199003
				WHEN a03.longitude IS NULL AND a02.centro='MTC'  THEN -70.55252455
				WHEN a03.longitude IS NULL AND a02.centro='SAH'  THEN -70.5563369
				END AS   longitude
				*/
			--,1::smallint as q
			--,a07.edad_en_agnios 
	  from covid19.hechos2 as a --select * from covid19.hechos2 as a
	 left join 
				(
				select distinct centro,medico,id,med_estamento_desc
			      from admomi.iddmed 
			    ) as a01 on a01.medico=a.medico
	 left join admomi.iddpacpa3 as a02 on a02.nif2=a.nif
	 left join geo.ancora_geo as a03 on a03.nifs=a.nif
	 left join 
	 			(
	  			select nif
	  					,fecha2
	  					,hora
	  					,covid_sospecha
	  					,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.episodios
	  			 where covid_sospecha=1
	  			) as a04 
	  			  on a04.nif	= a.nif 
	  			 and a04.fecha2	= a.fecha2
	  			 and a04.hora	= a.hora
	  			 and a04.rownum	= 1
	left join 
	 			(
	  			select nif
	  					,fecha2
	  					,hora
	  					,covid_sospecha
	  					,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.episodios --select * from covid19.episodios
	  			 where covid_confirmado=1
	  			) as a05 
	  			  on a05.nif	= a.nif 
	  			 and a05.fecha2	= a.fecha2
	  			 and a05.hora	= a.hora
	  			 and a05.rownum	= 1
	left join 
	  			(
	  			select distinct nif,1::smallint inscrito_validado2 
	  			  from fonasa.inscritos_validados_2020_corte_20190831 
	  			 where nif is not null and autorizado='X'
	  			) as a06 
	  			  on a06.nif=a.nif
	left join  
  	  			(
  	  			select nif2,edad_en_agnios(now(),nacimiento) as edad_en_agnios
  	  			  from admomi.iddpacpa3 
  	  			) as a07 on a07.nif2=a.nif	  			 
	left join 
	 			(
	  			select nif
	  					,fecha2
	  					,hora
	  					,covid_sospecha
	  					,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.episodios --select * from covid19.episodios
	  			 where covid_caso_probable=1
	  			) as a08 
	  			  on a08.nif	= a.nif 
	  			 and a08.fecha2	= a.fecha2
	  			 and a08.hora	= a.hora
	  			 and a08.rownum	= 1
  	  			;

drop table if exists covid19.hechos4; 
create table covid19.hechos4 as   			
select 		distinct
			 md5(a.nif::text||'-'||a.fecha2::date::text) as id
			,md5(a.nif::text||'-'||a.nif::text) as nif
			,md5(a.nif::text||'-'||a.nif::text) as nif2--a.nif::text as nif
			--,md5(a.nif::text||'-'||a.nif::text) as nif2 --a.nif as nif2			--md5(a.nif::text||'-'||a.nif::text) as nif			,a.fecha2
			,a.fecha2::Date fecha2
			,a.hora::TEXT as hora
			,a.hora as hora2
			,md5(a01.id::text||'-'||a01.id::text) as medico
			,case when a.morbilidad_respiratoria=1 then 'SI'::text else 'NO' 									end as morbilidad_respiratoria
			,case when a.morbilidad_respiratoria=1 then 1::smallint else 0::smallint 							end as morbilidad_respiratoria2
			,case when a.episodios_respiratoria=1 then 'SI'::text else 'NO' 									end as episodios_respiratoria
			,case when a.episodios_respiratoria=1 then 1::smallint else 0::smallint 							end as episodios_respiratoria2
			,case when a.consultas_controles_procedimientos=1 then 'SI'::text else 'NO' 						end as consultas_controles_procedimientos
			,case when a.consultas_controles_procedimientos=1 then 1::smallint else 0::smallint 				end as consultas_controles_procedimientos2
			,case when despacho_medicamento=1 then 'SI'::text else 'NO' 										end as despacho_medicamento
			,case when despacho_medicamento=1 then 1::smallint else 0::smallint 								end as despacho_medicamento2
			,case when vacuna_influenza=1 then 'SI'::text else 'NO' 											end as vacuna_influenza
			,case when vacuna_influenza=1 then 1::smallint else 0::smallint 									end as vacuna_influenza2
			,case when morbilidad_respiratoria=1 or episodios_respiratoria=1 then 'SI' else 'NO'::text 			end as consultas_respiratorias_totales
			,case when morbilidad_respiratoria=1 or episodios_respiratoria=1 then 1::smallint else 0::smallint 	end as consultas_respiratorias_totales2	
			,case when a04.rownum=1 then 'SI' else 'NO'::text 													end as covid_sospecha
			,case when a04.rownum=1 then 1::smallint else 0::smallint 											end as covid_sospecha2
			,case when a05.rownum=1 then 'SI' else 'NO'::text 													end as covid_confirmado
			,case when a05.rownum=1 then 1::smallint else 0::smallint 											end as covid_confirmado2
			,case when a09.rownum=1 then 'SI' else 'NO'::text 													end as covid_caso_probable
			,case when a09.rownum=1 then 1::smallint else 0::smallint 											end as covid_caso_probable2
			,a01.med_estamento_desc																					as estamento
			,case when a01.med_estamento_desc in ('MEDICO','PSIQUIATRIA','GASTROENTEROLOGIA','GINECOLOGIA') then 'SI'::text else 'NO'::text end as es_medico
			,case when a01.med_estamento_desc in ('MEDICO','PSIQUIATRIA','GASTROENTEROLOGIA','GINECOLOGIA') then 1::smallint else 0::smallint end as es_medico2
			,case when a06.inscrito_validado2=1 then 'SI' else 'NO'::text 													end as inscrito_validado
			,case when a06.inscrito_validado2=1 then 1::smallint else 0::smallint 											end as inscrito_validado2
			,a08.descripcion_exa
			,a08.resultado
			,a02.centro as pac_centro
			,case when a.covid_seguimiento=1 then 'SI'::text else 'NO' 														end as covid_seguimiento
			,case when a.covid_seguimiento=1 then 1::smallint else 0::smallint 												end as covid_seguimiento2

			,case 
				when a02.centro = 'MTC'  then 1
				when a02.centro = 'JPII' then 2
				when a02.centro = 'SAH'  then 3
				end as pac_centro2
			,case
				when a01.centro = 'MTC'  then 1
				when a01.centro = 'JPII' then 2
				when a01.centro = 'SAH'  then 3
				end as med_centro2
			,a01.centro as med_centro
			,CASE 
				WHEN a03.latitude IS NOT NULL THEN a03.latitude
				WHEN a03.latitude IS NULL AND a02.centro='JPII' THEN -33.59495818 --29708 --select distinct nif,latitude,longitude from covid19.hechos3 where nif in (29708,5543,46775)
				WHEN a03.latitude IS NULL AND a02.centro='MTC'  THEN -33.61403557 --5543
				WHEN a03.latitude IS NULL AND a02.centro='SAH'  THEN -33.59637181 --46775
				END AS   latitude
			,CASE
				WHEN a03.longitude IS NOT NULL THEN a03.longitude
				WHEN a03.longitude IS NULL AND a02.centro='JPII' THEN -70.62199003
				WHEN a03.longitude IS NULL AND a02.centro='MTC'  THEN -70.55252455
				WHEN a03.longitude IS NULL AND a02.centro='SAH'  THEN -70.5563369
				END AS   longitude
			,1::smallint as q
			,a07.edad_en_agnios
	  from covid19.hechos2 as a
	 left join 
				(
				select distinct centro,medico,id,med_estamento_desc,row_number() over (partition by id order by id,profesional) rownum
			      from admomi.iddmed order by 3
			    ) as a01 
			      on a01.medico=a.medico
			     and a01.rownum=1
	 left join 	(
	 			select nif2,centro,row_number() over (partition by nif2 order by fechalta) rownum
	 			from admomi.iddpacpa3 
	 			) as a02 
	 			  on a02.nif2=a.nif
	 			 and a02.rownum=1 
	 left join 
	 			(
	 			 select *,row_number() over (partition by nifs order by nifp) rownum 
	 			   from geo.ancora_geo order by 1,2
	 			) as a03 
	 			  on a03.nifs=a.nif 
	 			 and a03.rownum=1
	 left join 
	 			(
	  			select nif
	  					,fecha2
	  					,hora
	  					,covid_sospecha
	  					,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.episodios
	  			 where covid_sospecha=1
	  			) as a04 
	  			  on a04.nif	= a.nif 
	  			 and a04.fecha2	= a.fecha2
	  			 and a04.hora	= a.hora
	  			 and a04.rownum	= 1
	left join 
	 			(
	  			select nif
	  					,fecha2
	  					,hora
	  					,covid_sospecha
	  					,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.episodios
	  			 where covid_confirmado=1
	  			) as a05 
	  			  on a05.nif	= a.nif 
	  			 and a05.fecha2	= a.fecha2
	  			 and a05.hora	= a.hora
	  			 and a05.rownum	= 1
	left join 
	  			(
	  			select distinct nif,1::smallint inscrito_validado2 
	  			  from fonasa.inscritos_validados_2020_corte_20190831 
	  			 where nif is not null and autorizado='X'
	  			) as a06 
	  			  on a06.nif=a.nif
	left join  
  	  			(
  	  			select nif2,edad_en_agnios(now(),nacimiento) as edad_en_agnios
  	  					,row_number() over (partition by nif2 order by fechalta) rownum
  	  			  from admomi.iddpacpa3 
  	  			) as a07 
  	  			  on a07.nif2=a.nif	  			 
  	  			 and a07.rownum=1
	left join	(
				select nif2,fecha2,hora,RESULTADO,descripcion_exa from covid19.resultado_examen
				) as a08
				  on a08.nif2	= a.nif 
				 and a08.fecha2	= a.fecha2
				 and a08.hora	= a.hora
	left join 
	 			(
	  			select nif
	  					,fecha2
	  					,hora
	  					,covid_sospecha
	  					,row_number() over (partition by nif,fecha2,hora) rownum 
	  			  from covid19.episodios
	  			 where covid_caso_probable=1
	  			) as a09 
	  			  on a09.nif	= a.nif 
	  			 and a09.fecha2	= a.fecha2
	  			 and a09.hora	= a.hora
	  			 and a09.rownum	= 1
; 

drop table if exists covid19.hechos5; 
create table covid19.hechos5 as   			 			  
with consulta as (
select a.*,a08.wproto2,a08.protocolo,a08.nif2 as id_paciente,a08.clinico as clinico_run,a08.clinico_nombre 
  from covid19.hechos4 as a 
 left join
  	 			(
  	 			select   md5(aa.nif2::text||'-'||aa.nif2::text) as nif,aa.nif2
  	 					,aa.fecha2
  	 					,aa.hora2
  	 					,md5(bb.id::text||'-'||bb.id::text) as medico,bb.id as clinico,bb.med_nombre as clinico_nombre
  	 					,aa.wproto2
  	 					,replace(cc.wprotocolo,'.','') as protocolo
  	 					,row_number() over (partition by aa.nif2,aa.fecha2,aa.hora2,bb.id order by aa.wproto2) as rownum
  	 			  from covid19.todos_los_protocolos as aa,admomi.iddmed as bb,admomi.iddco5 as cc
  	 			  where aa.medico=bb.medico
  	 			    and aa.wproto2=cc.wproto
  	 			) as a08 
  	 			  on a08.nif 	= a.nif
  	 			 and a08.fecha2	= a.fecha2
  	 			 and a08.hora2	= a.hora2
  	 			 and a08.medico	= a.medico
  	 			 and a08.rownum	= 1
  	where a.pac_centro2 in (1,2,3)
) select distinct a.* from consulta as a;




drop table if exists riesgo_x_pcr;
create temp table riesgo_x_pcr as
select
ome.numorden
,replace(pac.centro,'00000000','MTC') as centro
,pac.nif
,ome.fechap as fecha_orden
        ,fechap as fecha_orden2 --,convert (char(10),(convert(datetime,'28/12/1800',103)+ome.fechap),103) as fecha_orden2
--,convert(char (5),convert(smalldatetime,(convert(char(2),(convert(int,(convert(decimal (10,3),ome.HORAp)/360000))))+ ':' + convert(char(2),((convert(int,((convert(decimal (10,3),ome.HORAp)/360000-(ome.HORAp)/360000))*60)))))),8) as HoraCita
  ,pac.tis as rut
  ,pac.nombre
  ,pac.apellido1
  ,pac.apellido2
  ,pac.nacimiento
  ,(ome.fecha-pac.nacimiento)/365 edad
  ,omr.texto as examen
	,case when omr.texto like '%con FDR%' then 'Alto' else 'Bajo' end as riesgo_x_pcr 
  ,dmo.texto resultado
  ,dmo.comentario
  from admomi.iddome as ome,admomi.iddoml as oml,
       admomi.iddomr as omr,admomi.iddomd as omd,
       admomi.iddpac as pac,admomi.idddmo as dmo
 where ome.numorden = oml.numorden
   and dmo.numorden=ome.numorden
   and oml.orden = omr.orden
   and omr.clave = omd.clave
   and ome.nif = pac.nif
   and omd.mostrar='1'
   and ome.clasepru = '1'
   and oml.tipolo='R'
   and omr.orden in (4549642,8473662)
   --AND omr.texto LIKE 'Coronavirus%'
   order by ome.fechap desc nulls last;


drop table if exists usr_edox.estado_de_seguimiento;
create table usr_edox.estado_de_seguimiento as
with consulta as	(
					select
						 a03.centro
						,a.nif2
						,a03.rut 
						,a03.pac_nombre_completo
						,a.fecha2 as fecha_confirmacion
						,a.fechalta2 as fecha_episodio_cierre
						,case 
							when ciap='G12' then 'Caso Confirmado'
							when ciap='G13' then 'Caso Probable'
							end as tipo_caso
						,(a.fecha2+'25 days'::interval)::date as fecha_confirmacion_mas_14dias 
						,row_number() over (partition by a.nif order by a.fecha2) caso_repetido
						,case 
							when a00.riesgo is not null then a00.riesgo else 'No registrado' end as riesgo
						,a07.riesgo_x_pcr
						,case 
							  when a00.riesgo is not null then a00.riesgo 
							  when a00.riesgo is null then a07.riesgo_x_pcr 
							  else 'No registrado'
							  end riesgo_calculado
						,a01.fecha_seguimiento
						,case 
							when a01.fecha_seguimiento is not null then now()::date-a01.fecha_seguimiento::Date end as latencia_seguimiento 
						,now()::date as fecha_hoy
						,a02.usuario_egresa
						,a02.fechr as usuario_egresa_fecha
						,a04.usuario_egresa_causa
						,a05.tiempo_proximo_seguimiento
						,case 
						    when a01.fecha_seguimiento is null then '1 days'::interval 
							when a05.tiempo_proximo_seguimiento is null and (a00.riesgo = 'Alto' or a07.riesgo_x_pcr = 'Alto') 	then '2 days'::interval 
							when a05.tiempo_proximo_seguimiento is null and a00.riesgo = 'Medio' 		 						then '3 days'::interval 
							when a05.tiempo_proximo_seguimiento is null and (a00.riesgo = 'Bajo'  or a07.riesgo_x_pcr = 'Bajo')	then '4 days'::interval
							when a05.tiempo_proximo_seguimiento is null and a00.riesgo = 'No registrado'						then '4 days'::interval
							when a05.tiempo_proximo_seguimiento is null 														then '4 days'::interval	
							else a05.tiempo_proximo_seguimiento
							end as tiempo_proximo_seguimiento_calculado
						,a06.clinico
						,a06.clinico_estamento
				  from admomi.iddncu as a 
				  left join	(
							 select nif2,fecha
				  					,case 
				  						when campo='1' then 'Alto'
				  						when campo='2' then 'Medio'
				  						when campo='3' then 'Bajo'
				  						end as riesgo
				  					,row_number() 
				  						over (partition by nif2 
				  								  order by fecha desc) rownum
				  			  from protocolos_omi_covid_base.iddcop 
				  			 where wproto=409 
				  			   and wnumero=534 
				  			   and campo is not null 
				  			   and campo<>''
				  			) as a00 
				  			  on a00.nif2	= a.nif2 
				  		     and a00.rownum	= 1	
				  left join (
				   			select nif,max(fechr) as fecha_seguimiento
				   			  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409	
				   			  group by 1
				   			) as a01 on a01.nif=a.nif2
				  left join (
				  			select nif,fechr,'SI' usuario_egresa
				  					,row_number() over (partition by nif order by fechr desc) rownum
				  			  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409
				  			 where usuario_egresa_wn_0219_t_v is not null
				  			) as a02
				  			  on a02.nif	= a.nif2
				  			 and a02.rownum	= 1
				  left join (
				  			select nif2
				  					,centro
				  					,pac_nombre_completo
				  					,tis as rut
				  					,row_number() over (partition by nif2 order by fechalta) rownum
				  			  from admomi.iddpacpa3 
				  			) as a03
				  			  on a03.nif2	= a.nif2
				  			 and a03.rownum	= 1
				  				  left join (
				  			select nif,fechr,causaegresosegcovid19_wn_0064_t_x usuario_egresa_causa
				  					,row_number() over (partition by nif order by fechr desc) rownum
				  			  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409
				  			 where causaegresosegcovid19_wn_0064_t_x is not null
				  			) as a04
				  			  on a04.nif	= a.nif2
				  			 and a04.rownum	= 1	
				  left join
				  			(
				  			select nif,fecha,row_number() over (partition by nif order by fecha desc ) as rownum
				  			,(substr(tiemposeguimientocovid19_wn_0054_t_x,1,1)||' days')::interval as tiempo_proximo_seguimiento
				  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 
				  		   where tiemposeguimientocovid19_wn_0054_t_x is not null 
				  		    order by 1,2
				  			) as a05
				  			  on a05.nif	= a.nif2 --and a.fecha2=a05.fecha
				  			 and a05.rownum	= 1	
  				  left join
				  			(
				  			select estamento clinico_estamento,med_nombre as clinico,nif,fecha,row_number() over (partition by nif order by fecha desc ) as rownum
				  			,(substr(tiemposeguimientocovid19_wn_0054_t_x,1,1)||' days')::interval as tiempo_proximo_seguimiento
				  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409  
				  		    order by 1,2
				  			) as a06
				  			  on a06.nif	= a.nif2 --and a.fecha2=a05.fecha
				  			 and a06.rownum	= 1	
			 	left join 	(
			 				select nif,fecha_orden,riesgo_x_pcr
			 						,row_number() over (partition by nif order by fecha_orden desc) as rownum
			 				  from riesgo_x_pcr order by nif,fecha_orden desc nulls last
			 				) as a07 on a07.nif=a.nif and a07.rownum=1
				 where a.ciap in ('G12','G13') --and fechalta is not null --order by 7 desc
				)
				select a.*
						,case when fecha_seguimiento is not null then 	(tiempo_proximo_seguimiento_calculado+fecha_seguimiento)::date 
								else									(tiempo_proximo_seguimiento_calculado+fecha_confirmacion)::date 
								end as fecha_proximo_seguimiento
					    ,case 
					    	when a.fecha_seguimiento is not null and a.latencia_seguimiento<4  										     then '1. Seguimiento menor a 4 días'
					    	when a.fecha_seguimiento is not null and a.latencia_seguimiento>=4 and a.fecha_confirmacion_mas_14dias<now() then '2. Seguimiento mayor o igual a 4 días pero con alta probable (Alerta)'
					    	when a.fecha_seguimiento is not null and a.latencia_seguimiento>=4 											 then '3. Seguimiento mayor o igual a 4 días (Alerta)'
							when a.fecha_seguimiento is null and a.fecha_confirmacion_mas_14dias<now() 									 then '4. Sin Seguimiento pero alta probable (Alerta)'
					    	when a.fecha_seguimiento is null and a.fecha_confirmacion_mas_14dias>now() 									 then '5. Sin Seguimiento sin alta (Alerta)'
					    	else ''
							end	as estado_seguimiento				    
				  from consulta as a --where a.usuario_egresa_fecha is null and fecha_episodio_cierre is null
				  ;

select a.*
		,now()::date - fecha_proximo_seguimiento diferencia_fecha_actual_prox_seguimiento
		,case when now()::date - fecha_proximo_seguimiento<=0 then 'Al día' else 'Atrasado' end estado_de_seguimiento   
  from usr_edox.estado_de_seguimiento as a 
 ;--where a.usuario_egresa_fecha is null 
   --and fecha_episodio_cierre is null;-- and tipo_caso = 'Caso Confirmado';

--select nif,fecha from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 order by fecha desc;


drop table if exists protocolos_omi_covid.estado_de_seguimiento02;
create table protocolos_omi_covid.estado_de_seguimiento02 as			--Estado de Seguimientos
with consulta as (
select distinct a.*
		,now()::date - fecha_proximo_seguimiento diferencia_fecha_actual_prox_seguimiento
		,case when now()::date - fecha_proximo_seguimiento<=0 then 'Al día' else 'Atrasado' end estado_de_seguimiento   
  from usr_edox.estado_de_seguimiento as a 
 where a.usuario_egresa_fecha is null 
   and a.fecha_episodio_cierre is null
   )
   select a.* --,a01.neotel 
     from consulta as a;

GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO ugina;    
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO consultaomi;    
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO ugina;
GRANT ALL ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO postgres;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO ehenriquez;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO consultaomi;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO jesoto;    

drop table if exists protocolos_omi_covid.estado_de_seguimiento03;
create table protocolos_omi_covid.estado_de_seguimiento03 as
select  
	centro,
	nif2,
	rut,
	pac_nombre_completo,
	fecha_confirmacion,
	fecha_episodio_cierre,
	tipo_caso,
	fecha_confirmacion_mas_14dias as fecha_confirmacion_mas_25dias,
	caso_repetido,
	riesgo,
	riesgo_x_pcr,
	riesgo_calculado,
	fecha_seguimiento,
	latencia_seguimiento,
	fecha_hoy,
	usuario_egresa,
	usuario_egresa_fecha,
	usuario_egresa_causa,
	tiempo_proximo_seguimiento::text as tiempo_proximo_seguimiento,
	tiempo_proximo_seguimiento_calculado::text as tiempo_proximo_seguimiento_calculado,
	clinico,
	clinico_estamento,
	fecha_proximo_seguimiento,
	estado_seguimiento,
	diferencia_fecha_actual_prox_seguimiento,
	estado_de_seguimiento
from protocolos_omi_covid.estado_de_seguimiento02;

GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 TO ugina;    
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 TO consultaomi;    
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 TO ugina;
GRANT ALL ON TABLE protocolos_omi_covid.estado_de_seguimiento03 TO postgres;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 TO ehenriquez;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 TO consultaomi;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 TO jesoto; 


----------------------------------------
-- indicadores para tableau
----------------------------------------
drop table if exists covid19.hechos_examenes_pcr;create table covid19.hechos_examenes_pcr as
with consulta as	(
 select distinct a.*
		,case 
			when a.fecha_apertura_episodio_confirmacion<a.fecha_identificacion_contactos 
				then (a.fecha_apertura_episodio_confirmacion-fecha_recepcion)::integer 
				else (a.fecha_identificacion_contactos-fecha_recepcion)::integer
				end as latencia1
  from
		(
		select   distinct 
		         a01.centro
				,a01.nif2
				,a.patient_rut as rut
				,a.orderdate as fecha_solicitud
				,a.receptiondate as fecha_recepcion
				,min(a04.q) over (partition by a04.tis) as es_confirmado
				,min(a02.fecha2) over (partition by a02.tis,a.receptiondate) as fecha_apertura_episodio_confirmacion --as positiva --2977
				,min(a02.fecha2) over (partition by a02.tis,a.receptiondate) as fecha_nutificacion --as positiva --2977
				,case 
					when  a03.fecha2 is not null 	then min(a03.fecha2) over (partition by a03.tis,a.receptiondate) 
													else min(a02.fecha2) over (partition by a02.tis,a.receptiondate)
				end as fecha_identificacion_contactos
				,a.interpretedresult as resultado_exacto
				,case 
					when lower(a.interpretedresult) like '%indeterminado%' 	then 'Positivo' 
					when lower(a.interpretedresult) like '%Pendiente%' 		then 'Positivo'
					else a.interpretedresult end as resultado
				,1::integer as q
		  		FROM 
		  				(
		  				select distinct patient_rut,orderdate::Date orderdate,receptiondate::date receptiondate,interpretedresult 
		  				from examenes_etl.mg_vw_ancora2
		  				where profilecode = '2798H'
		  				) as a
		  left join (
		  			select centro,tis,nif2 
		  					,row_number() over (partition by tis order by fechalta) rownum
		  			  from admomi.iddpacpa3 where tis like '%-%' and estado2 is not null
		  			) as a01 on a01.tis=a.patient_rut and a01.rownum =1 --2920
		  left join 
		  			( --confirmación
		  			select pac.tis,ncu.fecha2,ncu.ciap
		  					,row_number() over (partition by pac.tis) rownum
		  			  from admomi.iddncu as ncu,admomi.iddpacpa3 as pac
		  			 where ncu.ciap in ('G12','G13')
		  			   and ncu.nif2=pac.nif2 
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null union
		  			select pac.tis,ncu.fechalta2,ncu.ciap
		 				   ,row_number() over (partition by pac.tis) rownum
		  			  from admomi.iddncu as ncu,admomi.iddpacpa3 as pac
		  			 where ncu.ciap in ('G10')
		  			   and ncu.nif2=pac.nif2 and ncu.fechalta2 is not null
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null
		  			) as a02 
		  			  on a02.tis=a.patient_rut 
		  			 and a02.rownum=1  
		  			 and a02.fecha2>=a.orderdate
		  left join 
		  			( --seguimiento
		  			select pac.tis
		  					,seg.fecha as fecha2
		  					,row_number() over (partition by pac.tis) rownum 
		  			  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 seg
		  			  		,admomi.iddpacpa3 as pac
		  			 where seg.nif=pac.nif2
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null
		  			) as a03
		  			  on a03.tis=a.patient_rut
		  			 and a03.rownum=1
		  			 and a03.fecha2>=a.orderdate
		    left join 
		  			( --es_confirmación
		  			select pac.tis,ncu.fecha2,ncu.ciap,1::integer as q
		  					,row_number() over (partition by pac.tis) rownum
		  			  from admomi.iddncu as ncu,admomi.iddpacpa3 as pac
		  			 where ncu.ciap in ('G12')
		  			   and ncu.nif2=pac.nif2 
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null
		  			) as a04 
		  			  on a04.tis=a.patient_rut 
		  			 and a04.rownum=1  
		  			 and a04.fecha2>=a.orderdate
		  order by a.orderdate desc nulls last
		 ) as a --where es_confirmado=1
		)
	select distinct case when latencia1<=2 then 1 else 0 end as latencia1_kpi,* --((r_quantile(array_accum(latencia1),0.75) - r_quantile(array_accum(latencia1),0.25) )*1.5)+(r_quantile(array_accum(latencia1),0.75)) as iqr2
      from consulta;

-----------------------------------------------------------------------------------------------------------------------------------------------------------
-- CUBO COVID19 CARGA NEOTEL
-----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists seguimiento_covid_hechos01;
create temp table    seguimiento_covid_hechos01 as
select nif2,fecha2 from  protocolos_omi_covid_base.iddncu where ciap in ('G10','G12','G13','G14') group by 1,2;

drop table if exists protocolos_omi_covid_base.datos_contactos01; 
create table    protocolos_omi_covid_base.datos_contactos01 as
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'NOMBRE CONTACTO' as dato,upper(nombre_contacto_01_wn_0221_t_t) as VALOR		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_01_wn_0221_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_02_wn_0222_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_02_wn_0222_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_03_wn_0223_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_03_wn_0223_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_04_wn_0224_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_04_wn_0224_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_05_wn_0225_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_05_wn_0225_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_06_wn_0226_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_06_wn_0226_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_07_wn_0227_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_07_wn_0227_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_08_wn_0228_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_08_wn_0228_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'RUT CONTACTO',upper(rut_contacto_01_wn_0232_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_01_wn_0232_t_t 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'RUT CONTACTO',upper(rut_contacto_02_wn_0233_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_02_wn_0233_t_t 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'RUT CONTACTO',upper(rut_contacto_03_wn_0234_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_03_wn_0234_t_t						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'RUT CONTACTO',upper(rut_contacto_04_wn_0235_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_04_wn_0235_t_t 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'RUT CONTACTO',upper(rut_contacto_05_wn_0236_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_05_wn_0236_t_t 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'RUT CONTACTO',upper(rut_contacto_06_wn_0237_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_06_wn_0237_t_t 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'RUT CONTACTO',upper(rut_contacto_07_wn_0238_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_07_wn_0238_t_t 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'RUT CONTACTO',upper(rut_contacto_08_wn_0239_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_08_wn_0239_t_t 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_01_wn_0409_t_x)  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_01_wn_0409_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_02_wn_0410_t_x) 	 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_02_wn_0410_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_03_wn_0411_t_x)  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_03_wn_0411_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_04_wn_0412_t_x)	  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_04_wn_0412_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_05_wn_0413_t_x)  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_05_wn_0413_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_06_wn_0414_t_x)  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_06_wn_0414_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_07_wn_0415_t_x)	  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409	where tiene_sisntomas_contacto_07_wn_0415_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_08_wn_0416_t_x)  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_08_wn_0416_t_x 			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'EDAD CONTACTO',edad_contacto_01_wn_0356_t_n::text as valor 					from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_01_wn_0356_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'EDAD CONTACTO',edad_contacto_02_wn_0357_t_n::text as valor 					from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_02_wn_0357_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'EDAD CONTACTO',edad_contacto_03_wn_0358_t_n::text as valor 					from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_03_wn_0358_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'EDAD CONTACTO',edad_contacto_04_wn_0359_t_n::text as valor 					from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_04_wn_0359_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'EDAD CONTACTO',edad_contacto_05_wn_0360_t_n::text   						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_05_wn_0360_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'EDAD CONTACTO',edad_contacto_06_wn_0361_t_n::text   						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_06_wn_0361_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'EDAD CONTACTO',edad_contacto_07_wn_0362_t_n::text   						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_07_wn_0362_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'EDAD CONTACTO',edad_contacto_08_wn_0363_t_n::text 		  					from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_08_wn_0363_t_n 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_01_wn_0345_t_x)  as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_01_wn_0345_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_02_wn_0346_t_x)  as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_02_wn_0346_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_03_wn_0347_t_x)  as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_03_wn_0347_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_04_wn_0348_t_x)  as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_04_wn_0348_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_05_wn_0349_t_x)    						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_05_wn_0349_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_06_wn_0350_t_x)    						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_06_wn_0350_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_07_wn_0351_t_x)    						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_07_wn_0351_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_08_wn_0352_t_x) 	  						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_08_wn_0352_t_x 						is not null union
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_01_wn_0399_t_x)  				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_01_wn_0399_t_x 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_02_wn_0400_t_x)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_02_wn_0400_t_x 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_03_wn_0401_t_x)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_03_wn_0401_t_x 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_04_wn_0402_t_x)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_04_wn_0402_t_x 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_05_wn_0403_t_x)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_05_wn_0403_t_x 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_06_wn_0404_t_x)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_06_wn_0404_t_x					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_07_wn_0405_t_x)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_07_wn_0405_t_x 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_08_wn_0406_t_x) 	  				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_08_wn_0406_t_x 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_01_wn_0243_t_t)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_01_wn_0243_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_02_wn_0245_t_t)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_02_wn_0245_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_03_wn_0246_t_t)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_03_wn_0246_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_04_wn_0247_t_t)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_04_wn_0247_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_05_wn_0248_t_t)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_05_wn_0248_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_06_wn_0249_t_t)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_06_wn_0249_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_07_wn_0250_t_t)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_07_wn_0250_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_08_wn_0251_t_t) 	  				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_08_wn_0251_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso01' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_01_wn_0433_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_01_wn_0433_t_x			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso02' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_02_wn_0434_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_02_wn_0434_t_x			is not null union
select nif,fecha,null::date as fechalta,especiali,'caso03' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_03_wn_0435_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_03_wn_0435_t_x 		is not null union
select nif,fecha,null::date as fechalta,especiali,'caso04' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_04_wn_0436_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_04_wn_0436_t_x 		is not null union
select nif,fecha,null::date as fechalta,especiali,'caso05' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_05_wn_0437_t_x)	    	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_05_wn_0437_t_x 		is not null union
select nif,fecha,null::date as fechalta,especiali,'caso06' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_06_wn_0438_t_x) 	   	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_06_wn_0438_t_x 		is not null union
select nif,fecha,null::date as fechalta,especiali,'caso07' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_07_wn_0439_t_x)    		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_07_wn_0439_t_x 		is not null union
select nif,fecha,null::date as fechalta,especiali,'caso08' as num_caso,'PLAN INDICACIONES',upper(plan_indicaciones_contacto_08_wn_0440_t_x) 	  	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where plan_indicaciones_contacto_08_wn_0440_t_x 		is not null union
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'MODALIDAD DE CONTACTO',upper(modalidad_de_contacto_wn_0189_t_x) 	  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where modalidad_de_contacto_wn_0189_t_x 				is not null union
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'VIA DE CONTACTO',upper(via_de_contacto_wn_0191_t_x)		 	  				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where via_de_contacto_wn_0191_t_x 						is not null union 	
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'NOTIFICACION',upper(estado_notificacion_resultado_wn_0532_t_x)	  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where estado_notificacion_resultado_wn_0532_t_x			is not null union  	
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'FECHA INICIO SINTOMAS',upper(fechainiciosintomas_wn_0085_t_f::text)			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where fechainiciosintomas_wn_0085_t_f					is not null union --fecha_probable_fin_de_cuarentena_wn_0528_t_f
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'FECHA FIN',upper(fecha_probable_fin_de_cuarentena_wn_0528_t_f::text)		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where fecha_probable_fin_de_cuarentena_wn_0528_t_f		is not null union --fecha_probable_fin_de_cuarentena_wn_0528_t_f
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'TELEFONO01',upper(numero_de_telefono_1_wn_0195_t_t)		 	  				from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where numero_de_telefono_1_wn_0195_t_t 					is not null union
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'TELEFONO02',upper(numero_de_telefono_2_wn_0197_t_t)		 	  				from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where numero_de_telefono_2_wn_0197_t_t 					is not null union --fecha_primer_sintoma_wn_0179_t_f
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'TELEFONO01',upper(telefono_de_contacto_seguimiento_1_wn_0328_t_n::Text)		from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where telefono_de_contacto_seguimiento_1_wn_0328_t_n	is not null union
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'TELEFONO02',upper(telefono_de_contacto_seguimiento_2_wn_0334_t_n::text)		from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where telefono_de_contacto_seguimiento_2_wn_0334_t_n 	is not null union --fecha_primer_sintoma_wn_0179_t_f
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'TELEFONO01',upper(telefono_de_contacto_seguimiento_wn_0601_t_n::text)		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_de_contacto_seguimiento_wn_0601_t_n		is not null union --fecha_probable_fin_de_cuarentena_wn_0528_t_f
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'FECHA INICIO SINTOMAS',upper(fecha_primer_sintoma_wn_0179_t_f::text)		from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where fecha_primer_sintoma_wn_0179_t_f 					is not null union --fecha_primer_sintoma_wn_0179_t_f
select a.nif,a.fecha,null::date as fechalta,a.especiali,'caso00' as num_caso,'RUT CASO',upper(b.tis)		 	  		
  from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 as a,admomi.iddpacpa3 as b
 where a.nif=b.nif2 union 
select a.nif,a.fecha,null::date as fechalta,a.especiali,'caso00' as num_caso,'RUT CASO',upper(b.tis)		 	  		
  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 as a,admomi.iddpacpa3 as b
 where a.nif=b.nif2 union
select a.nif2,a.fecha2,a.fechalta::date fechalta,a.especiali,'caso00' as num_caso,'RUT CASO',(b.tis) 
  from  protocolos_omi_covid_base.iddncu as a,admomi.iddpacpa3 as b
 where a.ciap in ('G12','G13') and a.nif=b.nif union
select a.nif2,a.fecha2,a.fechalta::date fechalta,a.especiali,'caso00' as num_caso,'EPISODIO','SOSPECHA COVID-19' from  protocolos_omi_covid_base.iddncu as a,admomi.iddpacpa3 as b where a.ciap in ('G10') and a.nif=b.nif	union
select a.nif2,a.fecha2,a.fechalta::date fechalta,a.especiali,'caso00' as num_caso,'EPISODIO','CONFIRMADO COVID-19' from  protocolos_omi_covid_base.iddncu as a,admomi.iddpacpa3 as b where a.ciap in ('G12') and a.nif=b.nif	union
select a.nif2,a.fecha2,a.fechalta::date fechalta,a.especiali,'caso00' as num_caso,'EPISODIO','CASO PROBABLE COVID-19' from  protocolos_omi_covid_base.iddncu as a,admomi.iddpacpa3 as b where a.ciap in ('G13') and a.nif=b.nif	union
select a.nif2,a.fecha2,a.fechalta::date fechalta,a.especiali,'caso00' as num_caso,'EPISODIO','BUSQUEDA ACTIVA COVID-19' from  protocolos_omi_covid_base.iddncu as a,admomi.iddpacpa3 as b where a.ciap in ('G14') and a.nif=b.nif	
union
select nif,fecha,null::date as fechalta,especiali,'caso00' as num_caso,'TIENE CONTACTOS ESTRECHOS','NO'							 	  				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where no_tiene_contacto_estrechos_wn_0606_t_v=1			is not null
;

alter table protocolos_omi_covid_base.datos_contactos01 add column rut_nif 		text;
alter table protocolos_omi_covid_base.datos_contactos01 add column rut_num_caso text;
alter table protocolos_omi_covid_base.datos_contactos01 add column centro 		text;

 
update protocolos_omi_covid_base.datos_contactos01 set valor=BTRIM(upper(valor));
-- estado_notificacion_resultado
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'.','')		where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,',','')		where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'--','-')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,':','')		where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G10T','')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G12T','')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G13T','')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'*','')		where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'/','')		where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'+','')		where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,' ','')		where dato in ('RUT CASO','RUT CONTACTO');
  
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'.','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,',','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'--','-')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'-','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,':','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G10T','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G12T','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G13T','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'*','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'/','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'+','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,' ','')		where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');

with consulta as	(
					select centro,nif2,tis
						,row_number() over (partition by nif2 order by fechalta) rownum
				      from admomi.iddpacpa3 
				     where tis is not null and tis<>'' and tis like '%-%'
				       and estado2 is not null
					)
update protocolos_omi_covid_base.datos_contactos01 
   set rut_nif=consulta.tis,centro=consulta.centro
  from consulta
 where consulta.nif2=protocolos_omi_covid_base.datos_contactos01.nif
   and consulta.rownum=1;

--select * from protocolos_omi_covid_base.datos_contactos01 
  
update protocolos_omi_covid_base.datos_contactos01 --el nif=paciente indice, es igual a caso num_caso='caso00'  
   set rut_num_caso=rut_nif
 where num_caso='caso00';
  
update protocolos_omi_covid_base.datos_contactos01 
   set rut_num_caso=valor 
 where dato='RUT CONTACTO';

with consulta as 	(
					select distinct nif,num_caso,valor,fecha
					  from protocolos_omi_covid_base.datos_contactos01
					 where valor is not null and valor<>''
					   and dato='RUT CONTACTO'
					)
update protocolos_omi_covid_base.datos_contactos01 --el nif=paciente indice, es igual a caso num_caso='caso00'  
   set rut_num_caso=consulta.valor
  from consulta
 where protocolos_omi_covid_base.datos_contactos01.num_caso<>'caso00'
   and protocolos_omi_covid_base.datos_contactos01.rut_num_caso is null
   and protocolos_omi_covid_base.datos_contactos01.nif=consulta.nif;

delete from protocolos_omi_covid_base.datos_contactos01 where nif in (select nif2 from admomi.iddpacpa3 where estado2 is null);

--select * from protocolos_omi_covid_base.datos_contactos01 where fecha>='20200701' order by 1,2,5;
--select distinct nif from protocolos_omi_covid_base.datos_contactos01 where fecha>='20200701' and rut_num_caso is null and num_caso<>'caso00'
--select count(*) from protocolos_omi_covid_base.datos_contactos01

create index datos_contactos01_i_01 on protocolos_omi_covid_base.datos_contactos01(nif)									tablespace tb_index;
create index datos_contactos01_i_02 on protocolos_omi_covid_base.datos_contactos01(nif,centro)							tablespace tb_index;
create index datos_contactos01_i_03 on protocolos_omi_covid_base.datos_contactos01(fecha)								tablespace tb_index;
create index datos_contactos01_i_04 on protocolos_omi_covid_base.datos_contactos01(num_caso)							tablespace tb_index;
create index datos_contactos01_i_05 on protocolos_omi_covid_base.datos_contactos01(dato)								tablespace tb_index;
create index datos_contactos01_i_06 on protocolos_omi_covid_base.datos_contactos01(dato)								tablespace tb_index;
create index datos_contactos01_i_07 on protocolos_omi_covid_base.datos_contactos01(valor)								tablespace tb_index;
create index datos_contactos01_i_08 on protocolos_omi_covid_base.datos_contactos01(nif,num_caso)						tablespace tb_index;
create index datos_contactos01_i_09 on protocolos_omi_covid_base.datos_contactos01(nif,fecha,num_caso)					tablespace tb_index;
create index datos_contactos01_i_10 on protocolos_omi_covid_base.datos_contactos01(nif,fecha,num_caso,centro)			tablespace tb_index;
create index datos_contactos01_i_11 on protocolos_omi_covid_base.datos_contactos01(rut_nif)								tablespace tb_index;
create index datos_contactos01_i_12 on protocolos_omi_covid_base.datos_contactos01(rut_num_caso)						tablespace tb_index;
create index datos_contactos01_i_13 on protocolos_omi_covid_base.datos_contactos01(fechalta)							tablespace tb_index;
create index datos_contactos01_i_14 on protocolos_omi_covid_base.datos_contactos01(rut_nif,num_caso)					tablespace tb_index;
create index datos_contactos01_i_15 on protocolos_omi_covid_base.datos_contactos01(rut_nif,fecha,num_caso)				tablespace tb_index;
create index datos_contactos01_i_16 on protocolos_omi_covid_base.datos_contactos01(rut_num_caso,num_caso)				tablespace tb_index;
create index datos_contactos01_i_17 on protocolos_omi_covid_base.datos_contactos01(rut_num_caso,fecha,num_caso)			tablespace tb_index;
create index datos_contactos01_i_18 on protocolos_omi_covid_base.datos_contactos01(centro,rut_nif,num_caso)				tablespace tb_index;
create index datos_contactos01_i_19 on protocolos_omi_covid_base.datos_contactos01(centro,rut_nif,fecha,num_caso)		tablespace tb_index;
create index datos_contactos01_i_20 on protocolos_omi_covid_base.datos_contactos01(centro,rut_num_caso,num_caso)		tablespace tb_index;
create index datos_contactos01_i_21 on protocolos_omi_covid_base.datos_contactos01(centro,rut_num_caso,fecha,num_caso)	tablespace tb_index;

--select * from protocolos_omi_covid_base.datos_contactos01
--select count(*) from protocolos_omi_covid_base.datos_contactos01

drop table if exists seguimiento_covid_hechos02;
create temp table    seguimiento_covid_hechos02 as --select * from seguimiento_covid_hechos02
select distinct 
		 a01.centro	
		, a.nif2
		,a.fecha2
		,a01.tis		as caso_positivo_rut
		,a01.nombre		as caso_positivo_nomnre
		,a01.apellido1	as caso_positivo_apellido1
		,a01.apellido2	as caso_positivo_apellido2
		,a01.caso_positivo_domicilio
		,a01.caso_positivo_comuna
		,a02.fecha 		as fecha_seguimiento
		,a03.num_caso	
  from seguimiento_covid_hechos01 as a --select * from seguimiento_covid_hechos01 where nif2=84310   
  left join	(
  			select centro,nif2,tis,nombre,apellido1,apellido2
  					,coalesce(domicilio,' ')||' '||coalesce(tiscab,' ') as caso_positivo_domicilio,comuna as caso_positivo_comuna
  					,row_number() over (partition by nif order by fechalta) rownum
  			  from admomi.iddpacpa3 
  			) as a01
  			  on a01.nif2	= a.nif2
  			 and a01.rownum	= 1
 left join 	(
 			select distinct nif,fecha,especiali from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409
 			) as a02 
 			  on a02.nif=a.nif2 
 left join	(
			select distinct nif,num_caso from protocolos_omi_covid_base.datos_contactos01
			) as a03
			  on a03.nif=a.nif2;

drop 	table if exists  protocolos_omi_covid.seguimiento_covid_hechos03; --select * from  protocolos_omi_covid.seguimiento_covid_hechos03 where nif2=84310
create	table    		 protocolos_omi_covid.seguimiento_covid_hechos03 as	 
select a.*
		,a01.contacto_nombre
		,a02.contacto_rut
		,a03.contacto_tiene_sintomas
		,a04.contacto_edad
		,a05.contacto_sexo
		,a06.contacto_embarazado
		,a07.contacto_telefono
		,a08.fecha_inicio_cuarentena
		,(a08.fecha_inicio_cuarentena+'14 days'::interval)::date as fecha_termino_cuarentena
		,'APS '||coalesce(a09.contacto_modalidad,'')||' '||coalesce(a10.contacto_via,'') as responsable_del_seguimiento
		,null::text as CUMPLE_PERFIL_CASO_SOSPECHOSO
		,null::text as FECHA_TOMA_DE_MUESTRA
		,null::text as RESULTADO_DEL_EXAMEN
		,null::text as FECHA_CONFIRMACION
		,null::text as LUGAR_DE_DERIVACION_SI_SE_CONFIRMA
  from seguimiento_covid_hechos02 as a
  left join (
			select nif,num_caso,fecha,valor as contacto_nombre 
					,row_number() over (partition by nif,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='NOMBRE CONTACTO'
			   and valor<>'' and valor is not null
			) as a01
			  on a01.nif		= a.nif2
			 and a01.num_caso	= a.num_caso
			 and a01.rownum		= 1
  left join (
			select nif,num_caso,fecha,valor as contacto_rut
					,row_number() over (partition by nif,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='RUT CONTACTO'
			   and valor<>'' and valor is not null
			) as a02
			  on a02.nif		= a.nif2
			 and a02.num_caso	= a.num_caso
			 and a02.rownum		= 1		
  left join (
			select nif,num_caso,fecha,valor as contacto_tiene_sintomas
					,row_number() over (partition by nif,fecha,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='SINTOMAS CONTACTO'
			   and valor<>'' and valor is not null
			) as a03
			  on a03.nif		= a.nif2
			 and a03.num_caso	= a.num_caso
			 and a03.fecha		= a.fecha2
			 and a03.rownum		= 1		
  left join (
			select nif,num_caso,fecha,valor as contacto_edad
					,row_number() over (partition by nif,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='EDAD CONTACTO'
			   and valor<>'' and valor is not null
			) as a04
			  on a04.nif		= a.nif2
			 and a04.num_caso	= a.num_caso
			 and a04.rownum		= 1
left join 	(
			select nif,num_caso,fecha,valor as contacto_sexo
					,row_number() over (partition by nif,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='SEXO CONTACTO'
			   and valor<>'' and valor is not null
			) as a05
			  on a05.nif		= a.nif2
			 and a05.num_caso	= a.num_caso
			 and a05.rownum		= 1
left join 	(
			select nif,num_caso,fecha,valor as contacto_embarazado
					,row_number() over (partition by nif,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='EMBARAZO CONTACTO'
			   and valor<>'' and valor is not null
			) as a06
			  on a06.nif		= a.nif2
			 and a06.num_caso	= a.num_caso
			 and a06.rownum		= 1
left join 	(
			select nif,num_caso,fecha,valor as contacto_telefono
					,row_number() over (partition by nif,fecha,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='TELEFONO CONTACTO'
			   and valor<>'' and valor is not null
			) as a07
			  on a07.nif		= a.nif2
			 and a07.num_caso	= a.num_caso
			 and a07.fecha		= a.fecha2
			 and a07.rownum		= 1
left join 	(
			select nif2,fecha2 as fecha_inicio_cuarentena
					,row_number() over (partition by nif,fecha2 order by fecha2) rownum
			  from  protocolos_omi_covid_base.iddncu where ciap in ('G12','G13')  
			) as a08
			  on a08.nif2=a.nif2 
			 and a08.rownum=1
left join 	(
			select nif,num_caso,fecha,valor as contacto_modalidad
					,row_number() over (partition by nif,fecha,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='MODALIDAD DE CONTACTO'
			   and valor<>'' and valor is not null
			) as a09
			  on a09.nif		= a.nif2
			 and a09.num_caso	= a.num_caso
			 and a09.fecha		= a.fecha2
			 and a09.rownum		= 1			
left join 	(
			select nif,num_caso,fecha,valor as contacto_via
					,row_number() over (partition by nif,fecha,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='VIA DE CONTACTO'
			   and valor<>'' and valor is not null
			) as a10
			  on a10.nif		= a.nif2
			 and a10.num_caso	= a.num_caso
			 and a10.fecha		= a.fecha2
			 and a10.rownum		= 1			
			 ;			

drop table if exists seguimiento_covid_hechos04; --select * from seguimiento_covid_hechos04 where nif2=84310
create temp table    seguimiento_covid_hechos04 as	 
select a.*
		,a01.domicilio  as caso_contacto_domicilio
		,a01.comuna		as caso_contacto_comuna
  from  protocolos_omi_covid.seguimiento_covid_hechos03 as a
  left join	(
  			select centro,nif2,tis,nombre,apellido1,apellido2
  					,coalesce(domicilio,' ')||' '||coalesce(tiscab,' ') as domicilio,comuna
  					,row_number() over (partition by nif order by fechalta) rownum
  			  from admomi.iddpacpa3 
  			) as a01
  			  on a01.tis	= a.contacto_rut
  			 and a01.rownum	= 1;
			
drop table if exists protocolos_omi_covid.listado_contactos_seremi;
create table protocolos_omi_covid.listado_contactos_seremi as 
select distinct * from seguimiento_covid_hechos04;

grant select on protocolos_omi_covid.listado_contactos_seremi to consultaomi;

--Casos Confirmados Pentaho pentaho
drop table if exists casos_confirmados_pentaho;create table casos_confirmados_pentaho as --select * from casos_confirmados_pentaho where nif=84310
 select  distinct  
		a01.centro
		,a01.nif2 as nif
		,a01.tis as rut
		,a.ciap as diagnostico_cod
		,a.descripcio diagnostico_descripcion
		,a.fecha ciap_fecha
		,a01.nombre
		,a01.apellido1
		,a01.apellido2
		,a01.sexo
		,edad_en_agnios(now(),a01.nacimiento) as edad
		,(coalesce(a01.domicilio,'')||' '||coalesce(a01.tiscab,'')) as domicilio,a01.comuna
		,a02.telefono
  from  protocolos_omi_covid_base.iddncu as a
  	left join
  				(
  				select distinct nif2,nif,centro,nacimiento,sexo,domicilio,tiscab,tis,nombre,apellido1,apellido2,comuna,
  					row_number() over (partition by nif order by fechalta) rownum
  					from admomi.iddpacpa3
  				) as a01
  				  on a01.nif=a.nif
  				 and a01.rownum=1
	left join
				(
				select aa.nif,dato,replace(replace(replace(replace(telefono,'-',''),'*',''),'/',''),'+','') as telefono
						,row_number() over (partition by nif order by dato) as rownum
				  from
				  		(
						select nif,'telefono1' as dato,upper(numero_de_telefono_1_wn_0195_t_t) as telefono		 	  		
						  from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			
						 where numero_de_telefono_1_wn_0195_t_t is not null union
						select nif,'telefono2' as dato,upper(numero_de_telefono_2_wn_0197_t_t) 
						  from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			
						 where numero_de_telefono_2_wn_0197_t_t is not null 
						) as aa where telefono<>'' and telefono is not null						
				) as a02
				  on a02.nif=a.nif2
				 and a02.rownum=1  
 where a.ciap in ('G10','G12','G13','G14') --select distinct descripcio from  protocolos_omi_covid_base.iddncu where ciap in ('G10','G12','G13')
   and a.nif not in (select nif from admomi.iddpacpa3 where estado2 is null);

update casos_confirmados_pentaho 
   set telefono=NULLIF(regexp_replace(admomi.iddpacpa3.telpart, '\D','','g'), '')::numeric
  from admomi.iddpacpa3 
 where casos_confirmados_pentaho.telefono is null		
   and casos_confirmados_pentaho.nif=admomi.iddpacpa3.nif2;

update casos_confirmados_pentaho 
   set telefono=NULLIF(regexp_replace(admomi.iddpacpa3.teldesp, '\D','','g'), '')::numeric
  from admomi.iddpacpa3 
 where casos_confirmados_pentaho.telefono is null		
   and casos_confirmados_pentaho.nif=admomi.iddpacpa3.nif2;  
  
--select * from casos_confirmados_pentaho where diagnostico_cod in ('G12','G13') AND CENTRO='JPII'

--carga NEOTEL
drop table if exists carga_neotel_01; --select * from carga_neotel_01 where nif=84310
create table carga_neotel_01 as
select a.nif,b.num_caso,b.valor as rut_contacto
  from protocolos_omi_covid_base.datos_contactos01 as a
  left join protocolos_omi_covid_base.datos_contactos01 as b on a.nif=b.nif and b.dato in ('RUT CONTACTO','RUT CASO')
 group by 1,2,3 order by 1,2; 

--select * from carga_neotel_01 where nif=95975
--select * from carga_neotel_02 where nif=597
				
drop table if exists carga_neotel_02;
create table carga_neotel_02 as
select distinct 
		 a.*
		,a01.telefono1
		,a02.riesgo
		,a00.centro
		,a03.edad
		,a04.nombre 
		,a05.contacto_embarazado as embarazada
		,case 
			when a06.diagnostico_cod='G10' then 'Sospechoso'
			when a06.diagnostico_cod='G12' then 'Confirmado'
			when a06.diagnostico_cod='G13' then 'Probable'
			when a06.diagnostico_cod='G14' then 'Pesquiza Activa'
			else 'Contacto' end as condicion
		--,a06.ciap_fecha::Date as fecha_inicio
		,a08.notificacion
		,a09.fecha_inicio::Date as fecha_inicio
		,a10.fecha_fin::Date as fecha_fin
  from carga_neotel_01 as a
   left join (
  			select nif2,tis,pac_nombre_completo,centro
  					,row_number() over (partition by nif order by fechalta) as rownum
  			  from admomi.iddpacpa3  
  			) as a00
  			  on a00.nif2=a.nif
  left join 
			(
  			select *,NULLIF(regexp_replace(valor, '\D','','g'), '')::numeric as telefono1
  					,row_number() over (partition by nif order by fecha desc) rownum
  			  from protocolos_omi_covid_base.datos_contactos01 where dato='TELEFONO01'
  			) as a01 
 			  on a01.nif=a.nif 
 			 and a01.rownum=1
 left join 	(
  			select nif2,fecha
  					,case 
  						when campo='1' then 'Alto'
  						when campo='2' then 'Medio'
  						when campo='3' then 'Bajo'
  						end as riesgo
  					,row_number() over (partition by nif2 order by fecha desc) rownum
  			  from protocolos_omi_covid_base.iddcop 
  			 where wproto=409 
  			   and wnumero=534 
  			   and campo is not null 
  			   and campo<>''
  			) as a02
  			  on a02.nif2=a.nif
  			 and a02.rownum=1
  left join (
  			select NIF,num_caso
  					,NULLIF(regexp_replace(valor, '\D','','g'), '')::numeric as edad
  					,row_number() over (partition by nif,num_caso order by fecha) rownum
  			  from protocolos_omi_covid_base.datos_contactos01 
  			 where dato='EDAD CONTACTO'
  			   and valor<>'' and valor is not null
 			) as a03
 		      on a03.nif=a.nif
 		     and a03.num_caso=a.num_caso
 		     and a03.rownum=1
   left join 
  			(
  			select nif,num_caso,valor as nombre 
  					,row_number() over (partition by nif,num_caso order by fecha) rownum
  			  from protocolos_omi_covid_base.datos_contactos01 
  			 where dato='NOMBRE CONTACTO'
  			   and valor<>'' and valor is not null
  			) as a04
  			  on a04.nif=a.nif
  			 and a04.num_caso=a.num_caso
  			 and a04.rownum=1
left join 	(
			select nif,num_caso,fecha,valor as contacto_embarazado
					,row_number() over (partition by nif,num_caso order by fecha desc) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='EMBARAZO CONTACTO'
			   and valor<>'' and valor is not null
			) as a05
			  on a05.nif		= a.nif
			 and a05.num_caso	= a.num_caso
			 and a05.rownum		= 1
left join	(
			select * 
					,row_number() over (partition by rut order by ciap_fecha desc) as rownum
			  from casos_confirmados_pentaho			  
			) as a06
			  on a06.rut = a.rut_contacto 
			 and a06.rut is not null
			 and a06.rownum=1
left join	(
			select * 
					,row_number() over (partition by rut order by ciap_fecha desc) as rownum
			  from casos_confirmados_pentaho			  
			) as a07
			  on a07.nif = a.nif 
			 and a07.rownum=1
left join 	(
			select nif,num_caso,fecha,valor as NOTIFICACION
					,row_number() over (partition by nif order by fecha desc) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='NOTIFICACION'
			   and valor<>'' and valor is not null
			) as a08
			  on a08.nif		= a.nif
			 and a08.rownum		= 1
left join 	(
			select nif,num_caso,fecha,valor as FECHA_INICIO
					,row_number() over (partition by nif order by fecha desc) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='FECHA INICIO SINTOMAS'
			   and valor<>'' and valor is not null
			) as a09
			  on a09.nif		= a.nif
			 and a09.rownum		= 1
left join 	(
			select nif,num_caso,fecha,valor as FECHA_FIN
					,row_number() over (partition by nif order by fecha desc) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='FECHA FIN'
			   and valor<>'' and valor is not null
			) as a10
			  on a10.nif		= a.nif
			 and a10.rownum		= 1
;

update carga_neotel_02 
   set telefono1=NULLIF(regexp_replace(admomi.iddpacpa3.telpart, '\D','','g'), '')::numeric
  from admomi.iddpacpa3 
 where carga_neotel_02.telefono1 is null		
   and carga_neotel_02.nif=admomi.iddpacpa3.nif2;

update carga_neotel_02 
   set edad=edad_en_agnios(now(),admomi.iddpacpa3.nacimiento)
  from admomi.iddpacpa3 
 where carga_neotel_02.edad is null		
   and carga_neotel_02.nif=admomi.iddpacpa3.nif2;

update carga_neotel_02 
   set nombre=admomi.iddpacpa3.pac_nombre_completo 
  from admomi.iddpacpa3 
 where carga_neotel_02.nombre is null		
   and carga_neotel_02.nif=admomi.iddpacpa3.nif2;  
--select * from carga_neotel_02 
  
drop table if exists carga_neotel_0301;
create table carga_neotel_0301 as
select distinct
		a.nif 			as nif_caso_indice
		,a.num_caso
		,a.nombre
		,a.rut_contacto 	as rut
		,a.edad  			as edad
		,'No registrada'	as direccion
		,'correo@correo.cl' as email
		,a.telefono1	   as telefono1
		,a.centro 		as cesfam
		,case when a.riesgo is not null 		then a.riesgo 		else 'No registrado' end as fr
		,case when a.condicion is not null		then a.condicion 	else 'No registrado' end as condicion
		,case when a.notificacion is not null 	then a.notificacion else 'No registrado' end as Notificacion
		,case when a.embarazada is not null 	then a.embarazada 	else 'No registrado' end as embaraza
		,a.fecha_inicio	as Fecha_inicio
		,a.fecha_fin 	as fecha_fin --(a.fecha_inicio+'14 days'::interval)::date 	as Fecha_Fin
		,null::date 	as Fecha_Seguimiento
  from carga_neotel_02 as a;

drop index if exists carga_neotel_0301_i_01;create index carga_neotel_0301_i_01 on carga_neotel_0301(nif_caso_indice) 		tablespace tb_index;
drop index if exists carga_neotel_0301_i_02;create index carga_neotel_0301_i_02 on carga_neotel_0301(rut) 					tablespace tb_index;
drop index if exists carga_neotel_0301_i_03;create index carga_neotel_0301_i_03 on carga_neotel_0301(nif_caso_indice,rut) 	tablespace tb_index;

drop table if exists carga_neotel_0302; 
create table carga_neotel_0302 as
select distinct a01.fecha_confirmacion,a.*
  from carga_neotel_0301 as a
  left join
  			(
			select bb.tis as rut,min(fecha) fecha_confirmacion
 			  from protocolos_omi_covid_base.hisa_iddncu as aa
 					,admomi.iddpacpa3 as bb 
 			 where ciap in ('G12','G13') 
 			   and aa.nif=bb.nif and bb.tis is not null and aa.stk_tipo_operacion='A'
 			 group by 1 --2176
  			) as a01
  			  on a01.rut=a.rut;

with consulta as
				(
				select nif2,min(fecha) fecha_confirmacion
	 			  from protocolos_omi_covid_base.hisa_iddncu --select * from protocolos_omi_covid_base.hisa_iddncu
	 			 where ciap in ('G12','G13') and stk_tipo_operacion='A'
	 			 group by 1 --2181
	  			)	
update carga_neotel_0302
   set fecha_confirmacion = consulta.fecha_confirmacion
  from consulta
 where consulta.nif2=nif_caso_indice
   and carga_neotel_0302.fecha_confirmacion is null;
 
--select distinct * from carga_neotel_0302 order by 1,2;

drop table if exists carga_neotel_03	 ;create table carga_neotel_03 as select distinct * from carga_neotel_0302 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null);
drop index if exists carga_neotel_03_i_01;create index carga_neotel_03_i_01 on carga_neotel_03(nif_caso_indice) 		tablespace tb_index;
drop index if exists carga_neotel_03_i_02;create index carga_neotel_03_i_02 on carga_neotel_03(rut) 					tablespace tb_index;
drop index if exists carga_neotel_03_i_03;create index carga_neotel_03_i_03 on carga_neotel_03(nif_caso_indice,rut) 	tablespace tb_index;

  
drop table if exists 	carga_neotel_final_to_pentaho;
create temp table 		carga_neotel_final_to_pentaho as
with consulta as 	(
					select row_number() over (partition by nif_caso_indice order by fecha_confirmacion) rownum 
							,('0'::varchar||telefono1::varchar) telefono2,* 
					  from usr_edox.carga_neotel_03 
					 where nif_caso_indice in 	(
					 							select nif2 
					 							  from
					 							  		(
					 							  		select * from protocolos_omi_covid.estado_de_seguimiento03 where centro in ('SAH','JPII','MTC') 
					 							  		) as a
					 							)					
					)
select distinct 
		 a01.med_nombre
		 ,a01.medico
		 ,a.fecha_confirmacion::date as fecha_confirmacion2
		,a.rownum
		,a.telefono2
		,a.fecha_confirmacion
		,a.nif_caso_indice
		,a.num_caso
		,a.nombre
		,a.rut
		,a.edad
		,a.direccion
		,a.email
		,a.telefono2 as telefono1
		,a.cesfam
		,a.fr
		,a.condicion
		,a.notificacion
		,a.embaraza as embarazada
		,case when a.fecha_inicio is not null then a.fecha_inicio::date else a.fecha_confirmacion::date 						end as fecha_inicio 
		,case when a.fecha_fin is not null then a.fecha_fin::date 		else (a.fecha_confirmacion+'12 days'::interval)::date 	end as fecha_fin
		,a.fecha_seguimiento
  from consulta as a
  left join	(
  			select   NULLIF(regexp_replace(aa.nif, '\D','','g'), '')::numeric as nif 
  					,bb.med_nombre
  					,row_number() over (partition by aa.nif order by aa.fecha) rownum
  					,bb.medico
  			  from   admomi.iddage as aa
  			  		,admomi.iddmed as bb
  			  where aa.medico=bb.medico and bb.id>=2000000 			     
  			    and aa.nif<>'' and aa.nif is not null
  			    and aa.fecha>='20200723 00:00:00'
  			    and aa.medico in 	(
  			    					'LZS','AAG','DZO','ICM','JLO','JAV','VEP','VVM','MC7','JG2','MJ2','MLG','VNN','MP6',	--SAH
  			    					'LCV','CDM','CV3','VVG','DCM','MJL','NCM','GSK','JVP','IIH','TLB','VE2','SGU','NSM','LCF','RCF','SIS','AAB','AVP','MB','MBV','LGV','AML','LLS','MV4'	--JPII
  			    					)
  			) as a01
  			  on a01.nif=a.nif_caso_indice
  			 and a01.rownum=1
where a.nif_caso_indice not 	in (select nif2 from admomi.iddpacpa3 where estado2 is null)
  and a.nif_caso_indice 		in (select nif  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409)
  ;--and a.fecha_confirmacion>='20200723 00:00:00' order by 1,6,7;

--drop table if exists protocolos_omi_covid.carga_neotel_final_to_pentaho;
--create table protocolos_omi_covid.carga_neotel_final_to_pentaho as
truncate table protocolos_omi_covid.carga_neotel_final_to_pentaho;
insert into protocolos_omi_covid.carga_neotel_final_to_pentaho
select 	
		 a.rownum
		,a.med_nombre
		,a.fecha_confirmacion
		,a.nif_caso_indice
		,a.num_caso
		,case when a.medico is not null then (a.medico||' - '||a.nombre) else a.nombre end as nombre
		,a.rut
		,a.edad
		,a.direccion
		,a.email
		,a.telefono2 as telefono1
		,a.cesfam
		,a.fr
		,a.condicion
		,a.notificacion
		,a.embarazada
		,a.fecha_inicio
		,a.fecha_fin
  from carga_neotel_final_to_pentaho as a
 where num_caso||rut not in	( --saca las personas q están repetidas tanto en un grupo con otro caso indice cuando se vuelven probables 
			 				select num_caso||rut
							  from carga_neotel_final_to_pentaho
							 where num_caso='caso00'
							   and rut in
							   					(
							   					select rut --,*
												  from carga_neotel_final_to_pentaho
												 where num_caso<>'caso00'
												 )
							)
order by nombre;

grant select on table protocolos_omi_covid.carga_neotel_final_to_pentaho to consultaomi;
grant select on table protocolos_omi_covid.carga_neotel_final_to_pentaho to consulta;
grant select on table protocolos_omi_covid.carga_neotel_final_to_pentaho to ugina;
grant select on table protocolos_omi_covid.carga_neotel_final_to_pentaho to jesoto;
grant select on table protocolos_omi_covid.carga_neotel_final_to_pentaho to ehenriquez;

"

rm -rf /opt/backup/monitoreo_covid_ancora_20200506/
/usr/pgsql-12/bin/pg_dump -Fd ancorabi -t covid19.hechos5 -t covid19.hechos_examenes_pcr -p5432 -j8 -v -f /opt/backup/monitoreo_covid_ancora_20200506
/usr/pgsql-12/bin/pg_restore -d ancorabi2 -p5433 -c -v -j8 /opt/backup/monitoreo_covid_ancora_20200506/ 
