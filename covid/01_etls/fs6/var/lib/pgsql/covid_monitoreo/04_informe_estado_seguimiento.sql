drop table if exists riesgo_x_pcr;
create temp table riesgo_x_pcr as
select
ome.numorden
,replace(pac.centro,'00000000','MTC') as centro
,pac.nif
,ome.fechap as fecha_orden
,fechap as fecha_orden2
  ,pac.tis as rut
  ,pac.nombre
  ,pac.apellido1
  ,pac.apellido2
  ,pac.nacimiento
  ,(ome.fecha-pac.nacimiento)/365 edad
  ,omr.texto as examen
	,case when omr.texto like '%con FDR%' then 'Alto' else 'Bajo' end as riesgo_x_pcr 
  ,dmo.texto resultado
  ,dmo.comentario
  from admomi.iddome as ome,admomi.iddoml as oml,
       admomi.iddomr as omr,admomi.iddomd as omd,
       admomi.iddpac as pac,admomi.idddmo as dmo
 where ome.numorden = oml.numorden
   and dmo.numorden=ome.numorden
   and oml.orden = omr.orden
   and omr.clave = omd.clave
   and ome.nif = pac.nif
   and omd.mostrar='1'
   and ome.clasepru = '1'
   and oml.tipolo='R'
   and omr.orden in (4549642,8473662)	--AND omr.texto LIKE 'Coronavirus%'
   order by ome.fechap desc nulls last;

drop table if exists usr_edox.estado_de_seguimiento;
create table usr_edox.estado_de_seguimiento as
with consulta as	(
					select
						 a03.centro
						,a.nif2
						,a03.rut 
						,a03.pac_nombre_completo
						,a.fecha2 as fecha_confirmacion
						,a.fechalta2 as fecha_episodio_cierre
						,case 
							when ciap='G12' then 'Caso Confirmado'
							when ciap='G13' then 'Caso Probable'
							end as tipo_caso
						,(a.fecha2+'25 days'::interval)::date as fecha_confirmacion_mas_14dias 
						,row_number() over (partition by a.nif order by a.fecha2) caso_repetido
						,case 
							when a00.riesgo is not null then a00.riesgo else 'No registrado' end as riesgo
						,a07.riesgo_x_pcr
						,case 
							  when a00.riesgo is not null then a00.riesgo 
							  when a00.riesgo is null then a07.riesgo_x_pcr 
							  else 'No registrado'
							  end riesgo_calculado
						,a01.fecha_seguimiento
						,case 
							when a01.fecha_seguimiento is not null then now()::date-a01.fecha_seguimiento::Date end as latencia_seguimiento 
						,now()::date as fecha_hoy
						,a02.usuario_egresa
						,a02.fechr as usuario_egresa_fecha
						,a04.usuario_egresa_causa
						,a05.tiempo_proximo_seguimiento
						,case 
						    when a01.fecha_seguimiento is null then '1 days'::interval 
							when a05.tiempo_proximo_seguimiento is null and (a00.riesgo = 'Alto' or a07.riesgo_x_pcr = 'Alto') 	then '2 days'::interval 
							when a05.tiempo_proximo_seguimiento is null and a00.riesgo = 'Medio' 		 						then '3 days'::interval 
							when a05.tiempo_proximo_seguimiento is null and (a00.riesgo = 'Bajo'  or a07.riesgo_x_pcr = 'Bajo')	then '4 days'::interval
							when a05.tiempo_proximo_seguimiento is null and a00.riesgo = 'No registrado'						then '4 days'::interval
							when a05.tiempo_proximo_seguimiento is null 														then '4 days'::interval	
							else a05.tiempo_proximo_seguimiento
							end as tiempo_proximo_seguimiento_calculado
						,a06.clinico
						,a06.clinico_estamento
				  from admomi.iddncu as a 
				  left join	(
							 select nif2,fecha
				  					,case 
				  						when campo='1' then 'Alto'
				  						when campo='2' then 'Medio'
				  						when campo='3' then 'Bajo'
				  						end as riesgo
				  					,row_number() 
				  						over (partition by nif2 
				  								  order by fecha desc) rownum
				  			  from protocolos_omi_covid_base.iddcop 
				  			 where wproto=409 
				  			   and wnumero=534 
				  			   and campo is not null 
				  			   and campo<>''
				  			) as a00 
				  			  on a00.nif2	= a.nif2 
				  		     and a00.rownum	= 1	
				  left join (
				   			select nif,max(fechr) as fecha_seguimiento
				   			  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409	
				   			  group by 1
				   			) as a01 on a01.nif=a.nif2
				  left join (
				  			select nif,fechr,'SI' usuario_egresa
				  					,row_number() over (partition by nif order by fechr desc) rownum
				  			  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409
				  			 where usuario_egresa_wn_0219_t_v is not null
				  			) as a02
				  			  on a02.nif	= a.nif2
				  			 and a02.rownum	= 1
				  left join (
				  			select nif2
				  					,centro
				  					,pac_nombre_completo
				  					,tis as rut
				  					,row_number() over (partition by nif2 order by fechalta) rownum
				  			  from admomi.iddpacpa3 
				  			) as a03
				  			  on a03.nif2	= a.nif2
				  			 and a03.rownum	= 1
				  				  left join (
				  			select nif,fechr,causaegresosegcovid19_wn_0064_t_x usuario_egresa_causa
				  					,row_number() over (partition by nif order by fechr desc) rownum
				  			  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409
				  			 where causaegresosegcovid19_wn_0064_t_x is not null
				  			) as a04
				  			  on a04.nif	= a.nif2
				  			 and a04.rownum	= 1	
				  left join
				  			(
				  			select nif,fecha,row_number() over (partition by nif order by fecha desc ) as rownum
				  			,(substr(tiemposeguimientocovid19_wn_0054_t_x,1,1)||' days')::interval as tiempo_proximo_seguimiento
				  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 
				  		   where tiemposeguimientocovid19_wn_0054_t_x is not null 
				  		    order by 1,2
				  			) as a05
				  			  on a05.nif	= a.nif2 --and a.fecha2=a05.fecha
				  			 and a05.rownum	= 1	
  				  left join
				  			(
				  			select estamento clinico_estamento,med_nombre as clinico,nif,fecha,row_number() over (partition by nif order by fecha desc ) as rownum
				  			,(substr(tiemposeguimientocovid19_wn_0054_t_x,1,1)||' days')::interval as tiempo_proximo_seguimiento
				  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409  
				  		    order by 1,2
				  			) as a06
				  			  on a06.nif	= a.nif2 --and a.fecha2=a05.fecha
				  			 and a06.rownum	= 1	
			 	left join 	(
			 				select nif,fecha_orden,riesgo_x_pcr
			 						,row_number() over (partition by nif order by fecha_orden desc) as rownum
			 				  from riesgo_x_pcr order by nif,fecha_orden desc nulls last
			 				) as a07 on a07.nif=a.nif and a07.rownum=1
				 where a.ciap in ('G12','G13')
				)
				select a.*
						,case when fecha_seguimiento is not null then 	(tiempo_proximo_seguimiento_calculado+fecha_seguimiento)::date 
								else									(tiempo_proximo_seguimiento_calculado+fecha_confirmacion)::date 
								end as fecha_proximo_seguimiento
					    ,case 
					    	when a.fecha_seguimiento is not null and a.latencia_seguimiento<4  										     then '1. Seguimiento menor a 4 días'
					    	when a.fecha_seguimiento is not null and a.latencia_seguimiento>=4 and a.fecha_confirmacion_mas_14dias<now() then '2. Seguimiento mayor o igual a 4 días pero con alta probable (Alerta)'
					    	when a.fecha_seguimiento is not null and a.latencia_seguimiento>=4 											 then '3. Seguimiento mayor o igual a 4 días (Alerta)'
							when a.fecha_seguimiento is null and a.fecha_confirmacion_mas_14dias<now() 									 then '4. Sin Seguimiento pero alta probable (Alerta)'
					    	when a.fecha_seguimiento is null and a.fecha_confirmacion_mas_14dias>now() 									 then '5. Sin Seguimiento sin alta (Alerta)'
					    	else ''
							end	as estado_seguimiento				    
				  from consulta as a --where a.usuario_egresa_fecha is null and fecha_episodio_cierre is null
				  ;

drop table if exists protocolos_omi_covid.estado_de_seguimiento02;
create table protocolos_omi_covid.estado_de_seguimiento02 as			--Estado de Seguimientos
with consulta as (
select distinct a.*
		,now()::date - fecha_proximo_seguimiento diferencia_fecha_actual_prox_seguimiento
		,case when now()::date - fecha_proximo_seguimiento<=0 then 'Al día' else 'Atrasado' end estado_de_seguimiento   
  from usr_edox.estado_de_seguimiento as a --where a.usuario_egresa_fecha is null --and a.fecha_episodio_cierre is null
   )
   select a.* --,a01.neotel 
     from consulta as a;

GRANT SELECT 	ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO ugina;    
GRANT SELECT 	ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO consulta;
GRANT SELECT 	ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO consultaomi;    
GRANT SELECT 	ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO ugina;
GRANT ALL 		ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO postgres;
GRANT SELECT 	ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO consulta;
GRANT SELECT 	ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO ehenriquez;
GRANT SELECT 	ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO consultaomi;
GRANT SELECT 	ON TABLE protocolos_omi_covid.estado_de_seguimiento02 TO jesoto;    

drop table if exists protocolos_omi_covid.estado_de_seguimiento03;
create table protocolos_omi_covid.estado_de_seguimiento03 as
select  
	centro,
	nif2,
	rut,
	pac_nombre_completo,
	fecha_confirmacion,
	fecha_episodio_cierre,
	tipo_caso,
	fecha_confirmacion_mas_14dias as fecha_confirmacion_mas_25dias,
	caso_repetido,
	riesgo,
	riesgo_x_pcr,
	riesgo_calculado,
	fecha_seguimiento,
	latencia_seguimiento,
	fecha_hoy,
	usuario_egresa,
	usuario_egresa_fecha,
	usuario_egresa_causa,
	tiempo_proximo_seguimiento::text as tiempo_proximo_seguimiento,
	tiempo_proximo_seguimiento_calculado::text as tiempo_proximo_seguimiento_calculado,
	clinico,
	clinico_estamento,
	fecha_proximo_seguimiento,
	estado_seguimiento,
	diferencia_fecha_actual_prox_seguimiento,
	estado_de_seguimiento
from protocolos_omi_covid.estado_de_seguimiento02;

GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 	TO ugina;    
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 	TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 	TO consultaomi;    
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 	TO ugina;
GRANT ALL ON TABLE protocolos_omi_covid.estado_de_seguimiento03 	TO postgres;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 	TO consulta;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 	TO ehenriquez;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 	TO consultaomi;
GRANT SELECT ON TABLE protocolos_omi_covid.estado_de_seguimiento03 	TO jesoto; 

