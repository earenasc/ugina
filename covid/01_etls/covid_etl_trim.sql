drop table if exists protocolos_omi_covid_base.hisa_iddcop;
create table protocolos_omi_covid_base.hisa_iddcop  
as 
select 
    fecha,
    fecha::date as fecha2,
	btrim(usuario) as usuario,
	btrim(tipo_operacion) as tipo_operacion,
	id_hisa_iddcop,
	btrim(nif) as nif,
	NULLIF(regexp_replace(a.nif, '\D','','g'), '')::integer as nif2,
	btrim(especiali) as especiali,
	secuen,
	wproto,
	wnumero,
	btrim(wtipo) as wptipo,
	btrim(campo) as campo,
	btrim(tabla) as tabla
from protocolos_omi_covid_etl.hisa_iddcop as a;

drop table if exists protocolos_omi_covid_base.hisa_iddcoe;
create table protocolos_omi_covid_base.hisa_iddcoe  
as 
select 
    fecha,
    fecha::date as fecha2,
	btrim(usuario) as usuario,
	btrim(tipo_operacion) as tipo_operacion,
	id_hisa_iddcoe,
	btrim(nif) as nif,
	NULLIF(regexp_replace(a.nif, '\D','','g'), '')::integer as nif2,
	btrim(especiali) as especiali,
	secuen,
	wproto,
	wnumero,
	btrim(wtipo) as wptipo,
	btrim(campo) as campo,
	btrim(tabla) as tabla
from protocolos_omi_covid_etl.hisa_iddcoe as a;

drop table if exists protocolos_omi_covid_base.hisa_iddcor;
create table protocolos_omi_covid_base.hisa_iddcor  
as 
select 
	fecha as stk_fecha,
	fecha::date as stk_fecha2,
	btrim(usuario) as stk_usuario,
	btrim(tipo_operacion) as stk_tipo_operacion,
	id_hisa_iddcor,
	btrim(nhc) as nif,
	NULLIF(regexp_replace(a.nhc, '\D','','g'), '')::integer as nif2,
	btrim(especiali) as especiali,
	secuen,
	numero,
	fechaomi(fecha_1) as fecha,
	fechaomi(fecha_1)::date as fecha2,
	btrim(curso) as curso,
	btrim(tipolin) as tipolin,
	btrim(visitam) as visitam,
	btrim(origen) as origen,
	tipocor,
	wproto,
	btrim(medico) as medico
from protocolos_omi_covid_etl.hisa_iddcor as a;
create index hisa_iddcor_i_01 on protocolos_omi_covid_base.hisa_iddcor(numero,stk_tipo_operacion,wproto) tablespace tb_index;

drop table if exists protocolos_omi_covid_base.iddcor;
create table protocolos_omi_covid_base.iddcor as  
select 
	btrim(nhc) as nif,
	NULLIF(regexp_replace(a.nhc, '\D','','g'), '')::integer as nif2,
	btrim(especiali) as especiali,
	secuen,
	numero,
	case when fecha=0 then null else  (fechaomi(fecha)::text||' '||visitam)::timestamp end as fecha,
	fechaomi(fecha)::Date as fecha2,
	btrim(curso) as curso,
	btrim(tipolin) as tipolin,
	btrim(visitam) as visitam,
	btrim(origen) as origen,
	tipocor,
	wproto,
	btrim(medico) as medico,
	null::integer as clinico_id
 from protocolos_omi_covid_etl.iddcor as a;

update protocolos_omi_covid_base.iddcor set clinico_id = admomi.iddmed.id
  from admomi.iddmed
 where admomi.iddmed.medico=protocolos_omi_covid_base.iddcor.medico;

create index iddcor_i_01 on protocolos_omi_covid_base.iddcor using btree (nif) tablespace tb_index;
create index iddcor_i_02 on protocolos_omi_covid_base.iddcor using btree (fecha) tablespace tb_index;
create index iddcor_i_03 on protocolos_omi_covid_base.iddcor using btree (medico) tablespace tb_index;
create index iddcor_i_04 on protocolos_omi_covid_base.iddcor using btree (wproto) tablespace tb_index;
create index iddcor_i_05 on protocolos_omi_covid_base.iddcor using btree (numero) tablespace tb_index;
create index iddcor_i_06 on protocolos_omi_covid_base.iddcor using btree (nif, especiali, secuen, numero,wproto) tablespace tb_index;
create index iddcor_i_07 on protocolos_omi_covid_base.iddcor using btree (nif, fecha, medico) tablespace tb_index;
create index iddcor_i_09 on protocolos_omi_covid_base.iddcor using btree (nif, fecha) tablespace tb_index;
create index iddcor_i_10 on protocolos_omi_covid_base.iddcor using btree (nif, fecha, wproto) tablespace tb_index;
create index iddcor_i_11 on protocolos_omi_covid_base.iddcor using btree (especiali) tablespace tb_index;
create index iddcor_i_12 on protocolos_omi_covid_base.iddcor using btree (tipocor) tablespace tb_index;
create index iddcor_i_13 on protocolos_omi_covid_base.iddcor using btree (secuen) tablespace tb_index;
create index iddcor_i_14 on protocolos_omi_covid_base.iddcor using btree (nif2) tablespace tb_index;
create index iddcor_i_15 on protocolos_omi_covid_base.iddcor using btree (fecha2) tablespace tb_index;
create index iddcor_i_16 on protocolos_omi_covid_base.iddcor using btree (clinico_id) tablespace tb_index;
create index iddcor_i_17 on protocolos_omi_covid_base.iddcor using btree (nif2, especiali, secuen, numero,wproto) tablespace tb_index;
create index iddcor_i_18 on protocolos_omi_covid_base.iddcor using btree (nif2, fecha2, clinico_id) tablespace tb_index;
create index iddcor_i_19 on protocolos_omi_covid_base.iddcor using btree (wproto, numero,nif,fecha) tablespace tb_index;
create index iddcor_i_21 on protocolos_omi_covid_base.iddcor using btree (nif2, fecha2) tablespace tb_index;
create index iddcor_i_22 on protocolos_omi_covid_base.iddcor using btree (nif2, fecha2, wproto) tablespace tb_index;
create index iddcor_i_23 on protocolos_omi_covid_base.iddcor using btree (wproto, numero,medico,nif,fecha,especiali) tablespace tb_index;
create index iddcor_i_24 on protocolos_omi_covid_base.iddcor using btree (wproto, numero,medico,nif2,fecha,especiali) tablespace tb_index;

/*
 *  from admomi.iddcor as a,admomi.iddmed as b
 where a.wproto='||n||'
   and a.numero=1 and a.wproto>=1 
   and a.medico=b.medico
   and a.nif is not null 
   and a.fecha is not null 
   and a.especiali is not null;
 * */


drop table if exists protocolos_omi_covid_base.iddcoe;create table protocolos_omi_covid_base.iddcoe as  
select distinct 
	btrim(a.nif) as nif,
	NULLIF(regexp_replace(a.nif, '\D','','g'), '')::integer as nif2,
	btrim(a.especiali) as especiali,
	a.secuen,
	a.wproto,
	a.wnumero,
	btrim(a.wtipo) as wtipo,
	btrim(a.campo) as campo,
	btrim(b.medico) as medico,
	b.fecha as fecha,
	b.fecha::DATE as fecha2,
	'IDDCOE' as tabla,
	c.id as medico_id
  from protocolos_omi_covid_etl.iddcoe as a
  	,protocolos_omi_covid_base.iddcor as b --select count(*) from protocolos_omi_covid_etl.iddcoe as a --962149
  	,admomi.iddmed as c
  	where btrim(a.nif)			= btrim(b.nif)
 	  and btrim(b.especiali)	= btrim(a.especiali)
 	  and b.secuen				= a.secuen
 	  and b.wproto				= a.wproto
 	  and b.numero				= 1
 	  and b.medico				= c.medico;	 
--create index iddcoe_i_01 on protocolos_omi_covid_base.iddcoe(wproto,wnumero,nif2,fecha2) tablespace tb_index;
create index iddcoe_i_01 on protocolos_omi_covid_base.iddcoe(nif,especiali,secuen,wproto)  tablespace tb_index;
create index iddcoe_i_02 on protocolos_omi_covid_base.iddcoe(nif) tablespace tb_index;
create index iddcoe_i_03 on protocolos_omi_covid_base.iddcoe(especiali) tablespace tb_index;
create index iddcoe_i_04 on protocolos_omi_covid_base.iddcoe(secuen) tablespace tb_index;
create index iddcoe_i_05 on protocolos_omi_covid_base.iddcoe(wproto) tablespace tb_index;
create index iddcoe_i_06 on protocolos_omi_covid_base.iddcoe(wnumero) tablespace tb_index;
create index iddcoe_i_07 on protocolos_omi_covid_base.iddcoe(wtipo) tablespace tb_index;
create index iddcoe_i_08 on protocolos_omi_covid_base.iddcoe(wproto,wnumero) tablespace tb_index;
create index iddcoe_i_09 on protocolos_omi_covid_base.iddcoe(fecha) tablespace tb_index;
create index iddcoe_i_10 on protocolos_omi_covid_base.iddcoe(medico) tablespace tb_index;
create index iddcoe_i_11 on protocolos_omi_covid_base.iddcoe(nif,especiali,secuen,wproto,fecha,medico) tablespace tb_index;
create index iddcoe_i_12 on protocolos_omi_covid_base.iddcoe(wproto,wnumero,fecha,campo,nif,especiali,secuen) tablespace tb_index;
create index iddcoe_i_13 on protocolos_omi_covid_base.iddcoe(fecha2) tablespace tb_index;
create index iddcoe_i_14 on protocolos_omi_covid_base.iddcoe(wproto,wnumero,fecha2) tablespace tb_index;
create index iddcoe_i_15 on protocolos_omi_covid_base.iddcoe(wproto,wnumero,fecha) tablespace tb_index;
create index iddcoe_i_16 on protocolos_omi_covid_base.iddcoe(nif2,especiali,secuen,wproto,fecha2,medico) tablespace tb_index;
create index iddcoe_i_17 on protocolos_omi_covid_base.iddcoe(wproto,wnumero,fecha2,campo,nif2,especiali,secuen) tablespace tb_index;
create index iddcoe_i_18 on protocolos_omi_covid_base.iddcoe(wproto,wnumero,campo) tablespace tb_index;
create index iddcoe_i_19 on protocolos_omi_covid_base.iddcoe(nif2) tablespace tb_index;
create index iddcoe_i_21 on protocolos_omi_covid_base.iddcoe(medico_id) tablespace tb_index;
create index iddcoe_i_22 on protocolos_omi_covid_base.iddcoe(nif2,fecha2,medico_id) tablespace tb_index;
create index iddcoe_i_23 on protocolos_omi_covid_base.iddcoe(nif2,fecha2,medico_id,especiali,secuen) tablespace tb_index;
create index iddcoe_i_24 on protocolos_omi_covid_base.iddcoe(nif2,fecha2,medico_id,especiali,secuen,wproto) tablespace tb_index;
create index iddcoe_i_25 on protocolos_omi_covid_base.iddcoe(nif2,fecha2,medico_id,especiali,secuen,wproto,wnumero) tablespace tb_index;
create index iddcoe_i_26 on protocolos_omi_covid_base.iddcoe(nif2,fecha2,medico_id,wproto,wnumero) tablespace tb_index;
create index iddcoe_i_27 on protocolos_omi_covid_base.iddcoe(nif2,fecha2,medico_id,wproto) tablespace tb_index;
create index iddcoe_i_28 on protocolos_omi_covid_base.iddcoe(nif2,fecha2,medico_id,especiali) tablespace tb_index;

drop table if exists protocolos_omi_covid_base.iddcop;create table protocolos_omi_covid_base.iddcop as  
select distinct 
	btrim(a.nif) as nif,
	NULLIF(regexp_replace(a.nif, '\D','','g'), '')::integer as nif2,
	btrim(a.especiali) as especiali,
	a.secuen,
	a.wproto,
	a.wnumero,
	btrim(a.wtipo) as wtipo,
	btrim(a.campo) as campo,
	btrim(b.medico) as medico,
	b.fecha as fecha,
	b.fecha::DATE as fecha2,
	tabla,
	c.id as medico_id
  from protocolos_omi_covid_etl.iddcop as a
  	,protocolos_omi_covid_base.iddcor as b --select count(*) from protocolos_omi_covid_etl.iddcop as a --962149
  	,admomi.iddmed as c
  	where btrim(a.nif)			= btrim(b.nif)
 	  and btrim(b.especiali)	= btrim(a.especiali)
 	  and b.secuen				= a.secuen
 	  and b.wproto				= a.wproto
 	  and b.numero				= 1
 	  and b.medico				= c.medico;	 
create index iddcop_i_01 on protocolos_omi_covid_base.iddcop(nif,especiali,secuen,wproto)  tablespace tb_index;
create index iddcop_i_02 on protocolos_omi_covid_base.iddcop(nif) tablespace tb_index;
create index iddcop_i_03 on protocolos_omi_covid_base.iddcop(especiali) tablespace tb_index;
create index iddcop_i_04 on protocolos_omi_covid_base.iddcop(secuen) tablespace tb_index;
create index iddcop_i_05 on protocolos_omi_covid_base.iddcop(wproto) tablespace tb_index;
create index iddcop_i_06 on protocolos_omi_covid_base.iddcop(wnumero) tablespace tb_index;
create index iddcop_i_07 on protocolos_omi_covid_base.iddcop(wtipo) tablespace tb_index;
create index iddcop_i_08 on protocolos_omi_covid_base.iddcop(wproto,wnumero) tablespace tb_index;
create index iddcop_i_09 on protocolos_omi_covid_base.iddcop(fecha) tablespace tb_index;
create index iddcop_i_10 on protocolos_omi_covid_base.iddcop(medico) tablespace tb_index;
create index iddcop_i_11 on protocolos_omi_covid_base.iddcop(nif,especiali,secuen,wproto,fecha,medico) tablespace tb_index;
create index iddcop_i_12 on protocolos_omi_covid_base.iddcop(wproto,wnumero,fecha,campo,nif,especiali,secuen) tablespace tb_index;
create index iddcop_i_13 on protocolos_omi_covid_base.iddcop(fecha2) tablespace tb_index;
create index iddcop_i_14 on protocolos_omi_covid_base.iddcop(wproto,wnumero,fecha2) tablespace tb_index;
create index iddcop_i_15 on protocolos_omi_covid_base.iddcop(wproto,wnumero,fecha) tablespace tb_index;
create index iddcop_i_16 on protocolos_omi_covid_base.iddcop(nif2,especiali,secuen,wproto,fecha2,medico) tablespace tb_index;
create index iddcop_i_17 on protocolos_omi_covid_base.iddcop(wproto,wnumero,fecha2,campo,nif2,especiali,secuen) tablespace tb_index;
create index iddcop_i_18 on protocolos_omi_covid_base.iddcop(wproto,wnumero,campo) tablespace tb_index;
create index iddcop_i_19 on protocolos_omi_covid_base.iddcop(nif2) tablespace tb_index;
create index iddcop_i_21 on protocolos_omi_covid_base.iddcop(medico_id) tablespace tb_index;
create index iddcop_i_22 on protocolos_omi_covid_base.iddcop(nif2,fecha2,medico_id) tablespace tb_index;
create index iddcop_i_23 on protocolos_omi_covid_base.iddcop(nif2,fecha2,medico_id,especiali,secuen) tablespace tb_index;
create index iddcop_i_24 on protocolos_omi_covid_base.iddcop(nif2,fecha2,medico_id,especiali,secuen,wproto) tablespace tb_index;
create index iddcop_i_25 on protocolos_omi_covid_base.iddcop(nif2,fecha2,medico_id,especiali,secuen,wproto,wnumero) tablespace tb_index;
create index iddcop_i_26 on protocolos_omi_covid_base.iddcop(nif2,fecha2,medico_id,wproto,wnumero) tablespace tb_index;
create index iddcop_i_27 on protocolos_omi_covid_base.iddcop(nif2,fecha2,medico_id,wproto) tablespace tb_index;
create index iddcop_i_28 on protocolos_omi_covid_base.iddcop(nif2,fecha2,medico_id,especiali) tablespace tb_index;


drop table if exists protocolos_omi_covid_base.iddncu;
create table protocolos_omi_covid_base.iddncu as  
select 
	btrim(nif) as nif,
	NULLIF(regexp_replace(a.nif, '\D','','g'), '')::integer as nif2,
	btrim(ciap) as ciap,
	btrim(cipap) as cipap,
	btrim(prescrip) as prescrip,
	btrim(archivo) as archivo,
	btrim(tipoepi) as tipoepi,
	btrim(visitam) as visitam,
	btrim(origen) as origen,
	btrim(descripci2) as descripci2,
	btrim(histo) as histo,
	btrim(especiali) as especiali,
	case when a.fecha=0 then null else (fechaomi(a.fecha)::text||' '||horaomi(a.horaing)::text)::timestamp end as fecha,
	fechaomi(a.fecha)::date as fecha2,
	btrim(descripcio) as descripcio,
	btrim(medico) as medico,
	case when a.fechalta=0 then null else fechaomi(a.fechalta) end as fechalta,
	fechaomi(a.fechalta)::date as fechalta2,
	btrim(curobser) as curobser,
	btrim(subjetiva) as subjetiva,
	btrim(aparato) as aparato,
	btrim(especiali2) as especiali2,
	btrim(medicocap) as medicocap,
	btrim(codigoesp) as codigoesp,
	btrim(tipus) as tipus,
	t,
	n,
	m,
	btrim(destino) as destino,
	btrim(cie) as cie,
	case when a.fechaing=0 then null else  (fechaomi(a.fechaing)::text||' '||horaomi(a.horaing)::text)::timestamp end as fechaing,
	horaing,
	btrim(usuarioing) as usuarioing,
	btrim(lateralidad) as lateralidad,
	btrim(esp) as esp
  from protocolos_omi_covid_etl.iddncu as a;

drop table if exists protocolos_omi_covid_base.hisa_iddncu;
create table protocolos_omi_covid_base.hisa_iddncu as  
select
	stk_fecha as stk_fecha,
	stk_fecha::date as stk_fecha2,
	btrim(stk_usuario) as stk_usuario,
	btrim(stk_tipo_operacion) as stk_tipo_operacion,
	btrim(nif) as nif,
	NULLIF(regexp_replace(a.nif, '\D','','g'), '')::integer as nif2,
	btrim(ciap) as ciap,
	btrim(cipap) as cipap,
	btrim(prescrip) as prescrip,
	btrim(archivo) as archivo,
	btrim(tipoepi) as tipoepi,
	btrim(visitam) as visitam,
	btrim(origen) as origen,
	btrim(descripci2) as descripci2,
	btrim(histo) as histo,
	btrim(especiali) as especiali,
	case when a.fecha=0 then null else (fechaomi(a.fecha)::text||' '||horaomi(a.horaing)::text)::timestamp end as fecha,
	fechaomi(a.fecha)::date as fecha2,
	btrim(descripcio) as descripcio,
	btrim(medico) as medico,
	case when a.fechalta=0 then null else fechaomi(a.fechalta) end as fechalta,
	fechaomi(a.fechalta)::date as fechalta2,
	btrim(curobser) as curobser,
	btrim(subjetiva) as subjetiva,
	btrim(aparato) as aparato,
	btrim(especiali2) as especiali2,
	btrim(medicocap) as medicocap,
	btrim(codigoesp) as codigoesp,
	btrim(tipus) as tipus,
	t,
	n,
	m,
	btrim(destino) as destino,
	btrim(cie) as cie,
	case when a.fechaing=0 then null else  (fechaomi(a.fechaing)::text||' '||horaomi(a.horaing)::text)::timestamp end as fechaing,
	horaing,
	btrim(usuarioing) as usuarioing,
	btrim(lateralidad) as lateralidad,
	btrim(esp) as esp
  from protocolos_omi_covid_etl.hisa_iddncu as a;

DROP TABLE IF EXISTS protocolos_omi_covid_base.examenes_pcr_omi;
CREATE TABLE protocolos_omi_covid_base.examenes_pcr_omi AS
SELECT 
 	a.numorden,
	upper(btrim(a.codilab)) AS codilab,
	btrim(a.nif) AS nif,
	NULLIF(regexp_replace(a.nif, '\D','','g'), '')::integer as nif2,
	case when a.solicitud_fecha=0 then null else  (fechaomi(a.solicitud_fecha)::text||' '||horaomi(a.solicitud_hora)::text)::timestamp end as fecha_solicitud,
	case when a.cierre_fecha=0 then null else  (fechaomi(a.cierre_fecha)::text||' '||horaomi(a.cierre_hora)::text)::timestamp end as fecha_cierre,
	upper(btrim(a.resultado)) as resultado
	from protocolos_omi_covid_etl.examenes_pcr_omi as a;

update protocolos_omi_covid_etl.hisa_iddome
set
stk_usuario=btrim(stk_usuario),
stk_tipo_operacion=btrim(stk_tipo_operacion),
comentario= btrim(comentario),
respuestainter= btrim(respuestainter),
nif= btrim(nif),
prueba= btrim(prueba),
centro= btrim(centro),
prioridad= btrim(prioridad),
usuario= btrim(usuario),
observacio= btrim(observacio),
patologica= btrim(patologica),
tiporea= btrim(tiporea),
tiposol= btrim(tiposol),
otraspru= btrim(otraspru),
clasepru= btrim(clasepru),
solicita= btrim(solicita),
lugar= btrim(lugar),
tubo1= btrim(tubo1),
tubo2= btrim(tubo2),
tubo3= btrim(tubo3),
tubo4= btrim(tubo4),
tubo5= btrim(tubo5),
tubo6= btrim(tubo6),
tubo7= btrim(tubo7),
tubo8= btrim(tubo8),
nombrem= btrim(nombrem),
especia= btrim(especia),
especiali= btrim(especiali),
visto= btrim(visto),
codilab= btrim(codilab),
yo= btrim(yo),
liniacor= btrim(liniacor),
medext= btrim(medext),
usuome= btrim(usuome),
medico= btrim(medico),
usuarioconforme= btrim(usuarioconforme),
usuextraccion= btrim(usuextraccion),
usucierre= btrim(usucierre),
usuvisto= btrim(usuvisto),
cie9= btrim(cie9),
especialista= btrim(especialista),
diagnostico= btrim(diagnostico),
tiporadio= btrim(tiporadio),
dirweb= btrim(dirweb)
;

DROP INDEX IF EXISTS protocolos_omi_covid_base.hisa_iddome_i_01;
DROP INDEX IF EXISTS protocolos_omi_covid_base.hisa_iddome_i_02;
DROP INDEX IF EXISTS protocolos_omi_covid_base.hisa_iddome_i_03;
DROP INDEX IF EXISTS protocolos_omi_covid_base.hisa_iddome_i_04;
DROP INDEX IF EXISTS protocolos_omi_covid_base.hisa_iddome_i_05;
DROP INDEX IF EXISTS protocolos_omi_covid_base.hisa_iddome_i_06;

truncate table protocolos_omi_covid_base.hisa_iddome;
insert into protocolos_omi_covid_base.hisa_iddome
--DROP TABLE IF exists protocolos_omi_covid_base.hisa_iddome;
--CREATE TABLE protocolos_omi_covid_base.hisa_iddome as
select
stk_fecha,
stk_usuario,
stk_tipo_operacion,
id_hisa_iddome,
comentario,
respuestainter,
nif,
NULLIF(regexp_replace(nif, '\D','','g'), '')::integer as nif2,
prueba,
case when fecha = 0 then null else  fechaomi(fecha) end as fecha,
centro,
case when fechar =0 then null when fechar between -32768 and +32767  then (fechaomi(fechar)::text||' '||horaomi(horar)::text)::timestamp else null end as fechar,
prioridad,
usuario,
observacio,
numorden,
numage,
patologica,
case when fecharea=0 then null else  fechaomi(fecharea) end as fecharea,
aproxmes,
aproxdias,
tiporea,
tiposol,
otraspru,
clasepru,
solicita,
lugar,
numero1,
numero2,
numero3,
numero4,
numero5,
numero6,
numero7,
numero8,
tubo1,
tubo2,
tubo3,
tubo4,
tubo5,
tubo6,
tubo7,
tubo8,
nombrem,
especia,
especiali,
secuen,
visto,
case when fechav  = 0 then null else  (fechaomi(fechav)::text||' '||horaomi(horav)::text)::timestamp end as fechav,
case when fechac  = 0 then null else  (fechaomi(fechac)::text||' '||horaomi(horac)::text)::timestamp end as fechac,
case when fechap  = 0 then null else  (fechaomi(fechap)::text||' '||horaomi(horap)::text)::timestamp end as fechap,
case when fechare = 0 then null else  (fechaomi(fechare)::text||' '||horaomi(horare)::text)::timestamp end as fechare,
total,
realizadas,
codilab,
lab,
case when fecharev = 0 then null else  (fechaomi(fecharev)::text||' '||horaomi(horarev)::text)::timestamp end as fecharev,
yo,
liniacor,
medext,
usuome,
medico,
conformelab,
usuarioconforme,
case when fechaconforme = 0 then null else  (fechaomi(fechaconforme )::text||' '||horaomi(horaconforme)::text)::timestamp end as fechaconforme,
numerofichero,
usuextraccion,
usucierre,
usuvisto,
cie9,
especialista,
numordenrel,
diagnostico,
numagrusol,
secuenproto,
tiporadio,
case when fechainicita=0 then null else  fechaomi(fechainicita) end as fechainicita,
case when fechafincita=0 then null else  fechaomi(fechafincita) end as fechafincita,
dirweb,
actuesalus,
case when fechacitaext =0 then null else  (fechaomi(fechacitaext )::text||' '||horaomi(horacitaext)::text)::timestamp end as fechacitaext, 
tipopic
from protocolos_omi_covid_etl.hisa_iddome;

CREATE INDEX hisa_iddome_i_01 ON protocolos_omi_covid_base.hisa_iddome USING btree (id_hisa_iddome) tablespace tb_index;
CREATE INDEX hisa_iddome_i_02 ON protocolos_omi_covid_base.hisa_iddome USING btree (nif2) tablespace tb_index;
CREATE INDEX hisa_iddome_i_03 ON protocolos_omi_covid_base.hisa_iddome USING btree (nif) tablespace tb_index;
CREATE INDEX hisa_iddome_i_04 ON protocolos_omi_covid_base.hisa_iddome USING btree (numorden) tablespace tb_index;
CREATE INDEX hisa_iddome_i_05 ON protocolos_omi_covid_base.hisa_iddome USING btree (stk_fecha) tablespace tb_index;
CREATE INDEX hisa_iddome_i_06 ON protocolos_omi_covid_base.hisa_iddome USING btree (stk_tipo_operacion) tablespace tb_index;

drop table if exists protocolos_omi_covid_base.iddpacpa3;
create table protocolos_omi_covid_base.iddpacpa3 
as 
select * from admomi.iddpacpa3; 
