drop table if exists seguimiento_covid_hechos01;
create temp table    seguimiento_covid_hechos01 as
select nif2,fecha2 from protocolos_omi_covid_base.iddncu where ciap in ('G10','G12','G13','G14') group by 1,2;
--select * from protocolos_omi_covid_base.iddncu
--select count(*) from (select distinct nif2 from protocolos_omi_covid_base.iddncu where ciap in ('G12','G13') and fechalta is null) as a; = 1074
--select count(*) from (select distinct nif2 from protocolos_omi_covid_base.iddncu where ciap in ('G12','G13')) as a; = 1074
--select distinct nif from (select distinct nif2 from protocolos_omi_covid_base.iddncu where ciap in ('G13')) as a; = 1074
--select distinct nif2,fecha2 	from protocolos_omi_covid_base.hisa_iddncu where ciap in ('G10','G12','G13') and stk_tipo_operacion='A';
--select nif2,min(stk_fecha) from protocolos_omi_covid_base.hisa_iddncu where ciap in ('G10','G12','G13') and stk_tipo_operacion='A' group by 1 order by 2;
--select * from seguimiento_covid_hechos01
--select * from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 order by fechr desc nulls last
--select count(*) from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where fecha between '20200601' and '20200630' 				

--select * from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409; 

drop table if exists protocolos_omi_covid_base.datos_contactos01; --select * from protocolos_omi_covid_base.datos_contactos01 where nif='84310';
create table    protocolos_omi_covid_base.datos_contactos01 as
select nif,fecha,especiali,'caso01' as num_caso,'NOMBRE CONTACTO' as dato,upper(nombre_contacto_01_wn_0221_t_t) as VALOR	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_01_wn_0221_t_t 				is not null union
select nif,fecha,especiali,'caso02' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_02_wn_0222_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_02_wn_0222_t_t 				is not null union
select nif,fecha,especiali,'caso03' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_03_wn_0223_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_03_wn_0223_t_t 				is not null union
select nif,fecha,especiali,'caso04' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_04_wn_0224_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_04_wn_0224_t_t 				is not null union
select nif,fecha,especiali,'caso05' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_05_wn_0225_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_05_wn_0225_t_t 				is not null union
select nif,fecha,especiali,'caso06' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_06_wn_0226_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_06_wn_0226_t_t 				is not null union
select nif,fecha,especiali,'caso07' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_07_wn_0227_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_07_wn_0227_t_t 				is not null union
select nif,fecha,especiali,'caso08' as num_caso,'NOMBRE CONTACTO',upper(nombre_contacto_08_wn_0228_t_t) as nombre_contacto 	from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where nombre_contacto_08_wn_0228_t_t 				is not null union
select nif,fecha,especiali,'caso01' as num_caso,'RUT CONTACTO',upper(rut_contacto_01_wn_0232_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_01_wn_0232_t_t 					is not null union
select nif,fecha,especiali,'caso02' as num_caso,'RUT CONTACTO',upper(rut_contacto_02_wn_0233_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_02_wn_0233_t_t 					is not null union
select nif,fecha,especiali,'caso03' as num_caso,'RUT CONTACTO',upper(rut_contacto_03_wn_0234_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_03_wn_0234_t_t					is not null union
select nif,fecha,especiali,'caso04' as num_caso,'RUT CONTACTO',upper(rut_contacto_04_wn_0235_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_04_wn_0235_t_t 					is not null union
select nif,fecha,especiali,'caso05' as num_caso,'RUT CONTACTO',upper(rut_contacto_05_wn_0236_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_05_wn_0236_t_t 					is not null union
select nif,fecha,especiali,'caso06' as num_caso,'RUT CONTACTO',upper(rut_contacto_06_wn_0237_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_06_wn_0237_t_t 					is not null union
select nif,fecha,especiali,'caso07' as num_caso,'RUT CONTACTO',upper(rut_contacto_07_wn_0238_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_07_wn_0238_t_t 					is not null union
select nif,fecha,especiali,'caso08' as num_caso,'RUT CONTACTO',upper(rut_contacto_08_wn_0239_t_t) as rut_contacto 			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where rut_contacto_08_wn_0239_t_t 					is not null union
select nif,fecha,especiali,'caso01' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_01_wn_0409_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_01_wn_0409_t_x 		is not null union
select nif,fecha,especiali,'caso02' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_02_wn_0410_t_x) 	 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_02_wn_0410_t_x 		is not null union
select nif,fecha,especiali,'caso03' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_03_wn_0411_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_03_wn_0411_t_x 		is not null union
select nif,fecha,especiali,'caso04' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_04_wn_0412_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_04_wn_0412_t_x 		is not null union
select nif,fecha,especiali,'caso05' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_05_wn_0413_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_05_wn_0413_t_x 		is not null union
select nif,fecha,especiali,'caso06' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_06_wn_0414_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_06_wn_0414_t_x 		is not null union
select nif,fecha,especiali,'caso07' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_07_wn_0415_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409	where tiene_sisntomas_contacto_07_wn_0415_t_x 		is not null union
select nif,fecha,especiali,'caso08' as num_caso,'SINTOMAS CONTACTO',upper(tiene_sisntomas_contacto_08_wn_0416_t_x)  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where tiene_sisntomas_contacto_08_wn_0416_t_x 		is not null union
select nif,fecha,especiali,'caso01' as num_caso,'EDAD CONTACTO',edad_contacto_01_wn_0356_t_n::text as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_01_wn_0356_t_n 					is not null union
select nif,fecha,especiali,'caso02' as num_caso,'EDAD CONTACTO',edad_contacto_02_wn_0357_t_n::text as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_02_wn_0357_t_n 					is not null union
select nif,fecha,especiali,'caso03' as num_caso,'EDAD CONTACTO',edad_contacto_03_wn_0358_t_n::text as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_03_wn_0358_t_n 					is not null union
select nif,fecha,especiali,'caso04' as num_caso,'EDAD CONTACTO',edad_contacto_04_wn_0359_t_n::text as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_04_wn_0359_t_n 					is not null union
select nif,fecha,especiali,'caso05' as num_caso,'EDAD CONTACTO',edad_contacto_05_wn_0360_t_n::text   						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_05_wn_0360_t_n 					is not null union
select nif,fecha,especiali,'caso06' as num_caso,'EDAD CONTACTO',edad_contacto_06_wn_0361_t_n::text   						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_06_wn_0361_t_n 					is not null union
select nif,fecha,especiali,'caso07' as num_caso,'EDAD CONTACTO',edad_contacto_07_wn_0362_t_n::text   						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_07_wn_0362_t_n 					is not null union
select nif,fecha,especiali,'caso08' as num_caso,'EDAD CONTACTO',edad_contacto_08_wn_0363_t_n::text 		  					from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where edad_contacto_08_wn_0363_t_n 					is not null union
select nif,fecha,especiali,'caso01' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_01_wn_0345_t_x)  as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_01_wn_0345_t_x 					is not null union
select nif,fecha,especiali,'caso02' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_02_wn_0346_t_x)  as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_02_wn_0346_t_x 					is not null union
select nif,fecha,especiali,'caso03' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_03_wn_0347_t_x)  as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_03_wn_0347_t_x 					is not null union
select nif,fecha,especiali,'caso04' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_04_wn_0348_t_x)  as valor 				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_04_wn_0348_t_x 					is not null union
select nif,fecha,especiali,'caso05' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_05_wn_0349_t_x)    						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_05_wn_0349_t_x 					is not null union
select nif,fecha,especiali,'caso06' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_06_wn_0350_t_x)    						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_06_wn_0350_t_x 					is not null union
select nif,fecha,especiali,'caso07' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_07_wn_0351_t_x)    						from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_07_wn_0351_t_x 					is not null union
select nif,fecha,especiali,'caso08' as num_caso,'SEXO CONTACTO',upper(sexo_contacto_08_wn_0352_t_x) 	  					from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where sexo_contacto_08_wn_0352_t_x 					is not null union
select nif,fecha,especiali,'caso01' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_01_wn_0399_t_x)  				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_01_wn_0399_t_x 				is not null union
select nif,fecha,especiali,'caso02' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_02_wn_0400_t_x)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_02_wn_0400_t_x 				is not null union
select nif,fecha,especiali,'caso03' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_03_wn_0401_t_x)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_03_wn_0401_t_x 				is not null union
select nif,fecha,especiali,'caso04' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_04_wn_0402_t_x)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_04_wn_0402_t_x 				is not null union
select nif,fecha,especiali,'caso05' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_05_wn_0403_t_x)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_05_wn_0403_t_x 				is not null union
select nif,fecha,especiali,'caso06' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_06_wn_0404_t_x)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_06_wn_0404_t_x				is not null union
select nif,fecha,especiali,'caso07' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_07_wn_0405_t_x)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_07_wn_0405_t_x 				is not null union
select nif,fecha,especiali,'caso08' as num_caso,'EMBARAZO CONTACTO',upper(embarazo_contacto_08_wn_0406_t_x) 	  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where embarazo_contacto_08_wn_0406_t_x 				is not null union
select nif,fecha,especiali,'caso01' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_01_wn_0243_t_t)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_01_wn_0243_t_t 				is not null union
select nif,fecha,especiali,'caso02' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_02_wn_0245_t_t)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_02_wn_0245_t_t 				is not null union
select nif,fecha,especiali,'caso03' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_03_wn_0246_t_t)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_03_wn_0246_t_t 				is not null union
select nif,fecha,especiali,'caso04' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_04_wn_0247_t_t)  		 		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_04_wn_0247_t_t 				is not null union
select nif,fecha,especiali,'caso05' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_05_wn_0248_t_t)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_05_wn_0248_t_t 				is not null union
select nif,fecha,especiali,'caso06' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_06_wn_0249_t_t)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_06_wn_0249_t_t 				is not null union
select nif,fecha,especiali,'caso07' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_07_wn_0250_t_t)    				from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_07_wn_0250_t_t 				is not null union
select nif,fecha,especiali,'caso08' as num_caso,'TELEFONO CONTACTO',upper(telefono_contacto_08_wn_0251_t_t) 	  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_contacto_08_wn_0251_t_t 				is not null union
select nif,fecha,especiali,'caso08' as num_caso,'MODALIDAD DE CONTACTO',upper(modalidad_de_contacto_wn_0189_t_x) 	  		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where modalidad_de_contacto_wn_0189_t_x 			is not null union
select nif,fecha,especiali,'caso08' as num_caso,'VIA DE CONTACTO',upper(via_de_contacto_wn_0191_t_x)		 	  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where via_de_contacto_wn_0191_t_x 						is not null union 	
select nif,fecha,especiali,'caso00' as num_caso,'NOTIFICACION',upper(estado_notificacion_resultado_wn_0532_t_x)	  			from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where estado_notificacion_resultado_wn_0532_t_x			is not null union  	
select nif,fecha,especiali,'caso00' as num_caso,'FECHA INICIO SINTOMAS',upper(fechainiciosintomas_wn_0085_t_f::text)		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where fechainiciosintomas_wn_0085_t_f					is not null union --fecha_probable_fin_de_cuarentena_wn_0528_t_f
select nif,fecha,especiali,'caso00' as num_caso,'FECHA FIN',upper(fecha_probable_fin_de_cuarentena_wn_0528_t_f::text)		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where fecha_probable_fin_de_cuarentena_wn_0528_t_f		is not null union --fecha_probable_fin_de_cuarentena_wn_0528_t_f
select nif,fecha,especiali,'caso00' as num_caso,'TELEFONO01',upper(numero_de_telefono_1_wn_0195_t_t)		 	  			from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where numero_de_telefono_1_wn_0195_t_t 					is not null union
select nif,fecha,especiali,'caso00' as num_caso,'TELEFONO02',upper(numero_de_telefono_2_wn_0197_t_t)		 	  			from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where numero_de_telefono_2_wn_0197_t_t 					is not null union --fecha_primer_sintoma_wn_0179_t_f
select nif,fecha,especiali,'caso00' as num_caso,'TELEFONO01',upper(telefono_de_contacto_seguimiento_1_wn_0328_t_n::Text)	from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where telefono_de_contacto_seguimiento_1_wn_0328_t_n	is not null union
select nif,fecha,especiali,'caso00' as num_caso,'TELEFONO02',upper(telefono_de_contacto_seguimiento_2_wn_0334_t_n::text)	from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where telefono_de_contacto_seguimiento_2_wn_0334_t_n 	is not null union --fecha_primer_sintoma_wn_0179_t_f
select nif,fecha,especiali,'caso00' as num_caso,'TELEFONO01',upper(telefono_de_contacto_seguimiento_wn_0601_t_n::text)		from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 	where telefono_de_contacto_seguimiento_wn_0601_t_n		is not null union --fecha_probable_fin_de_cuarentena_wn_0528_t_f
select nif,fecha,especiali,'caso00' as num_caso,'FECHA INICIO SINTOMAS',upper(fecha_primer_sintoma_wn_0179_t_f::text)		from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			where fecha_primer_sintoma_wn_0179_t_f 					is not null union --fecha_primer_sintoma_wn_0179_t_f
select a.nif,a.fecha,a.especiali,'caso00' as num_caso,'RUT CASO',upper(b.tis)		 	  		
  from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 as a,admomi.iddpacpa3 as b
 where a.nif=b.nif2 union 
select a.nif,a.fecha,a.especiali,'caso00' as num_caso,'RUT CASO',upper(b.tis)		 	  		
  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 as a,admomi.iddpacpa3 as b
 where a.nif=b.nif2 union
select a.nif2,a.fecha2,a.especiali,'caso00' as num_caso,'RUT CASO',(b.tis) 
  from protocolos_omi_covid_base.iddncu as a,admomi.iddpacpa3 as b
 where a.ciap in ('G12','G13') and a.nif=b.nif;
 ;

select centro,count(*)
  from
  		(
		select distinct a01.centro,a01.nif2 
		  from 
		  		(
				select distinct nif,valor 
				  from protocolos_omi_covid_base.datos_contactos01 
				 where dato like '%RUT%'
				   and num_caso like '%caso0%'
				   and num_caso<>'caso00'
				   and fecha<'20200719'
				) as a
		left join	(
					select nif2,centro,row_number() over (partition by nif2 order by fechalta) rownum from admomi.iddpacpa3 as b
					) as a01 on a01.nif2=a.nif and a01.rownum=1
		) as a
		group by 1
;

/*OJO AGREGAR TELEFONO DE PROTOCOLO SEGUIMIENTO */

update protocolos_omi_covid_base.datos_contactos01 set valor=BTRIM(upper(valor));
-- estado_notificacion_resultado
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'.','')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,',','')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'--','-')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,':','')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G10T','')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G12T','')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G13T','')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'*','')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'/','')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'+','')	where dato in ('RUT CASO','RUT CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,' ','')	where dato in ('RUT CASO','RUT CONTACTO');
  
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'.','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,',','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'--','-')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'-','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,':','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G10T','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G12T','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'G13T','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'*','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'/','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,'+','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');
update protocolos_omi_covid_base.datos_contactos01 set valor=replace(valor,' ','')	where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO');

--select telpart,NULLIF(regexp_replace(telpart, '\D','','g'), '')::numeric from admomi.iddpac order by 2 desc nulls last
--select distinct valor from protocolos_omi_covid_base.datos_contactos01 where dato in ('TELEFONO1','TELEFONO2','TELEFONO CONTACTO') order by 1;

create index datos_contactos01_i_01 on protocolos_omi_covid_base.datos_contactos01(nif)					tablespace tb_index;
create index datos_contactos01_i_02 on protocolos_omi_covid_base.datos_contactos01(fecha)					tablespace tb_index;
create index datos_contactos01_i_03 on protocolos_omi_covid_base.datos_contactos01(num_caso)				tablespace tb_index;
create index datos_contactos01_i_04 on protocolos_omi_covid_base.datos_contactos01(dato)					tablespace tb_index;
create index datos_contactos01_i_05 on protocolos_omi_covid_base.datos_contactos01(dato)					tablespace tb_index;
create index datos_contactos01_i_06 on protocolos_omi_covid_base.datos_contactos01(valor)					tablespace tb_index;
create index datos_contactos01_i_07 on protocolos_omi_covid_base.datos_contactos01(nif,num_caso)			tablespace tb_index;
create index datos_contactos01_i_08 on protocolos_omi_covid_base.datos_contactos01(nif,fecha,num_caso)	tablespace tb_index;

drop table if exists seguimiento_covid_hechos02;
create temp table    seguimiento_covid_hechos02 as --select * from seguimiento_covid_hechos02
select distinct 
		 a01.centro	
		, a.nif2
		,a.fecha2
		,a01.tis		as caso_positivo_rut
		,a01.nombre		as caso_positivo_nomnre
		,a01.apellido1	as caso_positivo_apellido1
		,a01.apellido2	as caso_positivo_apellido2
		,a01.caso_positivo_domicilio
		,a01.caso_positivo_comuna
		,a02.fecha 		as fecha_seguimiento
		,a03.num_caso	
  from seguimiento_covid_hechos01 as a --select * from seguimiento_covid_hechos01 where nif2=84310   
  left join	(
  			select centro,nif2,tis,nombre,apellido1,apellido2
  					,coalesce(domicilio,' ')||' '||coalesce(tiscab,' ') as caso_positivo_domicilio,comuna as caso_positivo_comuna
  					,row_number() over (partition by nif order by fechalta) rownum
  			  from admomi.iddpacpa3 
  			) as a01
  			  on a01.nif2	= a.nif2
  			 and a01.rownum	= 1
 left join 	(
 			select distinct nif,fecha,especiali from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409
 			) as a02 
 			  on a02.nif=a.nif2 
 left join	(
			select distinct nif,num_caso from protocolos_omi_covid_base.datos_contactos01
			) as a03
			  on a03.nif=a.nif2;

drop table if exists seguimiento_covid_hechos03; --select * from seguimiento_covid_hechos03 where nif2=84310
create temp table    seguimiento_covid_hechos03 as	 
select a.*
		,a01.contacto_nombre
		,a02.contacto_rut
		,a03.contacto_tiene_sintomas
		,a04.contacto_edad
		,a05.contacto_sexo
		,a06.contacto_embarazado
		,a07.contacto_telefono
		,a08.fecha_inicio_cuarentena
		,(a08.fecha_inicio_cuarentena+'14 days'::interval)::date as fecha_termino_cuarentena
		,'APS '||coalesce(a09.contacto_modalidad,'')||' '||coalesce(a10.contacto_via,'') as responsable_del_seguimiento
		,null::text as CUMPLE_PERFIL_CASO_SOSPECHOSO
		,null::text as FECHA_TOMA_DE_MUESTRA
		,null::text as RESULTADO_DEL_EXAMEN
		,null::text as FECHA_CONFIRMACION
		,null::text as LUGAR_DE_DERIVACION_SI_SE_CONFIRMA
  from seguimiento_covid_hechos02 as a
  left join (
			select nif,num_caso,fecha,valor as contacto_nombre 
					,row_number() over (partition by nif,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='NOMBRE CONTACTO'
			   and valor<>'' and valor is not null
			) as a01
			  on a01.nif		= a.nif2
			 and a01.num_caso	= a.num_caso
			 and a01.rownum		= 1
  left join (
			select nif,num_caso,fecha,valor as contacto_rut
					,row_number() over (partition by nif,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='RUT CONTACTO'
			   and valor<>'' and valor is not null
			) as a02
			  on a02.nif		= a.nif2
			 and a02.num_caso	= a.num_caso
			 and a02.rownum		= 1		
  left join (
			select nif,num_caso,fecha,valor as contacto_tiene_sintomas
					,row_number() over (partition by nif,fecha,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='SINTOMAS CONTACTO'
			   and valor<>'' and valor is not null
			) as a03
			  on a03.nif		= a.nif2
			 and a03.num_caso	= a.num_caso
			 and a03.fecha		= a.fecha2
			 and a03.rownum		= 1		
  left join (
			select nif,num_caso,fecha,valor as contacto_edad
					,row_number() over (partition by nif,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='EDAD CONTACTO'
			   and valor<>'' and valor is not null
			) as a04
			  on a04.nif		= a.nif2
			 and a04.num_caso	= a.num_caso
			 and a04.rownum		= 1
left join 	(
			select nif,num_caso,fecha,valor as contacto_sexo
					,row_number() over (partition by nif,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='SEXO CONTACTO'
			   and valor<>'' and valor is not null
			) as a05
			  on a05.nif		= a.nif2
			 and a05.num_caso	= a.num_caso
			 and a05.rownum		= 1
left join 	(
			select nif,num_caso,fecha,valor as contacto_embarazado
					,row_number() over (partition by nif,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='EMBARAZO CONTACTO'
			   and valor<>'' and valor is not null
			) as a06
			  on a06.nif		= a.nif2
			 and a06.num_caso	= a.num_caso
			 and a06.rownum		= 1
left join 	(
			select nif,num_caso,fecha,valor as contacto_telefono
					,row_number() over (partition by nif,fecha,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='TELEFONO CONTACTO'
			   and valor<>'' and valor is not null
			) as a07
			  on a07.nif		= a.nif2
			 and a07.num_caso	= a.num_caso
			 and a07.fecha		= a.fecha2
			 and a07.rownum		= 1
left join 	(
			select nif2,fecha2 as fecha_inicio_cuarentena
					,row_number() over (partition by nif,fecha2 order by fecha2) rownum
			  from admomi.iddncu where ciap in ('G12','G13')  
			) as a08
			  on a08.nif2=a.nif2 
			 and a08.rownum=1
left join 	(
			select nif,num_caso,fecha,valor as contacto_modalidad
					,row_number() over (partition by nif,fecha,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='MODALIDAD DE CONTACTO'
			   and valor<>'' and valor is not null
			) as a09
			  on a09.nif		= a.nif2
			 and a09.num_caso	= a.num_caso
			 and a09.fecha		= a.fecha2
			 and a09.rownum		= 1			
left join 	(
			select nif,num_caso,fecha,valor as contacto_via
					,row_number() over (partition by nif,fecha,num_caso order by fecha) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='VIA DE CONTACTO'
			   and valor<>'' and valor is not null
			) as a10
			  on a10.nif		= a.nif2
			 and a10.num_caso	= a.num_caso
			 and a10.fecha		= a.fecha2
			 and a10.rownum		= 1			
			 ;			

drop table if exists seguimiento_covid_hechos04; --select * from seguimiento_covid_hechos04 where nif2=84310
create temp table    seguimiento_covid_hechos04 as	 
select a.*
		,a01.domicilio  as caso_contacto_domicilio
		,a01.comuna		as caso_contacto_comuna
  from seguimiento_covid_hechos03 as a
  left join	(
  			select centro,nif2,tis,nombre,apellido1,apellido2
  					,coalesce(domicilio,' ')||' '||coalesce(tiscab,' ') as domicilio,comuna
  					,row_number() over (partition by nif order by fechalta) rownum
  			  from admomi.iddpacpa3 
  			) as a01
  			  on a01.tis	= a.contacto_rut
  			 and a01.rownum	= 1;
			
drop table if exists protocolos_omi_covid.listado_contactos_seremi;
create table protocolos_omi_covid.listado_contactos_seremi as 
select distinct * from seguimiento_covid_hechos04;

grant select on protocolos_omi_covid.listado_contactos_seremi to consultaomi;

--Casos Confirmados Pentaho pentaho
drop table if exists casos_confirmados_pentaho;create table casos_confirmados_pentaho as --select * from casos_confirmados_pentaho where nif=84310
 select  distinct  
		a01.centro
		,a01.nif2 as nif
		,a01.tis as rut
		,a.ciap as diagnostico_cod
		,a.descripcio diagnostico_descripcion
		,a.fecha ciap_fecha
		,a01.nombre
		,a01.apellido1
		,a01.apellido2
		,a01.sexo
		,edad_en_agnios(now(),a01.nacimiento) as edad
		,(coalesce(a01.domicilio,'')||' '||coalesce(a01.tiscab,'')) as domicilio,a01.comuna
		,a02.telefono
  from protocolos_omi_covid_base.iddncu as a
  	left join
  				(
  				select distinct nif2,nif,centro,nacimiento,sexo,domicilio,tiscab,tis,nombre,apellido1,apellido2,comuna,
  					row_number() over (partition by nif order by fechalta) rownum
  					from admomi.iddpacpa3
  				) as a01
  				  on a01.nif=a.nif
  				 and a01.rownum=1
	left join
				(
				select aa.nif,dato,replace(replace(replace(replace(telefono,'-',''),'*',''),'/',''),'+','') as telefono
						,row_number() over (partition by nif order by dato) as rownum
				  from
				  		(
						select nif,'telefono1' as dato,upper(numero_de_telefono_1_wn_0195_t_t) as telefono		 	  		
						  from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			
						 where numero_de_telefono_1_wn_0195_t_t is not null union
						select nif,'telefono2' as dato,upper(numero_de_telefono_2_wn_0197_t_t) 
						  from protocolos_omi_covid.pr_morbilidad_aguda_covid19_wp_000412 			
						 where numero_de_telefono_2_wn_0197_t_t is not null 
						) as aa where telefono<>'' and telefono is not null						
				) as a02
				  on a02.nif=a.nif2
				 and a02.rownum=1  
 where a.ciap in ('G10','G12','G13','G14') --select distinct descripcio from admomi.iddncu where ciap in ('G10','G12','G13')
   and a.nif not in (select nif from admomi.iddpacpa3 where estado2 is null);

update casos_confirmados_pentaho 
   set telefono=NULLIF(regexp_replace(admomi.iddpacpa3.telpart, '\D','','g'), '')::numeric
  from admomi.iddpacpa3 
 where casos_confirmados_pentaho.telefono is null		
   and casos_confirmados_pentaho.nif=admomi.iddpacpa3.nif2;

update casos_confirmados_pentaho 
   set telefono=NULLIF(regexp_replace(admomi.iddpacpa3.teldesp, '\D','','g'), '')::numeric
  from admomi.iddpacpa3 
 where casos_confirmados_pentaho.telefono is null		
   and casos_confirmados_pentaho.nif=admomi.iddpacpa3.nif2;  
  
--select * from casos_confirmados_pentaho where diagnostico_cod in ('G12','G13') AND CENTRO='JPII'

--carga NEOTEL
drop table if exists carga_neotel_01; --select * from carga_neotel_01 where nif=84310
create table carga_neotel_01 as
select a.nif,b.num_caso,b.valor as rut_contacto
  from protocolos_omi_covid_base.datos_contactos01 as a
  left join protocolos_omi_covid_base.datos_contactos01 as b on a.nif=b.nif and b.dato in ('RUT CONTACTO','RUT CASO')
 group by 1,2,3 order by 1,2; 

--select * from carga_neotel_01 where nif=95975
--select * from carga_neotel_02 where nif=597
				
drop table if exists carga_neotel_02;
create table carga_neotel_02 as
select distinct 
		 a.*
		,a01.telefono1
		,a02.riesgo
		,a00.centro
		,a03.edad
		,a04.nombre 
		,a05.contacto_embarazado as embarazada
		,case 
			when a06.diagnostico_cod='G10' then 'Sospechoso'
			when a06.diagnostico_cod='G12' then 'Confirmado'
			when a06.diagnostico_cod='G13' then 'Probable'
			when a06.diagnostico_cod='G14' then 'Pesquiza Activa'
			else 'Contacto' end as condicion
		--,a06.ciap_fecha::Date as fecha_inicio
		,a08.notificacion
		,a09.fecha_inicio::Date as fecha_inicio
		,a10.fecha_fin::Date as fecha_fin
  from carga_neotel_01 as a
   left join (
  			select nif2,tis,pac_nombre_completo,centro
  					,row_number() over (partition by nif order by fechalta) as rownum
  			  from admomi.iddpacpa3  
  			) as a00
  			  on a00.nif2=a.nif
  left join 
			(
  			select *,NULLIF(regexp_replace(valor, '\D','','g'), '')::numeric as telefono1
  					,row_number() over (partition by nif order by fecha desc) rownum
  			  from protocolos_omi_covid_base.datos_contactos01 where dato='TELEFONO01'
  			) as a01 
 			  on a01.nif=a.nif 
 			 and a01.rownum=1
 left join 	(
  			select nif2,fecha
  					,case 
  						when campo='1' then 'Alto'
  						when campo='2' then 'Medio'
  						when campo='3' then 'Bajo'
  						end as riesgo
  					,row_number() over (partition by nif2 order by fecha desc) rownum
  			  from protocolos_omi_covid_base.iddcop 
  			 where wproto=409 
  			   and wnumero=534 
  			   and campo is not null 
  			   and campo<>''
  			) as a02
  			  on a02.nif2=a.nif
  			 and a02.rownum=1
  left join (
  			select NIF,num_caso
  					,NULLIF(regexp_replace(valor, '\D','','g'), '')::numeric as edad
  					,row_number() over (partition by nif,num_caso order by fecha) rownum
  			  from protocolos_omi_covid_base.datos_contactos01 
  			 where dato='EDAD CONTACTO'
  			   and valor<>'' and valor is not null
 			) as a03
 		      on a03.nif=a.nif
 		     and a03.num_caso=a.num_caso
 		     and a03.rownum=1
   left join 
  			(
  			select nif,num_caso,valor as nombre 
  					,row_number() over (partition by nif,num_caso order by fecha) rownum
  			  from protocolos_omi_covid_base.datos_contactos01 
  			 where dato='NOMBRE CONTACTO'
  			   and valor<>'' and valor is not null
  			) as a04
  			  on a04.nif=a.nif
  			 and a04.num_caso=a.num_caso
  			 and a04.rownum=1
left join 	(
			select nif,num_caso,fecha,valor as contacto_embarazado
					,row_number() over (partition by nif,num_caso order by fecha desc) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='EMBARAZO CONTACTO'
			   and valor<>'' and valor is not null
			) as a05
			  on a05.nif		= a.nif
			 and a05.num_caso	= a.num_caso
			 and a05.rownum		= 1
left join	(
			select * 
					,row_number() over (partition by rut order by ciap_fecha desc) as rownum
			  from casos_confirmados_pentaho			  
			) as a06
			  on a06.rut = a.rut_contacto 
			 and a06.rut is not null
			 and a06.rownum=1
left join	(
			select * 
					,row_number() over (partition by rut order by ciap_fecha desc) as rownum
			  from casos_confirmados_pentaho			  
			) as a07
			  on a07.nif = a.nif 
			 and a07.rownum=1
left join 	(
			select nif,num_caso,fecha,valor as NOTIFICACION
					,row_number() over (partition by nif order by fecha desc) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='NOTIFICACION'
			   and valor<>'' and valor is not null
			) as a08
			  on a08.nif		= a.nif
			 and a08.rownum		= 1
left join 	(
			select nif,num_caso,fecha,valor as FECHA_INICIO
					,row_number() over (partition by nif order by fecha desc) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='FECHA INICIO SINTOMAS'
			   and valor<>'' and valor is not null
			) as a09
			  on a09.nif		= a.nif
			 and a09.rownum		= 1
left join 	(
			select nif,num_caso,fecha,valor as FECHA_FIN
					,row_number() over (partition by nif order by fecha desc) rownum
			  from protocolos_omi_covid_base.datos_contactos01 
			 where dato='FECHA FIN'
			   and valor<>'' and valor is not null
			) as a10
			  on a10.nif		= a.nif
			 and a10.rownum		= 1
;

update carga_neotel_02 
   set telefono1=NULLIF(regexp_replace(admomi.iddpacpa3.telpart, '\D','','g'), '')::numeric
  from admomi.iddpacpa3 
 where carga_neotel_02.telefono1 is null		
   and carga_neotel_02.nif=admomi.iddpacpa3.nif2;

update carga_neotel_02 
   set edad=edad_en_agnios(now(),admomi.iddpacpa3.nacimiento)
  from admomi.iddpacpa3 
 where carga_neotel_02.edad is null		
   and carga_neotel_02.nif=admomi.iddpacpa3.nif2;

update carga_neotel_02 
   set nombre=admomi.iddpacpa3.pac_nombre_completo 
  from admomi.iddpacpa3 
 where carga_neotel_02.nombre is null		
   and carga_neotel_02.nif=admomi.iddpacpa3.nif2;  
--select * from carga_neotel_02 
  
drop table if exists carga_neotel_0301;
create table carga_neotel_0301 as
select distinct
		a.nif 			as nif_caso_indice
		,a.num_caso
		,a.nombre
		,a.rut_contacto 	as rut
		,a.edad  			as edad
		,'No registrada'	as direccion
		,'correo@correo.cl' as email
		,a.telefono1	   as telefono1
		,a.centro 		as cesfam
		,case when a.riesgo is not null 		then a.riesgo 		else 'No registrado' end as fr
		,case when a.condicion is not null		then a.condicion 	else 'No registrado' end as condicion
		,case when a.notificacion is not null 	then a.notificacion else 'No registrado' end as Notificacion
		,case when a.embarazada is not null 	then a.embarazada 	else 'No registrado' end as embaraza
		,a.fecha_inicio	as Fecha_inicio
		,a.fecha_fin 	as fecha_fin --(a.fecha_inicio+'14 days'::interval)::date 	as Fecha_Fin
		,null::date 	as Fecha_Seguimiento
  from carga_neotel_02 as a;

drop index if exists carga_neotel_0301_i_01;create index carga_neotel_0301_i_01 on carga_neotel_0301(nif_caso_indice) 		tablespace tb_index;
drop index if exists carga_neotel_0301_i_02;create index carga_neotel_0301_i_02 on carga_neotel_0301(rut) 					tablespace tb_index;
drop index if exists carga_neotel_0301_i_03;create index carga_neotel_0301_i_03 on carga_neotel_0301(nif_caso_indice,rut) 	tablespace tb_index;

drop table if exists carga_neotel_0302; 
create table carga_neotel_0302 as
select distinct a01.fecha_confirmacion,a.*
  from carga_neotel_0301 as a
  left join
  			(
			select bb.tis as rut,min(fecha) fecha_confirmacion
 			  from protocolos_omi_covid_base.hisa_iddncu as aa
 					,admomi.iddpacpa3 as bb 
 			 where ciap in ('G12','G13') 
 			   and aa.nif=bb.nif and bb.tis is not null and aa.stk_tipo_operacion='A'
 			 group by 1 --2176
  			) as a01
  			  on a01.rut=a.rut;

with consulta as
				(
				select nif2,min(fecha) fecha_confirmacion
	 			  from protocolos_omi_covid_base.hisa_iddncu --select * from protocolos_omi_covid_base.hisa_iddncu
	 			 where ciap in ('G12','G13') and stk_tipo_operacion='A'
	 			 group by 1 --2181
	  			)	
update carga_neotel_0302
   set fecha_confirmacion = consulta.fecha_confirmacion
  from consulta
 where consulta.nif2=nif_caso_indice
   and carga_neotel_0302.fecha_confirmacion is null;
 
--select distinct * from carga_neotel_0302 order by 1,2;

drop table if exists carga_neotel_03	 ;create table carga_neotel_03 as select distinct * from carga_neotel_0302 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null);
drop index if exists carga_neotel_03_i_01;create index carga_neotel_03_i_01 on carga_neotel_03(nif_caso_indice) 		tablespace tb_index;
drop index if exists carga_neotel_03_i_02;create index carga_neotel_03_i_02 on carga_neotel_03(rut) 					tablespace tb_index;
drop index if exists carga_neotel_03_i_03;create index carga_neotel_03_i_03 on carga_neotel_03(nif_caso_indice,rut) 	tablespace tb_index;

--select distinct nif2 from protocolos_omi_covid_base.iddncu where ciap in ('G12'); --1745 / 5216
--select distinct rut from carga_neotel_03 where num_caso<>'caso00';
--select 2400::numeric/1958::numeric
--select 5216::numeric/1745::numeric
--select * from carga_neotel_03
--

with consulta as 	(
					select nif2,min(fecha) as fecha
					  from protocolos_omi_covid_base.iddncu 
					 where ciap in ('G12','G13')
					 group by 1
					)
update carga_neotel_03 
   set fecha_confirmacion = consulta.fecha
  from consulta 
 where consulta.nif2=carga_neotel_03.nif_caso_indice 
   and carga_neotel_03.fecha_confirmacion is null;
  
drop table if exists usr_edox.carga_neotel_03;create table usr_edox.carga_neotel_03 as select distinct * from carga_neotel_03;

--select * from usr_edox.carga_neotel_03
--select distinct nif,fecha from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409	where fecha between '20200629' and '20200706' and nif not in (select nif2 from admomi.iddpacpa3 where estado2 is null) --1282 protoccolos seguimientos 
--select distinct nif	    from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409	where fecha between '20200629' and '20200706' and nif in (select nif_caso_indice from usr_edox.carga_neotel_03 where fecha_confirmacion is not null) and nif in (select distinct nif2 from protocolos_omi_covid_base.iddncu where ciap in ('G12')) --310 casos confirmados select 647-310 = 337 sospechosos protoccolos seguimientos
--select distinct rut,*       from usr_edox.carga_neotel_03 where nif_caso_indice in (select distinct nif from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409	where fecha between '20200629' and '20200706') --1080 persoans totales
--select distinct rut       from usr_edox.carga_neotel_03 where fecha_confirmacion is not null and nif_caso_indice in (select distinct nif from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409	where fecha between '20200629' and '20200706') --970 rut 



drop table if exists usr_edox.carga_neotel_0303; --select * from  usr_edox.carga_neotel_0303
create table         usr_edox.carga_neotel_0303 as
select 
	row_number() over (partition by a.rut) as rownum,a.*
  from	( 
		select distinct 
		 a.usuario
		,a.fecha_confirmacion
		,a.nif_caso_indice
		,a.num_caso
		,a.nombre
		,a.rut
		,a.edad
		,a.direccion
		,a.email
		,('0'::varchar||a.telefono1::varchar) as telefono1
		,a.cesfam
		,a.fr
		,a.condicion
		,a.notificacion
		,a.embaraza
		,a.fecha_inicio
		,a.fecha_fin
		,a.fecha_seguimiento
		,a.carga_dia
		  from 
		  		(
				select distinct a.*
				  from
				  		(
						select 'BECAD0S'::text as usuario,*,'20200616'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
						   							(
						   							select nif_caso_indice
						   							  from carga_neotel_03 
						   							 where rut in 
									   							 (
																'10477840-2' --carga 16/06/2020
																,'10467244-2'
																,'9619770-5'
																,'14187222-2'
																,'10724971-0'
																,'7107709-8'
																,'11978830-7'
																,'11442860-4'
																,'11424428-7'
																,'17575622-1'
																,'4075418-0'
																,'21682111-4'
																,'12262273-8'
																,'20046275-0'
																) 
													) 
						union
						select 'BECAD0S'::text as usuario,*,'20200615'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 (
																		 '16718242-9' --carga 15/06/2020
																		,'18603839-8'
																		,'8854787-K'
																		,'11168093-0'
																		,'20243681-1'
																		,'20117924-6'
																		,'8814032-K'
																		,'8010955-5'
																		,'16267075-1'
																		,'9075380-0'
																		,'8711929-7'
																		,'8929454-1'
																		,'16861240-0'
																		,'21371644-1'
																		,'9611090-1'
																		) 
															)
						union
						select 'BECAD0S'::text as usuario,*,'20200618'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 					
																			'11572483-5' --carga 18/06/2020
																			,'9596474-5'
																			,'15471413-8'
																			,'12408039-8'
																			,'13031922-K'
																			) 
															) 
						union
						select 'BECADOS'::text as usuario,*,'20200619'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 					
																			'13087315-4' --carga 19/06/2020
																			,'20465947-8'
																			) 
															) 
						union
						select null::text as usuario,*,'20200620'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 					
																			'12262273-8'
																			,'8345886-0'
																			) 
															)											
						union
						select null::text as usuario,*,'20200623'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 					
																			'15200475-3'
																			,'7516008-9'
																			) 
															)											
						union
						select 'Alejandra Arenas',*,'20200625'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 --select * from carga_neotel_03  
								   							 where rut in 
											   							 	( 
																			'6355812-5'      
																			,'13935695-0'       
																			,'17612713-9'      
																			,'10120668-8'      
																			,'10793315-8'            			
																			) 
															)	
										union
						select 'Linian Zech',*,'20200625'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'10818252-0'      
																			,'7628419-9'       
																			,'9609458-2'       
																			,'17307897-8'      
																			,'10949352-K'      
																			,'22474929-5'      
																			,'8332485-6'       
																			,'10761101-0'        			
																			) 
															)	
										union
						select 'Diego Zamorano',*,'20200625'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																				'13163174-K'      
																				,'20561675-6'      
																				,'17064138-8'      
																				,'6570194-4'       
																				,'6443602-3'       
																				,'6224710-K'       
																				,'11485196-5'      
																				,'16028808-6'      
																				,'17970524-9'      
																				,'17483132-7'      																	
																			) 
															)	
		union				select 'Alejandra Arenas',*,'20200626'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'10032318-4'      
																			,'13690663-1'      
																			,'13705943-6'      
																			,'13706395-6'      
																			,'14154393-8'      
																			,'14427913-1'      
																			,'16473397-1'      
																			,'9006265-4'             																	
																			) 
															)	
		union				select 'Diego Zamorano',*,'20200626'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'15667053-7'      
																			,'16683060-5'      
																			,'19921688-0'      
																			,'26292011-9'      																	
																			) 
															)	
		union				select 'Lilian Zech',*,'20200626'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'10553501-5'      
																			,'11548507-5'      
																			,'11667304-5'      
																			,'12272565-0'      
																			,'12342939-7'      
																			,'18248961-1'      
																			,'6102058-6'       
																			,'9909304-8'            																	
																			) 
															)	

				union		select 'Cata Castro',*,'20200626'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'16346566-3'
																			,'19633426-2'
																			,'10591778-3'
																			,'15453942-5'
																			,'10480138-2'															          																	
																			) 
															)	
				union		select 'Maria Jose Jorquera',*,'20200626'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'15200475-3'															          																	
																			) 
															)
				union		select 'Alejandra Arenas',*,'20200701'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'17421767-K'     
																			,'10210964-3'      
																			,'8312914-K' 															          																	
																			) 
															)																														
				union		select 'Lilian Zeck',*,'20200701'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'11108760-1'      
																			,'17065003-4'       															          																	
																			) 
															)	
				union		select 'Becados',*,'20200701'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'20122178-1'
																			,'10572816-6'
																			,'7515064-4'
																			,'18061350-1'       															          																	
																			) 
															)	
				union		select 'Alejandra Arenas',*,'20200702'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'17928733-1'      
																			,'8317516-8'       															          																	
																			) 
															)																														
				union		select 'Lilian Zeck',*,'20200702'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'15451814-2'       															          																	
																			) 
															)	
				union		select 'Victor Neira',*,'20200702'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'14179509-0','9575958-0'       															          																	
																			) 
															)	
				union		select 'Alejandra Arenas',*,'20200703'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'7516008-9'      
																			,'10062695-0'      
																			,'16985820-9'      
																			,'17928733-1'             															          																	
																			) 
															)																														
				union		select 'Lilian Zeck',*,'20200703'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'10846597-2'       															          																	
																			) 
															)	
				union		select 'BECADOS',*,'20200703'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'12427883-K'
																			,'12068473-6'
																			,'10407770-6'
																			,'10465021-K'
																			,'11550686-2'
																			,'9795799-1'      															          																	
																			) 
															)	
		union				select 'Alejandra Arenas',*,'20200707'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'17283454-K'      
																			,'16985820-9'      
																			,'19165554-0'      
																			,'11550686-2'      
																			,'10407770-6'                   																	
																			) 
															)	
		union				select 'Diego Zamorano',*,'20200707'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'27138698-2'      
																			,'19840101-3'      
																			,'5921224-9'             																	
																			) 
															)	
		union				select 'Lilian Zech',*,'20200707'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'15451814-2'     
																			,'11055151-7'      
																			,'8756188-7'       
																			,'9795799-1'                																	
																			) 
															)	
		union				select 'Lilian Zech',*,'20200708'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
											   							 	'17307897-8'      
																			,'9795799-1'       
																			,'13259240-3'      
																			,'17065003-4'      
																			,'5754944-0'      
																			,'22964013-5'      
																			) 
															)	
		union				select 'Alejandra Arenas',*,'20200708'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
 '8312914-K'       
,'10062695-0'      
,'10821013-3'      
,'24856062-2'      
,'19793398-4'      
,'11550686-2'      
,'17065003-4'                         																	
																			) 
															)	
		union				select 'Diego Zamorano',*,'20200708'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'8317516-8'       
,'16683060-5'      
,'26292011-9'      
,'17970524-9'      
,'12102364-4'      
,'6224710-K'       
,'6681061-5'       
,'19840101-3'      
,'5511734-9'                    																	
																			) 
															)

															
union				select 'Alejandra Arenas',*,'20200710'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'16985820-9'      
,'10062695-0'      
,'10407770-6'
,'10100313-2'      																			) 
															)	
		union				select 'Diego Zamorano',*,'20200710'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
 '6681061-5'       
,'11485196-5'      
,'26292011-9'      
,'17970524-9'      
,'16683060-5'      
,'25047754-6'      
,'12102364-4'      
,'9649321-5'       
,'26292011-9'      
,'13926855-5'                 																	
																			) 
															)	
		union				select 'Lilian Zech',*,'20200710'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'9795799-1'       
,'15451814-2'      
,'11055151-7'      
,'8756188-7'       
,'13259240-3'      
,'5754944-0'       
,'17065003-4'      
,'16122037-K'      																			) 
															)	
union				select 'Alejandra Arenas',*,'20200713'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'10793315-8'      
,'10100313-2'      
,'10407770-6'      
,'10210964-3'      
,'19165554-0'      
,'18830556-3'      
,'10062695-0'      
,'11550686-2'      
,'16985820-9'      
,'11939664-6'      
,'8776405-2'       
,'6763052-1'       
											   							 	) 
															)	
		union				select 'Diego Zamorano',*,'20200713'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'6681061-5'       
,'9649321-5'       
,'13926855-5'      
,'5045806-7'       
,'10703453-6'      
,'17970524-9'      
,'16683060-5'      
,'26292011-9'                      																	
																			) 
															)	
		union				select 'Lilian Zech',*,'20200713'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'3812832-9'       
,'11055151-7'      
,'8756188-7'       
,'13259240-3'      
,'17307897-8'      
,'9795799-1'       
,'17065003-4'      
,'11839564-6'      
											   							 	) 
															)																										
union				select 'Alejandra Arenas',*,'20200714'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'7436560-4'       
,'11550686-2'      
,'18830556-3'      
,'10210964-3'      
,'10407770-6'      
,'10147907-2'      
,'10747532-K'      
,'24856062-2'      
,'13296863-2'      
,'14302305-2'      
,'19669053-0'             
											   							 	) 
															)	
		union				select 'Diego Zamorano',*,'20200714'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'5045806-7'       
,'6681061-5'       
,'6224710-K'       																			) 
															)	
		union				select 'Lilian Zech',*,'20200714'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'15451814-2'      
'16122037-K'      
'17065003-4'      
'9795799-1'       
'11839564-6'      
'3812832-9'       											   							 	) 
															)															
union				select 'Alejandra Arenas',*,'20200717'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'7436560-4'       
,'16985820-9'      
,'10747532-K'      
,'10793315-8'      
,'8776405-2'       
,'22778182-3'      
,'11550686-2'      
,'10210964-3'      
,'10100313-2'      											   							 	) 
															)	
		union				select 'Diego Zamorano',*,'20200717'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'6224710-K'       
,'6681061-5'       
,'8317516-8'       
,'9649321-5'       
,'14142825-K'            																			) 
															)	
		union				select 'Lilian Zech',*,'20200717'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
																			'15451814-2'      
																			,'9795799-1'       
																			,'17065003-4'      
																			,'22964013-5'      
																			,'16122037-K'      
																			,'8882097-5'       
																			,'3812832-9'       														
																			)
															)
		union				select 'JPII',*,'20200720'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'18901437-6',
'20186350-3',
'20789349-8',
'19633474-2',
'13783545-2',
'4092524-4',
'23809560-3',
'16691464-7',
'6043594-4',
'13088682-5',
'19877567-3',
'10680186-K',
'11865763-2',
'10428195-8',
'8665676-0',
'7432527-0',
'10706050-2',
'12925330-4',
'16938915-2',
'15394653-1',
'7845235-8',
'9842190-4',
'16559138-0',
'17232567-K',
'6985091-K',
'15479545-6',
'8110985-0',
'19381632-0',
'17245785-1',
'10976983-5',
'12467227-9',
'17383776-3',
'6531931-4',
'19732787-1',
'16296199-3',
'7383668-9',
'26217767-K',
'17543894-7',
'17152428-8',
'20186647-2',
'21055326-6',
'9257113-0',
'18605405-9',
'6888832-8',
'10991820-2',
'27115081-4',
'19225151-6',
'8117690-6',
'6441845-9',
'7845209-9',
'18439401-4',
'19918170-K',
'24163668-2',
'19113437-0',
'8111822-1',
'10185568-6',
'12284746-2',
'10154528-8',
'13836350-3',
'12130151-2',
'6692339-8',
'18184397-7',
'20814693-9',
'22966584-7',
'8013640-4',
'22407860-9',
'21604981-0',
'20246458-0',
'24864980-1',
'20907579-2',
'20929968-2',
'15199207-2',
'13489842-9',
'7738601-7',
'17064464-6',
'18908982-1',
'13468544-1',
'17545795-K',
'26939998-8',
'22385900-3',
'26711523-0',
'20793939-0',
'6526531-1',
'18194322-K',
'4557401-6',
'27285967-1',
'18605421-0',
'23849088-K',
'15591573-0'      														
																			)
															)
union				select 'Alejandra Arenas',*,'20200721'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'11550686-2'      
,'8776405-2'       
,'16985820-9'      
,'11939664-6'      
,'10100313-2'      
,'10210964-3'      
,'18830556-3'      
,'7436560-4'       
,'10747532-K'
,'12902332-5'      
,'10062695-0'      
,'10473604-1'      
,'10701886-7'      
,'4998256-9'       
,'7436560-4'       
,'11550686-2'      
,'10210964-3'      
,'12902332-5'      
,'12100994-3'                
,'10473604-1'      ) 
															)	
		union				select 'Diego Zamorano',*,'20200721'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'14194198-4'      
,'8317516-8'       
,'9970459-4'       
,'9649321-5'       
,'6681061-5'       
,'19918694-9'     
,'14193334-5'      
,'6224710-K'       
,'14142825-K'      
,'8317516-8'       
,'6224710-K'       
,'6681061-5'       
,'6681061-5'                 																			) 
															)	
		union				select 'Lilian Zech',*,'20200721'::date carga_dia 
						  from carga_neotel_03 as a 
						 where nif_caso_indice in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
						   and nif_caso_indice in 
								   							(
								   							select nif_caso_indice
								   							  from carga_neotel_03 
								   							 where rut in 
											   							 	( 
'8756188-7'       
,'11839564-6'      
,'22964013-5'      
,'15451814-2'      
,'17065003-4'      
,'8882097-5'       
,'3812832-9'       
,'16122037-K'      
,'19706096-4'      
,'19443275-5'      
,'9795799-1'       
,'17065003-4'      
,'22964013-5'      
,'9795799-1'       
,'8882097-5'       
,'19706096-4'      
,'19443275-5'      																			)
															)
				) as a			
				 where rut not in 
				 					(
				 					select tis 
				 					  from admomi.iddpacpa3 
				 					 where estado2 is null 
				 					   and tis<>'' 
				 					   and tis is not null
				 					) 
				   and nif_caso_indice not in 
				 					(
				 					select nif2 
				 					  from admomi.iddpacpa3 
				 					 where estado2 is null 
				 					) 
			) as a 
	
		) as a;  --order by nif_caso_indice,num_caso; --rut nulls first,num_caso;

-- diego zamorano(14 hrs en adelante) , ale arenas, lilian zech (casos confirmado/probable, sospechoso, contacto, )
-- ('LZS','AAG','DZO')
--select * from carga_neotel_0303 where rut='11424428-7' or telefono1 in (977170253,948542951);

--select * from carga_neotel_0303 where telefono1 in (948799190);

--select dist	inct a.*,b.* from usr_edox.carga_neotel_0303 as a left join usr_edox.estado_de_seguimiento as b on a.rut=b.rut;

--select * from usr_edox.estado_de_seguimiento

--Pacientes Gestionados NEOTEL
drop table if exists usr_edox.carga_neotel_0304;
create table usr_edox.carga_neotel_0304 as
select * 
  from usr_edox.carga_neotel_0303 
 where usuario is not null 
   and nif_caso_indice in 
 							(
 							select nif 
 							 from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409
 							)
 and telefono1 not in
 (
'0222680796',
'0224570228',
'0225678542',
'0225679321',
'0228812920',
'0911111111',
'0911112222',
'0920610975',
'0922222223',
'0930878748',
'0931422472',
'0931743314',
'0932046188',
'0932135394',
'0932330163',
'0932542225',
'0932931865',
'0936420100',
'0936772020',
'0938842785',
'0941052193',
'0941754972',
'0941768852',
'0942378394',
'0942463034',
'0942640030',
'0944444444',
'0944770984',
'0945003884',
'0945003885',
'0945003886',
'0945003890',
'0945168864',
'0945316990',
'0945328719',
'0945502735',
'0945593508',
'0945984851',
'0947147935',
'0947424815',
'0947545696',
'0948542951',
'0948729292',
'0948799190',
'0949177884',
'0950697937',
'0950769152',
'0951279796',
'0952533646',
'0953567122',
'0953954315',
'0954124931',
'0954233851',
'0955119988',
'0955296463',
'0955555555',
'0956560350',
'0956741795',
'0957496163',
'0957665561',
'0958002558',
'0958731439',
'0958963568',
'0961366854',
'0961931414',
'0963194637',
'0965249704',
'0965261935',
'0965261938',
'0965793638',
'0966034268',
'0966291618',
'0966291620',
'0966291621',
'0966291624',
'0966291666',
'0966666667',
'0967301019',
'0968130909',
'0968341709',
'0968729791',
'0971207885',
'0971984121',
'0972072086',
'0972376793',
'0974506045',
'0974662878',
'0974973160',
'0975225159',
'0975451652',
'0975889458',
'0976243651',
'0976729719',
'0977112051',
'0977170253',
'0977173789',
'0977389321',
'0977861342',
'0978141760',
'0978171752',
'0979080386',
'0979399134',
'0981525621',
'0981525626',
'0981525688',
'0981525699',
'0982350715',
'0982396265',
'0982925016',
'0983252811',
'0983548134',
'0983868536',
'0984281982',
'0985544726',
'0985814289',
'0985892862',
'0986727983',
'0987049585',
'0987217496',
'0987346588',
'0987427073',
'0987472961',
'0987584603',
'0988266573',
'0988887777',
'0988887778',
'0988919060',
'0989071720',
'0990572715',
'0990685009',
'0990777592',
'099152567',
'099152568',
'099152569',
'0991751040',
'0992064892',
'0992276757',
'0992276759',
'0992276760',
'0992523062',
'0992658111',
'0992763230',
'0992991304',
'0992991305',
'0992991306',
'0993348092',
'0994163937',
'0994325607',
'0994983272',
'0995114350',
'0995391035',
'0995616770',
'0996572084',
'0996602714',
'0996909455',
'0997672919',
'0997862503',
'099795756',
'0998378932',
'971984121',
'0222654511',
'0222680796',
'0225678542',
'0912345678',
'0932330163',
'0932542225',
'0934214074',
'0936420100',
'0937501763',
'0937767743',
'0938842785',
'0945046967',
'0945168864',
'0945917711',
'0955296463',
'0956785815',
'0957682914',
'0965261938',
'0965793638',
'0965816702',
'0966291618',
'0968130909',
'0968729791',
'0971207885',
'0971396560',
'0972072086',
'0972376793',
'0974506045',
'0974508596',
'0975451652',
'0975580727',
'0977173789',
'0978300825',
'0982396265',
'0982477386',
'0984515908',
'0985544726',
'0987049585',
'0987144119',
'0987472961',
'0987584603',
'0987847615',
'0988919060',
'0990572715',
'0990685009',
'0992064892',
'0993695596',
'0993756553',
'0994325607',
'0222680796',
'0225678542',
'023015383',
'0911111111',
'0912345678',
'0932046188',
'0937767743',
'0945003885',
'0945003886',
'0945003890',
'0955296463',
'0955555555',
'0956785815',
'096176466',
'0965261935',
'0965261938',
'0965793638',
'0966291618',
'0966291666',
'0966291667',
'0968130909',
'0971207885',
'0974506045',
'0974508596',
'0975451652',
'0977173789',
'0981525626',
'0982396265',
'0985544726',
'0987584603',
'0988919060',
'0992991304',
'0993695596',
'0993756553',
'0222680796',
'0225678542',
'023015383',
'0911111111',
'0912345678',
'0932046188',
'0937767743',
'0944666565',
'0945003846',
'0945003885',
'0945003886',
'0945003889',
'0945003890',
'0945168864',
'0951796510',
'0953567122',
'0955296463',
'0955555555',
'0956785815',
'0958713864',
'0958963568',
'096176466',
'0965142094',
'0965261935',
'0965261938',
'0965793638',
'0966291618',
'0966291666',
'0966291667',
'0968130909',
'0968729791',
'0971207885',
'0972376793',
'0974506045',
'0974508596',
'0975451652',
'0975889458',
'0976808563',
'0977173789',
'0977684705',
'0981525626',
'0982396265',
'0984895553',
'0984968471',
'0985544726',
'0986646025',
'0987049585',
'0987584603',
'0987847615',
'0988919060',
'0992064892',
'0992991304',
'0993695596',
'0993756553',
'0997750106',
'0200000000',
'0222680450',
'0222680796',
'0225363694',
'0225366113',
'0225678542',
'023015383',
'053572008',
'057631864',
'078112149',
'078619136',
'079112149',
'081987257',
'084235589',
'0905416125',
'0911111111',
'0912345678',
'091821876',
'0931187251',
'0933234178',
'0933360121',
'0934231434',
'0937664462',
'0937767743',
'0938880219',
'0940003042',
'0941392844',
'0942812508',
'0942963610',
'0944666565',
'0945003885',
'0945003886',
'0945168864',
'0945200419',
'0946208180',
'0946408174',
'0947938734',
'0948749120',
'0949440290',
'0950677761',
'0950756889',
'0950769468',
'0950897530',
'0950897870',
'0950993142',
'0951796510',
'0953567122',
'0954325723',
'0954623639',
'0955296463',
'0955555555',
'095631864',
'0956785815',
'0958713864',
'0958963568',
'0961244201',
'0961452540',
'0961499890',
'096176466',
'0961892910',
'0963387308',
'0964192579',
'0965142094',
'0965215381',
'0965261935',
'0965261938',
'0965793638',
'0966240898',
'0966291618',
'0966291666',
'0966291667',
'0966339254',
'0967376440',
'0968130909',
'0968414912',
'0968737365',
'0968783640',
'0971207885',
'0972305028',
'0972738337',
'0972864787',
'0972881014',
'0972881914',
'0973251124',
'0974049887',
'0974506045',
'0974508596',
'0975169441',
'0975288333',
'0975451652',
'0975866376',
'0976808563',
'0976951509',
'0977173789',
'0977684705',
'0977997549',
'0978113932',
'0978503214',
'0979095488',
'0979229140',
'0981227554',
'0981525626',
'0982396265',
'0983716379',
'0983904146',
'0984895553',
'0984968471',
'0985336558',
'0985544726',
'0985681047',
'0986143419',
'0986444134',
'0986646025',
'0987049585',
'0987250789',
'0987616857',
'0989075211',
'0989802562',
'0992064892',
'0992190056',
'0992276757',
'0992991304',
'0993695596',
'0993756553',
'0994633538',
'0994971227',
'0996779682',
'0997132996',
'0997750106',
'0998590580',
'0998668650',
'0999595575',
'0999724697',
'967593539'
); --usuario,nif_caso_indice ,num_caso;

select distinct a.*,a02.med_nombre,a01.fecha 
  from usr_edox.carga_neotel_0304 as a --select * from usr_edox.carga_neotel_0304 as a
  left join 
  			(
  			select NULLIF(regexp_replace(nif, '\D','','g'), '')::integer as nif,medico
  					,fechaomi(fecha) as fecha
  					,row_number() over (partition by nif order by fecha) as rownum
  			  from usr_edox.iddage_carga_jpii 
  			 where btrim(nif)<>'' 
  			   and btrim(nif) is not null
  			) as a01 on a.nif_caso_indice=a01.nif and a01.rownum=1
  left join (
  			select medico,med_nombre,row_number() over (partition by medico) rownum from admomi.iddmed  
  			) as a02 on a02.rownum=1 and a02.medico=a01.medico
	where a.cesfam='JPII' order by a.nif_caso_indice, a.num_caso;
--drop table if exists usr_edox.admomi_iddage_20200722;create table usr_edox.admomi_iddage_20200722 as select * from admomi.iddage where fecha>='20200722'
/*
select * from usr_edox.iddage_carga_jpii
 */

--TOMA MUESTRA EXAMNES NEGATIGO CARGA NEOTEL
select   a01.apellido1,a01.pac_nombre_completo as  nombre
		,a01.tis as ut 
		,edad_en_agnios(now()::date,a01.nacimiento) as edad
		,'direccion' as direccion
		,'correo@correo.cl' as mail
		,case when a01.telefono1 is not null then a01.telefono1 else telefono2 end as telefono1
		,centro as cesfam
		,''::text as fr  
		,'PCR '||a.examen_resultado_interpretado as condicion
		,'Por Notificar' as notificacion
		,'NO' as embarazada
		,a.orden_fecha_toma as "fecha inicio"
		,a.orden_fecha_recepcion as "fecha termino"
		,null::date as "fecha Seguimiento" 		--,'20200713'::date as carga_dia
  FROM examenes_etl.mg_vw_ancora_final2 as a 	--select * FROM examenes_etl.mg_vw_ancora_final2 as a  
  left join 
  			(
  			select centro,tis,nacimiento,pac_nombre_completo,apellido1
	  				,NULLIF(regexp_replace(telpart, '\D','','g'), '')::numeric as telefono1
	  				,NULLIF(regexp_replace(teldesp, '\D','','g'), '')::numeric as telefono2
  					,row_number() over (partition by nif2 order by fechalta) rownum
  			  from admomi.iddpacpa3 where tis<>'' and tis is not null
  			) as a01 on a01.tis=a.paciente_rut and a01.rownum=1 --  select * FROM examenes_etl.mg_vw_ancora_final2 as a
 where a.examen_codigo = '2798H' 
   --and a.examen_resultado_interpretado <> 'Positivo'
   and a.orden_fecha_recepcion >= '20200722 00:00:00' 
   and a01.centro in ('JPII')
 order by 1,a.orden_fecha_toma; --a.orden_fecha_toma desc nulls last;

--tabla madre
with consulta as 
				(
				select * 
				  from examenes_etl.mg_vw_ancora2 
				 where interpretedresult<>'' 
				   and interpretedresult is not null 
				   and profilecode='2798H' 
				   and patient_rut in (
				    '9743359-3'
					,'9497123-3'
					,'17951084-7'
					,'23852751-1')
				 --limit 100
				)
				select distinct 
						 a01.apellido1,a01.pac_nombre_completo as  nombre
						,a01.tis as ut 
						,edad_en_agnios(now()::date,a01.nacimiento) as edad
						,'direccion' as direccion
						,'correo@correo.cl' as mail
						,case when a01.telefono1 is not null then a01.telefono1 else telefono2 end as telefono1
						,a01.centro as cesfam
						,''::text as fr  
						,'PCR '||a.interpretedresult as condicion
						,'Por Notificar' as notificacion
						,'NO' as embarazada
						,a.receptiondate::date as "fecha inicio"
						,a.receptiondate::date as "fecha termino"
						,null::date as "fecha Seguimiento"
						,a.*
			      from consulta as a
			      left join 
				  			(
				  			select centro,tis,nacimiento,pac_nombre_completo,apellido1
					  				,NULLIF(regexp_replace(telpart, '\D','','g'), '')::numeric as telefono1
					  				,NULLIF(regexp_replace(teldesp, '\D','','g'), '')::numeric as telefono2
				  					,row_number() over (partition by nif2 order by fechalta) rownum
				  			  from admomi.iddpacpa3 where tis<>'' and tis is not null
				  			) as a01 on a01.tis=a.patient_rut and a01.rownum=1 --  select * FROM examenes_etl.mg_vw_ancora_final2 as a
			     ;

select *
FROM examenes_etl.mg_vw_ancora_final2 as a --select * FROM examenes_etl.mg_vw_ancora_final2 as a  
  left join 
  			(
  			select centro,tis,nacimiento,pac_nombre_completo
	  				,NULLIF(regexp_replace(telpart, '\D','','g'), '')::numeric as telefono1
	  				,NULLIF(regexp_replace(teldesp, '\D','','g'), '')::numeric as telefono2
  					,row_number() over (partition by nif2 order by fechalta) rownum
  			  from admomi.iddpacpa3 where tis<>'' and tis is not null
  			) as a01 on a01.tis=a.paciente_rut and a01.rownum=1
 where a.examen_codigo = '2798H' 
   --and a.examen_resultado_interpretado = 'Negativo'
   and paciente_rut='12561968-1' --and a.orden_fecha_toma >= '20200709 00:00:00'
   and a01.centro='SAH'
 order by a.orden_fecha_toma desc nulls last;


select row_number() over (partition by rut order by carga_dia),* from usr_edox.carga_neotel_0303 
 where fecha_confirmacion is not null 
   and nif_caso_indice in (select NIF from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409)
   and nif_caso_indice in (select nif2 from admomi.iddncu where ciap in ('G12'));

  select * 
  from usr_edox.carga_neotel_0303    
 where rut = '10995968-5';
--mauri poli beca  
select * 
  from usr_edox.carga_neotel_0303    
 where telefono1 in 
						  (
						 select distinct telefono1 from usr_edox.carga_neotel_0303 where edad<=15
						  );

select a01.pac_nombre_completo as nombre
		,a01.tis as rut
		,edad_en_agnios(now(),a01.nacimiento) as edad
		,'no registrada' direccion
		,'correo@correo.cl' as email
		,NULLIF(regexp_replace(a01.telpart, '\D','','g'), '')::numeric telefono1
		,a01.centro
		,'' as fr
		,examen_resultado_interpretado as conndicion --'COVID Descartado' as condicion
		,'POR NOTIFICAR' as notificacion
		,'NO REGISTRA' as embarazada
		,a.orden_fecha_solicitud as fecha_inicio
		,a.orden_fecha_toma as fecha_fin
		FROM examenes_etl.mg_vw_ancora_final2 as a 
   left join 
   				(
   				select nif2,pac_nombre_completo,nacimiento,centro,tis,telpart
   						,row_number() over (partition by nif2 order by fechalta) as rownum
   				  from admomi.iddpacpa3 
   				) as a01 on a01.nif2=a.paciente_nif and a01.rownum=1
 where a.examen_codigo = '2798H' 
   and a.examen_resultado_interpretado = 'Negativo'
   and a.orden_fecha_solicitud >=
  '20200707';

  
select 
		 row_number() over (partition by a.rut order by a.fecha_confirmacion) as rownum
		,substr(a.num_caso,5,2)::integer as num_caso2
		,(select array(select distinct t.nif_caso_indice from usr_edox.carga_neotel_0303 t where t.rut=a.rut) ) as en_otros_grupos
		,a.*
  from usr_edox.carga_neotel_0303 as a 
 where rut in ('5754944-0','13259240-3') order by nombre;

drop table if exists casos_0001;create temp table casos_0001 as  
select 
	 row_number() over (partition by a.rut order by a.fecha_confirmacion) as rownum
	,substr(a.num_caso,5,2)::integer as num_caso2
	,(select array(select distinct t.nif_caso_indice 
					 from usr_edox.carga_neotel_0303 t
					where t.rut=a.rut) ) as en_otros_grupos
	,a.*
 from carga_neotel_03 as a where fecha_confirmacion is not null;
			     
drop table if exists casos_0002;create temp table casos_0002 as
select a.*
		,row_number() over (partition by a.nif_caso_inidice_indice)
  from
		(
		select  distinct 
				case when a.en_otros_grupos<>'{}' 		then 	a01.nif_caso_indice 	else a.nif_caso_indice			end 	as nif_caso_inidice_indice
			   ,min(case when a.en_otros_grupos<>'{}' 	then	a01.fecha_confirmacion  else a.fecha_confirmacion   	end)  	as nif_fecha_confirmacion_indice
			   --,case when a.en_otros_grupos<>'{}' then	a01.telefono1  			else a.telefono1  			end as telefono1_indice
			   --,case when a02.rut is not null then a02.pac_nombre_completo 		else a03.nombre				end as nombre
			   --,a.rut
			   --,row_number() over (partition by a.rut) 
			   --,row_number() over (partition by a.rut)    
		  from casos_0001 as a
		 	left join 
						(
						select nif_caso_indice,en_otros_grupos,fecha_confirmacion,telefono1,nombre 
								,row_number() over (partition by en_otros_grupos order by fecha_confirmacion) rownum
						  from casos_0001 where en_otros_grupos<>'{}'
						) as a01 on a01.rownum=1 and a.en_otros_grupos=a01.en_otros_grupos
						group by 1	  
		 ) as a
  where nif_caso_inidice_indice in (select nif2 from admomi.iddncu where ciap in ('G12','G13') );

 
 drop table if exists casos_0003;create temp table casos_0003 as
 select * from casos_0002
 
		 where a.rut is not null and a.rut<>''
			left join 
						(
						select tis rut,pac_nombre_completo
								,row_number() over (partition by nif2 order by fechalta) rownum
						  from admomi.iddpacpa3 
						 where estado2 is not null and run is not null
						) as a02 on a02.rownum=1 and a02.rut=a.rut
			 left join 
						(
						select  rut
								,btrim(replace(replace(nombre,'-',''),'  ',' ')) as nombre
								,length(nombre) as largo
								,row_number() over (partition by rut order by length(nombre) desc) rownum
								,edad,condicion,embaraza as embarazada
						  from casos_0001 --select * from casos_0001
						 where rut<>'' and rut is not null
						) as a03 on a03.rownum=1 and a.rut=a03.rut

-- order by 5 desc--and a.rut in (select tis from admomi.iddpacpa3 where estado2 is not null and length(tis)>=3 and tis like '%-%')
;

select a.rut 
  from casos_0001 as a 
  left join 
  			(
  			select   nif_caso_indice
  					,rut
  					,fecha_confirmacion 
  		      from carga_neotel_03 
  		     where fecha_confirmacion is not null
  			) as b 
 group by 1;

--------------------------------------------------------------------------------
-- diferencia casos activos vs casos en NEOTEL
--------------------------------------------------------------------------------

 
 

with consulta as (
				select row_number() over (partition by nif_caso_indice order by fecha_confirmacion) rownum 
						,('0'::varchar||telefono1::varchar) telefono2,* 
				  from carga_neotel_03 
				 where nif_caso_indice in 	(
				 							select nif2 
				 							  from
				 							  		(
				 							  		select * from usr_edox.estado_de_seguimiento02 where centro in ('JPII','SAH') 
				 							  		) as a
				 							)--and estado_seguimiento not like '%alta probable%' 						
				)
select distinct 
		 a01.med_nombre
		 ,a.fecha_confirmacion::date as fecha_confirmacion2
		,a.rownum
		,a.telefono2
		,a.fecha_confirmacion
		,a.nif_caso_indice
		,a.num_caso
		,a.nombre
		,a.rut
		,a.edad
		,a.direccion
		,a.email
		,a.telefono1
		,a.cesfam
		,a.fr
		,a.condicion
		,a.notificacion
		,a.embaraza
		,a.fecha_inicio
		,a.fecha_fin
		,a.fecha_seguimiento
  from consulta as a
  left join	(
  			select   NULLIF(regexp_replace(aa.nif, '\D','','g'), '')::numeric as nif 
  					,bb.med_nombre
  					,row_number() over (partition by aa.nif order by aa.fecha) rownum
  			  from usr_edox.admomi_iddage_20200722 as aa
  			  		,admomi.iddmed as bb
  			  where aa.medico=bb.medico and bb.id>=2000000 			     
  			    and aa.nif<>'' and aa.nif is not null
  			) as a01
  			  on a01.nif=a.nif_caso_indice
  			 and a01.rownum=1
where a.nif_caso_indice not 	in (select nif2 from admomi.iddpacpa3 where estado2 is null)
  --and a.nif_caso_indice 		in (select nif from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409)
  and a.nif_caso_indice 		in (select nif_caso_indice from carga_neotel_03 where condicion='Confirmado')
  and a.fecha_confirmacion>='20200723 00:00:00' order by 1,6,7;
  and a.telefono2 not in 
(
'00225679008',
'0200000000',
'0222654511',
'0222680450',
'0222680796',
'0225363694',
'0225366113',
'0225678542',
'022681545',
'0229953119',
'023015383',
'0232642731',
'0232657439',
'027613855',
'042126951',
'045328719',
'050941866',
'053572008',
'053610801',
'057631864',
'068188772',
'068315004',
'071533650',
'074023997',
'078112149',
'078619136',
'079112149',
'081987257',
'082856112',
'084222605',
'084235589',
'084815955',
'086661086',
'0902680651',
'0902681567',
'0902684086',
'0902829582',
'0903587528',
'0904933005',
'0905139951',
'0905416125',
'090547062',
'0907250143',
'0908745717',
'0908819519',
'0909194847',
'0909312592',
'0911111111',
'0912345678',
'091807437',
'091821876',
'0930935311',
'0931126874',
'0931187251',
'0932046188',
'0932330163',
'0932542225',
'0933234178',
'0933360121',
'0933772020',
'0934214074',
'0934231434',
'0934427422',
'0936101107',
'0936420100',
'0936910190',
'0937501763',
'0937628866',
'0937664462',
'0937767743',
'0938842785',
'0938880219',
'0940003042',
'0940550029',
'0941052193',
'0941392844',
'0942111574',
'0942812508',
'0942963610',
'0944666565',
'0945003846',
'0945003885',
'0945003886',
'0945003889',
'0945003890',
'0945046967',
'0945168864',
'0945200419',
'0945917711',
'0945984851',
'0946202070',
'0946208180',
'0946260199',
'0946408174',
'0947545696',
'0947938734',
'0948749120',
'0949440290',
'0950064916',
'0950677761',
'0950756889',
'0950769152',
'0950769468',
'0950897530',
'0950897870',
'0950993142',
'0951796510',
'0952038194',
'095279111',
'0953567122',
'0954325723',
'0954436314',
'0954623639',
'0954765143',
'0955119988',
'0955296463',
'0955555555',
'095631864',
'0956785815',
'0957313882',
'0957496163',
'095755711',
'0957665561',
'095774906',
'0958116241',
'0958173227',
'0958713864',
'0958963568',
'0959506786',
'0961244201',
'0961359369',
'0961452540',
'0961499890',
'096176466',
'0961892910',
'0962140287',
'0962262869',
'0963069456',
'0963387308',
'0964192579',
'0964901250',
'0965142094',
'0965215381',
'0965261333',
'0965261334',
'0965261335',
'0965261935',
'0965261938',
'0965793638',
'0965816702',
'0966240898',
'0966291618',
'0966291666',
'0966291667',
'0966339254',
'0967131872',
'0967376440',
'0967593539',
'0968120435',
'0968130909',
'0968414912',
'0968729791',
'0968737365',
'0968783640',
'0971207885',
'0971396560',
'0972072086',
'0972305028',
'0972376793',
'0972738337',
'0972864787',
'0972881014',
'0972881914',
'0973251124',
'0974049887',
'0974082381',
'0974506045',
'0974508596',
'0975169441',
'0975288333',
'0975367475',
'0975451652',
'0975476433',
'0975580727',
'0975866376',
'0975889458',
'0976808563',
'0976951509',
'0977173789',
'0977684705',
'0977997549',
'0978113932',
'0978171752',
'0978300825',
'0978503214',
'0978521645',
'0978637490',
'0979095488',
'0979229140',
'0981227554',
'0981525626',
'0982256624',
'0982396265',
'0982477386',
'0982716275',
'0982992657',
'0983716379',
'0983868536',
'0983904146',
'0984515908',
'0984895553',
'0984968471',
'0985336558',
'0985544726',
'0985681047',
'0986143419',
'0986444134',
'0986646025',
'0987049585',
'0987144119',
'0987250789',
'0987441875',
'0987472961',
'0987584603',
'0987616857',
'0987711360',
'0987847615',
'0988004013',
'0988919060',
'0989075211',
'0989802562',
'0990070700',
'0990111002',
'0990572715',
'0990685009',
'0992064892',
'0992190056',
'0992276757',
'0992991304',
'0993695596',
'0993756553',
'0994325607',
'0994633538',
'0994971227',
'0996779682',
'0997132996',
'0997750106',
'0997763132',
'0998590580',
'0998668650',
'0999190317',
'0999595575',
'0999724697',
'0967593539'
) order by 1,6,7;
 
--desduplicar casos indice indice
select *
  from
  		(
		select a.*,a01.nif_caso_indice as nif_caso_indice2,a01.fecha_confirmacion  
		  from
				(		
				select distinct b.nif_caso_indice,a.rut 
				  from usr_edox.carga_neotel_03 as a
				  left join usr_edox.carga_neotel_03 as b on a.rut=b.rut and b.rut<>''
				 where a.rut<>''
				) as a
				left join	(
							select nif_caso_indice,fecha_confirmacion from usr_edox.carga_neotel_03 as a
							) as a01
							  on a01.nif_caso_indice=a.nif_caso_indice
		);

	
	

with consulta as (
				select row_number() over (partition by nif_caso_indice order by fecha_confirmacion) rownum 
						,('0'::varchar||telefono1::varchar) telefono2,* 
				  from usr_edox.carga_neotel_03 
				 where nif_caso_indice in 	(
				 							select nif2 
				 							  from
				 							  		(
				 							  		select * from usr_edox.estado_de_seguimiento02 where centro in ('JPII','SAH') 
				 							  		) as a
				 							)--and estado_seguimiento not like '%alta probable%' 						
				)
select distinct 
		 a01.med_nombre
		 ,a.fecha_confirmacion::date as fecha_confirmacion2
		,a.rownum
		,a.telefono2
		,a.fecha_confirmacion
		,a.nif_caso_indice
		,a.num_caso
		,a.nombre
		,a.rut
		,a.edad
		,a.direccion
		,a.email
		,a.telefono1
		,a.cesfam
		,a.fr
		,a.condicion
		,a.notificacion
		,a.embaraza
		,a.fecha_inicio
		,a.fecha_fin
		,a.fecha_seguimiento
  from consulta as a
  left join	(
  			select   NULLIF(regexp_replace(aa.nif, '\D','','g'), '')::numeric as nif 
  					,bb.med_nombre
  					,row_number() over (partition by aa.nif order by aa.fecha) rownum
  			  from usr_edox.admomi_iddage_20200722 as aa
  			  		,admomi.iddmed as bb
  			  where aa.medico=bb.medico and bb.id>=2000000 			     
  			    and aa.nif<>'' and aa.nif is not null
  			) as a01
  			  on a01.nif=a.nif_caso_indice
  			 and a01.rownum=1
where a.nif_caso_indice not 	in (select nif2 from admomi.iddpacpa3 where estado2 is null)
  and a.nif_caso_indice 		in (select nif from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409)
  and a.nif_caso_indice 		in (select nif_caso_indice from carga_neotel_03 where condicion='Confirmado')
  and a.fecha_confirmacion>='20200723 00:00:00' order by 1,6,7;	
	
	
	
	
	
--select row_number() over (partition by rut) as rut_repetido,* from carga_neotel_03 where nif_caso_indice not in (select nif2 from admomi.iddpacpa3 where estado2 is null)
--select count(*) from (select distinct nif_caso_indice from carga_neotel_03 where centro='JPII') as a --totales 4007, activos 552
--select count(*) from (select distinct nif_caso_indice,num_caso from carga_neotel_03 where num_caso<>'caso00') as a --2301 casos contacto
--select count(*) from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 --seguimientos 11828 (568 x neotel)

--totales casos confirmados 4007, casos activos 552, --2301 casos contacto --seguimientos 11828 (568 x neotel)

/*
select a.*,b.day_of_week
  from usr_edox.carga_neotel_0303 as a
  left join 
  			(
  			select full_date,day_of_week
  					,row_number() over (partition by full_date) as rownum
  			  from biancora.dim_tiempo
  			) as b on a.fecha_confirmacion=b.full_date and b.rownum=1
  			--select * from biancora.dim_tiempo
*/
--select * from usr_edox.carga_neotel_0303 order by nif_caso_indice ,num_caso;

/*
select a.medico,a.nif ,b.tis
  from admomi.iddage as a left join admomi.iddpac as b on a.nif=b.nif
 where a.medico in ('LZS','AAG','DZO') 
   and a.fecha in (80190) --= 80174 
   and a.nif<>'' order by a.medico
   */
   /* 
   order by a.medico
   and a.nif not in (
					   select a.nif
					  from admomi.iddage as a left join admomi.iddpac as b on a.nif=b.nif
					 where a.medico in ('LZS','AAG','DZO') 
					   and a.fecha between 80168 and 80180-- (80168,80169,80174) 
					   and a.nif<>''
					) order by a.medico*/

/*
select distinct * 
  from carga_neotel_03 
 where rut not in 
 					(
 					select tis 
 					  from admomi.iddpacpa3 
 					 where estado2 is null 
 					   and tis<>'' 
 					   and tis is not null
 					) 
   and nif_caso_indice not in 
 					(
 					select nif2 
 					  from admomi.iddpacpa3 
 					 where estado2 is null 
 					) 
order by rut nulls first,num_caso;
*/

/*
select * from carga_neotel_03;
select * from usr_edox.carga_neotel_0303;

select a.*
		,case when fecha_seg_previo is not null then fecha_seguimiento-fecha_seg_previo end as dif
  from 
  		(
		select a.nif
			,count(*) over (partition by a.nif) as q
			,a.fecha2 as fecha_seguimiento
			,LAG(a.fecha2,1) OVER (PARTITION BY a.nif ORDER BY a.fecha2) fecha_seg_previo
			,a01.riesgo
			,case when a02.rownum is not null then 'SI' else 'NO' end as telesaluc 
		  from admomi.iddcor as a --select * from admomi.iddcor as a where a.wproto=412 and a.numero=1
		  left join
		  			(
		  	  			select nif2,fecha
		  					,case 
		  						when campo='1' then 'Alto'
		  						when campo='2' then 'Medio'
		  						when campo='3' then 'Bajo'
		  						end as riesgo
		  					,row_number() over (partition by nif2 order by fecha desc) rownum
		  			  from protocolos_omi_covid_base.iddcop 
		  			 where wproto=409 
		  			   and wnumero=534 
		  			   and campo is not null 
		  			   and campo<>''
		  			) as a01 on a01.nif2=a.nif and a01.rownum=1
		  	left join 
		  			(
		  			select 
		  					 nif_caso_indice
		  					,row_number() over (partition by nif_caso_indice) rownum
		  				from usr_edox.carga_neotel_0303 where condicion='Confirmado'
		  			) as a02 on a02.nif_caso_indice=a.nif and a02.rownum=1
		 where a.numero=1 
		   and a.wproto=409
		   and a.fecha2 between '20200615' and '20200626'
		   and a.nif in 	(
		   					select nif2 from admomi.iddpacpa3 where centro='SAH'
		   					)
		) as a where q>=2
  order by a.nif,a.fecha_seguimiento;
*/

/* --SCRIPT PARA DEDUPLICAR PACIENTES CASOS Y GRUPOS
drop table if exists usr_edox2.covid_casos001;
create table usr_edox2.covid_casos001 (id serial,nif2 integer, fecha timestamp);
insert into usr_edox2.covid_casos001
select nextval('usr_edox2.covid_casos001_id_seq') as id,a.nif2,a.fecha
  from 
  		(
		select nif2,min(stk_fecha) as fecha
		  from protocolos_omi_covid_base.hisa_iddncu 
		 where ciap in ('G10','G12','G13') 
		   and stk_tipo_operacion='A' 
		   and nif2 in (select nif2 from admomi.iddpacpa3 where estado2 is not null)
		 group by 1 order by 2
		) as a;

--select * from usr_edox2.covid_casos001


drop table if exists usr_edox2.covid_casos002;
create table usr_edox2.covid_casos002 as
select a.*
		,a04.rut
		,a01.stk_fecha Sospecha
		,a02.stk_fecha Confirmacion
		,a03.stk_fecha Probable
  from usr_edox2.covid_casos001 as a
  left join
  			(select nif2,stk_fecha,row_number() over (partition by nif2 order by stk_fecha) rownum
			  from protocolos_omi_covid_base.hisa_iddncu 
		 	 where ciap in ('G10') 
		       and stk_tipo_operacion='A' 
  			) as a01 on a01.nif2=a.nif2 and a01.rownum=1
  left join
  			(select nif2,stk_fecha,row_number() over (partition by nif2 order by stk_fecha) rownum
			  from protocolos_omi_covid_base.hisa_iddncu 
		 	 where ciap in ('G12') 
		       and stk_tipo_operacion='A' 
  			) as a02 on a02.nif2=a.nif2 and a02.rownum=1
  left join
  			(select nif2,stk_fecha,row_number() over (partition by nif2 order by stk_fecha) rownum
			  from protocolos_omi_covid_base.hisa_iddncu 
		 	 where ciap in ('G13') 
		       and stk_tipo_operacion='A' 
  			) as a03 on a03.nif2=a.nif2 and a03.rownum=1
  left join 
  			(
  			select nif2,tis as rut,row_number() over (partition by nif2 order by fechalta) as rownum
  			  from admomi.iddpacpa3
  			) as a04 on a04.nif2=a.nif2 and a04.rownum=1
;
  
update usr_edox2.covid_casos002 set rut=nif2::text||'-P' where rut not like '%-%';

select * from usr_edox2.covid_casos002 order by rut;
-- where sospecha is not null and confirmacion is not null
--   order by 7 nulls last
  
select * from carga_neotel_03 where nif_caso_indice in (select nif2 from protocolos_omi_covid_base.iddncu where ciap in ('G12','G13'));
 * */