/*
 casos = casos confirmados con PCR positiva, episodio G12 covid confirmado, episodio g13 caso covid probable
 */
drop table if exists total_casos_identificados01;
create temp table total_casos_identificados01 as
select   a.patient_rut --NULLIF(regexp_replace(a01.nif, '\D','','g'), '')::numeric as nif2
		,a.orderdate::timestamp as fecha
		,1::smallint as pcr
  from examenes_etl.mg_vw_ancora2 as a 
 where a.profilecode = '2798H' 
   and a.interpretedresult='Positivo'
union
select a01.rut--,NULLIF(regexp_replace(a.nif, '\D','','g'), '')::numeric as nif2
	,a02.stk_fecha::timestamp
	,1::smallint as pcr
  from protocolos_omi_covid_base.examenes_pcr_omi as a
  left join	(
  			select nif2,tis as rut,row_number() over (partition by nif order by fechalta) rownum
    		  from admomi.iddpacpa3
    		 where tis is not null and tis<>'' and tis like '%-%'
    		) as a01 on a01.nif2=a.nif2 and a01.rownum=1
  left join	(
  			select * 
  					,row_number() over (partition by nif2,numorden order by stk_fecha) rownum
  			   from protocolos_omi_covid_base.hisa_iddome as a 
  			  where stk_tipo_operacion='A'
  			) as a02 on a02.nif2=a.nif2 and a02.rownum=1 and a02.numorden=a.numorden
 where a.resultado like '%POSITIV%'
 union
 select   a01.rut
		,a02.stk_fecha::timestamp as fecha--a.fecha
		,0::smallint as pcr
  from protocolos_omi_covid_base.iddncu as a --select * from protocolos_omi_covid_base.hisa_iddncu as a
  left join	(
  			select nif2,tis as rut,row_number() over (partition by nif order by fechalta) rownum
    		  from admomi.iddpacpa3
    		 where tis is not null and tis<>'' and tis like '%-%'
    		) as a01 on a01.nif2=a.nif2 and a01.rownum=1
  left join	(
  			select * 
  					,row_number() over (partition by nif2,especiali order by stk_fecha) rownum
  			   from protocolos_omi_covid_base.hisa_iddncu as a 
  			  where stk_tipo_operacion='A'
  			) as a02 on a02.nif2=a.nif2 and a02.rownum=1 and a02.especiali=a.especiali
  where a.ciap in ('G12','G13')
; 

delete from total_casos_identificados01 where patient_rut is null;
--select patient_rut,min(fecha) over (partition by patient_rut,extract(week from fecha) order by fecha ) from total_casos_identificados01;
--select * from total_casos_identificados01 order by 1 nulls first;

drop table if exists total_casos_identificados02;
create temp table    total_casos_identificados02 as
select   upper(btrim(a.patient_rut)) as rut
		--a01.nif2
		,min(a.fecha) as fecha 
  from total_casos_identificados01 as a
  left join
  			(
  			select nif2,tis as rut 
  					,row_number() over (partition by nif2 order by fechalta) rownum
  			  from admomi.iddpacpa3
  			 where tis is not null and tis<>'' and tis like '%-%'
  			) as a01 on a01.rut=a.patient_rut
 group by 1 order by 1 nulls first; --2842 --2815 --2836

delete from  total_casos_identificados02 where rut is null;

--select *,row_number() over (partition by fecha) from total_casos_identificados02 order by 3 desc,2

drop table if exists casos_contactos01;
create temp table    casos_contactos01 as
select  distinct a01.rut as rut_caso,valor as rut_contacto
  from protocolos_omi_covid_base.datos_contactos01 as a
  left join	(
  			select nif2,tis as rut 
  					,row_number() over (partition by nif2 order by fechalta) rownum
  			  from admomi.iddpacpa3
  			 where tis is not null and tis<>'' and tis like '%-%'
  			) as a01 on a01.nif2=a.nif
 where a.dato like 'RUT%' 
   and a.valor<>''
 order by 1,2 nulls last; --7293

drop table if exists pacientes01;
create temp table    pacientes01 as 
with consulta as 	(
					select btrim(upper(rut_caso)) as rut_paciente 	from casos_contactos01 union
					select btrim(upper(rut_caso)) as rut_contacto 	from casos_contactos01 union
					select btrim(upper(rut))	  as rut			from total_casos_identificados02
					)
					select distinct rut_paciente 
					  from consulta 
					 where rut_paciente is not null order by 1;

drop table if exists pacientes02;
create temp table    pacientes02 as 
select a.rut_paciente,rut_caso 
  from pacientes01 as a
  left join casos_contactos01 as a01 on a01.rut_contacto=a.rut_paciente;
  
drop table if exists pacientes03;create temp table    pacientes03 as 
select distinct a.rut_paciente
		,substr(rut_paciente,1,position('-' in rut_paciente)-1) as rut_pac2
		,a.rut_caso
		,substr(rut_caso,1,position('-' in rut_caso)-1) as rut_caso2
		,a01.fecha 
  from pacientes02 as a
  left join total_casos_identificados02 as a01 on a01.rut=a.rut_caso
order by 2,3;

--select distinct substr(rut_paciente,1,position('-' in rut_paciente)-1) from pacientes03 where rut_paciente like '%-%' and rut_caso like '%-%';
--select * from total_casos_identificados02
/*
select  distinct row_number() over (partition by rut_caso) repetido_caso
		,rut_caso
		,row_number() over (partition by rut_paciente) repetido_contacto
		,rut_paciente as rut_contacto
		,fecha as fecha_caso
		,min(fecha) over (partition by rut_paciente) as min_fecha_caso
  from pacientes03 
 where fecha is not null
   and rut_paciente 
  	  in (
'15939383-6'
'16646722-5',
'18609435-2',
'26153173-9',
'7073503-2',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'26153173-9',
'26153173-9',
'26153173-9',
'26153173-9',
'26153173-9',
'15939383-6',
'17672873-6',
'18609435-2',
'23010471-9',
'26153173-9',
'7073503-2',
'9253244-5') or rut_caso in (
'15939383-6'
'16646722-5',
'18609435-2',
'26153173-9',
'7073503-2',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'26153173-9',
'26153173-9',
'26153173-9',
'26153173-9',
'26153173-9',
'15939383-6',
'17672873-6',
'18609435-2',
'23010471-9',
'26153173-9',
'7073503-2',
'9253244-5')
  order by 5; 
*/

drop table if exists pacientes04;create temp table    pacientes04 as
select  distinct row_number() over (partition by rut_caso) repetido_caso
		,rut_caso
		,row_number() over (partition by rut_paciente) repetido_contacto
		,rut_paciente as rut_contacto
		,fecha as fecha_caso
		,min(fecha) over (partition by rut_paciente) as min_fecha_caso
  from pacientes03;

drop table if exists pacientes05;create temp table pacientes05 as 
 select distinct a.*,a01.rut_caso as rut_caso_primario
   from pacientes04 as a
   left join 	(
   				select rut_caso,fecha_caso
   						,row_number() over (partition by rut_caso order by fecha_caso) rownum
   				  from pacientes04 where fecha_caso is not null
   )as a01 on a.min_fecha_caso=a01.fecha_caso and a01.rownum=1;

delete 
  from pacientes05 
 where rut_caso in 
						(
						select tis from admomi.iddpacpa3 where estado2 is null and tis is not null and tis<>'' and tis like '%-%'
						)
    or rut_contacto in 
						(
						select tis from admomi.iddpacpa3 where estado2 is null and tis is not null and tis<>'' and tis like '%-%'
						);

drop table if exists protocolos_seguimient01;create temp table protocolos_seguimient01 as 
with consulta as	(
					select a.nif2,a.fecha2,a.campo 
					  from protocolos_omi_covid_base.iddcop as a,admomi.iddcol as b --select * from protocolos_omi_covid_base.iddcop limit 100
					 where a.wproto=b.wproto
					   and b.wproto=409 
					   and a.wnumero=b.wnumero
					   and upper(b.wcomentari) like '%RUT%'
					   and a.campo<>'' 
					   and a.campo is not null
					)
select a01.rut as rut_caso,a.fecha2,a.campo as rut_contacto
  from consulta as a --select * from protocolos_omi_covid_base.iddcop limit 100
  	left join	(
  				select nif2,tis as rut 
  						,row_number() over (partition by tis order by fechalta) rownum
  				  from admomi.iddpacpa3 
  				 where estado2 is not null 
  				   and tis<>'' 
  				   and tis is not null 
  				   and tis like '%-%'
  				) as a01 
  				  on a01.nif2=a.nif2 
  				 and a01.rownum=1
; 

update protocolos_seguimient01 set rut_caso=replace(rut_caso,'.','')				;
update protocolos_seguimient01 set rut_caso=replace(rut_caso,',','')				;
update protocolos_seguimient01 set rut_caso=replace(rut_caso,'--','-')				;
update protocolos_seguimient01 set rut_caso=replace(rut_caso,':','')				;
--update protocolos_seguimient01 set rut_caso=replace(rut_caso,'G10T','')			;
--update protocolos_seguimient01 set rut_caso=replace(rut_caso,'G12T','')			;
--update protocolos_seguimient01 set rut_caso=replace(rut_caso,'G13T','')			;
update protocolos_seguimient01 set rut_caso=replace(rut_caso,'*','')				;
update protocolos_seguimient01 set rut_caso=replace(rut_caso,'/','')				;
update protocolos_seguimient01 set rut_caso=replace(rut_caso,'+','')				;
update protocolos_seguimient01 set rut_caso=replace(rut_caso,' ','')				;

update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'.','')		;
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,',','')		;
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'--','-')		;
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,':','')		;
--update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'G10T','')	;
--update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'G12T','')	;
--update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'G13T','')	;
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'*','')		;
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'/','')		;
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'+','')		;
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,' ','')		;

delete from protocolos_seguimient01 where rut_caso is null or rut_caso='';
delete from protocolos_seguimient01 where rut_contacto is null or rut_contacto='';

--select count(*) from protocolos_seguimient01 as a --3389

drop table if exists protocolos_seguimient02;create temp table protocolos_seguimient02 as
select a.rut_caso,a.fecha2,a01.rut_contacto as rut_contacto2
  from protocolos_seguimient01 as a
  left join protocolos_seguimient01 as a01 on a01.rut_caso=a.rut_caso and a.fecha2>=a01.fecha2;


drop table if exists protocolos_seguimient03;create temp table protocolos_seguimient03 as 
select rut_caso as rut,fecha2 from protocolos_seguimient02 union
select rut_caso,fecha2 from protocolos_seguimient02 union
select rut_contacto2,fecha2 from protocolos_seguimient02 union   
select rut_contacto2,fecha2 from protocolos_seguimient02;
create index protocolos_seguimient03_i_01 on protocolos_seguimient03(rut);
create index protocolos_seguimient03_i_02 on protocolos_seguimient03(rut,fecha2);
  --select * from protocolos_omi_covid.seguimiento_covid_hechos03 where contacto_rut<>'' and contacto_rut is not null;

--select rut,fecha2,count(*) from protocolos_seguimient03 group by 1,2

drop table if exists contactos_identificados01;create temp table contactos_identificados01 as
with consulta as 
				(
				select distinct rut_caso,rut_contacto2,fecha2 from protocolos_seguimient02
				)
				select rut_caso,rut_contacto2,fecha2,count(*) as q from consulta group by 1,2,3 order by 1,3,2;
			
			--select * from contactos_identificados01 where rut_caso='10032318-4'


--------------------------------------------------------------------------------
-- HECHOS
--------------------------------------------------------------------------------  
drop table if exists covid_kpi_hechos01;
create temp table covid_kpi_hechos01 as
select patient_rut as rut,fecha::date as fecha2 			from total_casos_identificados01 union
select rut_caso,fecha_caso::date 						from pacientes05 union
select rut_caso,min_fecha_caso::date					from pacientes05 union
select rut_contacto,fecha_caso::date 					from pacientes05 union
select rut_contacto,min_fecha_caso::date 				from pacientes05 union
select caso_positivo_rut,fecha2::date					from protocolos_omi_covid.seguimiento_covid_hechos03 union
select contacto_rut,fecha2::date 						from protocolos_omi_covid.seguimiento_covid_hechos03 union
select caso_positivo_rut,fecha_seguimiento::date		from protocolos_omi_covid.seguimiento_covid_hechos03 union
select contacto_rut,fecha_seguimiento::date 			from protocolos_omi_covid.seguimiento_covid_hechos03 union
select rut_caso,fecha2   								from protocolos_seguimient02 union
select rut_contacto2,fecha2 							from protocolos_seguimient02 union
select rut_caso,fecha2 									from contactos_identificados01 union
select rut_contacto2,fecha2 							from contactos_identificados01;
delete from covid_kpi_hechos01 where rut   is null or rut='';
delete from covid_kpi_hechos01 where fecha2 is null;

--------------------------------------------------------------------------------
-- CUBO
--------------------------------------------------------------------------------
drop table if exists covid_kpi_hechos02;
create temp table covid_kpi_hechos02 as
select distinct  a.rut
		,a.fecha2
		,case when a01.rownum is not null then 1 else 0 end as confirmacion_pcr
		,case when a04.rownum is not null then 1 else 0 end as confirmacion
		,case when a02.es_funcionario is not null then a02.es_funcionario else 0 end as es_funcionario
		,a03.min_fecha_caso
		,case when a03.rownum is not null then a03.rownum else 0 end as es_caso_indice
		,a03.rut_caso_primario
		,a05.rownum as seguimiento
		,a06.rownum as contactos_identificados
		,a06.q as contactos_identificados_q
		,0::integer as kpi01_01_numerador
		,0::integer	as kpi01_02_denominador
  from covid_kpi_hechos01 as a
left join
			(
			select patient_rut,fecha::date as fecha
					,row_number() over (partition by patient_rut,fecha::date order by fecha) rownum 
			 from total_casos_identificados01 w
			where pcr=1 
			) as a01 
			  on a01.patient_rut=a.rut 
			 and a01.rownum=1 
			 and a01.fecha>=a.fecha2
left join
  			(
  			select numcolegia,1::smallint as es_funcionario
  					,row_number() over (partition by numcolegia) rownum
  			  from admomi.iddmed
  			) as a02 
  			  on a02.numcolegia=a.rut 
  			 and a02.rownum=1
left join 
			(
			select rut_contacto,rut_caso_primario,min_fecha_caso::Date as min_fecha_caso
					,row_number() over (partition by rut_caso_primario,min_fecha_caso) rownum
			  from pacientes05 as a where rut_caso_primario is not null
			)  as a03 
			   on a03.rut_contacto=a.rut 
			  and a03.rownum=1 
			  --and a03.min_fecha_caso=a.fecha2
left join
			(
			select patient_rut,fecha::date as fecha
					,row_number() over (partition by patient_rut,fecha::date order by fecha) rownum 
			 from total_casos_identificados01 
			where pcr=0 --episodio confirmado
			) as a04 
			  on a04.patient_rut=a.rut 
			 and a04.rownum=1 
			 and a04.fecha>=a.fecha2
left join
			(
			select rut,fecha2 
					,row_number() over (partition by rut,fecha2 order by fecha2) rownum
			  from protocolos_seguimient03
			) as a05
			  on a05.rut=a.rut 
			and a05.fecha2=a.fecha2 
			and a05.rownum=1
left join	(
			select * 
					,row_number() over (partition by rut_caso,fecha2 order by fecha2) rownum
			  from contactos_identificados01
			) as a06 on A06.rut_caso=a.rut and a06.fecha2=a.fecha2 and a06.rownum=1
where a.rut in (select tis from admomi.iddpacpa3 where tis like '%-%' and estado2 is not null);



select * from covid_kpi_hechos02 order by rut,fecha2;

--select rut,fecha2,count(*) from covid_kpi_hechos02 where rut_caso_primario<>'' and rut_caso_primario is not null group by 1,2;
--select distinct rut,fecha2,rut_caso_primario from covid_kpi_hechos02 where seguimiento=1 order by 3 nulls last;