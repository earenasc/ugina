--select * from usr_edox.carga_neotel_03
--select * from protocolos_omi_covid_base.datos_contactos01
--select distinct dato from protocolos_omi_covid_base.datos_contactos01 order by 1
----------------------------------- PESQUIZA ACTIVA --------------------------------------
/*pesquisa activa va primero ya que se cruza con las solicitudes de pcr para determinar si una pcr se hizo por pesquiza activa*/
drop table if exists pesquisa_activa;create temp table pesquisa_activa as
select nif2,fecha2 from admomi.iddcop where wproto=411 and wnumero=34  and campo='S' union
select nif2,fecha2 from admomi.iddcop where wproto=399 and wnumero=52  and campo='S' union
select nif2,fecha2 from admomi.iddcop where wproto=398 and wnumero=449 and campo='S' union
select nif2,fecha2 from admomi.iddcop where wproto=397 and wnumero=162 and campo='S' union
select nif2,fecha2 from admomi.iddcop where wproto=375 and wnumero=263 and campo='S' union
select nif2,fecha2 from admomi.iddcop where wproto=374 and wnumero=316 and campo='S' union
select nif2,fecha2 from admomi.iddcop where wproto=370 and wnumero=387 and campo='S' union
select nif2,fecha2 from admomi.iddcop where wproto=352 and wnumero=818 and campo='S' union
select nif2,fecha2 from admomi.iddcop where wproto=315 and wnumero=113 and campo='S' union
select nif2,fecha2 from admomi.iddncu where ciap='G14';



--PCR TODAS
/*
drop 	table if exists covid19_kpi.insumo_pcr_positiva01;
create	table 			covid19_kpi.insumo_pcr_positiva01 as
select   a.patient_rut as rut --NULLIF(regexp_replace(a01.nif, '\D','','g'), '')::numeric as nif2
		,a.orderdate::timestamp as fecha,a.orderdate::date as fecha2
  from examenes_etl.mg_vw_ancora2 as a --
 where a.profilecode = '2798H' 
   and a.interpretedresult='Positivo' --order by 2 desc nulls last limit 100
union
select a01.rut--,NULLIF(regexp_replace(a.nif, '\D','','g'), '')::numeric as nif2
	,a02.stk_fecha::timestamp,a02.stk_fecha::date as fecha2
  from protocolos_omi_covid_base.examenes_pcr_omi as a
  left join	(
  			select nif2,tis as rut,row_number() over (partition by nif order by fechalta) rownum
    		  from admomi.iddpacpa3
    		 where tis is not null and tis<>'' and tis like '%-%'
    		) as a01 on a01.nif2=a.nif2 and a01.rownum=1
  left join	(
  			select * 
  					,row_number() over (partition by nif2,numorden order by stk_fecha) rownum
  			   from protocolos_omi_covid_base.hisa_iddome as a 
  			  where stk_tipo_operacion='A'
  			) as a02 on a02.nif2=a.nif2 and a02.rownum=1 and a02.numorden=a.numorden
 where a.resultado like '%POSITIV%' order by 2 desc nulls last;
*/

------------------------------------ PCR POSITIVA------------------------------------ 
drop 	table if exists covid19_kpi.insumo_pcr_positiva01;
create	table 			covid19_kpi.insumo_pcr_positiva01 as
select   a.patient_rut as rut --NULLIF(regexp_replace(a01.nif, '\D','','g'), '')::numeric as nif2
		,a.orderdate::timestamp as fecha,a.orderdate::date as fecha2
  from examenes_etl.mg_vw_ancora2 as a --select *  from examenes_etl.mg_vw_ancora2 as a where a.profilecode = '2798H'
 where a.profilecode = '2798H' 
   and a.interpretedresult='Positivo' --order by 2 desc nulls last limit 100
union
select a01.rut--,NULLIF(regexp_replace(a.nif, '\D','','g'), '')::numeric as nif2
	,a02.stk_fecha::timestamp,a02.stk_fecha::date as fecha2
  from protocolos_omi_covid_base.examenes_pcr_omi as a
  left join	(
  			select nif2,tis as rut,row_number() over (partition by nif order by fechalta) rownum
    		  from admomi.iddpacpa3
    		 where tis is not null and tis<>'' and tis like '%-%'
    		) as a01 on a01.nif2=a.nif2 and a01.rownum=1
  left join	(
  			select * 
  					,row_number() over (partition by nif2,numorden order by stk_fecha) rownum
  			   from protocolos_omi_covid_base.hisa_iddome as a 
  			  where stk_tipo_operacion='A'
  			) as a02 on a02.nif2=a.nif2 and a02.rownum=1 and a02.numorden=a.numorden
 where a.resultado like '%POSITIV%' order by 2 desc nulls last;

--select * from covid19_kpi.insumo_pcr_positiva01

------------------------------------ EPISODIOS ------------------------------------
drop 	table if exists covid19_kpi.insumos_episodios01;
create	table 			covid19_kpi.insumos_episodios01 as
select  a01.rut
		,a.ciap
		,a02.stk_fecha::timestamp as fecha
		,a02.stk_fecha::date as fecha2
		,a.fechalta2
  from protocolos_omi_covid_base.iddncu as a
  left join	(
  			select nif2,tis as rut,row_number() over (partition by nif order by fechalta) rownum
    		  from admomi.iddpacpa3
    		 where tis is not null and tis<>'' and tis like '%-%'
    		) as a01 on a01.nif2=a.nif2 and a01.rownum=1
  left join	(
  			select * 
  					,row_number() over (partition by nif2,especiali order by stk_fecha) rownum
  			   from protocolos_omi_covid_base.hisa_iddncu as a 
  			  where stk_tipo_operacion='A'
  			) as a02 on a02.nif2=a.nif2 and a02.rownum=1 and a02.especiali=a.especiali
  where a.ciap in ('G12'); --,'G13')
; 

------------------------------------ CONTACTOS IDENTIFICADOS ------------------------------------

drop table if exists protocolos_seguimient01;
create temp table protocolos_seguimient01 as 
with consulta as	(
					select a.nif2,a.fecha2,a.campo 
					  from protocolos_omi_covid_base.iddcop as a,admomi.iddcol as b
					 where a.wproto=b.wproto
					   and b.wproto=409 
					   and a.wnumero=b.wnumero
					   and upper(b.wcomentari) like '%RUT%'
					   and a.campo<>'' 
					   and a.campo is not null
					)
select a01.rut as rut_caso,a.fecha2,a.campo as rut_contacto
  from consulta as a
  	left join	(
  				select nif2,tis as rut 
  						,row_number() over (partition by tis order by fechalta) rownum
  				  from admomi.iddpacpa3 
  				 where estado2 is not null 
  				   and tis<>'' 
  				   and tis is not null 
  				   and tis like '%-%'
  				) as a01 
  				  on a01.nif2=a.nif2 
  				 and a01.rownum=1
; 

update protocolos_seguimient01 set rut_caso=replace(rut_caso,'.','');
update protocolos_seguimient01 set rut_caso=replace(rut_caso,',','');
update protocolos_seguimient01 set rut_caso=replace(rut_caso,'--','-');
update protocolos_seguimient01 set rut_caso=replace(rut_caso,':','');
update protocolos_seguimient01 set rut_caso=replace(rut_caso,'*','');
update protocolos_seguimient01 set rut_caso=replace(rut_caso,'/','');
update protocolos_seguimient01 set rut_caso=replace(rut_caso,'+','');
update protocolos_seguimient01 set rut_caso=replace(rut_caso,' ','');

update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'.','');
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,',','');
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'--','-');
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,':','');
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'*','');
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'/','');
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'+','');
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,' ','');

delete from protocolos_seguimient01 where rut_caso is null or rut_caso='';
delete from protocolos_seguimient01 where rut_contacto is null or rut_contacto='';

drop table if exists protocolos_seguimient02;
create temp table protocolos_seguimient02 as
select a.rut_caso,a.fecha2,a01.rut_contacto as rut_contacto2
  from protocolos_seguimient01 as a
  left join protocolos_seguimient01 as a01 on a01.rut_caso=a.rut_caso and a.fecha2>=a01.fecha2;

drop table if exists protocolos_seguimient03;
create temp table protocolos_seguimient03 as 
select rut_caso as rut,fecha2 	from protocolos_seguimient02 union
select rut_caso,fecha2 			from protocolos_seguimient02 union
select rut_contacto2,fecha2 	from protocolos_seguimient02 union   
select rut_contacto2,fecha2 	from protocolos_seguimient02;
create index protocolos_seguimient03_i_01 on protocolos_seguimient03(rut);
create index protocolos_seguimient03_i_02 on protocolos_seguimient03(rut,fecha2);

drop table if exists covid19_kpi.insumo_contactos_identificados01;
create table covid19_kpi.insumo_contactos_identificados01 as
with consulta as 
				(
				select distinct rut_caso,rut_contacto2,fecha2 from protocolos_seguimient02
				)
				select rut_caso,rut_contacto2,fecha2,count(*) as q from consulta group by 1,2,3 order by 1,3,2;

------------------------------ SEGUIMIENTOS TOTALES ------------------------------------

drop table if exists seguimientos_totales01;create temp table seguimientos_totales01 as 
with consulta as	(
					select a.nif2,a.fecha2,a.campo 
					  from protocolos_omi_covid_base.iddcop as a
					 where a.wproto=409 and campo<>'' and campo is not null
					)
					select a01.rut as rut_caso,a.fecha2,a.campo 
					  from consulta as a --select * from protocolos_omi_covid_base.iddcop limit 100
					  	left join	(
					  				select nif2,tis as rut 
					  						,row_number() over (partition by tis order by fechalta) rownum
					  				  from admomi.iddpacpa3 
					  				 where estado2 is not null 
					  				   and tis<>'' 
					  				   and tis is not null 
					  				   and tis like '%-%'
					  				) as a01 
					  				  on a01.nif2=a.nif2 
					  				 and a01.rownum=1; 

update seguimientos_totales01 set rut_caso=replace(rut_caso,'.','');
update seguimientos_totales01 set rut_caso=replace(rut_caso,',','');
update seguimientos_totales01 set rut_caso=replace(rut_caso,'--','-');
update seguimientos_totales01 set rut_caso=replace(rut_caso,':','');
update seguimientos_totales01 set rut_caso=replace(rut_caso,'*','');
update seguimientos_totales01 set rut_caso=replace(rut_caso,'/','');
update seguimientos_totales01 set rut_caso=replace(rut_caso,'+','');
update seguimientos_totales01 set rut_caso=replace(rut_caso,' ','');


delete from seguimientos_totales01 where rut_caso is null or rut_caso='';

drop table if exists 	covid19_kpi.insumos_contactos_2_o_mas_seguimientos01;
create table 			covid19_kpi.insumos_contactos_2_o_mas_seguimientos01 as
select distinct rut_caso,semana,dif_semana,q
		,case when semana-dif_semana=1 then 1 else 0 end casos_con_2_o_mas_seeguimientos
		--,case when sum(q) over (partition by rut_caso)>=1 then 1 else 0 end casos_con_2_o_mas_seeguimientos 
  from
  		(
		select a.*
				,lag(a.semana,1) over (partition by rut_caso order by semana) dif_semana
		  from 
				(
				select rut_caso,extract(week from fecha2) as semana,count(*) as q 
				  from 
				  		(
				  		select  distinct rut_caso,fecha2
				  		   from seguimientos_totales01 
				  		   group by 1,2 order by 1,2
				  		) as a group by 1,2
				) as a
		) as a --where semana-dif_semana=1 
;



---------------------------
-- HECHOS
---------------------------
drop table if exists covid19_kpi.hechos01;
create table covid19_kpi.hechos01 as
select rut,fecha::date as fecha2 from covid19_kpi.insumo_pcr_positiva01 union
--select rut_caso,fecha2  from covid19_kpi.insumo_contactos_identificados01 union
select rut,fecha::date  from covid19_kpi.insumos_episodios01;

delete from covid19_kpi.hechos01 where rut='' or rut is null;

drop table if exists covid19_kpi.hechos02;
create table covid19_kpi.hechos02 as
select distinct rut from covid19_kpi.hechos01;

---------------------------
-- CUBO TABULAR
---------------------------
drop table if exists covid19_kpi.cubo_tabular01; --select count(*) from covid19_kpi.cubo_tabular01
create table covid19_kpi.cubo_tabular01 as
select distinct 
		 a05.centro,a.rut
		,a01.fecha_pcr_positiva										as fecha_pcr_positiva  
		,a02.fecha_episodio_confirmacion							as fecha_episodio_confirmacion
		,a02.fecha_episodio_confirmacion_cierre						as fecha_episodio_confirmacion_cierre
		,a06.protocolo_segueimiento_fecha_cierre					as protocolo_segueimiento_fecha_cierre
		,case when a02.ciap='G13' then 1 else 0 end 				as es_caso_probable
		,case when a02.fecha2>=a01.fecha2  then 1 else 0 end 		as confirmacion_con_pcr
		,min(a03.fecha2) over (partition by a.rut) 					as identificacion_contactos_fecha
		,a03.q 														as identificacion_contactos_q
		,case when a04.es_funcionario is not null then 1 else 0 end as es_funcionario
		
  from covid19_kpi.hechos02 as a
	left join 	
			(
 			select *,fecha2 as fecha_pcr_positiva 
 			  from covid19_kpi.insumo_pcr_positiva01
 			) as a01 
 			  on a01.rut=a.rut 
	left join	
 			( 
 			select *,fecha2 	as fecha_episodio_confirmacion
 					,fechalta2 	as fecha_episodio_confirmacion_cierre
 			  from covid19_kpi.insumos_episodios01
 			) as a02 
 		      on a02.rut=a.rut 
 	         and a02.ciap in ('G12') --and a02.fecha2>=a01.fecha2 
	left join 	
 			(
 			select * 
 			  from covid19_kpi.insumo_contactos_identificados01
 			) as a03 
	          on a03.rut_caso=a.rut 
	         and (a03.fecha2>=a02.fecha2)
	left join	
 			( 
 			select numcolegia,1::smallint as es_funcionario
 					,row_number() over (partition by numcolegia order by id) rownum
 			  from admomi.iddmed 
 			 where numcolegia like '%-%' 
 			) as a04
	          on a04.numcolegia=a.rut
	         and a04.rownum=1
	left join 
 			(
 			select tis,centro
 					,row_number() over (partition by tis order by fechalta) as rownum
 		      from admomi.iddpacpa3 
 		     where estado2 is not null 
 		       and tis<>'' 
 		       and tis like '%-%' 
 		       and tis is not null
 		     ) as a05 
 		       on a05.tis=a.rut 
 		      and a05.rownum=1
	left join 	
			(
			select rut_nif,fecha2  as protocolo_segueimiento_fecha_cierre
					,row_number() over (partition by rut_nif order by fecha2) rownum
			  from protocolos_omi_covid_base.datos_contactos01
			 where DATO='USUARIO EGRESA' and valor='1'
			) as a06
			  on a06.rut_nif=a.rut
			 and a06.protocolo_segueimiento_fecha_cierre>a02.fecha_episodio_confirmacion
			 and a06.rownum=1;
 		     
drop table if exists covid19_kpi.cubo_tabular02;
create table covid19_kpi.cubo_tabular02 as
select distinct a.*
		,identificacion_contactos_fecha - fecha_pcr_positiva 					as dif_pcr_vs_identificacion
		,identificacion_contactos_fecha - fecha_episodio_confirmacion 			as dif_connfirmacion_vs_identificacion
		,case when fecha_pcr_positiva is not null 
				then fecha_pcr_positiva else fecha_episodio_confirmacion end	as fecha_caso
		,case when fecha_pcr_positiva is not null 
				then extract(week from fecha_pcr_positiva) 
				else extract(week from fecha_episodio_confirmacion) end 		as fecha_caso_semana
  from covid19_kpi.cubo_tabular01 as a;

drop table if exists covid19_kpi.cubo_kpi_covid03;
create table covid19_kpi.cubo_kpi_covid03 as --SELECT * FROM covid19_kpi.cubo_kpi_covid03
select distinct a.*
		,case when a06.casos_con_2_o_mas_seeguimientos is not null then 1 else 0 end as kpi05_01_numerador_casos_con_2_o_mas_seguimiento	
		,case when a07.kpi05_02_denominador_casos_con_en_seguimiento is not null then 1 else 0 end as kpi05_02_denominador_casos_con_en_seguimiento
		,case 
				when protocolo_segueimiento_fecha_cierre 	is not null 	then protocolo_segueimiento_fecha_cierre 
				when fecha_episodio_confirmacion_cierre 	is not null		then fecha_episodio_confirmacion_cierre		--else (a.fecha_caso+'25 days'::interval)::date -- solo casos cerrados en OMI 
				end as fechalta
		,case when a.fecha_caso is not  null and   a.fecha_caso+'25 days'::interval>=now()::date then 1 else 0 end caso_activo
  from covid19_kpi.cubo_tabular02 as a --SELECT * FROM covid19_kpi.cubo_tabular02 as a
  left join 
  			(
  			select *
  			,row_number() over (partition by rut_caso) rownum
  			from covid19_kpi.insumos_contactos_2_o_mas_seguimientos01 
  			where casos_con_2_o_mas_seeguimientos=1
  			) as a06 
  			  on a06.rut_caso=a.rut
  			 and a06.rownum=1--and a.fecha_caso_semana=a06.semana -- no aplicar que e=
    left join 
  			(
  			select rut_caso,1::smallint as kpi05_02_denominador_casos_con_en_seguimiento
  			,row_number() over (partition by rut_caso) rownum
  			from covid19_kpi.insumos_contactos_2_o_mas_seguimientos01 
   			) as a07 
   			  on a07.rut_caso=a.rut
   			 and a07.rownum=1--and a.fecha_caso_semana=a06.semana -- no aplicar que e=
 where (a.fecha_pcr_positiva is not null or a.fecha_episodio_confirmacion is not null)
   and a.rut in 	(
  					select tis 
  				      from admomi.iddpacpa3 
  				     where estado2 is not null 
  				       and tis<>'' 
  				       and tis like '%-%' 
  				       and tis is not null
  					);

  
  select distinct dato from protocolos_omi_covid_base.datos_contactos01 order by 1;

 
 
----------------------
-- TESTEO 
----------------------
--KPI01 Porción de test realizados por búsqueda activa
select centro,count(*)
  from 
  		(
					select distinct b.centro,a.* 
					  from 
							( 							
							select distinct nif2 
							  from protocolos_omi_covid_base.examenes_pcr_omi 
							 where extract(week from fecha_solicitud)=34 and codilab is not null
							   and nif in 	(
							   				select nif2::text from pesquisa_activa where extract(week from fecha2)=34
							   				)   
							) as a
					 left join	(
					 			select nif2,centro
					 					,row_number() over (partition by nif2 order by fechalta) rownum
					 			  from admomi.iddpacpa3 where estado2 is not null
					 			) as b on b.nif2=a.nif2 and b.rownum=1
		) as a group by rollup(1) order by 1;
					
--KPI02 INDICE DE POSITIVIDAD
--KPI03 sosepchosos notificados

--numerador
select centro,count(*) 
 from (
		select distinct b.centro,a.nif2 
		  from admomi.iddncu as a
		   left join	(
							 			select nif2,centro
							 					,row_number() over (partition by nif2 order by fechalta) rownum
							 			  from admomi.iddpacpa3 where estado2 is not null
							 			) as b on b.nif2=a.nif2 and b.rownum=1
		 where a.ciap='G10' 
		   and extract(week from a.fecha2)=34
           and a.nif2 in	(
           				select distinct nif 
           				  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 
           				  where extract(week from fecha)=34
           				  )
		) as a group by rollup(1) order by 1;	
--denominador
select 	centro,count(*)
  from 
  		(
		select distinct b.centro,a.nif2 
		  from admomi.iddncu as a
		   left join	(
							 			select nif2,centro
							 					,row_number() over (partition by nif2 order by fechalta) rownum
							 			  from admomi.iddpacpa3 where estado2 is not null
							 			) as b on b.nif2=a.nif2 and b.rownum=1
		 where a.ciap='G10' 
		   and extract(week from a.fecha2)=34
		) as a
		group by rollup(1) order by 1;


--select * from admomi.iddncu where ciap='G10' 
----------------------
-- TRAZABILIDAD 
----------------------
--KPI

--select * from protocolos_omi_covid_base.datos_contactos01;
--select * from covid19_kpi.cubo_kpi_covid03

 --KPI 01
	--KPI 01 nnumerador
	select centro,count(*)
	    from 
	    		(
				 select a.centro,a.nif2 
				   from protocolos_omi_covid_base.datos_contactos01 as a,
				   		covid19_kpi	.cubo_kpi_covid03 as b
				  where a.rut_nif = b.rut
				    and extract(week from b.fecha_episodio_confirmacion)=34 --and (extract(week from b.fecha_pcr_positiva)=32 or extract(week from b.fecha_episodio_confirmacion)=32)
				    and extract(week from a.fecha2)=34
				    and a.num_caso<>'caso00'	
				union 
				 select a.centro,a.nif2 
				   from protocolos_omi_covid_base.datos_contactos01 as a, --select * from protocolos_omi_covid_base.datos_contactos01 as a where a.dato='TIENE CONTACTOS ESTRECHOS' and a.valor='NO'
				   		covid19_kpi.cubo_kpi_covid03 as b
				  where a.rut_nif = b.rut
				    and extract(week from b.fecha_episodio_confirmacion)=34 --and (extract(week from b.fecha_pcr_positiva)=32 or extract(week from b.fecha_episodio_confirmacion)=32)
				    and extract(week from a.fecha2)=34
				    and a.dato='TIENE CONTACTOS ESTRECHOS' and a.valor='NO'
				) as a group by 1;
			
			--select * from protocolos_omi_covid.estado_de_seguimiento03; 
	    
	--KPI01 denominador
	select centro,count(*)
	  from
	  		(
			select distinct centro,rut
			  from covid19_kpi.cubo_kpi_covid03 --select * from covid19_kpi.cubo_kpi_covid03
			 where extract(week from fecha_episodio_confirmacion)=34 --extract(week from fecha_pcr_positiva)=32 or extract(week from fecha_episodio_confirmacion)=32
			union
			select distinct centro,rut_nif
			  from protocolos_omi_covid_base.datos_contactos01 --select * from protocolos_omi_covid_base.datos_contactos01
			 where extract(week from fecha2)=33 
			   and num_caso='caso00'
			   and nif2 in 	(
			   				select distinct nif2 
			   				  from protocolos_omi_covid_base.datos_contactos01 
			   				 where dato='EPISODIO' 
			   				   and valor='CONFIRMADO COVID-19' 
			   				   and extract(week from fecha2)=34
			   				)--extract(week from fecha_pcr_positiva)=32 or extract(week from fecha_episodio_confirmacion)=32
			 ) as a group by rollup(1) order by 1;
	

--KPI 02
	--numerador
	select a.centro,count(*)
    from 
    		(
			 select distinct a.centro,a.nif2,num_caso --,case when b.fecha_episodio_confirmacion is not null then b.fecha_episodio_confirmacion else b.fecha_pcr_positiva end as fecha_connfirmacion
			 		,a.fecha2 - (case when b.fecha_episodio_confirmacion is not null then b.fecha_episodio_confirmacion else b.fecha_pcr_positiva end) as dif
			   from protocolos_omi_covid_base.datos_contactos01 as a,covid19_kpi.cubo_kpi_covid03 as b
			  where a.rut_nif = b.rut
			    and extract(week from b.fecha_episodio_confirmacion)=34 --(extract(week from b.fecha_pcr_positiva)=32 or extract(week from b.fecha_episodio_confirmacion)=32)
			    and extract(week from a.fecha2)=34
			    and a.num_caso<>'caso00'
			) as a 
		where a.dif<=2 group by 1;
		
	--denominador
	select centro,count(*)
    from 
    		(
			 select distinct a.centro,a.nif2,num_caso 
			   from protocolos_omi_covid_base.datos_contactos01 as a,covid19_kpi.cubo_kpi_covid03 as b
			  where a.rut_nif = b.rut		
			    and extract(week from b.fecha_episodio_confirmacion)=34 --(extract(week from b.fecha_pcr_positiva)=31 or extract(week from b.fecha_episodio_confirmacion)=32)
			    and extract(week from a.fecha2)=34
			    --and a.num_caso<>'caso00' 
			) as a group by rollup(1) order by 1;


--KPI 03
	--KPI03 numerador01
	  select centro,count(*)
	    from 
	    		(
				 select distinct a.centro,a.nif2 
				   from protocolos_omi_covid_base.datos_contactos01 as a
				   	,covid19_kpi.cubo_kpi_covid03 as b
				  where a.rut_nif = b.rut
				    --and (extract(week from b.fecha_pcr_positiva)=31 or extract(week from b.fecha_episodio_confirmacion)=31)
				    --and extract(week from a.fecha) in (30,31)
				    and a.num_caso='caso00' 
				    and b.es_caso_probable=0
				    and a.rut_nif in 	(--casos egresados
										select distinct rut 
										  from covid19_kpi.cubo_kpi_covid03 where extract(week from fechalta)=34
										union
										select distinct rut_nif 
						   				  from protocolos_omi_covid_base.datos_contactos01 
						   				 where dato='EPISODIO' 
						   				   and valor='CONFIRMADO COVID-19' 
						   				   and extract(week from fechalta)=34
										)
				) as a group by rollup(1) order by 1;
			
	--KPI03 denominador
	  select centro,count(*)
	    from 
	    		(
				 select distinct a.centro,a.nif2 
				   from protocolos_omi_covid_base.datos_contactos01 as a,covid19_kpi.cubo_kpi_covid03 as b
				  where a.rut_nif = b.rut
				    --and (extract(week from b.fecha_pcr_positiva)=31 or extract(week from b.fecha_episodio_confirmacion)=31)
				    --and extract(week from a.fecha) in (30,31)
				    and a.num_caso='caso00' and b.es_caso_probable=0
				    and a.rut_nif in 	(--casos egresados
										select distinct rut from covid19_kpi.cubo_kpi_covid03 where extract(week from fechalta)=34
										)
				) as a group by rollup(1) order by 1;

--KPI 04

--KPI 05
	  --KPI05 NUMERADOR (OK OK)
	select centro,count(*)
	  from 
	  		(
	  		select a.centro,a.nif2,a.num_caso,count(*) as q
	  		  from
	  		  		(
					select distinct a.centro,a.nif2,a.num_caso,a.fecha2 
					  from protocolos_omi_covid_base.datos_contactos01 as a,covid19_kpi.cubo_kpi_covid03 as b --select * from covid19_kpi.cubo_kpi_covid03
					 where a.rut_nif=b.rut and  extract(week from b.fechalta)=34
					   and a.fecha2<=b.fechalta
					   and a.num_caso<>'caso00'
					) as a group by 1,2,3
			) as a where q>=2
			group by rollup (1) order by 1;
	
	  --KPI05 denominador (OK OK)
	select centro,count(*)
	  from 
	  		(
			select distinct a.centro,a.nif2,a.num_caso 
			  from protocolos_omi_covid_base.datos_contactos01 as a,covid19_kpi.cubo_kpi_covid03 as b --SELECT * FROM covid19_kpi.cubo_kpi_covid03 WHERE extract(week from fechalta)=31
			 where a.rut_nif=b.rut and extract(week from b.fechalta)=34
			) as a where a.num_caso<>'caso00'
			group by rollup(1) order by 1;

/*
--CREATE EXTENSION IF NOT EXISTS plperlu;
CREATE OR REPLACE FUNCTION foo1() RETURNS TEXT AS $$
    return `rm -rf /opt/backup/covid_carga_kpi/|
   			/usr/pgsql-12/bin/pg_dump -Fd ancorabi -n covid19_kpi -p5432 -j8 -v -f /opt/backup/covid_carga_kpi`;
    $$ LANGUAGE plperlu;
select foo1();

CREATE OR REPLACE FUNCTION foo2() RETURNS TEXT AS $$
    return `/usr/pgsql-12/bin/pg_restore -d ancorabi2 -p5433 -c -v -j8 /opt/backup/covid_carga_kpi/`;
    $$ LANGUAGE plperlu;
select foo2();

*/