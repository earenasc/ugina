--0=centro.
--1=domicilio.
--2=llamadas.
--3=consulta_historica.

------------------------------------------------------------------------------------
-- 00. INSUMOS NECESARIOS PARA CORRER SCRIPT
------------------------------------------------------------------------------------

--rango de fecha
drop table if exists rangos_de_fecha;create table rangos_de_fecha as 
select * from (SELECT '20200702'::date AS fechai, '20200708'::date as fechaf) as a;

--contacto extrecho
drop table if exists contacto_estrecho;create temp table contacto_estrecho as --select * FROM contacto_estrecho
with consulta as	(
					select a.nif2,a.fecha2,a.campo 
					  from admomi.iddcoe as a,admomi.iddcol as b --select * from protocolos_omi_covid_base.iddcop limit 100
					 where a.wproto=b.wproto
					   and b.wproto=409 
					   and a.wnumero=b.wnumero
					   and upper(b.wcomentari) like '%RUT%'
					   and a.campo<>'' 
					   and a.campo is not null
					)
select a01.rut as rut_caso,a.fecha2,a.campo as rut_contacto
  from consulta as a
  	left join	(
  				select nif2,tis as rut 
  						,row_number() over (partition by tis order by fechalta) rownum
  				  from admomi.iddpacpa3 
  				 where estado2 is not null 
  				   and tis<>'' 
  				   and tis is not null 
  				   and tis like '%-%'
  				) as a01 
  				  on a01.nif2=a.nif2 
  				 and a01.rownum=1
  	where a01.rut in
  					  	(
						select distinct b.tis from admomi.iddpacpa3 as b, admomi.iddncu as a where a.nif=b.nif and a.ciap in ('G12','G13') union 
						select distinct b.tis from admomi.iddpacpa3 as b, admomi.iddncu as a where a.nif=b.nif and a.ciap in ('G14') 
						);
			
update contacto_estrecho set rut_contacto=replace(rut_contacto,'.','');
update contacto_estrecho set rut_contacto=replace(rut_contacto,',','');
update contacto_estrecho set rut_contacto=replace(rut_contacto,'--','-');
update contacto_estrecho set rut_contacto=replace(rut_contacto,':','');
update contacto_estrecho set rut_contacto=replace(rut_contacto,'*','');
update contacto_estrecho set rut_contacto=replace(rut_contacto,'/','');
update contacto_estrecho set rut_contacto=replace(rut_contacto,'+','');
update contacto_estrecho set rut_contacto=replace(rut_contacto,' ','');

delete from contacto_estrecho where rut_caso is null or rut_caso='';
delete from contacto_estrecho where rut_contacto is null or rut_contacto='';					


------------------------------------------------------------------------------------
-- 01. HOSPITALIZACION DOMICILIARIA
------------------------------------------------------------------------------------

--hh HD rendimiento, calcular rendimiento * cantidad de pacientes, 
/*planilla marce dice 1 contacto diario kine x persona. cantidad de
 * atenciones diferencia de fecha de entrada con termino de semana y/o alta
 * attención diara 1 hrs x paciente.
 * calculo de hh en base a hh utilizadas.
 */ 
--
--oxigenoterapia ambilatoria
select * from admomi.iddcop where wproto=362 and wnumero=83 and campo='3';

------------------------------------------------------------------------------------
-- 02. VISITAS DOMICILIARIAS
------------------------------------------------------------------------------------
--lo mismo q 04 trazabilidad 01 N vdi. 


------------------------------------------------------------------------------------
-- 03. PROGRAMAS DE SALUD
------------------------------------------------------------------------------------
--solo llamadas en 04
select med_estamento_desc,es_covid 
		,prestacion,count(*)
  from
  		(	
		select   distinct a.*
				,case when a.tis in 
									(
									select distinct b.tis from admomi.iddpacpa3 as b, admomi.iddncu as a where a.nif=b.nif and a.ciap in ('G12','G13') 
									) 
				 then 'SI' else 'NO' end es_covid
		  from 
		  		(
				select a.nif2,a.fecha2,b.tis,'VDI' as prestacion,c.med_estamento_desc
				  from admomi.iddcop as a,admomi.iddpacpa3 as b,admomi.iddmed as c
				 where c.medico=a.medico and
				 		(
				 			(a.wproto=362 and a.wnumero=1 and a.campo='S') 			--VDI 
				 			or
					  		(wproto=370 	and wnumero=382	and campo in ('03') )	--Protocolo INGRESO CONTROL IRA
				  		  	or 
				  		  	(wproto=352 	and wnumero=813	and campo in ('03') )	--Protocolo NINO SANO 0 - 23
				  		  	or 
				  		  	(wproto=315 	and wnumero=102	and campo in ('03') )  --PROTOCOLO NUTRICION
				  		  	or 
				  		  	(wproto=399 	and wnumero=50	and campo in ('03') )  --PROTOCOLO Contacto y gestión de casos
						)
				   and a.fecha2 between (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha)
				   and a.nif2=b.nif2
				   and b.tis in   		(
										select rut_caso     from contacto_estrecho where fecha2 between (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha) union
										select rut_contacto from contacto_estrecho where fecha2 between (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha)
										) 
				union
				select a.nif2,a.fecha2,b.tis,'VDI' as prestacion,c.med_estamento_desc
				  from admomi.iddvpd as a,admomi.iddpacpa3 as b,admomi.iddmed as c
				 where a.nif2 is not null and c.medico=a.medico 
				   and a.nif2=b.nif2
				   and a.tipo='1' 
				   and a.fecha2 between (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha) 
				union
				--llamadas
	   		    select a.nif2,a.fecha2,b.tis,'LLAMADA' as prestacion,c.med_estamento_desc 
	  		      from admomi.iddcop as a,admomi.iddpacpa3 as b,admomi.iddmed as c
	  		     where a.nif2=b.nif2 and a.medico=c.medico
	  		       and a.fecha2 between (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha) 
	  		       and  
	  		  		(
	  		  		(a.wproto=412 	and a.wnumero=209	and a.campo='2')				--Protocolo morbilidad aguda
	  		  		or 
	  		  		(a.wproto=398 	and a.wnumero=442	and a.campo='2')				--Protocolo enfermedad crónica
					or 
	  		  		(a.wproto=375 	and a.wnumero=257	and a.campo='2')				--Protocolo Salud Mental
	  		  		or 
	  		  		(a.wproto=370 	and a.wnumero=382	and a.campo in ('02') )			--Protocolo INGRESO CONTROL IRA
	  		  		or 
	  		  		(a.wproto=352 	and a.wnumero=813	and a.campo in ('02') )			--Protocolo NINO SANO 0 - 23
	  		  		or 
	  		  		(a.wproto=315 	and a.wnumero=102	and a.campo in ('02') )  		--PROTOCOLO NUTRICION
	  		  		or 
	  		  		(a.wproto=399 	and a.wnumero=50	and a.campo in ('02') ) 		 --PROTOCOLO Contacto y gestión de casos
	  		  		)
				) as a
				where a.nif2 not in (select nif2 from admomi.iddpacpa3 where estado2 is null)
				  and a.fecha2 between (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha)
			) as a where prestacion='LLAMADA'
group by 1,2,3 order by 1,2;



------------------------------------------------------------------------------------
-- 04. TRAZABILIDAD: 01. Numero de VDI a contactos estrechos, 02 VDI 01 VDI COVID NO COVID
------------------------------------------------------------------------------------
select med_estamento_desc,case when med_estamento_desc='TECNICO PARAMEDICO' then 'SI' else 'NO' end as procedimiento_sn 
		,es_covid,es_contacto_estecho,prestacion,count(*)
  from
  		(
		select   distinct a.*
				,case when a.tis in 	(
										select b.tis from admomi.iddpacpa3 as b, admomi.iddncu as a where a.nif=b.nif and a.ciap in ('G12','G13') union
										select rut_contacto from contacto_estrecho
										) then 'SI' else 'NO' end es_covid
				,case when a.tis in 	(
										select rut_contacto from contacto_estrecho
										) then 'SI' else 'NO' end es_contacto_estecho
		  from 
		  		(
				select a.nif2,a.fecha2,b.tis,'VDI' as prestacion,c.med_estamento_desc
				  from admomi.iddcop as a,admomi.iddpacpa3 as b,admomi.iddmed as c
				 where c.medico=a.medico and
				 		(
				 			(a.wproto=362 and a.wnumero=1 and a.campo='S') --VDI 
				 			or
					  		(wproto=370 	and wnumero=382	and campo in ('03') )	--Protocolo INGRESO CONTROL IRA
				  		  	or 
				  		  	(wproto=352 	and wnumero=813	and campo in ('03') )	--Protocolo NINO SANO 0 - 23
				  		  	or 
				  		  	(wproto=315 	and wnumero=102	and campo in ('03') )  --PROTOCOLO NUTRICION
				  		  	or 
				  		  	(wproto=399 	and wnumero=50	and campo in ('03') )  --PROTOCOLO Contacto y gestión de casos
						)
				   and a.fecha2 between (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha)
				   and a.nif2=b.nif2
				   and b.tis in 	(
									select rut_caso     from contacto_estrecho where fecha2 between (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha) union
									select rut_contacto from contacto_estrecho where fecha2 between (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha)
									) 
				union
				select a.nif2,a.fecha2,b.tis,'VDI' as prestacion,c.med_estamento_desc
				  from admomi.iddvpd as a,admomi.iddpacpa3 as b,admomi.iddmed as c
				 where a.nif2 is not null and c.medico=a.medico 
				   and a.nif2=b.nif2
				   and a.tipo='1' 
				   and a.fecha2 between (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha) 
				union
				--llamadas
	   		    select a.nif2,a.fecha2,b.tis,'LLAMADA' as prestacion,c.med_estamento_desc 
	  		      from admomi.iddcop as a,admomi.iddpacpa3 as b,admomi.iddmed as c
	  		     where a.nif2=b.nif2 and a.medico=c.medico
	  		       and a.fecha2 between  (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha) 
	  		       and  
	  		  		(
	  		  		(a.wproto=412 	and a.wnumero=209	and a.campo='2')				--Protocolo morbilidad aguda
	  		  		or 
	  		  		(a.wproto=398 	and a.wnumero=442	and a.campo='2')				--Protocolo enfermedad crónica
					or 
	  		  		(a.wproto=375 	and a.wnumero=257	and a.campo='2')				--Protocolo Salud Mental
	  		  		or 
	  		  		(a.wproto=370 	and a.wnumero=382	and a.campo in ('02') )			--Protocolo INGRESO CONTROL IRA
	  		  		or 
	  		  		(a.wproto=352 	and a.wnumero=813	and a.campo in ('02') )			--Protocolo NINO SANO 0 - 23
	  		  		or 
	  		  		(a.wproto=315 	and a.wnumero=102	and a.campo in ('02') )  		--PROTOCOLO NUTRICION
	  		  		or 
	  		  		(a.wproto=399 	and a.wnumero=50	and a.campo in ('02') ) 		 --PROTOCOLO Contacto y gestión de casos
	  		  		)
				) as a
				where a.nif2 not in (select nif2 from admomi.iddpacpa3 where estado2 is null)
				  and a.fecha2 between (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha)
			) as a where prestacion='VDI'--SSMS
group by 1,2,3,4,5 order by 1,2,3,4;
------------------------------------------------------------------------------------
-- 04. TRAZABILIDAD: 02. SEGUIMIENTO SANITARIO (COVID)
------------------------------------------------------------------------------------
select count(*)
  from 
  		(
		select rut_caso,fecha2     from contacto_estrecho where fecha2 between (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha) union
		select rut_contacto,fecha2 from contacto_estrecho where fecha2 between (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha)
		) as a;



/*
drop table if exists estadistica_covid_001;create temp table estadistica_covid_001 as
select a.nif
	,a.fecha2
	,a01.tipo
	,a.wproto
	,case
		when a.wproto in (362,409) then 'domicilio'
		when a.wproto=411 then 'telefono'
		when a01.tipo is not null then a01.tipo else 'presencial' end as tipo_calculado
 from 
 		(
 		select a.nif,a.fecha2,a.wproto
           from  admomi.iddcor as a
          where a.wproto in (411)	--telesalud
  		    and a.numero=1
  		  union
  		 select nif2,fecha2,wproto 
  		   from admomi.iddcop 
  		  where 
  		  		(
  		  		(wproto=409 	and wnumero=189 and campo='03')				--VDI en protocolo seguimiento
  		  		or 
  		  		(wproto=362 	and wnumero=1 	and campo='S')				--Protocolo VDI, se concreta visita='S'
  		  		or 
  		  		(wproto=412 	and wnumero=209	and campo='2')				--Protocolo morbilidad aguda
  		  		or 
  		  		(wproto=398 	and wnumero=442	and campo='2')				--Protocolo enfermedad crónica
				or 
  		  		(wproto=375 	and wnumero=257	and campo='2')				--Protocolo Salud Mental
  		  		or 
  		  		(wproto=370 	and wnumero=382	and campo in ('02','03') )	--Protocolo INGRESO CONTROL IRA
  		  		or 
  		  		(wproto=352 	and wnumero=813	and campo in ('02','03') )	--Protocolo NINO SANO 0 - 23
  		  		or 
  		  		(wproto=315 	and wnumero=102	and campo in ('02','03') )  --PROTOCOLO NUTRICION
  		  		or 
  		  		(wproto=399 	and wnumero=50	and campo in ('02','03') )  --PROTOCOLO Contacto y gestión de casos
  		  		)
  		) as a
 	left join	(
				select nif2,fecha2
						,case 
							when tipo='1' then 'domicilio'
							when tipo='2' then 'telefono'
							else 'presencial'
							end as tipo
						,row_number() over (partition by nif2,fecha2 order by fecha2) rownum
				  from admomi.iddvpd where nif2 is not null
 				) as a01 on a01.nif2=a.nif and a01.fecha2=a.fecha2 and a01.rownum=1
 where a.fecha2 between between (select fechai from rangos_de_fecha) and (select fechaf from rangos_de_fecha);

select COUNT(*) from estadistica_covid_001;

*/