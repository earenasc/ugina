/*todas las PCR positivas OMI y lab UC*/
  drop table if exists total_casos_identificados;create temp table total_casos_identificados as
select NULLIF(regexp_replace(a01.nif, '\D','','g'), '')::numeric as nif2
		,a.orderdate::date as fecha 
  from examenes_etl.mg_vw_ancora2 as a 
  left join admomi.iddpacpa3 as a01 
  		 on a01.tis=a.patient_rut 
 where a.profilecode = '2798H' 
   and a.interpretedresult='Positivo'
union
select NULLIF(regexp_replace(a.nif, '\D','','g'), '')::numeric as nif2
	,a.fecha_solicitud::date 
  from protocolos_omi_covid_base.examenes_pcr_omi as a 
 where a.resultado like '%POSITIV%'; 
--union select b.nif2,A.FECHA from admomi.iddncu as a,admomi.iddpacpa3 as b where a.ciap in ('G12','G13') and a.nif=b.nif and b.estado2 is not null; --1883 --1928

--select distinct nif2 from total_casos_identificados where nif in ()
--select * from total_casos_identificados

/*
distinct pac.nif,ome.fecha --,DMO.TEXTO
  from   admomi.iddome as ome
  		,admomi.iddoml as oml
        ,admomi.iddomr as omr
        ,admomi.iddomd as omd
        ,admomi.iddpacpa3 as pac
        ,admomi.idddmo as dmo
 where ome.numorden = oml.numorden
   and dmo.numorden=ome.numorden
   and oml.orden = omr.orden
   and omr.clave = omd.clave
   and ome.nif = pac.nif
   and omd.mostrar='1'
   and ome.clasepru = '1'
   and oml.tipolo='R'
   and omr.orden in (4549642,8473662)
   and UPPER(dmo.texto) like '%POSITIV%'
   ;*/ --1264 --1285

--PCRS TOTALES
select distinct NULLIF(regexp_replace(a01.nif, '\D','','g'), '')::numeric as nif2
		,a.orderdate::date as fecha 
  from examenes_etl.mg_vw_ancora2 as a 
  left join admomi.iddpacpa3 as a01 
  		 on a01.tis=a.patient_rut 
 where a.profilecode = '2798H' 
   --and a.interpretedresult='Positivo' 
union
select NULLIF(regexp_replace(a.nif, '\D','','g'), '')::numeric as nif2
	,a.fecha_solicitud::date 
  from protocolos_omi_covid_base.examenes_pcr_omi as a 
 --where a.resultado like '%POSITIV%' 
; 


--select distinct a.nif from admomi.iddncu as a,admomi.iddpacpa3 as b where a.ciap in ('G13') and a.nif=b.nif and b.estado2 is not null; --836 probables select 578::numeric/836::numeric

--select * from total_casos_identificados

--pcr solicitadas
select a.semana,a.centro,count(*) as q
  from
  		(
		select distinct a01.centro,a.nif2,a.fecha_solicitud::Date as fecha_solicitud,extract(week from a.fecha_solicitud) semana
		 from protocolos_omi_covid_base.examenes_pcr_omi as a
			left join	(
						select nif2,centro,row_number() over (partition by nif2 order by fechalta) rownum from admomi.iddpacpa3 as b
						) as a01 on a01.nif2=a.nif2 and a01.rownum=1
		  where a.codilab like 'SP%'
			order by 4 desc
		) as a 
group by rollup(1,2) order by 1 desc,2;

--positiviidad
select extract(week from a.fecha_solicitud) as semana,a01.centro,a.resultado,count(*) 
  from protocolos_omi_covid_base.examenes_pcr_omi as a
  	left join	(
				select nif2,centro,row_number() over (partition by nif2 order by fechalta) rownum from admomi.iddpacpa3 as b
				) as a01 on a01.nif2=a.nif2 and a01.rownum=1
 where a.resultado is not null 
   and a.fecha_solicitud <='20200726 23:59:59' 
 group by rollup(1,2,3)
 order by 1 desc,2,3;

--pcr positivo vs procesados
select a.semana,a.centro,count(*) as q
  from
  		(
		select distinct a01.centro,a.nif2,a.fecha,extract(week from a.fecha) semana 
		  from total_casos_identificados as a --select * from total_casos_identificados as a
			left join	(
						select nif2,centro,row_number() over (partition by nif2 order by fechalta) rownum from admomi.iddpacpa3 as b
						) as a01 on a01.nif2=a.nif2 and a01.rownum=1
			where a.fecha between '20200501 00:00:00' and '20200726 23:59:59' 
			order by 4 desc
		) as a 
group by rollup(1,2) order by 1 desc,2;

--TOTAL CASOS
select a.centro,count(*) as q
  from
  		(
		select distinct a01.centro,a.nif2,a01.rut,a.fecha,extract(week from a.fecha) semana,a02.q 
		  from total_casos_identificados as a
			left join	(
						select nif2,centro,tis as rut,row_number() over (partition by nif2 order by fechalta) rownum from admomi.iddpacpa3 as b
						) as a01 on a01.nif2=a.nif2 and a01.rownum=1
			left join	(
						select distinct nif,1::smallint as q from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409
						) as a02 on a02.nif=a.nif2
		 where a01.rut not in 	(
		 						select numcolegia from admomi.iddmed
		 						)
		   and a.nif2 in 		(
		   						select nif2 from admomi.iddpacpa3 where estado2 is not null
		    						)
		   and a.fecha between '20200501' and '20200726'
		  order by 6 desc nulls last
		) as a 
group by rollup(1);

--TOTAL CASOS CON contactos identificados
select centro,count(*)
  from
  		(
		select distinct a01.centro,a01.nif2 
		  from 
		  		(
				select distinct nif,valor 
				  from protocolos_omi_covid_base.datos_contactos01 
				 where dato like '%RUT%'
				   and num_caso like '%caso0%'
				   and num_caso<>'caso00' 
				   and fecha between '20200501' and '20200726' --select * from protocolos_omi_covid_base.datos_contactos01 where dato like '%RUT%' and num_caso like '%caso0%' and num_caso<>'caso00' and fecha between '20200501' and '20200726' and valor<>''
				) as a
		left join	(
					select nif2,centro,row_number() over (partition by nif2 order by fechalta) rownum from admomi.iddpacpa3 as b
					) as a01 on a01.nif2=a.nif and a01.rownum=1
		) as a
		group by rollup(1);

		--total casos sin contactos identificados
		select distinct a.nif,a01.q 
		  from protocolos_omi_covid_base.datos_contactos01 as a--select * from protocolos_omi_covid_base.datos_contactos01
		left join
					(
					select pac_nif,responsable_de_familia_nif 
							,count(*) over (partition by responsable_de_familia_nif) as q
					  from familia.cubo_familias 
					) as a01 on a01.pac_nif=a.nif
		where a.nif in (
							select distinct a01.nif2 
							  from 
							  		(
									select distinct nif,valor 
									  from protocolos_omi_covid_base.datos_contactos01 
									 where dato like '%RUT%'
									   and num_caso like '%caso0%'
									   and num_caso<>'caso00' 
									   and fecha between '20200501' and '20200726' --select * from protocolos_omi_covid_base.datos_contactos01 where dato like '%RUT%' and num_caso like '%caso0%' and num_caso<>'caso00' and fecha between '20200501' and '20200726' and valor<>''
									) as a
							left join	(
										select nif2,centro,row_number() over (partition by nif2 order by fechalta) rownum from admomi.iddpacpa3 as b
										) as a01 on a01.nif2=a.nif and a01.rownum=1 --select * from protocolos_omi_covid_base.datos_contactos01 where dato like '%RUT%' and num_caso like '%caso0%' and num_caso<>'caso00' and fecha between '20200501' and '20200726' and valor<>''
						)
			and a.nif in  (
						select nif2 from total_casos_identificados
						)	
			and a.num_caso='caso00'
			and a.fecha between '20200501' and '20200726';
	
--contactos nuevos investigados en menos de 48 hrs
drop table if exists contactos_identificados_antes_48hrs_01;create temp table contactos_identificados_antes_48hrs_01 as
select *
  from
  		(
		select a.*,a.fecha_identificacion-a.fecha_pcr as dif
		  from 
		  		(
				select a.*
						,max(a02.fecha) over (partition by a.nif2) as fecha_pcr
						,min(a.fecha) over (partition by a.nif2) as fecha_identificacion	
				  from
				  		(
						select distinct a01.centro,a01.nif2,a.valor,a.fecha 
						  from 
						 		(
								select distinct nif,valor,fecha 
								  from protocolos_omi_covid_base.datos_contactos01 --select * from protocolos_omi_covid_base.datos_contactos01 where dato like '%RUT%' and num_caso like '%caso0%' and num_caso<>'caso00'
								 where dato like '%RUT%' and num_caso like '%caso0%' and num_caso<>'caso00' and valor<>''
								) as a
								left join	(
											select nif2,centro,row_number() over (partition by nif2 order by fechalta) rownum from admomi.iddpacpa3 as b
											) as a01 on a01.nif2=a.nif and a01.rownum=1
						) as a	
						left join total_casos_identificados as a02 on a02.nif2=a.nif2
				) as a
		) as a;

		--select * from contactos_identificados_antes_48hrs_01
		--total contactos identificados ANTES DE 48 HRS
		select centro,count(*) from (select distinct centro,valor from contactos_identificados_antes_48hrs_01 WHERE dif<=4) as a group by rollup(1) order by 1,2;

		--total contactos identificados ANTES DE 48 HRS
c		select centro,count(*) from (select distinct centro,valor from contactos_identificados_antes_48hrs_01 WHERE FECHA_PCR between '20200627' and '20200726' and dif<=4) as a group by rollup(1) order by 1,2;

		--total contactos identificados
		select centro,count(*) from (select distinct centro,valor from contactos_identificados_antes_48hrs_01 WHERE FECHA_PCR between '20200627' and '20200726') as a group by rollup(1) order by 1,2;

	
--cumplimiento cuarentena en la semana con 2 o mas seguimientos
--num
select centro,nif2,count(*)
  from
  		(
		select *,extract(week from fecha) as semana
		  from
		  		(
				select distinct centro,nif2,fecha
				  from contactos_identificados_antes_48hrs_01 where fecha between '20200713' and '20200726'
				) as a
		) as a group by 1,2 order by 3;
	;
--den
select centro,semana,count(*)
  from
  		(
		select centro,valor,extract(week from fecha) as semana 
		  from contactos_identificados_antes_48hrs_01 where fecha between '20200720' and '20200726'
		) as a
	group by 1,2;
--select distinct valor from protocolos_omi_covid_base.datos_contactos01 where num_caso='caso00' and fecha between '20200720' and '20200726'
----------------------------------------------------------
-- END
----------------------------------------------------------
drop table if exists usr_edox2.covid_casos001;create table usr_edox2.covid_casos001 (id serial,nif2 integer, fecha timestamp,rut text);
insert into usr_edox2.covid_casos001
select nextval('usr_edox2.covid_casos001_id_seq') as id,a.nif2,a.fecha,null::text as rut
  from 
  		(
  		select nif2::integer as nif2,min(fecha) fecha
  		  from total_casos_identificados 
  		 where nif2 is not null and nif2<>'' 
  		   and nif2 in 	(
  		   				select nif from admomi.iddpacpa3 where estado2 is not null
  		   				)
  		 group by 1
		) as a;

--update rut 
with consulta as (
select nif2,upper(tis) as tis,row_number() over (partition by nif2 order by fechalta) rownum from admomi.iddpacpa3 where estado2 is not null
) 
update usr_edox2.covid_casos001
   set rut=consulta.tis
  from consulta
 where usr_edox2.covid_casos001.nif2=consulta.nif2
   and consulta.rownum=1;
			

--select * from usr_edox2.covid_casos001
drop table if exists usr_edox2.covid_casos002;create table usr_edox2.covid_casos002 as --select * from usr_edox2.covid_casos002 as 
select a.*
		,max(a01.fecha) over (partition by a.rut2) ultimo_seguimiento
  from
  		(
		select a.*
				,case when a01.rut is not null then a01.rut else a.rut end as rut2 --casos probables sin caso indice confirmado es su propio caso indice
		  from
		  		(
				select distinct rut
						,case when min_id is not null then min_id else id end min_id2
				  from 
				  		(
						select a.*
								,min(id) over (partition by rut) min_id
						  from 
						  		(
								select  distinct a.*
										,a01.id 
								  from (
								  		select nif_caso_indice,rut 
										  from usr_edox.carga_neotel_03 
										 where rut is not null and rut<>''
										 group by 1,2
										) as a
								 left join 	(
								 			select b.id,a.nif_caso_indice,a.rut 
								 			  from usr_edox.carga_neotel_03 as a,usr_edox2.covid_casos001 as b
								 			 where a.nif_caso_indice = b.nif2 
											 ) as a01 
											   on a01.rut=a.rut
								order by 3 desc
								) as a order by a.id		  
						) as a order by 2 desc
				) as a 
				left join usr_edox2.covid_casos001 as a01 on a01.id =a.min_id2 		
		where a.rut not in (
							'26838260-7'
							,'NPASAPORTE141702577'
							,'25920932-3'
							,'NPASAPORTE149350868' 
						)
		) as a
		left join 	(
					select a.nif,a.fecha,b.tis from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 as a,admomi.iddpacpa3 as b
					  where a.nif=b.nif2
					) as a01 on a01.tis=a.rut2
where min_id2 is not null;

select * from usr_edox2.covid_casos002 where ultimo_seguimiento is null;

select distinct rut from usr_edox2.covid_casos002;

select distinct min_id2
  from usr_edox2.covid_casos002 where rut2 not in (select numcolegia from admomi.iddmed);

 --numero de casos confirmados
select distinct min_id2 --select distinct rut,min_id2 from usr_edox2.covid_casos002  --3522 select 3522-1774
  from usr_edox2.covid_casos002 --SELECT * from usr_edox2.covid_casos002 
 where min_id2 in 	(
					select distinct min_id2
				      from usr_edox2.covid_casos002 
				     where rut2 not in (select numcolegia from admomi.iddmed) 
				       and rut2 in 	(
				       					select b.tis from total_casos_identificados as a,admomi.iddpacpa3 as b where a.nif2=b.nif and a.fecha<='20200719 23:59:59'
				       					)
				        --select * from usr_edox2.covid_casos002 
					);
--numero de casos totales con contactos
select distinct min_id2
  from usr_edox2.covid_casos002 
 where rut<>rut2
   and min_id2 in  (
					select distinct min_id2
					   from usr_edox2.covid_casos002
					  where rut2 not in (select numcolegia from admomi.iddmed)
					    and rut2 in 	(
				       					select b.tis from total_casos_identificados as a,admomi.iddpacpa3 as b 
				       					 where a.nif2=b.nif 
				       					   and a.fecha<='20200719 23:59:59'
				       					) 
					); 
--numero de contactos nuevos identificados
select count(*)
  from 
  		(
		select rut2
		  from	(
				select distinct rut2
				  from usr_edox2.covid_casos002 
				 where rut<>rut2
				   and min_id2 in  (
									select distinct min_id2
									   from usr_edox2.covid_casos002
									  where rut2 not in (select numcolegia from admomi.iddmed)
									    and rut2 in 	(
								       					select b.tis from total_casos_identificados as a,admomi.iddpacpa3 as b 
								       					 where a.nif2=b.nif 
								       					   and a.fecha between '20200713 00:00:00' and '20200719 23:59:59'
								       					) 
									) 
				) as a	
		) as a;
	
	
--contactos identificados anrtres de 48 hrs
select distinct rut2
  from
  		(
		select *,fecha_seguimiento - fecha_notificacion dif
		  from (
				with consulta as	(
				select distinct rut2
				  from usr_edox2.covid_casos002 
				 where rut<>rut2
				   and min_id2 in  (
									select distinct min_id2
								      from usr_edox2.covid_casos002 
								     where rut2 not in (select numcolegia from admomi.iddmed)
								       and rut2 in 		(
								       					select b.tis from total_casos_identificados as a,admomi.iddpacpa3 as b 
								       					 where a.nif2=b.nif 
								       					   and a.fecha<='20200719 23:59:59'
								       					)
									)
				)
				select *
						,max(a01.fecha) over (partition by a.rut2) as fecha_notificacion
						,min(a02.fecha) over (partition by a.rut2) as fecha_seguimiento
				  from consulta as a
				  left join	(
				  			select rut,fecha::date as fecha from usr_edox2.covid_casos001			   
				  ) as a01 on a01.rut=a.rut2
				  left join	(
				  			select b.tis,a.fecha 
				  			  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 as a
				  			  		,admomi.iddpacpa3 as b
				  			 where b.nif2=a.nif
				  ) as a02 on a02.tis=a.rut2
		  ) as a
		) as a where dif<=4
  ; --1748

select distinct a.nif
  from 
  		(
		select nif,count(*) as q
		  from
		  		(
				select nif,extract(week from fecha) 
				  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409
				 where fecha between '20200720' and '2020070726'
				) as a
 		group by 1
		order by 2
		) as a where a.q>=2; --select 2431/3765::numeric


----------------------------------------------------------
-- END
----------------------------------------------------------













/*
select * from admomi.iddome as ome;
select * from admomi.iddoml as oml;
select * from admomi.iddomr as omr;
select * from admomi.iddomd as omd;
select * from admomi.iddpac as pac;
select count(*) FROM (select distinct auto from admomi.idddmo) AS A ; --5903306
SELECT COUNT(*) FROM admomi.idddmo; 5903306
select row_number() over (partition by auto),* from admomi.hisa_idddmo as dmo;*/

--numero total de casos identificados 1926
/*todas las PCR positivas OMI y lab UC*/
drop table if exists total_casos_identificados;create temp table total_casos_identificados as
select a01.nif,a.orderdate::date as fecha from examenes_etl.mg_vw_ancora2 as a left join admomi.iddpacpa3 as a01 on a01.tis=a.patient_rut where profilecode = '2798H'  and interpretedresult='Positivo'  --1146
 union
select   pac.nif,ome.fecha::date --,DMO.TEXTO
  from   admomi.iddome as ome
  		,admomi.iddoml as oml
        ,admomi.iddomr as omr
        ,admomi.iddomd as omd
        ,admomi.iddpacpa3 as pac
        ,admomi.idddmo as dmo
 where ome.numorden = oml.numorden
   and dmo.numorden=ome.numorden
   and oml.orden = omr.orden
   and omr.clave = omd.clave
   and ome.nif = pac.nif
   and omd.mostrar='1'
   and ome.clasepru = '1'
   and oml.tipolo='R'
   and omr.orden in (4549642,8473662)
   and UPPER(dmo.texto) like '%POSITIV%'; --1266
--union
--select distinct a.nif,A.FECHA2 from admomi.iddncu as a,admomi.iddpacpa3 as b where a.ciap='G12' and a.nif=b.nif and b.estado2 is not null;

--numero de casos identificados
select distinct nif from total_casos_identificados 
 where fecha<='20200713 23:59:59';
   
--numero de casos nuevos
select nif,min(fecha) 
 from total_casos_identificados 
where fecha between '20200720' and '20200726' group by 1; 

--casos con identificación de contactos
select distinct nif_caso_indice 
  from usr_edox.carga_neotel_03 
 where num_caso<>'caso00'
   and nif_caso_indice in 	(
  							select nif::integer 
  						 	  from total_casos_identificados
  						 	 where fecha between '20200713' and '20200719' group by 1 
  						 	)
   and nif_caso_indice;

select distinct nif_caso_indice 
  from usr_edox.carga_neotel_03 
 where num_caso<>'caso00'
   and nif_caso_indice in 	(
  							select nif::integer 
  						 	  from total_casos_identificados
  						 	 where fecha between '20200720' and '20200726' group by 1 
  						 	)
   and nif_caso_indice;



--contactos identificados
select * 
  from 
  		( --numero de contactos identificados
		select distinct nif_caso_indice,num_caso from usr_edox.carga_neotel_03 --select * from usr_edox.carga_neotel_03 
		 where num_caso<>'caso00'
		   and fecha_confirmacion between '20200713 00:00:00' and '20200719 23:59:59'
		) as a;

select * 
  from 
  		( --numero de contactos identificados
		select distinct nif_caso_indice,num_caso from usr_edox.carga_neotel_03 --select * from usr_edox.carga_neotel_03 
		 where num_caso<>'caso00'
		   and fecha_confirmacion between '20200720 00:00:00' and '20200726 23:59:59'
		) as a;
	
				
   --and nif_caso_indice in (select nif::integer from total_casos_identificados);

--casos seguimiento antes de 48hrs

from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409

select * from total_casos_identificados

--select distinct a.nif from admomi.iddncu as a,admomi.iddpacpa3 as b where a.ciap='G12' and a.nif=b.nif and b.estado2 is not null --1883

--select * from solicitud_examen_pcr_01;

with consulta as 
				(
				select a.nif,max(a.fecha2) as fecha_confirmacion
				  from admomi.iddncu as a,admomi.iddpacpa3 as b
				 where a.ciap='G12'
				   and a.nif=b.nif
				   and b.estado2 is not null
				 group by 1
				) 
				select a.*,a01.codilab 
			      from consulta as a
			     left join 
			    			(select nif,fecha,codilab
			    					,row_number() over (partition by codilab) rownum
			    		       from solicitud_examen_pcr_01
			    		    ) as a01 
			    		      on a01.nif=a.nif
			    		     and a01.fecha<=a.fecha_confirmacion
			    		     and a01.rownum=1
			    order by 3 desc; --1976
			
select distinct codilab from solicitud_examen_pcr_01 order by 1;
select row_number() over (partition by codilab) as codilab_rep,* from solicitud_examen_pcr_01 where codilab<>'' order by 1 desc nulls last,6;

--PCR02
drop table if exists solicitud_examen_pcr_01;
create table solicitud_examen_pcr_01 as 
select NULLIF(regexp_replace(a01.nif, '\D','','g'), '')::numeric as nif
		,a01.tis
		,a.orderdate::date as fecha_solicitud
		,a.interpretedresult resultado
		,a.num_oa
		,null::integer as numorden
  from examenes_etl.mg_vw_ancora2 as a 
  left join admomi.iddpacpa3 as a01 
  		 on a01.tis=a.patient_rut 
 where a.profilecode = '2798H' 
union
select   a.nif2
		,a01.tis
		,a.fecha_solicitud
		,a.resultado
		,a.codilab
		,a.numorden
  from protocolos_omi_covid_base.examenes_pcr_omi as a
    left join admomi.iddpacpa3 as a01 
  		 on a01.nif2=a.nif2  
; 

/*
drop table if exists solicitud_examen_pcr_01;
create table solicitud_examen_pcr_01 as 
select   pac.nif
		,pac.tis
		,ome.fechap as fecha
		,dmo.texto as resultado
		,ome.codilab
		,dmo.numorden 
		,dmo.auto
  from   admomi.iddome as ome
  		,admomi.iddoml as oml
        ,admomi.iddomr as omr
        ,admomi.iddomd as omd
        ,admomi.iddpacpa3 as pac
        ,admomi.idddmo as dmo
 where ome.numorden = oml.numorden
   and dmo.numorden=ome.numorden
   and oml.orden = omr.orden
   and omr.clave = omd.clave
   and ome.nif = pac.nif
   and omd.mostrar='1'
   and ome.clasepru = '1'
   and oml.tipolo='R'
   and omr.orden in (4549642,8473662) --AND omr.texto LIKE 'Coronavirus%'
union
select   a01.nif
		,patient_rut
		,orderdate::Date orderdate
		,interpretedresult 
		,num_oa,null,null
  from examenes_etl.mg_vw_ancora2 as a --select * from examenes_etl.mg_vw_ancora2 as a
 left join admomi.iddpacpa3 as a01 on a01.tis=a.patient_rut
 where profilecode = '2798H'  --and interpretedresult='Positivo'  
;
*/

drop table if exists covid19.hechos_examenes_pcr;create table covid19.hechos_examenes_pcr as
with consulta as	(
 select distinct a.*
		,case 
			when a.fecha_apertura_episodio_confirmacion<a.fecha_identificacion_contactos 
				then (a.fecha_apertura_episodio_confirmacion-fecha_recepcion)::integer 
				else (a.fecha_identificacion_contactos-fecha_recepcion)::integer
				end as latencia1
  from
		(
		select   distinct 
		         a01.centro
				,a01.nif2
				,a.patient_rut as rut
				,a.orderdate as fecha_solicitud
				,a.receptiondate as fecha_recepcion
				,min(a04.q) over (partition by a04.tis) as es_confirmado
				,min(a02.fecha2) over (partition by a02.tis,a.receptiondate) as fecha_apertura_episodio_confirmacion --as positiva --2977
				,min(a02.fecha2) over (partition by a02.tis,a.receptiondate) as fecha_nutificacion --as positiva --2977
				,case 
					when  a03.fecha2 is not null 	then min(a03.fecha2) over (partition by a03.tis,a.receptiondate) 
													else min(a02.fecha2) over (partition by a02.tis,a.receptiondate)
				end as fecha_identificacion_contactos
				,a.interpretedresult as resultado_exacto
				,case 
					when lower(a.interpretedresult) like '%indeterminado%' 	then 'Positivo' 
					when lower(a.interpretedresult) like '%Pendiente%' 		then 'Positivo'
					else a.interpretedresult end as resultado
				,1::integer as q
		  		FROM 
		  				(
		  				select distinct patient_rut,orderdate::Date orderdate,receptiondate::date receptiondate,interpretedresult 
		  				from examenes_etl.mg_vw_ancora2
		  				where profilecode = '2798H'
		  				) as a
		  left join (
		  			select centro,tis,nif2 
		  					,row_number() over (partition by tis order by fechalta) rownum
		  			  from admomi.iddpacpa3 where tis like '%-%' and estado2 is not null
		  			) as a01 on a01.tis=a.patient_rut and a01.rownum =1 --2920
		  left join 
		  			( --confirmación
		  			select pac.tis,ncu.fecha2,ncu.ciap
		  					,row_number() over (partition by pac.tis) rownum
		  			  from admomi.iddncu as ncu,admomi.iddpacpa3 as pac
		  			 where ncu.ciap in ('G12','G13')
		  			   and ncu.nif2=pac.nif2 
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null union
		  			select pac.tis,ncu.fechalta2,ncu.ciap
		 				   ,row_number() over (partition by pac.tis) rownum
		  			  from admomi.iddncu as ncu,admomi.iddpacpa3 as pac
		  			 where ncu.ciap in ('G10')
		  			   and ncu.nif2=pac.nif2 and ncu.fechalta2 is not null
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null
		  			) as a02 
		  			  on a02.tis=a.patient_rut 
		  			 and a02.rownum=1  
		  			 and a02.fecha2>=a.orderdate
		  left join 
		  			( --seguimiento
		  			select pac.tis
		  					,seg.fecha as fecha2
		  					,row_number() over (partition by pac.tis) rownum 
		  			  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 seg
		  			  		,admomi.iddpacpa3 as pac
		  			 where seg.nif=pac.nif2
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null
		  			) as a03
		  			  on a03.tis=a.patient_rut
		  			 and a03.rownum=1
		  			 and a03.fecha2>=a.orderdate
		    left join 
		  			( --es_confirmación
		  			select pac.tis,ncu.fecha2,ncu.ciap,1::integer as q
		  					,row_number() over (partition by pac.tis) rownum
		  			  from admomi.iddncu as ncu,admomi.iddpacpa3 as pac
		  			 where ncu.ciap in ('G12')
		  			   and ncu.nif2=pac.nif2 
		  			   and pac.tis<>'' 
		  			   and pac.tis is not null
		  			) as a04 
		  			  on a04.tis=a.patient_rut 
		  			 and a04.rownum=1  
		  			 and a04.fecha2>=a.orderdate
		  order by a.orderdate desc nulls last
		 ) as a --where es_confirmado=1
		)
	select distinct case when latencia1<=2 then 1 else 0 end as latencia1_kpi,* --((r_quantile(array_accum(latencia1),0.75) - r_quantile(array_accum(latencia1),0.25) )*1.5)+(r_quantile(array_accum(latencia1),0.75)) as iqr2
      from consulta;

select latencia1_kpi,count(*) 
  from covid19.hechos_examenes_pcr 
 where latencia1<=7 and es_confirmado=1 
 group by rollup(1) order by 1;

--SELECT 531::NUMERIC/1131::NUMERIC

select * from covid19.hechos_examenes_pcr order by latencia1;

       
       
select * from examenes_etl.mg_vw_ancora2 where profilecode = '2798H';

/*
--PCR01
drop table if exists covid19.hechos_examenes_pcr;
create table covid19.hechos_examenes_pcr as
select   distinct a01.centro
		,a.paciente_nif
		,a.paciente_rut as rut
		,a.orden_fecha_solicitud as fecha_solicitud
		,a.examen_resultado_interpretado as resultado_exacto
		,case 
			when lower(a.examen_resultado_interpretado) like '%indeterminado%' 	then 'Positivo' 
			when lower(a.examen_resultado_interpretado) like '%Pendiente%' 		then 'Positivo'
			
			else a.examen_resultado_interpretado end as resultado
		,1::integer as q
		--,case 
		--		when a.examen_resultado_interpretado<>'Positivo' 
		--		then (select a02.fecha_cierre_sospecha order by 1 limit 1) 
		--		end as fecha_cierre_sospecha
		--,case when a.examen_resultado_interpretado<>'Positivo' then a02.fecha_cierre_sospecha - a.orden_fecha_solicitud end as latencia_de_descarte
		--,case when a.examen_resultado_interpretado<>'Positivo' then min(A02.fecha_cierre_sospecha) end as fecha_cierre_sospecha
  FROM examenes_etl.mg_vw_ancora_final2 as a --select * from examenes_etl.mg_vw_ancora_final2 as a limit 10
  left join (
  			select centro,tis 
  					,row_number() over (partition by tis order by fechalta) rownum
  			  from admomi.iddpacpa3 where tis like '%-%' and estado2 is not null
  			) as a01 on a01.tis=a.paciente_rut and a01.rownum =1 --2920
  			where a.examen_codigo = '2798H';
*/  		
/*
  left join (
			select nif2,FECHA2 as fecha_cierre_sospecha
  			  from admomi.iddcop
  			 where wproto=409
  			   and wnumero=64 
  			   and campo='DESCARTA' 
  			) as a02 on a02.nif2=a.paciente_nif and a02.fecha_cierre_sospecha>=a.orden_fecha_solicitud
 where a.examen_codigo = '2798H'; --group by 1,2,3,4,5,6,7; 
 */


--pacientes PCR vs contactados

  		--positividad
select *
  from 
  		(
		select   a.nif 
				,sum(a01.q) as sospecha
				,sum(a02.q) as confirmacion
		  from 
		  		(
				 select distinct a.nif
				   from admomi.iddncu as a
				  where a.ciap in ('G10','G12','G13')
		  		) as a
		  left join 
		  			(
		  			select distinct a.nif,1::smallint q
							  from admomi.iddncu as a
							 where a.ciap in ('G10')
		  			) as a01 on a01.nif=a.nif
		  left join 
		  			(
		  			select distinct a.nif,1::smallint q
							  from admomi.iddncu as a
							 where a.ciap in ('G12','G13')
		  			) as a02 on a02.nif=a.nif
		  			group by 1
		) as a
		
		--oxigenotera
  
 		--,case when ciap='G10' then 'Sospecha' else 'Confirmacion' end as sospecha_confirmacion
 		 		
select distinct a.paciente_nif,a01.nif 
  from covid19.hechos_examenes_pcr as a
  left join
  			(
  			select * 
  			  from protocolos_omi_covid.pr__covid_seguimiento_casos_y_contacto_wp_000409 
  			) as a01 
  			  on a01.nif = a.paciente_nif and a01.fecha>=a.fecha_solicitud
order by 2;

--N1
--den
select count(*)
  from 
  		(
  		select distinct nif
  		  from admomi.iddcor 
  		 where numero=1 
  		   and wproto=409
  		   and nif in 
  				   		(
  				   		select distinct nif2 
						  from admomi.iddncu as a
						 where a.ciap in ('G12','G13')
						)
  		) as a;
--num
select count(*) 
  from
  		(
		select distinct nif 
		  from admomi.iddncu as a
		 where a.ciap in ('G12','G13')
		) as a;
--N2		
select count(*)
  from 
  		(
  		select distinct nif
  		  from admomi.iddcor 
  		 where numero=1 
  		   and wproto=409
  		) as a;
--N3
select distinct nif
  from
  		(
		select *
				,case when fecha_seguimiento_siguiente is not null
					then fecha_seguimiento_siguiente - fecha_seguimiento
					end as dif
		   from 
		   		(
				 select nif,fecha2 as fecha_seguimiento 
				 		,lead (fecha2,1) over (partition by nif order by fecha2) fecha_seguimiento_siguiente
				   from admomi.iddcor 
				  where numero=1 
				    and wproto=409 order by 1,2
				) as a
		) as a where dif>=3; 
--N4
--numerdor
select distinct nif2 --,fecha_seguimiento,num_caso,contacto_tiene_sintomas 
  from protocolos_omi_covid.listado_contactos_seremi
 where contacto_tiene_sintomas<>'' 
   and contacto_tiene_sintomas is not null
   and contacto_tiene_sintomas<>'ASINTOMÁTICO'
  ;
--DENOMINADOR
select distinct nif2,fecha_seguimiento,num_caso,contacto_tiene_sintomas 
  from protocolos_omi_covid.listado_contactos_seremi
  ;

--N5
select distinct a.nif 
  from admomi.iddncu as a,admomi.iddpacpa3 as b
 where a.ciap in ('G12','G13')
   and a.nif2=b.nif2
   and b.tis in 
   				(
				select distinct contacto_rut 
				  from protocolos_omi_covid.listado_contactos_seremi
				)
  ;

--N6
select avg(orden_fecha_recepcion-orden_fecha_solicitud) 
  from examenes_etl.mg_vw_ancora_final2
 where examen_descripcion1 like '%COVID%';
