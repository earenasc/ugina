------------------------------------ PCR ------------------------------------ 
drop 	table if exists covid19_kpi.insumo_pcr_positiva01;
create	table 			covid19_kpi.insumo_pcr_positiva01 as
select   a.patient_rut as rut --NULLIF(regexp_replace(a01.nif, '\D','','g'), '')::numeric as nif2
		,a.orderdate::timestamp as fecha,a.orderdate::date as fecha2
  from examenes_etl.mg_vw_ancora2 as a 
 where a.profilecode = '2798H' 
   and a.interpretedresult='Positivo' --order by 2 desc nulls last limit 100
union
select a01.rut--,NULLIF(regexp_replace(a.nif, '\D','','g'), '')::numeric as nif2
	,a02.stk_fecha::timestamp,a02.stk_fecha::date as fecha2
  from protocolos_omi_covid_base.examenes_pcr_omi as a
  left join	(
  			select nif2,tis as rut,row_number() over (partition by nif order by fechalta) rownum
    		  from admomi.iddpacpa3
    		 where tis is not null and tis<>'' and tis like '%-%'
    		) as a01 on a01.nif2=a.nif2 and a01.rownum=1
  left join	(
  			select * 
  					,row_number() over (partition by nif2,numorden order by stk_fecha) rownum
  			   from protocolos_omi_covid_base.hisa_iddome as a 
  			  where stk_tipo_operacion='A'
  			) as a02 on a02.nif2=a.nif2 and a02.rownum=1 and a02.numorden=a.numorden
 where a.resultado like '%POSITIV%' order by 2 desc nulls last
;


------------------------------------ EPISODIOS ------------------------------------
drop 	table if exists covid19_kpi.insumos_episodios01;
create	table 			covid19_kpi.insumos_episodios01 as
select  a01.rut
		,a.ciap
		,a02.stk_fecha::timestamp as fecha--a.fecha
		,a02.stk_fecha::date as fecha2
  from protocolos_omi_covid_base.iddncu as a --select * from protocolos_omi_covid_base.hisa_iddncu as a
  left join	(
  			select nif2,tis as rut,row_number() over (partition by nif order by fechalta) rownum
    		  from admomi.iddpacpa3
    		 where tis is not null and tis<>'' and tis like '%-%'
    		) as a01 on a01.nif2=a.nif2 and a01.rownum=1
  left join	(
  			select * 
  					,row_number() over (partition by nif2,especiali order by stk_fecha) rownum
  			   from protocolos_omi_covid_base.hisa_iddncu as a 
  			  where stk_tipo_operacion='A'
  			) as a02 on a02.nif2=a.nif2 and a02.rownum=1 and a02.especiali=a.especiali
  where a.ciap in ('G12','G13')
; 

------------------------------------ CONTACTOS IDENTIFICADOS ------------------------------------

drop table if exists protocolos_seguimient01;create temp table protocolos_seguimient01 as 
with consulta as	(
					select a.nif2,a.fecha2,a.campo 
					  from protocolos_omi_covid_base.iddcop as a,admomi.iddcol as b --select * from protocolos_omi_covid_base.iddcop limit 100
					 where a.wproto=b.wproto
					   and b.wproto=409 
					   and a.wnumero=b.wnumero
					   and upper(b.wcomentari) like '%RUT%'
					   and a.campo<>'' 
					   and a.campo is not null
					)
select a01.rut as rut_caso,a.fecha2,a.campo as rut_contacto
  from consulta as a --select * from protocolos_omi_covid_base.iddcop limit 100
  	left join	(
  				select nif2,tis as rut 
  						,row_number() over (partition by tis order by fechalta) rownum
  				  from admomi.iddpacpa3 
  				 where estado2 is not null 
  				   and tis<>'' 
  				   and tis is not null 
  				   and tis like '%-%'
  				) as a01 
  				  on a01.nif2=a.nif2 
  				 and a01.rownum=1
; 

update protocolos_seguimient01 set rut_caso=replace(rut_caso,'.','');
update protocolos_seguimient01 set rut_caso=replace(rut_caso,',','');
update protocolos_seguimient01 set rut_caso=replace(rut_caso,'--','-');
update protocolos_seguimient01 set rut_caso=replace(rut_caso,':','');
update protocolos_seguimient01 set rut_caso=replace(rut_caso,'*','');
update protocolos_seguimient01 set rut_caso=replace(rut_caso,'/','');
update protocolos_seguimient01 set rut_caso=replace(rut_caso,'+','');
update protocolos_seguimient01 set rut_caso=replace(rut_caso,' ','');

update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'.','');
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,',','');
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'--','-');
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,':','');
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'*','');
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'/','');
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,'+','');
update protocolos_seguimient01 set rut_contacto=replace(rut_contacto,' ','');

delete from protocolos_seguimient01 where rut_caso is null or rut_caso='';
delete from protocolos_seguimient01 where rut_contacto is null or rut_contacto='';

--select count(*) from protocolos_seguimient01 as a --3389

drop table if exists protocolos_seguimient02;create temp table protocolos_seguimient02 as
select a.rut_caso,a.fecha2,a01.rut_contacto as rut_contacto2
  from protocolos_seguimient01 as a
  left join protocolos_seguimient01 as a01 on a01.rut_caso=a.rut_caso and a.fecha2>=a01.fecha2;


drop table if exists protocolos_seguimient03;create temp table protocolos_seguimient03 as 
select rut_caso as rut,fecha2 from protocolos_seguimient02 union
select rut_caso,fecha2 from protocolos_seguimient02 union
select rut_contacto2,fecha2 from protocolos_seguimient02 union   
select rut_contacto2,fecha2 from protocolos_seguimient02;
create index protocolos_seguimient03_i_01 on protocolos_seguimient03(rut);
create index protocolos_seguimient03_i_02 on protocolos_seguimient03(rut,fecha2);
  --select * from protocolos_omi_covid.seguimiento_covid_hechos03 where contacto_rut<>'' and contacto_rut is not null;

--select rut,fecha2,count(*) from protocolos_seguimient03 group by 1,2

drop table if exists covid19_kpi.insumo_contactos_identificados01;
create table covid19_kpi.insumo_contactos_identificados01 as
with consulta as 
				(
				select distinct rut_caso,rut_contacto2,fecha2 from protocolos_seguimient02
				)
				select rut_caso,rut_contacto2,fecha2,count(*) as q from consulta group by 1,2,3 order by 1,3,2;

------------------------------ SEGUIMIENTOS TOTALES ------------------------------------

drop table if exists seguimientos_totales01;create temp table seguimientos_totales01 as 
with consulta as	(
					select a.nif2,a.fecha2,a.campo 
					  from protocolos_omi_covid_base.iddcop as a
					 where a.wproto=409 and campo<>'' and campo is not null
					)
					select a01.rut as rut_caso,a.fecha2,a.campo 
					  from consulta as a --select * from protocolos_omi_covid_base.iddcop limit 100
					  	left join	(
					  				select nif2,tis as rut 
					  						,row_number() over (partition by tis order by fechalta) rownum
					  				  from admomi.iddpacpa3 
					  				 where estado2 is not null 
					  				   and tis<>'' 
					  				   and tis is not null 
					  				   and tis like '%-%'
					  				) as a01 
					  				  on a01.nif2=a.nif2 
					  				 and a01.rownum=1
					; 

update seguimientos_totales01 set rut_caso=replace(rut_caso,'.','')				;
update seguimientos_totales01 set rut_caso=replace(rut_caso,',','')				;
update seguimientos_totales01 set rut_caso=replace(rut_caso,'--','-')				;
update seguimientos_totales01 set rut_caso=replace(rut_caso,':','')				;
update seguimientos_totales01 set rut_caso=replace(rut_caso,'*','')				;
update seguimientos_totales01 set rut_caso=replace(rut_caso,'/','')				;
update seguimientos_totales01 set rut_caso=replace(rut_caso,'+','')				;
update seguimientos_totales01 set rut_caso=replace(rut_caso,' ','')				;


delete from seguimientos_totales01 where rut_caso is null or rut_caso='';

drop table if exists 	covid19_kpi.insumos_contactos_2_o_mas_seguimientos01;
create table 			covid19_kpi.insumos_contactos_2_o_mas_seguimientos01 as
select distinct rut_caso,semana,dif_semana,q
		,case when semana-dif_semana=1 then 1 else 0 end casos_con_2_o_mas_seeguimientos
		--,case when sum(q) over (partition by rut_caso)>=1 then 1 else 0 end casos_con_2_o_mas_seeguimientos 
  from
  		(
		select a.*
				,lag(a.semana,1) over (partition by rut_caso order by semana) dif_semana
		  from 
				(
				select rut_caso,extract(week from fecha2) as semana,count(*) as q 
				  from 
				  		(
				  		select  distinct rut_caso,fecha2
				  		   from seguimientos_totales01 
				  		   group by 1,2 order by 1,2
				  		) as a group by 1,2
				) as a
		) as a --where semana-dif_semana=1 
;
---------------------------
-- HECHOS
---------------------------
drop table if exists covid19_kpi.hechos01;
create table covid19_kpi.hechos01 as
select rut,fecha::date as fecha2 from covid19_kpi.insumo_pcr_positiva01 union
--select rut_caso,fecha2  from covid19_kpi.insumo_contactos_identificados01 union
select rut,fecha::date  from covid19_kpi.insumos_episodios01;

delete from covid19_kpi.hechos01 where rut='' or rut is null;

drop table if exists covid19_kpi.hechos02;
create table covid19_kpi.hechos02 as
select distinct rut from covid19_kpi.hechos01;

---------------------------
-- CUBO TABULAR
---------------------------
drop table if exists covid19_kpi.cubo_tabular01;
create table covid19_kpi.cubo_tabular01 as
select distinct a05.centro,a.rut
		,a01.fecha2 	as fecha_pcr_positiva  
		,a02.fecha2 	as fecha_episodio_confirmacion
		,case when a02.ciap='G13' then 1 else 0 end as es_caso_probable
		,case when a02.fecha2>=a01.fecha2  then 1 else 0 end as confirmacion_con_pcr
		,min(a03.fecha2) over (partition by a.rut) 	as identificacion_contactos_fecha
		,a03.q 				as identificacion_contactos_q
		,case when a04.es_funcionario is not null then 1 else 0 end as es_funcionario
  from covid19_kpi.hechos02 as a
 left join covid19_kpi.insumo_pcr_positiva01 				
  	    as a01 on a01.rut=a.rut 
 left join covid19_kpi.insumos_episodios01   				
 		as a02 
 		on a02.rut=a.rut 
 	   and a02.ciap in ('G12','G13') --and a02.fecha2>=a01.fecha2 
 left join covid19_kpi.insumo_contactos_identificados01 	
        as a03 
        on a03.rut_caso=a.rut 
       and (a03.fecha2>=a02.fecha2)
 left join	( 
 			select numcolegia,1::smallint as es_funcionario
 					,row_number() over (partition by numcolegia order by id) rownum
 			  from admomi.iddmed 
 			 where numcolegia like '%-%' 
 			) as a04
	          on a04.numcolegia=a.rut
	         and a04.rownum=1
  left join 
 			(
 			select tis,centro
 					,row_number() over (partition by tis order by fechalta) as rownum
 		      from admomi.iddpacpa3 where estado2 is not null and tis<>'' and tis like '%-%' and tis is not null
 		     ) as a05 on a05.tis=a.rut and a05.rownum=1;
  
drop table if exists covid19_kpi.cubo_tabular02;
create table covid19_kpi.cubo_tabular02 as
select distinct a.*
		,identificacion_contactos_fecha - fecha_pcr_positiva 			as dif_pcr_vs_identificacion
		,identificacion_contactos_fecha - fecha_episodio_confirmacion 	as dif_connfirmacion_vs_identificacion
		,case when fecha_pcr_positiva is not null then fecha_pcr_positiva else fecha_episodio_confirmacion end as fecha_caso
		,case when fecha_pcr_positiva is not null then extract(week from fecha_pcr_positiva) else extract(week from fecha_episodio_confirmacion) end as fecha_caso_semana
  from covid19_kpi.cubo_tabular01 as a;

drop table if exists covid19_kpi.cubo_kpi_covid03;create table covid19_kpi.cubo_kpi_covid03 as
select distinct a.*
		,case when a06.casos_con_2_o_mas_seeguimientos is not null then 1 else 0 end as kpi05_01_numerador_casos_con_2_o_mas_seguimiento	
		,case when a07.kpi05_02_denominador_casos_con_en_seguimiento is not null then 1 else 0 end as kpi05_02_denominador_casos_con_en_seguimiento
		,case when a.fecha_caso is not  null then (a.fecha_caso+'14 days'::interval)::date end as fechalta
		,case when a.fecha_caso is not  null and a.fecha_caso+'14 days'::interval>=now()::date then 1 else 0 end caso_activo
  from covid19_kpi.cubo_tabular02 as a
  left join 
  			(
  			select *
  			,row_number() over (partition by rut_caso) rownum
  			from covid19_kpi.insumos_contactos_2_o_mas_seguimientos01 
  			where casos_con_2_o_mas_seeguimientos=1
  			) as a06 
  			  on a06.rut_caso=a.rut
  			 and a06.rownum=1--and a.fecha_caso_semana=a06.semana -- no aplicar que e=
    left join 
  			(
  			select rut_caso,1::smallint as kpi05_02_denominador_casos_con_en_seguimiento
  			,row_number() over (partition by rut_caso) rownum
  			from covid19_kpi.insumos_contactos_2_o_mas_seguimientos01 
   			) as a07 
   			  on a07.rut_caso=a.rut
   			 and a07.rownum=1--and a.fecha_caso_semana=a06.semana -- no aplicar que e=
 where (a.fecha_pcr_positiva is not null or a.fecha_episodio_confirmacion is not null)
   and a.rut in (select tis from admomi.iddpacpa3 where estado2 is not null and tis<>'' and tis like '%-%' and tis is not null);

select * from covid19_kpi.cubo_kpi_covid03;

/*
--CREATE EXTENSION IF NOT EXISTS plperlu;
CREATE OR REPLACE FUNCTION foo1() RETURNS TEXT AS $$
    return `rm -rf /opt/backup/covid_carga_kpi/|
   			/usr/pgsql-12/bin/pg_dump -Fd ancorabi -n covid19_kpi -p5432 -j8 -v -f /opt/backup/covid_carga_kpi`;
    $$ LANGUAGE plperlu;
select foo1();

CREATE OR REPLACE FUNCTION foo2() RETURNS TEXT AS $$
    return `/usr/pgsql-12/bin/pg_restore -d ancorabi2 -p5433 -c -v -j8 /opt/backup/covid_carga_kpi/`;
    $$ LANGUAGE plperlu;
select foo2();

*/