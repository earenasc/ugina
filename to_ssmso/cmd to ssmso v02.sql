-------------------------------------------------------------------------------------------------------------------
--RANGOS DE FECHA
-------------------------------------------------------------------------------------------------------------------

drop table if exists rango_fechas;
create table rango_fechas as
select '20200701 00:00:00'::timestamp as fechai,'20200731 23:59:59'::timestamp as fechaf;
--(select fechai from rango_fechas) and (select fechaf from rango_fechas) 
-------------------------------------------------------------------------------------------------------------------
-- CMD FARMACOS
-------------------------------------------------------------------------------------------------------------------

drop table if exists despacho_de_farmaco; create temp table despacho_de_farmaco as
--truncate table a_mis_ssmso.despacho_de_farmaco; --select count(*) from a_mis_ssmso.despacho_de_farmaco limit 100
--insert into a_mis_ssmso.despacho_de_farmaco
select auto::numeric as id_despacho
	,null::numeric as id_receta_proveedor
	,a.nif as id_paciente
	,b.run::numeric as run
	,b.dv::character(1) as dv
	,to_char(b.nacimiento::date,'yyyymmdd') as fec_nac
	,case when b.run is not null then 1::numeric(1) else 9::numeric(1) end tipo_identificacion
	,(select c.id from admomi.iddmed as c where upper(a.usuario)=c.medico limit 1)::numeric(8) as run_funcionario_despachador
	,(select c.dv from admomi.iddmed as c where upper(a.usuario)=c.medico limit 1)::character(1) as dv_funcionario_despachador
	,3::numeric 					as tipo_codigo_medicamento
	,a.codigo::character varying			as codigo_medicamento
	,a.desfar::character varying			as glosa_medicamento
	,cast(fcantidad as int)::numeric		as cantidad_despachada
	,(select case when c.envase = '1' then 1::numeric -- 'COMPRIMIDO'
		when c.envase = '%' then 2::numeric -- 'CAPSULAS'
		when c.envase = 'G' then 3::numeric -- 'CAJA'
		when c.envase = '2' then 4::numeric -- 'FRASCO'
		when c.envase = 'X' then 5::numeric -- 'AMPOLLA'
		when c.envase = 'E' then 8::numeric -- 'POMO'
		when c.envase = 'H' then 9::numeric -- 'SUPOSITORIO'
		when c.envase = 'I' then 10::numeric -- 'OVULO'
		/*
		when c.envase = '$' then ::numeric -- 'Grageas'
		when c.envase = '#' then ::numeric -- 'sachet'
		when c.envase = '3' then ::numeric -- 'BOTELLA'
		when c.envase = '4' then ::numeric -- 'TALONARIO'
		when c.envase = '7' then ::numeric -- 'SET'
		when c.envase = '8' then ::numeric -- 'LITRO'
		when c.envase = '9' then ::numeric -- 'KILO'
		when c.envase = 'A' then ::numeric -- 'MATRAZ'
		when c.envase = 'B' then ::numeric -- 'BOLSA'
		when c.envase = 'C' then ::numeric -- 'PAQUETE'
		when c.envase = 'D' then ::numeric -- 'BLOCK'
		when c.envase = 'F' then ::numeric -- 'PAR'
		when c.envase = 'K' then ::numeric -- 'TARRO'
		when c.envase = 'M' then ::numeric -- 'PLIEGO'
		when c.envase = 'N' then ::numeric -- 'UNIDAD'
		when c.envase = 'O' then ::numeric -- 'SOBRE'
		when c.envase = 'P' then ::numeric -- 'DOSIS'
		when c.envase = 'Q' then ::numeric -- 'RESMA'
		when c.envase = 'R' then ::numeric -- 'ROLLO'
		when c.envase = 'Z' then ::numeric -- 'BIDOM'
		*/
		else 0::numeric  end unidad_despacho
		from admomi.iddshcm as c where a.codigo=c.material and c.envase<>'' order by fecha desc limit 1)as unidad_despacho
	,null::numeric as cantidad_penmdiente
	,to_char(ufecha::date,'yyyymmdd') as fecha_despacho
	,to_char(ufecha::time,'hh:mm') as hora_despacho
	,case when b.centro = 'MTC' 	then 114323::numeric
	      when b.centro = 'JPII' 	then 114327::numeric
	      when b.centro = 'SAH' 	then 114329::numeric	end				as codigo_deis_rx		
from admomi.iddfm2 as a ,admomi.iddpacpa3 as b --select * from admomi.iddfm2 limit 10
where a.nif=b.nif
and a.situacion='1'
and a.ufecha between (select fechai from rango_fechas) and (select fechaf from rango_fechas);

truncate table mis.despacho_de_farmaco; --
insert into mis.despacho_de_farmaco select * from despacho_de_farmaco;

-------------------------------------------------------------------------------------------------------------------
-- CMD ACTIVIDAD
-------------------------------------------------------------------------------------------------------------------
--drop table if exists a_mis_ssmso.actividad; create table a_mis_ssmso.actividad as
--truncate table a_mis_ssmso.actividad;
--insert into a_mis_ssmso.actividad
drop table if exists actividad; create temp table actividad as
select a1.numage 										as id_atencion
	,a.run::numeric 									as run_sindv
	,a.dv 											as dv
	,case when a1.asignada='S' and a1.usuario <>''then a3.id::int end 			as run_responsable
	,case when a1.asignada='S' and a1.usuario <>'' then a3.dv::char(1) end 		as dv_responsable
	,null::int 																	as numero_identificacion
	,case when (select 1::smallint   
			from admomi.iddpag as b 
			where a.nif2=b.nif2 and b.dgp='*P98' 
				and date_part('years',b.fecha)=date_part('years',now()) 
				order by b.fecha limit 1) = 1 					then 2::numeric
		when a.run is not null then 1::numeric 						end 		as id_tipo_identicficacion
	,a.nif::char(50) 															as id_usuario_proveedor
	,a1.acto::char(50) 															as cod_actividad
	,a2.denomina::char(50) 														as glosa_actividad 
from admomi.iddpacpa3 as a ,admomi.iddage as a1,admomi.iddact as a2,admomi.iddmed as a3,admomi.iddesp as a4
where a.nif=a1.nif
and a1.acto=a2.acto
and a1.medico=a3.medico
and a3.medicof=a4.codigoesp
and a1.fecha between (select fechai from rango_fechas) and (select fechaf from rango_fechas)
and a3.med_estamento_cod is not null and a3.medicof <> '';

truncate table mis.actividad;
insert into mis.actividad select * from actividad;

--select * from mis.actividad
-------------------------------------------------------------------------------------------------------------------
-- CMD PROCESO ADMISION
-------------------------------------------------------------------------------------------------------------------
drop table if exists numero_casa; -- select * from numero_casa
create temp table numero_casa as
select distinct nif::int
	,numero::numeric
	,case when numero <> '' then numero::numeric end numero_0
from (
select distinct nif ,tiscab
	,regexp_split_to_table(ltrim(rtrim(coalesce (tiscab,''))),'[^0-9]+') as numero
from admomi.iddpacpa3

) as a where numero <>'';
create index numero_casa_i_01 on numero_casa using btree (nif,numero);
------------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists admision_direccion; create temp table admision_direccion as
select distinct a.nif::char (50) 						as id_usuario_proveedor
	,case 	when (	rtrim(a.domicilio) like 'PJE%' 
		or 	rtrim(a.domicilio) like 'PJ%' 
		or 	rtrim(a.domicilio) like 'PSJE%' 
		or 	rtrim(a.domicilio) like 'PASAJE%')
			then 3::int
		when (	rtrim(a.domicilio) like 'AV%' 
		or 	rtrim(a.domicilio)like 'AVENIDA%'
			) then 2::int
		else 1::int							end via
	,rtrim(a.domicilio)::char(100) 						as nombre_calle
	,(select b.numero from numero_casa as b where a.nif2=b.nif limit 1) 	as numero
	,trim(a.domicilio)::char(100) 						as resto_dirreccion
	,case when a.ga in ('','.') then null else a.ga::numeric 		end id_comuna
	,now()::date 								as fecha_actualizacion
from admomi.iddpacpa3 as a
where a.estado2 <>'';

truncate table mis.admision_direccion;
insert into mis.admision_direccion select distinct * from admision_direccion;

-------------------------------------------------------------------------------------------------------------------
-- CMD PROCESO ADMISION FAMILIA, USUARIO
-------------------------------------------------------------------------------------------------------------------
drop table if exists admision_usuario; create temp table admision_usuario as
select a.nif::char (50) 												as id_usuario_proveedor
	,(case 	when char_length(replace (a.tis,'-','')) =9 then substring (a.tis,0,9)::numeric(8) 
		when char_length(replace (a.tis,'-','')) =8 then substring (a.tis,0,8)::numeric(8) end )::numeric(8) 	as run_sndv
	,right (tis,1)::char(1) 											as dv
	,(case 	when char_length(replace (a.tis,'-','')) =9 then substring (a.tis,0,9)::numeric(8) 
		when char_length(replace (a.tis,'-','')) =8 then substring (a.tis,0,8)::numeric(8) end )::numeric(8) 	run_responsable
	,right (tis,1)::char(1) 											as dv_responsable
	,''::char(50) 														as numero_idebtificacion
	,''::char(50) 														as id_tipo_idebtificacion
	,a.nombre::char(50) 												as nombres
	,a.apellido1::char(50) 												as primer_apellido
	,a.apellido2::char(50) 												as segundo_apellido
	,''::char(50) 														as responde_a
	,a.nacimiento::date 												as fecha_nacimiento
	,(case 	when a.sexo='M' then 1
		when a.sexo='F' then 2
		when a.sexo='I' then 3
		when a.sexo='D' then 9
	end )::numeric(1) 													as id_sexo
	,null::numeric (6) 													as id_actividad_economica
	,(case 
		when a.coberturap in ('00000001','00000002','00000003','00000004') then 1::smallint -- FONASA
		when a.coberturap='00000006' then 2::smallint -- ISPARE
		when a.coberturap='00000000' then 3::smallint -- SIN PREVEISION
		when a.coberturap='00000005' then 7::smallint --PRAIS		
		when a.coberturap='00000007' then 9::smallint -- DESCONOCIDA
		else 9::smallint
	end)::NUMERIC(1) 												as id_prevision
	,null::numeric(1) 												as id_leyes_sociales
	,null::numeric(1) 												as id_religion
	,null::numeric(1) 												as id_administradores_seguro
	,(case 	when a.estado='S' then 1::smallint -- SOLTERO 
		when a.estado='C' then 2::smallint -- CASADO
		when a.estado='V' then 3::smallint -- VIUDO
		when a.estado='E' then 4::smallint -- SEPARADO
		when a.estado='T' then 5::smallint -- CONVIVIENTE 
		when a.estado='D' then 6::smallint -- DIVORCIADO
		when a.estado='X' then 9::smallint -- DESCONOCIDO
		when a.estado=''  then 9::smallint -- CAMPO EN BLANCO
		end)::numeric(1) 											as id_estado_conyugal
	,null::numeric(2) 												as id_pueblos_originarios
	,null::numeric(2) 												as id_nivel_instruccion
	,null::numeric(2) 												as id_ocupacion
	,0::smallint 													as id_nacionalidad
	/*,case 	when pais in ('12','19','02','03','04','15','08','20','A','07','14','11','23','22','17','18','16','21','13','99','09','05','10','06') then 1::smallint
		when pais= '01' then 0::smallint
		else 0::smallint end id_nacionalidad */
	,(select case when texto ='S' then 1::smallint when texto ='N' then 0::smallint else null end from admomi.iddpag as b where a.nif=b.nif and b.dgp='NIPREMAT' limit 1) as es_prematuro
	--,(select b.fechahora::date from admomi.stk_hisa as b where b.nhc=a.nif and b.tablahisa ='HISA_IDDPAC' and usuarioomi <>''order by b.fechahora desc limit 1) as fecha_actualizacion
	,now()::date as fecha_actualizacion
from admomi.iddpacpa3 as a
where a.estado2 <> '';

--delete from a_mis.admision_usuario;insert into a_mis.admision_usuario 
truncate table mis.admision_usuario cascade;
insert into mis.admision_usuario select distinct * from admision_usuario;

-------------------------------------------------------------------------------------------------------------------
-- CMD PROCESO ADMISION - FAMILIA
-------------------------------------------------------------------------------------------------------------------
drop table if exists fono_contacto; -- select * from fono_contacto
create temp table fono_contacto as
select distinct familia_id
	,paciente_id
	,telefono
	--,fecha::date
	,max(fecha::date) as fecha
	,regexp_split_to_array(telefono, '[^0-9]+') as fono1
	,regexp_split_to_array(coalesce (telefono,''), '[^0-9]+') as fono2
from contactabilidad.telefonos_historicos_07
group by 1,2,3,5,6; --326002<<
create index fono_contacto_i_01 on fono_contacto using btree (familia_id,paciente_id,telefono,fecha,fono1,fono2);

---------------------------------------------------------------------------------------------------------------------------------------------------------

drop table if exists base_fono_contacto;
create temp table base_fono_contacto as  -- select * from base_fono_contacto
select distinct familia_id
	,paciente_id
	,fecha
	,telefono
	,case 	when right(coalesce(fono1[1],'')||''||coalesce(fono1[2],'')||''||coalesce(fono1[3],''),9 )='' then null::int
		when right(coalesce(fono1[1],'')||''||coalesce(fono1[2],'')||''||coalesce(fono1[3],''),9 )<> '' 
		then right(coalesce(fono1[1],'')||''||coalesce(fono1[2],'')||''||coalesce(fono1[3],''),9 )::int
	end telefono1
from fono_contacto;
create index base_fono_contacto_i_01 on base_fono_contacto using btree(familia_id,paciente_id,fecha,telefono,telefono1);
create index base_fono_contacto_i_02 on base_fono_contacto using btree(paciente_id,fecha);
create index base_fono_contacto_i_03 on base_fono_contacto using btree(paciente_id);

---------------------------------------------------------------------------------------------------------------------------------------------------------

--DIMENSION NUMERO CASA
drop table if exists numero_casa; -- select * from numero_casa limit 10
create temp table numero_casa as
select distinct nif::int
	,numero::numeric
	,case when numero <> '' then numero::numeric end numero_0
from (
select distinct nif ,tiscab
	,regexp_split_to_table(ltrim(rtrim(coalesce (tiscab,''))),'[^0-9]+') as numero
from admomi.iddpacpa3
	) as a where numero <>'';
create index numero_casa_i_01 on numero_casa using btree (nif,numero,numero_0);

-------------------------------------------------------------------------------------------
--create index usr_jesoto_pacientes_validos_i_01 on usr_jesoto.pacientes_validos using btree(nifs);
--create index usr_jesoto_pacientes_validos_i_02 on usr_jesoto.pacientes_validos using btree(nifp);

drop table if exists familia_parentesco;
create temp table familia_parentesco as 
with consulta as (
select   nifp
	,nifs
	,parentesco as parentesco_00
	,case when parentesco in (1,2) 	then 1::int 	--1='MADRE'/ 2='PADRE'
			when parentesco =3  		then 2::int	--3='HIJO(A)'
			when parentesco =5 		then 3::int 	--5='ABUELO(A)'
			when parentesco =6 		then 4::int 	--6='NIETO(A)'
			when parentesco =4 		then 9::int 	--4='HERMANO(A)'
			when parentesco in (23,7,8,12,13,14,16,18,19,20,21,22)	then 0::int	--23='OTROS'  
			when parentesco =9 		then 10::int 	--9='TIO(A)'
			when parentesco =10 		then 11::int 	--10='SOBRINO(A)'
			when parentesco =11		then 12::int	--11='PRIMO(A)'
			when parentesco in (15,17) 	then 13::int   --15='SUEGRO(A)' / 17='NUERA'
			 end parentesco
from admomi.iddgfm as a --select count(*) from admomi.iddgfm
)
select distinct a.* 
	,b.nif as nifp2,b.nif as nifs2  --,(select nif from admomi.pacientes_validos as b where b.nif2=a.nifp limit 1) nifp2
	--,(select nif from admomi.pacientes_validos as b where b.nif2=a.nifs limit 1) nifs2
  from consulta as a
  left join admomi.pacientes_validos as b on b.nif2=a.nifp
  left join admomi.pacientes_validos as c on c.nif2=a.nifs
;
create index familia_parentesco_i_01 on familia_parentesco using btree (nifp,nifs,parentesco_00,parentesco);
create index familia_parentesco_i_02 on familia_parentesco using btree (nifp,nifs2,parentesco_00,parentesco);
create index familia_parentesco_i_03 on familia_parentesco using btree (nifp2,nifs2,parentesco_00,parentesco);
create index familia_parentesco_i_04 on familia_parentesco using btree (nifp2,nifs2);
create index familia_parentesco_i_05 on familia_parentesco using btree (nifp2);
create index familia_parentesco_i_06 on familia_parentesco using btree (nifs2);

-------------------------------------------------------------------------------------------

drop table if exists tabla_familia;
create temp table tabla_familia as
select distinct a.nif::int as nif
	,case 	when (	rtrim(a.domicilio) like 'PJE%' 
		or 	rtrim(a.domicilio) like 'PJ%' 
		or 	rtrim(a.domicilio) like 'PSJE%' 
		or 	rtrim(a.domicilio) like 'PASAJE%')
			then 3::int
		when (	rtrim(a.domicilio) like 'AV%' 
		or 	rtrim(a.domicilio)like 'AVENIDA%'
			) then 2::int
		else 1::int											end via
	,rtrim(a.domicilio)::char(100) 										as nombre_calle
	,(select b.numero from numero_casa as b where a.nif2=b.nif limit 1) as numero
	,trim(a.domicilio)::char(100) as resto_direccion
	,now()::date 		as fecha_actualizacion
	,case when a.ga in ('','.') then null else a.ga::numeric end id_comuna
from admomi.iddpacpa3 as a
where a.estado2 <> '';
create index tabla_familia_i_01 on tabla_familia using btree (nif);
create index tabla_familia_i_02 on tabla_familia using btree (via);
create index tabla_familia_i_03 on tabla_familia using btree (nombre_calle);
create index tabla_familia_i_04 on tabla_familia using btree (numero);
create index tabla_familia_i_05 on tabla_familia using btree (resto_direccion);
create index tabla_familia_i_06 on tabla_familia using btree (fecha_actualizacion);
create index tabla_familia_i_07 on tabla_familia using btree (id_comuna); --select count(*) from tabla_familia

-------------------------------------------------------------------------------------------

drop table if exists cubo_familia;
create temp table cubo_familia as
select a.nif::char (50) 	as id_usuario_proveedor
	,a01.cod_familia 				--(select b.responsable_de_familia_nif::char(9) from familia.cubo_familias as b where a.nif=b.pac_nif limit 1)as cod_familia
	,a02.telefono1 					--(select b.telefono1 from  base_fono_contacto as b where a.nif=b.paciente_id order by fecha desc limit 1) as telefono1
	,a.fecha_actualizacion
	,a03.parentesco 				--(select b.parentesco from familia_parentesco as b where a.nif=b.nifs2 or a.nif=b.nifp2 limit 1) as parentesco
	,a.via
	,a.nombre_calle 
	,a.numero
	,a.resto_direccion
	,a.id_comuna
from tabla_familia as a
	left join 	(
				select   responsable_de_familia_nif::char(9) as cod_familia
						,pac_nif
						,row_number() over (partition by responsable_de_familia_nif) as rownum
				  from familia.cubo_familias
	) as a01 on a.nif=a01.pac_nif and a01.rownum=1
	left join 	(
				select paciente_id,fecha,telefono1
						,row_number() over (partition by paciente_id order by fecha desc) as rownum
				  from base_fono_contacto
	) as a02 on a.nif=a02.paciente_id and a02.rownum=1
	left join 	(
				select aa.nifs2,aa.nifp2,aa.parentesco
						,row_number() over (partition by nifp2,nifs2) rownum
				  from
				  		(
						select nifs2,nifp2,parentesco from familia_parentesco union
						select nifp2,nifs2,parentesco from familia_parentesco
						) as aa		
	) as a03 on a.nif=a03.nifs2 and a03.rownum=1 
	;
--select id_parentesco,count(*) from mis.admision_familia group by 1 order by 1
--select parentesco,count(*) from cubo_familia group by 1 order by 1
--left join familia_parentesco as a1 on a.nif::text=a1.nifs or a.nif::text=a1.nifp
--limit 100;

drop table if exists admision_familia0001; create temp table admision_familia0001 as --select * from admision_familia0001 limit 10
select distinct id_usuario_proveedor
	,cod_familia
	,telefono1
	,fecha_actualizacion
	,case when parentesco is null then 0::int else parentesco end id_parentesco
	,via
	,nombre_calle
	,numero
	,resto_direccion
	,id_comuna
from cubo_familia;

delete from mis.admision_familia;
insert into mis.admision_familia select * from admision_familia0001 where id_usuario_proveedor in (select distinct id_usuario_proveedor from mis.admision_usuario);

--ALTER TABLE mis.admision_familia
--ADD CONSTRAINT id_usuario_proveedor_fk FOREIGN KEY (id_usuario_proveedor) REFERENCES mis.admision_usuario (id_usuario_proveedor);

COMMENT ON COLUMN mis.admision_familia.id_usuario_proveedor IS '
Código identificador de cada usuario según software de origen o proveedor.';

COMMENT ON COLUMN mis.admision_familia.cod_familia IS '
Código identificador de cada grupo familiar según software de origen o proveedor.';

COMMENT ON COLUMN mis.admision_familia.telefono1 IS '
Corresponde al número telefónico fijo alternativo o número de telefono móvil alternativo del usuario registrado en el Registro clínico electónico de Origen';

COMMENT ON COLUMN mis.admision_familia.fecha_actualizacion IS '
Fecha de actualización del registro del usuario en el Registro clínico electrónico de origen ';

COMMENT ON COLUMN mis.admision_familia.id_parentesco IS '
0=Desconocido
1=Padres
2=Hijos
3=Abuelos
4=Nietos
5=Bisabuelos
6=Bisnietos
7=Tatarabuelos
8=Tataranietos
9=Hermanos
10=Tíos
11=Sobrino
12=Primos
13=Suegros/Nueras
14=Cuñados
15=Jefe Familia
';

COMMENT ON COLUMN mis.admision_familia.via IS '
Corresponde al tipo de via de la última dirección registrada por el usuario.
"1=CALLE
2=AVENIDA
3=PASAJE
4=OTRO
"';

COMMENT ON COLUMN mis.admision_familia.nombre_calle IS '
Corresponde al nombre de la calle o avenida de la última dirección registrada por el usuario.';

COMMENT ON COLUMN mis.admision_familia.numero IS '
Corresponde al número de la última dirección registrada por el usuario. En caso de existir información anexa, como block o departamento, no debe ser considerada.';

COMMENT ON COLUMN mis.admision_familia.resto_direccion IS '
Corresponde a información anexa a la última dirección registrada, como block o departamento del usuario.';

COMMENT ON COLUMN mis.admision_familia.id_comuna IS '
Corresponde a la comuna de la última dirección registrada por el usuario.';

-------------------------------------------------------------------------------------------------------------------
-- CMD ADMISION - INSCRIPCION
-------------------------------------------------------------------------------------------------------------------
drop table if exists admision_inscripcion;create temp table admision_inscripcion as 
select a.nif::char (50) 			as id_usuario_proveedor
	,fechalta::date 			as fecha_inscripcion
	---,null::bytea 				as es_inscrito
	,case when tipopac<>'I' or estado2 ='HISTORICO' then 1::int  else 0::int end es_inscrito
	,case when tipopac<>'I' or estado2 ='HISTORICO' then 1::int  else 0::int end es_activo
	,null::int 				as id_motivo_pasivacion
	--,case when causa in ('21','03') or estado2 = 'HISTORICO' then fechalta::date else null end fecha_pasivacion
	,case when causa in ('21','03') or estado2 = 'HISTORICO' then exitus::date else null
	--	then (select b.fechahora::date from admomi.stk_hisa as b where b.nhc=a.nif and tipooperacion='M' and b.tablahisa='HISA_IDDPAC' and nombreomi not in ('','OMI') order by  b.fechahora desc limit 1)
	 end fecha_pasivacion
	 ,case 	when consultorio='VERDE' then 1::int
		when consultorio='AZUL' then  2::int 	end id_sector
	,case 	when centro ='MTC' 	then 323::int 
		when centro ='JPII' 	then 327::int
		when centro ='SAH' 	then 329::int 	end id_centro
	,now()::date 					as fecha_actualizacion
from admomi.iddpacpa3 as a
where a.estado2 <> '';

truncate table mis.admision_inscripcion;
insert into mis.admision_inscripcion select * from admision_inscripcion;

-------------------------------------------------------------------------------------------------------------------
-- CMD PROCEO ADMISION - MEDIOS DE CONTACTO
-------------------------------------------------------------------------------------------------------------------
--- DATOS CONTACTOS TELEFONICO BASE
drop table if exists fono_contacto; -- select * from fono_contacto
create temp table fono_contacto as
select distinct familia_id
	,paciente_id
	,telefono
	--,fecha::date
	,max(fecha::date) as fecha
	,regexp_split_to_array(telefono, '[^0-9]+') as fono1
	,regexp_split_to_array(coalesce (telefono,''), '[^0-9]+') as fono2
from contactabilidad.telefonos_historicos_07
group by 1,2,3,5,6; --326002<<
create index fono_contacto_id_i_01 on fono_contacto using btree (familia_id,paciente_id,telefono,fecha,fono1,fono2);

drop table if exists base_fono_contacto;
create temp table base_fono_contacto as  -- select * from base_fono_contacto
select distinct familia_id
	,paciente_id
	,fecha
	,telefono
	,case 	when right(coalesce(fono1[1],'')||''||coalesce(fono1[2],'')||''||coalesce(fono1[3],''),9 )='' then null::int
		when right(coalesce(fono1[1],'')||''||coalesce(fono1[2],'')||''||coalesce(fono1[3],''),9 )<> '' 
		then right(coalesce(fono1[1],'')||''||coalesce(fono1[2],'')||''||coalesce(fono1[3],''),9 )::int
	end telefono1
from fono_contacto;
create index base_fono_contacto_i_01 on base_fono_contacto using btree(paciente_id,telefono,fecha);

drop table if exists admision_medios_de_contacto;
create temp table admision_medios_de_contacto as 
select distinct a.nif::char (50) 	as id_usuario_proveedor
	,(select b.telefono1 from  base_fono_contacto as b where a.nif2=b.paciente_id order by fecha desc limit 1) as telefono1
	,(select b.telefono1 from  base_fono_contacto as b where a.nif2=b.paciente_id order by fecha desc limit 1) as telefono2
	,null::char(100) 	as email
	,now()::date 		as fecha_actualizacion
	--,(select b.fecha::date from  base_fono_contacto as b where a.nif2=b.paciente_id order by fecha desc limit 1) as fecha_actualizacion
from admomi.iddpacpa3 as a
where a.estado2 <> '';

truncate table mis.admision_medios_de_contacto;
insert into mis.admision_medios_de_contacto select * from admision_medios_de_contacto;


-------------------------------------------------------------------------------------------------------------------
-- CMD PROCEO ATENCION
-------------------------------------------------------------------------------------------------------------------

drop table if exists atenciones; create temp table atenciones as
--truncate table atenciones;
--insert into a_mis_ssmso.atenciones
select a.run::numeric 										as run_sindv
	,a.dv 											as dv
	,a.run::numeric 									as run_responsable
	,a.dv 											as dv_responsable
	,null::int 										as numero_identificacion
	,case when (select 1::smallint   from admomi.iddpag as b 
		    where a.nif2=b.nif2 and b.dgp='*P98' 
			and date_part('years',b.fecha)=date_part('years',now()) 
		    order by b.fecha limit 1) = 1 then 2::numeric
		when a.run is not null then 1::numeric 						end id_tipo_identicficacion
	,a.nif::char(50) 									as id_usuario_proveedor
	,a1.numage 										as id_atencion
	,null::int  										as id_cita
	,a1.fecha::date 									as fecha_atencion
	,a1.horaf::date 									as fecha_cierre_atencion
	,case 	when a1.asignada='S' and vino <>'N' is null 	then 1::character varying(100)
		when a1.asignada='S' and vino in('S','')  	then 2::character varying(100)
		when a1.asignada='S' and vino='N' 		then 3::character varying(100)
												end estado_atencion
		,a.consultorio::character varying(100) 						as codigo_sector
	,a.consultorio::character varying(100) 							as sector
	,null::int 										as eliminado 
	,null::int 										as fecha_eliminado
	,case 	when a2.denomina  like 'I%' then 0::int
		when a2.denomina  like 'g%' then 1::int 					end atencion_individual_grupal
	,a3.id 											as run_profesional
	,a3.dv 											as dv_profesional
	,a3.med_nombres::character varying(100)							as nombre_profesional
	,split_part (a3.med_apellidos,' ',1)::character varying(100)				as primer_apellido_profesional
	,split_part (a3.med_apellidos,' ',2)::character varying(100)				as segundo_apellido_profesional
	,case 	when a4.codigoesp='103' then '1 Asistente Social'::character varying(100) 
		when a4.codigoesp='093' then '2 Educador(a) de Párvulos'::character varying(100)
		when a4.codigoesp='000' then '3 Enfermero(a)'::character varying(100)
		when a4.codigoesp='060' then '4 Fonoaudiólogo(a)'::character varying(100)
		when a4.codigoesp='101' then '5 Kinesiólogo(a)'::character varying(100)
		when a4.codigoesp='100' then '6 Matron(a)'::character varying(100)
		when a4.codigoesp='117' then '7 Médico'::character varying(100)
		when a4.codigoesp='400' then '8 Nutricionista'::character varying(100)
		when a4.codigoesp='099' then '10 Psicólogo(a)'::character varying(100)
		when a4.codigoesp='012' then '11 Psiquiatra'::character varying(100)
		when a4.codigoesp='270' then '12 Técnico Paramédico'::character varying(100)
		when a4.codigoesp='280' then '13 Terapeuta Ocupacional'::character varying(100)
		when a4.codigoesp='085' then '14 Ginecólogo'::character varying(100)
		when a4.codigoesp='031' then '15 Odontólogo(a)'::character varying(100)
		when a4.codigoesp='996' then '16 Podólogo(a)'::character varying(100)
		when a4.codigoesp='975' then '26 Laboratorio'::character varying(100)
		when a4.codigoesp='271' then '9 Otros Profesionales'::character varying(100)
		/*
		when a4.codigoesp='' then '17 Tecnólogo Medico'::character varying(100)
		when a4.codigoesp='' then '18 Profesor(a) de Educación Física'::character varying(100)
		when a4.codigoesp='' then '19 Técnico Paramédico Dental'::character varying(100)
		when a4.codigoesp='' then '20 Monitor(a) de la Comunidad'::character varying(100)
		when a4.codigoesp='' then '21 Educador(a) Diferencial'::character varying(100)
		when a4.codigoesp='' then '22 Técnico en Rehabilitación'::character varying(100)
		when a4.codigoesp='' then '23 Psicopedagogo(a)'::character varying(100)
		when a4.codigoesp='' then '25 Agente Medicina Indigena'::character varying(100)
		when a4.codigoesp='' then '27 Químico Farmacéutico'::character varying(100)
		when a4.codigoesp='' then '28 Sociólogo(a)'::character varying(100)
		when a4.codigoesp='' then '29 Asesor Espiritual'::character varying(100)
		when a4.codigoesp='' then '30 Terapeuta en Actividad Fisica y Salud'::character varying(100)
		*/
		end tipo_profesional
	,case when a3.med_especialidad_desc is not null then a3.med_especialidad_desc else 'SIN DATO PARA INFORMAR'::text end as especilidad_profesional
from admomi.iddpacpa3 as a 
	,admomi.iddage as a1
	,admomi.iddact as a2
	,admomi.iddmed as a3
	,admomi.iddesp as a4
where a.nif=a1.nif
and a1.acto=a2.acto
and a1.medico=a3.medico
and a3.medicof=a4.codigoesp
and a1.fecha between (select fechai from rango_fechas) and (select fechaf from rango_fechas)
and a3.med_estamento_cod is not null and a3.medicof <> '' and a3.opcional2 <>'';

truncate table mis.atenciones;
insert into mis.atenciones select * from atenciones;

-------------------------------------------------------------------------------------------------------------------
-- CMD USUARIOS COMGES 22.2
-------------------------------------------------------------------------------------------------------------------
drop table if exists conexiones;create table conexiones as
select   usuarioomi
		,aym(fecha) mes
		,count(*) as q
  from
  		(
		select  medico  	usuarioomi,fecha::date as fecha		from admomi.iddcor,rango_fechas  where fecha 	between fechai and fechaf and numero=1 union
		select  usuario 	usuarioomi,fecha::date as fecha		from admomi.iddpar,rango_fechas  where fecha 	between fechai and fechaf union 
		select  medico  	usuarioomi,fecha::date as fecha		from admomi.iddpag,rango_fechas  where fecha 	between fechai and fechaf union
		select  medico  	usuarioomi,fechavac::date as fecha	from admomi.iddhva,rango_fechas  where fechavac between fechai and fechaf union  	
		select  solicita	usuarioomi,fechap::Date				from admomi.iddome,rango_fechas  where fechap 	between fechai and fechaf union
		select  usuario		extrae ,fechac::Date				from admomi.iddome,rango_fechas  where fechac 	between fechai and fechaf union
		select  usuvisto	visto  ,fechav::Date				from admomi.iddome,rango_fechas  where fechav 	between fechai and fechaf union
		select  usuario		realiza,fechar::Date				from admomi.iddome,rango_fechas  where fechar 	between fechai and fechaf union
		select  usucierre	cierra ,fechare::Date				from admomi.iddome,rango_fechas  where fechare 	between fechai and fechaf 
		) as a where usuarioomi is not null or usuarioomi<>''
group by 1,2 order by 1,2;
create index conexiones_i_01 on conexiones(usuarioomi) tablespace tb_index;   

--select * from conexiones
drop table if exists mis.usuarios;create table mis.usuarios as   
select distinct a.* 
  from 
  		(
		select  distinct 	
				'14'::text cod_ss
				,case 
					when a.centro='MTC'  then 14323
					when a.centro='JPII' then 14327
					when a.centro='SAH'  then 14329
					end as cod_estab --14323 MTC, 14327 JPII, 14329 SAH 
				,A.MED_ESTAMENTO_DESC::text expecialidad
				 ,'1'::smallint proceso --1 ambulatorio APS
				 ,a.medico id_usuario_en_sistema
				 ,a.id run_usuario
				,a.dv dv_usuario
				,(select c.permisos::text from admomi.empleados2 as c where c.rut = a.numcolegia limit 1) perfil_o_grupo_de_seguridad --select * from admomi.empleados2 limit 10
				,b.q numero_de_accesos_en_el_mes
				,b.mes
				--,* 
		  from admomi.iddmed as a 
		  left join conexiones as b on b.usuarioomi=a.medico
		  where a.id>=1000000
 		) as a where a.mes is not null and a.mes<>'' and a.numero_de_accesos_en_el_mes is not null; 


-------------------------------------------------------------------------------------------------------------------
-- CMD DIAGNOSTICOS
-------------------------------------------------------------------------------------------------------------------
drop table if exists diagnostico;create temp table diagnostico as 
select nhc,fecha,especiali,medico from admomi.iddcor 
 where fecha between (select fechai from rango_fechas) and (select fechaf from rango_fechas) 
   and numero=1;

drop table if exists diagnostico01;create temp table diagnostico01 as 
select distinct * from diagnostico;

drop table if exists diagnostico02;create temp table diagnostico02 as 
select 
	a.nhc
	,a.especiali as ID_ATENCION
	,(select c.run from admomi.iddpacpa3 as c where a.nhc=c.nif limit 1) as RUN_SINDV --select * from admomi.iddpacpa3 limit 100
	,(select c.dv  from admomi.iddpacpa3 as c where a.nhc=c.nif limit 1) as DV
	,(select c.id from admomi.iddmed as c where c.medico=a.medico limit 1) as RUN_RESPONSABLE
	,(select c.dv from admomi.iddmed as c where c.medico=a.medico limit 1) as DV_RESPONSABLE
	,null::varchar(50) as NUMERO_IDENTIFICACION
	,null::integer as ID_TIPO_IDENTIFICACION
	,nhc as ID_USUARIO_PROVEEDOR
	,case when b.ciap is null or b.ciap<>'' then 2::smallint else 3::smallint end as TIPO_DIAG 
	,btrim(b.ciap)	as COD_DIAG
	,b.descripcio	as GLOSA_DIAG 
	,case 
		when b.fechalta is not null then 9::smallint
		--when substr(ciap,2,2) in (select generate_series(1,27)::text) then 1::smallint
		else 2::smallint
		end  as ESTADO_DIAG 
	,b.fecha::date as FECHA_APERTURA_DIAG 
	,case when descripcio like '%(GES)%' then 1::smallint else 0::smallint end as GES
	,null::integer as PRINCIPAL 
	,a.fecha::date as FECHA_ATENCION_DIAGNOSTICO 
	,0::smallint as ELIMINADO 
 from diagnostico01 as a 
 left join admomi.iddncu as b on a.nhc=b.nif and a.especiali=b.especiali;

--4.543.175

update diagnostico02 set tipo_diag=3 where cod_diag='EMPA' or cod_diag like 'G%' or cod_diag is null or cod_diag='';

update diagnostico02 set cod_diag='U07.2' ,tipo_diag=1 where cod_diag='G10'; --coronavirus CIE10 sospecha
update diagnostico02 set cod_diag='U07.1' ,tipo_diag=1 where cod_diag='G12'; --coronavirus CIE10 confirmación
update diagnostico02 set cod_diag='U07.1' ,tipo_diag=1 where cod_diag='G13'; --coronavirus CIE10 confirmación

update diagnostico02 set numero_identificacion=id_usuario_proveedor where run_sindv is null;
update diagnostico02 set id_tipo_identificacion=case when numero_identificacion is null then 1::smallint else 3::smallint end;

drop table if exists diagnostico; create temp table diagnostico as select distinct * from diagnostico02;

truncate table mis.diagnostico;
insert into mis.diagnostico select * from diagnostico; 

--SELECT * FROM mis.diagnostico where cod_diag like 'U07.%'
--select distinct ciap,descripcio from admomi.iddncu where ciap like 'G1%'

------------------------------------------------------------------------------------------------------------------
-- CMD EMISION RECETAS DE MEDICAMENTOS
-------------------------------------------------------------------------------------------------------------------
drop table if exists emision_recetas01;create temp table emision_recetas01 as
select a.* from (select codigo
		,desfar
		,idpar
		,nif2 as nif
		,fecha
		,fechaemi
		,posologia
		,especiali
		,usuario
		,activo
		,cierre 
		,dosis 
		,intervalo
		,duradias
		,durameses
  from admomi.iddpar where fechaemi between (select fechai from rango_fechas) and (select fechaf from rango_fechas)
   ) as a;

  --where fecha>='20170101' limit 1000) as a; --union select b.* from (select * from admomi.iddpah limit 10) as b 
--select * from admomi.iddpar where nif2=113147 limit 10
--select * from admomi.iddpar limit 10
--select * from admomi.iddpah limit 1000

drop table if exists emision_recetas02;create temp table emision_recetas02 as 
select 
	 especiali as id_atencion
	,nif as id_paciente 
	,(select c.run from admomi.iddpacpa3 as c where a.nif=c.nif2 limit 1) as RUN --select * from admomi.iddpacpa3 limit 100
	,(select c.dv  from admomi.iddpacpa3 as c where a.nif=c.nif2 limit 1) as DV
	,(select c.id from admomi.iddmed as c where c.medico=a.usuario limit 1) as RUN_RESPONSABLE
	,(select c.dv from admomi.iddmed as c where c.medico=a.usuario limit 1) as DV_RESPONSABLE
	,(select c.nacimiento::Date from admomi.iddpacpa3 as c where a.nif=c.nif2 limit 1) as fecha_nac
	,1::varchar as Tipo_Identificación 
	,idpar as ID_RECETA_PROVEEDOR
	,case when activo='C' then 'CRONICA' else 'AGUDA' end as TIPO_RECETA
	,(duradias+(durameses*30))::integer as PERIODICIDAD
	,a.fechaemi::date as FECHA_EMISION_RECETA
	,'12:00'::varchar as HORA_EMISION
	,case when activo='C' then 30::smallint else (duradias+(durameses*30))::integer end as VIGENCIA
	,a.cierre as ESTADO --(select b.cierre from admomi.iddpar as b where a.idpar=b.idpar limit 1) as ESTADO
	,idpar as ID_PRESCRIPCION
	,3::varchar as TIPO_CODIGO_MEDICAMENTO --propio
	,codigo CODIGO_MEDICAMENTO
	,desfar as GLOSA_MEDICAMENTO
	,2::varchar TIPO_CODIGO_DIAG --ciap
	,(select b.ciap from admomi.iddncu as b       where a.nif=b.nif2 and a.especiali=b.especiali limit 1) as CODIGO_DIAGNOSTICO
	,(select b.descripcio from admomi.iddncu as b where a.nif=b.nif2 and a.especiali=b.especiali limit 1) GLOSA_DIAGNOSTICO
	--,dosis,intervalo,duradias,durameses
	,case when activo<>'C' then round((dosis*intervalo*duradias),0) when dosis/intervalo>0 then round(((dosis*intervalo)/30),0) end  as TOTAL_DOSIS_UNITARIAS
	,'1'::varchar as UNIDAD
	,case when intervalo>0 then round(((intervalo/dosis)*24),0) else null end as FRECUENCIA
	,3::smallint UNIDAD_TIEMPO
	,round((duradias+(durameses*30)),0) as PERIODO_TRATAMIENTO --dias
	,4::smallint as UNIDAD_PERIODO_TRATAMIENTO  --meses
from emision_recetas01 as a;--where a.fecha>='20170101';
--select * from emision_recetas01 as a
update emision_recetas02 set CODIGO_DIAGNOSTICO='U07.2' ,TIPO_CODIGO_DIAG=1 where CODIGO_DIAGNOSTICO='G10'; --coronavirus CIE10 sospecha
update emision_recetas02 set CODIGO_DIAGNOSTICO='U07.1' ,TIPO_CODIGO_DIAG=1 where CODIGO_DIAGNOSTICO='G11'; --coronavirus CIE10 confirmación

drop table if exists emision_receta; create temp table emision_receta as select * from emision_recetas02;

truncate table mis.emision_receta;
insert into mis.emision_receta select * from emision_receta;
--select * from a_mis_ssmso.emision_receta limit 10;

--select count(*) from mis.emision_receta limit 10;

/*
update diagnostico02 set tipo_diag=3 where cod_diag='EMPA' or cod_diag like 'G%' or cod_diag is null or cod_diag='';  
update diagnostico02 set numero_identificacion=id_usuario_proveedor where run_sindv is null;
update diagnostico02 set id_tipo_identificacion=case when numero_identificacion is null then 1::smallint else 3::smallint end;
*/
-------------------------------------------------------------------------------------------------------------------
-- CMD SIGNOS VITALES
-------------------------------------------------------------------------------------------------------------------
--select * from mapping.all_presion_arterial 		limit 100
--select * from mapping.all_peso_talla_imc 		 limit 10 
--select * from mapping.all_circunferencia_cintura	limit 100	
drop table if exists signos_vitales01;create temp table signos_vitales01 as
select nif,fecha2 as fecha,clinico_id 	from mapping.all_presion_arterial  		union
select nif,fecha,clinico_id  			from mapping.all_peso_talla_imc 			union --select * from mapping.all_peso_talla_imc 		 limit 10 
select nif,fecha,clinico_id				from mapping.all_circunferencia_cintura		union
select nif,fecha,clinico_id				from mapping.all_frecuencia_cardiaca		union
select nif,fecha,clinico_id				from mapping.all_frecuencia_respiratoria	union
select nif,fecha,clinico_id				from mapping.all_temperatura_axilar			union
select nif,fecha,clinico_id				from mapping.all_glicemia_capilar; 	

drop table if exists signos_vitales02;create temp table signos_vitales02 as 
select distinct 
	 (select c.especiali from admomi.iddcor_min as c where a.nif=c.nif and a.fecha=c.fecha2 and a.clinico_id=c.clinico_id limit 1) ID_ATENCION
	,(select c.run from admomi.iddpacpa3 as c where a.nif=c.nif2 limit 1) as RUN_SINDV --select * from admomi.iddpacpa3 limit 100
	,(select c.dv  from admomi.iddpacpa3 as c where a.nif=c.nif2 limit 1) as DV
	,(select c.id from admomi.iddmed as c where c.id=a.clinico_id limit 1) as RUN_RESPONSABLE
	,(select c.dv from admomi.iddmed as c where c.id=a.clinico_id limit 1) as DV_RESPONSABLE
	,null::varchar(50) as NUMERO_IDENTIFICACION
	,null::integer as ID_TIPO_IDENTIFICACION
	,a.nif as ID_USUARIO_PROVEEDOR
	,a01.valor as PRESION_ARTERIAL_SISToLICA
	,a02.valor as PRESION_ARTERIAL_DIASTOLICA
	,a05.valor as FRECUENCIA_CARDIACA
	,a06.valor as FRECUENCIA_RESPIRATORIA
	,a07.valor as TEMPERATURA_AXILAR
	,a03.peso  as PESO
	,a03.talla as TALLA
	,a03.imc   as IMC
	,a04.valor as CIRCUNFERENCIA_DE_CINTURA
	,a08.valor as GLICEMIA_CAPILAR
	,a.fecha
 from signos_vitales01  as a
  left join mapping.all_presion_arterial 	    a01 on a.nif=a01.nif and a.fecha=a01.fecha2 and a.clinico_id=a01.clinico_id and a01.tipo='PAS'
  left join mapping.all_presion_arterial 	    a02 on a.nif=a02.nif and a.fecha=a02.fecha2 and a.clinico_id=a02.clinico_id and a02.tipo='PAD'		 --select * from mapping.all_presion_arterial limit 10
  left join mapping.all_peso_talla_imc 		    a03 on a.nif=a03.nif and a.fecha=a03.fecha  and a.clinico_id=a03.clinico_id 				 --select * from mapping.all_peso_talla_imc limit 100
  left join mapping.all_circunferencia_cintura 	a04 on a.nif=a04.nif and a.fecha=a04.fecha  and a.clinico_id=a04.clinico_id 				 --select * from mapping.all_circunferencia_cintura limit 100	
  left join mapping.all_frecuencia_cardiaca 	a05 on a.nif=a05.nif and a.fecha=a05.fecha  and a.clinico_id=a05.clinico_id 				 --select * from mapping.all_frecuencia_cardiaca limit 100	
  left join mapping.all_frecuencia_respiratoria a06 on a.nif=a06.nif and a.fecha=a06.fecha  and a.clinico_id=a06.clinico_id 				 --select * from mapping.all_frecuencia_cardiaca limit 100	
  left join mapping.all_temperatura_axilar      a07 on a.nif=a07.nif and a.fecha=a07.fecha  and a.clinico_id=a07.clinico_id 				 --select * from mapping.all_frecuencia_cardiaca limit 100	
  left join mapping.all_glicemia_capilar     	a08 on a.nif=a08.nif and a.fecha=a08.fecha  and a.clinico_id=a08.clinico_id 				 --select * from mapping.all_frecuencia_cardiaca limit 100	
where a.fecha between (select fechai from rango_fechas) and (select fechaf from rango_fechas);

drop table if exists signos_vitales;create temp table signos_vitales as select distinct * from signos_vitales02;

truncate table mis.signos_vitales;

insert into mis.signos_vitales select * from signos_vitales;
-------------------------------------------------------------------------------------------------------------------
-- CMD SOLICITUDES DE INTERCONSULTAS SIC
-------------------------------------------------------------------------------------------------------------------
drop table if exists sic;create temp table sic as 
select a.nif as nhc,fecha_orden::Date as fecha,a.*,b.especiali --a.nif as nhc,a.fecha_orden::Date as fecha,a.numorden,b.especiali,a.clinico_rut
  from sidra.cubo_ic_electronica_v03 as a,admomi.iddome as b
 where fecha_orden between (select fechai from rango_fechas) and (select fechaf from rango_fechas)
   and b.numorden=a.numorden::integer;

drop table if exists sic01;create temp table sic01 as 
select distinct * from sic;

drop table if exists sic02;create temp table sic02 as 
select 
	--a.nhc
	 a.especiali as ID_ATENCION
	,(select c.run from admomi.iddpacpa3 as c where a.nhc=c.nif limit 1) as RUN_SINDV --select * from admomi.iddpacpa3 limit 100
	,(select c.dv  from admomi.iddpacpa3 as c where a.nhc=c.nif limit 1) as DV
	,(select c.id from admomi.iddmed as c where c.numcolegia=a.CLINICO_rut limit 1) as RUN_RESPONSABLE
	,(select c.dv from admomi.iddmed as c where c.numcolegia=a.CLINICO_rut limit 1) as DV_RESPONSABLE
	,null::varchar(50) as NUMERO_IDENTIFICACION
	,null::integer as ID_TIPO_IDENTIFICACION
	,nhc as ID_USUARIO_PROVEEDOR
	,a.numorden as folio
	,a.fecha as FECHA_GENERACION_SIC
	,'ENVIADA'::text as ESTADO
	,case when prioridad_envio like '%Sin P%' then null else prioridad_envio end as PRIORIDAD
	,1::text as TIPO_PRESTACION --1=consulta especialidad.
	,case when (select b.especialidad_codigo_homologado from sidra_base.homologa_especialidad as b where b.especialidad_codigo=a.especialidad limit 1) 
		is not null then (select b.especialidad_codigo_homologado from sidra_base.homologa_especialidad as b where b.especialidad_codigo=a.especialidad limit 1) 
		else a.especialidad end 
	as COD_ESPECIALIDAD
	,(select c.descripcion from  sidra_base.especialidad as c where c.codigo= (case when (select b.especialidad_codigo_homologado from sidra_base.homologa_especialidad as b where b.especialidad_codigo=a.especialidad limit 1) 
		is not null then (select b.especialidad_codigo_homologado from sidra_base.homologa_especialidad as b where b.especialidad_codigo=a.especialidad limit 1) 
		else a.especialidad end ) limit 1)
	as GLOSA_ESPECIALIDAD
	,case when b.ciap is null or b.ciap<>'' then 2::smallint else 3::smallint end as TIPO_DIAG 
	,btrim(b.ciap)	as COD_DIAG
	,b.descripcio	as GLOSA_DIAGNOSTICO 
	,a.ges_s_n as GES
	,(select c.descripcio
	    from sidra_base.ic_para_enviar as b,admomi.iddtau AS C 
	   where b.folio_interno=a.numorden
	     and C.tabla like '%ICGES%'
	     and c.codigo = b.codigo_sospecha_auge 
	    limit 1) as PROBLEMA_SALUD
	,0::Text as RESOLUTIVIDAD --0=no 1=si
 from sic01 as a 
 left join admomi.iddncu as b on a.nhc=b.nif and a.especiali=b.especiali;

update sic02 set cod_diag='U07.2' ,tipo_diag=1 where cod_diag='G10'; --coronavirus CIE10 sospecha
update sic02 set cod_diag='U07.1' ,tipo_diag=1 where cod_diag='G12'; --coronavirus CIE10 confirmación
update sic02 set cod_diag='U07.1' ,tipo_diag=1 where cod_diag='G13'; --coronavirus CIE10 confirmación (caso probable)

--select * from sic02 where cod_diag in ('U07.1','U07.2')
--select * from admomi.iddncu where ciap in ('G12','G13')

--update sic02 set tipo_diag=3 where cod_diag='EMPA' or cod_diag like 'G%' or cod_diag is null or cod_diag='';  
update sic02 set numero_identificacion	= id_usuario_proveedor where run_sindv is null;
update sic02 set id_tipo_identificacion	= case when numero_identificacion is null then 1::smallint else 3::smallint end;

--select count(*) from sic02  limit 100
drop table if exists sic;create temp table sic as select distinct * from sic02;

truncate table mis.sic;insert into mis.sic select * from sic;
-------------------------------------------------------------------------------------------------------------------
-- ADICIONALES - Protocolos
-------------------------------------------------------------------------------------------------------------------
--create index hisa_iddcor_i_01 on protocolos_omi_covid_base.hisa_iddcor(numero,stk_tipo_operacion,wproto) tablespace tb_index;
drop table if exists mis.nuevo_protocolos_formularios;
create table mis.nuevo_protocolos_formularios as --select * from mis.nuevo_protocolos_formularios
with consulta as 
				(
				select nif2,stk_fecha2,wproto
				  from protocolos_omi_covid_base.hisa_iddcor as a
				 where a.numero=1 
				   and a.stk_tipo_operacion = 'A'
				   and a.wproto>=1
			   )
select   
		 a.nif2 as ID_USUARIO_PROVEEDOR
		,a01.run
		,a01.dv
		,a.stk_fecha2 as fecha
		,a00.wprotocolo as protocolo_formulario
  from consulta as a
  left join (
  			select wproto,wprotocolo 
  					,row_number() over (partition by wproto) rownum
  			  from admomi.iddco5
  			) as a00 
  			  on a00.wproto=a.wproto 
  			 and a00.rownum=1
  left join
  			(
  			select 	 nif2,run,dv 
  					,row_number() over (partition by nif2 order by fechalta) rownum
  			  from admomi.iddpacpa3
  			) as a01 
  			  on a01.nif2=a.nif2 
  			 and a01.rownum=1
; --in (409,412);


-------------------------------------------------------------------------------------------------------------------
-- DOUBLE CHECK
-------------------------------------------------------------------------------------------------------------------

select * from (
select 'CMD ATENCIONES' as CMD,to_char(fecha_atencion, 'YYYY-MM'),count(*) from mis.atenciones group by 1,2 UNION ALL--OK
select 'CMD ACTIVIDAD',to_char(fecha_atencion::Date, 'YYYY-MM'),count(*) from mis.actividad as a left join mis.atenciones using (id_atencion) group by 1,2 union ALL
select 'CMD DIAGNOSTICO',to_char(fecha_atencion_diagnostico, 'YYYY-MM'),count(*) from mis.diagnostico group by 1,2 union ALL
select 'CMD SIGNOS VITALES',to_char(fecha, 'YYYY-MM'),count(*) from mis.signos_vitales group by 1,2 union ALL
select 'CMD SIC',to_char(fecha_generacion_sic, 'YYYY-MM'),count(*) from mis.sic group by 1,2 union ALL
select 'CMD RECETAS EMITIDAS',to_char(fecha_emision_receta::Date, 'YYYY-MM'),count(*) from mis.emision_receta group by 1,2 union ALL
select 'CMD DESPACHO FARMACOS',to_char(fecha_despacho::Date, 'YYYY-MM'),count(*) from mis.despacho_de_farmaco group by 1,2
) as a order by 1,2;