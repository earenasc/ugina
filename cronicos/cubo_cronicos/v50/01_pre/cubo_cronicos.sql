--select * from rem_sp.cubo_patologias_cronicas_paso01();
DROP TABLE IF EXISTS patologias_cronicas_all_01;
CREATE TEMP TABLE patologias_cronicas_all_01
(
  nif character varying(10),
  ciap character varying(60),
  especiali character varying(10),
  fecha timestamp without time zone,
  fechalta timestamp without time zone,
  patologia_cronica character varying(20),
  q smallint,
  medico character varying(10)
);
--CARDIOVASCULAR
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_hta'::varchar(20) as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('K86','K87');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_dm2'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	  where a.ciap in ('T89','T90');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_dlp'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a where a.ciap in ('T93');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_obe'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('T82');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_tb'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('P17');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_erc'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('U99');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_inf'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('k75','K76');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_ecv'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('K92');
	--INSUFICIENCIA RENAL CRONICA
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_irc'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('U99')
	   and a.descripcio2 @@ 'fall&renal|enfermed&renal|insuficient&renal';

	 --INSUFICIENCIA CARDIACA
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_insucar'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('K77');
	-- INFARTO AGUDO AL MIOCARDIO
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_iam'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('K75');
	-- ACCIDENTE VASCULAR ENCEFÁLICO
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_ave'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('K90');
	--FIBRILACION AURICULAR
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_fibau'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('K78');
	--DAÑO HEPÁTICO
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_dhepa'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('D97');
	--ULCERA VARICOSA
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_ulcera'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('S97');

	--RETINOPATIA DIABETICA
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_retindm'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('F83');
	--UROPATIA
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_uropat'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where a.descripcio2 @@ 'uropat&obstruct';
	--TVP TROMBOSIS VENOSA PRODUNDA
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'cv_tvp'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where a.ciap in ('K94');


--OTROAS CRONICAS ( RESPIRATORIO, MUSCULOESQUELETICO, NEURO)
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'resp_sbor'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where a.ciap in ('R91')
	  and a.descripcio2 @@ 'sbor';

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'resp_asma'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where a.ciap in ('R96')
	  and a.descripcio2 @@ 'asma';

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'resp_epoc'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where a.ciap in ('R95')
	  and a.descripcio2 @@ 'epoc';

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'otro_pali'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where a.ciap in ('A28')
	  and a.descripcio2 @@ 'paliat';

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'otro_prem'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where a.ciap in ('A93','A97')
	  and a.descripcio2 @@ 'prematur';

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'neuro_fibq'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where a.ciap in ('T99')
	  and a.descripcio2 @@ 'fibrosis&quistic';

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'neuro_epil'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where a.ciap in ('N88')
	  and a.descripcio2 @@ 'epilep|epilepsi';

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'neuro_park'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where a.ciap in ('N89')
	  and a.descripcio2 @@ 'parkins|parkins';

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'otro_glau'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where a.ciap in ('P93');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'otro_hipo'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where a.ciap in ('T86')
	  and a.descripcio2 @@ 'hipotd|hipotiroid|hipotir';

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'otro_artro'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where a.ciap in ('L89','L90');

--artritis reumatoide
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'otro_artrir'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where a.ciap in ('L88');

insert into patologias_cronicas_all_01
	select  nif,'OXD'::Varchar as ciap,a.especiali
		,fecha,NULL::TIMESTAMP fechalta,'resp_oxd'::varchar(20)  as patologia_cronica,1::smallint q, medico_id
	  from admomi.iddcop as a
		  where (
			(wproto in (194,290) and wnumero=3 and campo='16') 			or
			(wproto=373 and wnumero=3 and campo='06') 				or
			(wproto IN (333,337,371) and wnumero IN (202,342) and campo='1')
			);

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'neuro_postr'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	where ((a.descripcio2 @@ 'postr|dependient&postr') or descripcio like '%POSTRADO%')
	  and a.ciap in ('A28');

--SALUD MENTAL
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_vi_gen'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where 	(a.descripcio2 @@ 'vif|vif&famili|violenci&parej|violenci&gener|violenci&intrafamili') 	or
		(a.descripcio2 @@ 'violenci&parej' and a.ciap='Z25')					or
		(a.descripcio2 @@ 'abus&emocional|problem&parej' and a.ciap='Z12')			or
		(a.descripcio2 @@ 'abandon&parej|problem&parej' and a.ciap='Z16');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_vi_ad'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where 	(a.descripcio2 @@ 'violenci&adult&mayor') 						or
		(a.descripcio2 @@ 'violenci&adult&mayor' and a.ciap='Z25');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_vi_malinf'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where 	(a.descripcio2 @@ 'maltrat&infantil|maltrat&escol|vif&niñ|vif&nin|bullyng') 		or
		(a.descripcio2 @@ 'infantil' and a.ciap='Z25');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_vi_absex'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where 	((a.descripcio2 @@ 'abus&sexual') and a.descripcio not like '%SOSP%') 		or
		(a.descripcio2 @@ 'abus&sexual' and a.ciap='Z25');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_vi_absex'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where 	((a.descripcio2 @@ 'abus&sexual') and a.descripcio not like '%SOSP%') 		or
		(a.descripcio2 @@ 'abus&sexual' and a.ciap='Z25');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_oh'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where (a.descripcio2 @@ 'cronic' and a.ciap='P15');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_policons'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where 	(a.descripcio2 @@ 'policonsum');

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_droga'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where 	(a.descripcio2 @@ 'consum&drog|drog|past&bas|cocain|marihuan|consum&sustanci');
		--trastornos de ansiedad

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_ansi'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where 	(a.descripcio2 @@ 'ansied|panic|angusti' and a.ciap='P74');
		--alzhaimer

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_alzh'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where 	(a.descripcio2 @@ 'demenci' and a.ciap='P70')				or
		(a.descripcio2 @@ 'alzheim');
		--esquizofrenia

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_eqz'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where 	(a.descripcio2 @@ 'eqz|esquizofreni|esquizafect' and a.ciap in ('P72','P73') );
		--trastorno alimenticio

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_alim'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where 	(a.descripcio2 @@ 'trast&aliment|trastorn&aliment|trastorn&alimentari|trast&alimentari|trast&alimentacion' and a.ciap in ('P11','P86') )	or
		(a.descripcio2 @@ 'anorexi|bulimi' );
		/*TRASTORNO HIPERCINÉTICOS, DE LA ACTIVIDAD Y DE LA ATENCION*/

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_thda'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('P21','P81');
		/*TRASTORNOS EMOCIONALES Y DEL COMPORTAMIENTO DE LA INFANCIA Y ADOLESCENCIA*/

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_tecia'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('P22','P81');
		/*TRASTORNO DE PERSONALIDAD*/

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_tpers'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('T80');
		/*RETRASO MENTAL*/

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_retme'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('T85');
		/*TRASTORNO GENERALIZADOS DEL DESARROLLO*/

insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_tdesa'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('P99');
	--DEPRESION
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'sm_depre'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in ('P76');

--OTRAS CRÓNICAS GENERALES (EDOI 2 Y 3 O LISTA DE CRONICOS acg)
/*
insert into patologias_cronicas_all_01
	select  a.nif,a.ciap,a.especiali
		,a.fecha,fechalta,'otro_cronic'::varchar(20)  as patologia_cronica,1::smallint q, medico
	  from admomi.iddncu as a
	 where a.ciap in (

			with consulta as (
					select codigo from admomi.iddcin where edoi in ('3')
					union
					select distinct ciap as codigo from acg_base.ciap_cronicos_ssmso_mediq
					)
					select a.codigo from consulta as a where a.codigo not in (select distinct ciap from patologias_cronicas_all_01)
			);
*/
create index m_patologias_cv_01 on patologias_cronicas_all_01 using btree(nif,ciap,especiali);
create index m_patologias_cv_02 on patologias_cronicas_all_01 using btree(patologia_cronica);
create index m_patologias_cv_03 on patologias_cronicas_all_01 using btree(ciap);
create index m_patologias_cv_04 on patologias_cronicas_all_01 using btree(nif);

update patologias_cronicas_all_01 set medico=btrim(upper(medico));

drop table if exists patologias_cronicas_all_02;
create temp table patologias_cronicas_all_02 as
select  distinct
	nif
	,(select b.nif as nif2 from admomi.pacientes_validos as b where b.nif2=a.nif limit 1)
	,a.ciap,a.especiali
	,a.fecha
	,a.fecha::Date as fecha2
	,a.fechalta
	,a.fechalta::date as fechalta2
	,a.patologia_cronica,a.q
	,a.medico
	,(select b.id from admomi.iddmed as b where b.medico=a.medico and b.id is not null limit 1) as clinico_id
  from patologias_cronicas_all_01 as a;
create index patologias_cronicas_all_02_i_01 on patologias_cronicas_all_02 using btree(nif2);
create index patologias_cronicas_all_02_i_02 on patologias_cronicas_all_02 using btree(patologia_cronica);
create index patologias_cronicas_all_02_i_03 on patologias_cronicas_all_02 using btree(nif2,patologia_cronica);

--long
drop table if exists 	rem_sp.patologias_cronicas_all_long;
create table    	rem_sp.patologias_cronicas_all_long as
select distinct * from  patologias_cronicas_all_02;
create index patologias_cronicas_all_long_i_01 on rem_sp.patologias_cronicas_all_long using btree(nif2,patologia_cronica,fecha2);
create index patologias_cronicas_all_long_i_02 on rem_sp.patologias_cronicas_all_long using btree(nif2,patologia_cronica);
create index patologias_cronicas_all_long_i_03 on rem_sp.patologias_cronicas_all_long using btree(nif2,fecha2,clinico_id,patologia_cronica);
create index patologias_cronicas_all_long_i_04 on rem_sp.patologias_cronicas_all_long using btree(nif2, fecha2);


--TRANSPUESTA
drop table if exists patologias_cronicas_all_03;
create temp table patologias_cronicas_all_03 as
select distinct nif2 from patologias_cronicas_all_02 where nif2 is not null order by 1;

drop table if exists patologias_cronicas_all_04; --select * from patologias_cronicas_all_04 limit 100
create temp table patologias_cronicas_all_04 as
select distinct a.nif2 as nif3
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_hta' limit 1),0) 	as hta		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_dm2' limit 1),0) 	as dm		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_dlp' limit 1),0) 	as dl		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_obe' limit 1),0) 	as ob		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_tb' limit 1),0) 	as tb   	--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_erc' limit 1),0) 	as erc		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_inf' limit 1),0) 	as inf		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_ecv' limit 1),0) 	as ecv		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_ave' limit 1),0)	as cv_ave
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_dhepa' limit 1),0) 	as cv_dhepa
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_dlp' limit 1),0) 	as cv_dlp
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_dm2' limit 1),0) 	as cv_dm2
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_ecv' limit 1),0) 	as cv_ecv
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_erc' limit 1),0) 	as cv_erc
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_fibau' limit 1),0) 	as cv_fibau
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_hta' limit 1),0) 	as cv_hta
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_iam' limit 1),0) 	as cv_iam
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_inf' limit 1),0) 		as cv_inf
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_insucar' limit 1),0) 	as cv_insucar
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_irc' limit 1),0) as cv_irc
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_obe' limit 1),0) as cv_obe
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_retindm' limit 1),0) as cv_retindm
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_tb' limit 1),0) as cv_tb
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_tvp' limit 1),0) as cv_tvp
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_ulcera' limit 1),0) as cv_ulcera
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='cv_uropat' limit 1),0) as cv_uropat
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='otro_artro' limit 1),0) as otro_artro
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='neuro_epil' limit 1),0) as neuro_epil
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='neuro_fibq' limit 1),0) as neuro_fibq
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='neuro_postr' limit 1),0) as neuro_postr
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='otro_artrir' limit 1),0) as otro_artrir
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='otro_hipo' limit 1),0) as otro_hipo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='otro_pali' limit 1),0) as otro_pali
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='otro_prem' limit 1),0) as otro_prem
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='resp_asma' limit 1),0) as resp_asma
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='resp_epoc' limit 1),0) as resp_epoc
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='resp_oxd' limit 1),0) as resp_oxd
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='resp_sbor' limit 1),0) as resp_sbor
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_alim' limit 1),0) as sm_alim
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_alzh' limit 1),0) as sm_alzh
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_ansi' limit 1),0) as sm_ansi
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_depre' limit 1),0) as sm_depre
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_droga' limit 1),0) as sm_droga
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_eqz' limit 1),0) as sm_eqz
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_oh' limit 1),0) as sm_oh
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_policons' limit 1),0) as sm_policons
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_retme' limit 1),0) as sm_retme
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_tdesa' limit 1),0) as sm_tdesa
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_tecia' limit 1),0) as sm_tecia
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_thda' limit 1),0) as sm_thda
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_tpers' limit 1),0) as sm_tpers
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_vi_absex' limit 1),0) as sm_vi_absex
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_vi_ad' limit 1),0) as sm_vi_ad
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_vi_gen' limit 1),0) as sm_vi_gen
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and b.patologia_cronica='sm_vi_malinf' limit 1),0) as sm_vi_malinf
from patologias_cronicas_all_03 as a;

drop table if exists patologias_cronicas_all_03_b;
create temp table patologias_cronicas_all_03_b as
select distinct nif2,fecha2,clinico_id
  from rem_sp.patologias_cronicas_all_long
 where nif2 is not null or fecha2 is not null or clinico_id is not null;

drop table if exists patologias_cronicas_all_03_c;
create temp table patologias_cronicas_all_03_c as
select distinct nif2,fecha2,fechalta2,clinico_id
  from rem_sp.patologias_cronicas_all_long
 where nif2 is not null or fecha2 is not null or clinico_id is not null;


--TABLA DE PATOLOGIA Y CLINICO QUE CREÓ EL DG
drop table if exists 	rem_sp.patologias_cronicas_all_transpuesta_b;
create 	table 		rem_sp.patologias_cronicas_all_transpuesta_b as
select distinct a.nif2 as nif,fecha2 as fecha,clinico_id as medico_id
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_hta' limit 1),0) 	as hta		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_dm2' limit 1),0) 	as dm		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_dlp' limit 1),0) 	as dl		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_obe' limit 1),0) 	as ob		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_tb' limit 1),0) 	as tb   	--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_erc' limit 1),0) 	as erc		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_inf' limit 1),0) 	as inf		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_ecv' limit 1),0) 	as ecv		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_ave' limit 1),0)	as cv_ave
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_dhepa' limit 1),0) 	as cv_dhepa
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_dlp' limit 1),0) 	as cv_dlp
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_dm2' limit 1),0) 	as cv_dm2
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_ecv' limit 1),0) 	as cv_ecv
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_erc' limit 1),0) 	as cv_erc
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_fibau' limit 1),0) 	as cv_fibau
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_hta' limit 1),0) 	as cv_hta
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_iam' limit 1),0) 	as cv_iam
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_inf' limit 1),0) 	as cv_inf
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_insucar' limit 1),0) 	as cv_insucar
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_irc' limit 1),0) 	as cv_irc
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_obe' limit 1),0) 	as cv_obe
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_retindm' limit 1),0) 	as cv_retindm
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_tb' limit 1),0) 	as cv_tb
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_tvp' limit 1),0) 	as cv_tvp
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_ulcera' limit 1),0) 	as cv_ulcera
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_uropat' limit 1),0) 	as cv_uropat
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='otro_artro' limit 1),0) 	as otro_artro
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='neuro_epil' limit 1),0) 	as neuro_epil
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='neuro_fibq' limit 1),0)	as neuro_fibq
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='neuro_postr' limit 1),0) 	as neuro_postr
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='otro_artrir' limit 1),0)  as otro_artrir
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='otro_hipo' limit 1),0) 	as otro_hipo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='otro_pali' limit 1),0) 	as otro_pali
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='otro_prem' limit 1),0) 	as otro_prem
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='resp_asma' limit 1),0) 	as resp_asma
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='resp_epoc' limit 1),0) 	as resp_epoc
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='resp_oxd' limit 1),0) 	as resp_oxd
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='resp_sbor' limit 1),0) 	as resp_sbor
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_alim' limit 1),0) 	as sm_alim
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_alzh' limit 1),0) 	as sm_alzh
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_ansi' limit 1),0) 	as sm_ansi
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_depre' limit 1),0) 	as sm_depre
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_droga' limit 1),0) 	as sm_droga
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_eqz' limit 1),0) 	as sm_eqz
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_oh' limit 1),0) 	as sm_oh
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_policons' limit 1),0)  as sm_policons
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_retme' limit 1),0) 	as sm_retme
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_tdesa' limit 1),0) 	as sm_tdesa
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_tecia' limit 1),0) 	as sm_tecia
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_thda' limit 1),0) 	as sm_thda
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_tpers' limit 1),0) 	as sm_tpers
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_vi_absex' limit 1),0)  as sm_vi_absex
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_vi_ad' limit 1),0) 	as sm_vi_ad
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_vi_gen' limit 1),0) 	as sm_vi_gen
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_vi_malinf' limit 1),0) as sm_vi_malinf
from patologias_cronicas_all_03_b as a;

--TABLA DE PATOLOGIAS FECHAI FECHAF
drop table if exists 	rem_sp.patologias_cronicas_all_transpuesta_c;
create 	table 		rem_sp.patologias_cronicas_all_transpuesta_c as  --select * from rem_sp.patologias_cronicas_all_transpuesta_c where nif=110 order by fecha limit 10
select distinct a.nif2 as nif,a.fecha2 as fecha,a.fechalta2,clinico_id as medico_id
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta
and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_hta' limit 1),0) 	as hta		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_dm2' limit 1),0) 	as dm		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_dlp' limit 1),0) 	as dl		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_obe' limit 1),0) 	as ob		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_tb' limit 1),0) 	as tb   	--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_erc' limit 1),0) 	as erc		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_inf' limit 1),0) 	as inf		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_ecv' limit 1),0) 	as ecv		--cardio antiguo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_ave' limit 1),0)	as cv_ave
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_dhepa' limit 1),0) 	as cv_dhepa
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_dlp' limit 1),0) 	as cv_dlp
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_dm2' limit 1),0) 	as cv_dm2
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_ecv' limit 1),0) 	as cv_ecv
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_erc' limit 1),0) 	as cv_erc
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_fibau' limit 1),0) 	as cv_fibau
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_hta' limit 1),0) 	as cv_hta
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_iam' limit 1),0) 	as cv_iam
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_inf' limit 1),0) 	as cv_inf
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_insucar' limit 1),0) 	as cv_insucar
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_irc' limit 1),0) 	as cv_irc
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_obe' limit 1),0) 	as cv_obe
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_retindm' limit 1),0) 	as cv_retindm
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_tb' limit 1),0) 	as cv_tb
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_tvp' limit 1),0) 	as cv_tvp
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_ulcera' limit 1),0) 	as cv_ulcera
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='cv_uropat' limit 1),0) 	as cv_uropat
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='otro_artro' limit 1),0) 	as otro_artro
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='neuro_epil' limit 1),0) 	as neuro_epil
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='neuro_fibq' limit 1),0)	as neuro_fibq
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='neuro_postr' limit 1),0) 	as neuro_postr
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='otro_artrir' limit 1),0)  as otro_artrir
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='otro_hipo' limit 1),0) 	as otro_hipo
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='otro_pali' limit 1),0) 	as otro_pali
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='otro_prem' limit 1),0) 	as otro_prem
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='resp_asma' limit 1),0) 	as resp_asma
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='resp_epoc' limit 1),0) 	as resp_epoc
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='resp_oxd' limit 1),0) 	as resp_oxd
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='resp_sbor' limit 1),0) 	as resp_sbor
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_alim' limit 1),0) 	as sm_alim
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_alzh' limit 1),0) 	as sm_alzh
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_ansi' limit 1),0) 	as sm_ansi
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_depre' limit 1),0) 	as sm_depre
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_droga' limit 1),0) 	as sm_droga
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_eqz' limit 1),0) 	as sm_eqz
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_oh' limit 1),0) 	as sm_oh
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_policons' limit 1),0)  as sm_policons
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_retme' limit 1),0) 	as sm_retme
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_tdesa' limit 1),0) 	as sm_tdesa
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_tecia' limit 1),0) 	as sm_tecia
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_thda' limit 1),0) 	as sm_thda
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_tpers' limit 1),0) 	as sm_tpers
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_vi_absex' limit 1),0)  as sm_vi_absex
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_vi_ad' limit 1),0) 	as sm_vi_ad
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_vi_gen' limit 1),0) 	as sm_vi_gen
,coalesce((select 1::smallint from rem_sp.patologias_cronicas_all_long as b where b.nif2=a.nif2 and a.fecha2=b.fecha2 and fechalta2=b.fechalta and a.clinico_id=b.clinico_id  and b.patologia_cronica='sm_vi_malinf' limit 1),0) as sm_vi_malinf
from patologias_cronicas_all_03_c as a;

delete from rem_sp.patologias_cronicas_all_transpuesta_b where
cv_ave+
cv_dhepa+
cv_dlp+
cv_dm2+
cv_ecv+
cv_erc+
cv_fibau+
cv_hta+
cv_iam+
cv_inf+
cv_insucar+
cv_irc+
cv_obe+
cv_retindm+
cv_tb+
cv_tvp+
cv_ulcera+
cv_uropat+
otro_artro+
neuro_epil+
neuro_fibq+
neuro_postr+
otro_artrir+
otro_hipo+
otro_pali+
otro_prem+
resp_asma+
resp_epoc+
resp_oxd+
resp_sbor+
sm_alim+
sm_alzh+
sm_ansi+
sm_depre+
sm_droga+
sm_eqz+
sm_oh+
sm_policons+
sm_retme+
sm_tdesa+
sm_tecia+
sm_thda+
sm_tpers+
sm_vi_absex+
sm_vi_ad+
sm_vi_gen+
sm_vi_malinf+
dm+
dl+
hta+
ob+
tb+
erc+
inf+
ecv<1;

drop table if exists 	rem_sp.patologias_cronicas_all_transpuesta;
create table    	rem_sp.patologias_cronicas_all_transpuesta as
select distinct *,
cv_ave+
cv_dhepa+
cv_dlp+
cv_dm2+
cv_ecv+
cv_erc+
cv_fibau+
cv_hta+
cv_iam+
cv_inf+
cv_insucar+
cv_irc+
cv_obe+
cv_retindm+
cv_tb+
cv_tvp+
cv_ulcera+
cv_uropat+
otro_artro+
neuro_epil+
neuro_fibq+
neuro_postr+
otro_artrir+
otro_hipo+
otro_pali+
otro_prem+
resp_asma+
resp_epoc+
resp_oxd+
resp_sbor+
sm_alim+
sm_alzh+
sm_ansi+
sm_depre+
sm_droga+
sm_eqz+
sm_oh+
sm_policons+
sm_retme+
sm_tdesa+
sm_tecia+
sm_thda+
sm_tpers+
sm_vi_absex+
sm_vi_ad+
sm_vi_gen+
sm_vi_malinf as q_condiciones_cronicas
from patologias_cronicas_all_04
where
cv_ave+
cv_dhepa+
cv_dlp+
cv_dm2+
cv_ecv+
cv_erc+
cv_fibau+
cv_hta+
cv_iam+
cv_inf+
cv_insucar+
cv_irc+
cv_obe+
cv_retindm+
cv_tb+
cv_tvp+
cv_ulcera+
cv_uropat+
otro_artro+
neuro_epil+
neuro_fibq+
neuro_postr+
otro_artrir+
otro_hipo+
otro_pali+
otro_prem+
resp_asma+
resp_epoc+
resp_oxd+
resp_sbor+
sm_alim+
sm_alzh+
sm_ansi+
sm_depre+
sm_droga+
sm_eqz+
sm_oh+
sm_policons+
sm_retme+
sm_tdesa+
sm_tecia+
sm_thda+
sm_tpers+
sm_vi_absex+
sm_vi_ad+
sm_vi_gen+
sm_vi_malinf+
dm+
dl+
hta+
ob+
tb+
erc+
inf+
ecv>=1;
create index patologias_cronicas_all_transpuesta_i_01 on rem_sp.patologias_cronicas_all_transpuesta using btree(nif3) tablespace tb_index;

drop table if exists 	patologias_cronicas_all_05;
create temp table    	patologias_cronicas_all_05 as
select distinct nif2,nif,ciap,especiali from  patologias_cronicas_all_02;
create index patologias_cronicas_all_05_i_01 on patologias_cronicas_all_05 using btree(nif,ciap,especiali)   tablespace tb_index;

drop table if exists 	rem_sp.patologias_cronicas_all_registro;
create table    	rem_sp.patologias_cronicas_all_registro as
select a.nif2 as nif
	,b.fecha2
	,b.clinico_id
  from patologias_cronicas_all_05 as a
	,admomi.iddcor as b,admomi.iddncu as c
  where a.nif2=b.nif
    and a.especiali=b.especiali
    and a.ciap=c.ciap
    and a.nif2=c.nif2
    and a.especiali=c.especiali
    group by 1,2,3;

--CREADOR DE EPISODIO
drop table if exists patologias_cronicas_all_05;
create temp table patologias_cronicas_all_05 as
with consulta as (
		select nif2,patologia_cronica,min(fecha::Date) as minfecha
		 from rem_sp.patologias_cronicas_all_long
		group by 1,2
		)
		select a.nif2
			,a.patologia_cronica
			,a.minfecha as fecha
			,(select b.clinico_id
			    from rem_sp.patologias_cronicas_all_long as b
			    where a.nif2=b.nif2
			      and a.patologia_cronica=b.patologia_cronica
			      and a.minfecha=b.fecha2
			      order by b.fecha limit 1
			 )
		  from consulta as a;

GRANT SELECT ON TABLE rem_sp.acg_rub_ancora_2015 TO jesoto;
GRANT SELECT ON TABLE rem_sp.acg_rub_ancora_2015 TO consultaomi;
GRANT SELECT ON TABLE rem_sp.acg_rub_ancora_2015 TO ugina;
GRANT SELECT ON TABLE rem_sp.acg_rub_ancora_2015 TO ehenriquez;
GRANT SELECT ON TABLE rem_sp.acg_rub_ancora_2015 TO consulta;

GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_long TO jesoto;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_long TO consultaomi;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_long TO ugina;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_long TO ehenriquez;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_long TO consulta;

GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_registro TO jesoto;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_registro TO consultaomi;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_registro TO ugina;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_registro TO ehenriquez;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_registro TO consulta;

GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_transpuesta TO jesoto;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_transpuesta TO consultaomi;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_transpuesta TO ugina;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_transpuesta TO ehenriquez;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_transpuesta TO consulta;

GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_transpuesta_b TO jesoto;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_transpuesta_b TO consultaomi;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_transpuesta_b TO ugina;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_transpuesta_b TO ehenriquez;
GRANT SELECT ON TABLE rem_sp.patologias_cronicas_all_transpuesta_b TO consulta;





------------------------------------------------------------------------------------------------------------------------------
--
-- PASO 2 CUBO CV
--
------------------------------------------------------------------------------------------------------------------------------


-- CUBO CARDIOVASCULAR
------------------------------------------------------------------------------------------------------------------------------------------
--autor			: EDUARDO ARENAS C.
--modificaciones	:
	--20150923 -- 	: Tranforma cubo antiguo que calcula PCV al dia, en un cubo multidimencional.		(v06.03)
	--20151120 --   : earenas, jesoto ; se corrige poblacion cv, se corrige pie dm 							(V06.04)
	--20151215 --   : se corrige cobertura de acuerdo a atenciones cardiovasculares con protocolo        	(v06.05)
	--20160107 --   : earenas jesoto, mapeo de datos col-ldl, fondo de ojos, estatinas, aspirina,
	--			ieca ara, insulina, curaciones 							(v08)
	--20160311 --	: earenas, mantencion tabla agenda.cubo_agenda						(v11)
	--20160322 	: earenas, agregar microalbuminuria							(v12)
	--20160711	: earenas, cambian varios indices a tipo btree mas rapidos en general
	--                         - se calcula outlier para presion arterial muchos pacientes con valores
	--                             cero o uno lo cual lo hacía ver como compensados                      	(v14)
	--20160813	: earenas, varias mejoras de performance						(v16)
	--20160823  	: earenas, corrección mal calculo riesgo cardivascular					(v18)
	--20170414	: earenas, corrección duplicidad por ejecutor						(v19)
	--20170415	: earenas , correción medico_id integer							(v20)
	--20170416	: earenas , peso, talla , imc, circunferencia cintura					(v21)
	--20170418	: earenas , fecha::date en ezamenes							(v22)
	--20170430  	: earenas, corrección presiones arteriales						(v23)
	--20180108  	: earenas, corrección atenci{on con protocolo CV           				(v24)
	--20180201	: earenas, protocolo control cv se agregan protocolos CDI ingreso y control
	--20180207	: earenas, tranforma cubo cardiovascular en cubo de enfermedades crónicas		(v28)
	--20180423	: earenas, agregar datos de protocolo multimorvilidad					(v29)
	--20190611	: earenas, agrega datos Rep P03 ASMA EPOC SBOR, P06 Salud mental, fecha control cv,
	--			 cdi, respiratorio, salud mental para calcular IAAPS 12 y 13			(v30)
	--20180722	: earenas, se agrega salufam familiar (registrado en cualquier integrante de la familia	(v31)
	--20180903	: earenas, se agrega fecha de apertura episodio -63 cdi
				-- correccion tabla de hechos iddpag cop coe					(v32)
	--20180928	: earenas, se agregan tablas de diagnostico de patologia a tabla de hechos		(v33)
	--20181024	: earenas, fecha_proximo_examene,canales_de_comunicación con usuario			(v34)
	--20181214	: earenas, se agrega rub2013 al 2017							(v41)
	--20190218	: earenas, se corrige mapeo pie dm protocolos nuevos					(v42)
	--20190226	: earenas, se dato de ingreso cdi protocolo nuevo, s mapea nuevo protocolo control_con control_en					(v43)
	--20200226	: earenas, se agrega el dato de riesgo alto o mediando dependiendo de lo que considere el clínico					(v48)
------------------------------------------------------------------------------------------------------------------------------------------
--@@@ DIMENSIONES NUEVAS
------------------------------------------------------------------------------------------------------------------------------------------
/*
drop table if exists pacientes_validos;
create temp table pacientes_validos as
select generate_series::varchar as nif,generate_series as nif2 from (select generate_series(1,200000)) as a;
create index pacientes_validos_i_01 on pacientes_validos using btree(nif);
create index pacientes_validos_i_02 on pacientes_validos using btree(nif2);
create index pacientes_validos_i_03 on pacientes_validos using btree(nif,nif2);


drop table if exists rem_sp_h04_base.pacientes_fallecidos_o_particulares;
create table rem_sp_h04_base.pacientes_fallecidos_o_particulares as
select distinct a.rut,(select nif2 from admomi.iddpacpa3 as b where a.rut=b.tis limit 1) as nif
  from
	(
	select distinct substr(field_000,6,9)::integer::Text||'-'||substr(field_000,15,1) as rut
	  from fonasa_etl.rechazo20150930
	 where   field_000 like '%M01902%'   --fallecidos
	)as a ;
create index pacientes_fallecidos_i_01 on rem_sp_h04_base.pacientes_fallecidos_o_particulares using btree (rut);
create index pacientes_fallecidos_i_02 on rem_sp_h04_base.pacientes_fallecidos_o_particulares using btree (nif,rut);
create index pacientes_fallecidos_i_03 on rem_sp_h04_base.pacientes_fallecidos_o_particulares using btree (nif);
*/
----------------------------------------------------------------------------------
-- CREA TABLA patologias
----------------------------------------------------------------------------------

drop table if exists m_solo_cv_00;
create temp table m_solo_cv_00 as
select nif2 nif,min(fecha)::date as fecha
  from admomi.iddncu as a
 where ciap in ('K86','K87','T89','T90','T93')
   group by 1;
create index m_solo_cv_00_i_01 on m_solo_cv_00 using btree(nif)  tablespace tb_index;
create index m_solo_cv_00_i_02 on m_solo_cv_00 using btree(nif,fecha) tablespace tb_index;
create index m_solo_cv_00_i_03 on m_solo_cv_00 using btree(fecha) tablespace tb_index;

drop table if exists rem_sp_h04_base.m_solo_cv;
create table rem_sp_h04_base.m_solo_cv as
	select distinct a.nif2 as nif
	  from admomi.iddncu as a
	 where a.ciap in ('K86','K87','T89','T90','T93')
	    or a.nif2 in (select nif3 from rem_sp.patologias_cronicas_all_transpuesta where nif3 is not null)
	    or nif2   in (select b.nif2 from admomi.iddpacpa3 as b where edad_en_agnios('20040101',b.nacimiento)>=15 and b.nif2 is not null)
	    ;
create index m_solo_cv_i_01 on rem_sp_h04_base.m_solo_cv using btree(nif) tablespace tb_index;


drop table if exists m_patologias_cv;
create temp table m_patologias_cv as
select   distinct
	 a.nif2 as nif
	,a.fecha::date as fecha
	,case when a.ciap in ('K86','K87')	then 1::smallint else 0 end as hta
	,case when a.ciap in ('T89','T90')  	then 1::smallint else 0 end as dm
	,case when a.ciap in ('T93')	  	then 1::smallint else 0 end as dl
	,case when a.ciap='T82' 		then 1::smallint else 0 end as ob
	,case when a.ciap='P17' 		then 1::smallint else 0 end as tb
	,case when a.ciap='U99' 		then 1::smallint else 0 end as erc
	,case when a.ciap in ('K75','K76') 	then 1::smallint else 0 end as inf
	,case when a.ciap='K92' 		then 1::smallint else 0 end as ecv
	,(select d.id as medico_id from admomi.iddmed as d where d.medico=a.medico limit 1) as medico_id
  from admomi.iddncu as a,rem_sp_h04_base.m_solo_cv as c
 where a.nif2 = c.nif and (a.nif2 is not null or a.fecha is not null);
 create index m_patologias_cv_i_001 on m_patologias_cv using btree(nif) tablespace tb_index;

drop table if exists rem_sp_h04_base.m_patologias_cv; --select * from rem_sp_h04_base.m_patologias_cv limit 10
create table rem_sp_h04_base.m_patologias_cv as
select distinct * from rem_sp.patologias_cronicas_all_transpuesta_b;
/*
drop table if exists rem_sp_h04_base.m_patologias_cv; --select * from rem_sp_h04_base.m_patologias_cv limit 10
create table rem_sp_h04_base.m_patologias_cv as
with consulta as
		(
		select distinct a.*
		  from m_patologias_cv as a , rem_sp_h04_base.m_solo_cv as b
		 where a.nif = b.nif
		)
		select *
		  from consulta
   --and (a.hta=1 or a.dm=1 or a.dl=1 or a.ob=1 or a.tb=1 or a.erc=1 or a.inf=1 or a.ecv=1)
   ;
*/
drop index if exists rem_sp_h04_base.m_patologias_cv_i_01;create index m_patologias_cv_i_01 on rem_sp_h04_base.m_patologias_cv using btree(nif) tablespace tb_index;
drop index if exists rem_sp_h04_base.m_patologias_cv_i_02;create index m_patologias_cv_i_02 on rem_sp_h04_base.m_patologias_cv using btree(nif,fecha) tablespace tb_index;
drop index if exists rem_sp_h04_base.m_patologias_cv_i_03;create index m_patologias_cv_i_03 on rem_sp_h04_base.m_patologias_cv using btree(dm) tablespace tb_index;
drop index if exists rem_sp_h04_base.m_patologias_cv_i_04;create index m_patologias_cv_i_04 on rem_sp_h04_base.m_patologias_cv using btree(hta) tablespace tb_index;
drop index if exists rem_sp_h04_base.m_patologias_cv_i_05;create index m_patologias_cv_i_05 on rem_sp_h04_base.m_patologias_cv using btree(dl) tablespace tb_index;
drop index if exists rem_sp_h04_base.m_patologias_cv_i_06;create index m_patologias_cv_i_06 on rem_sp_h04_base.m_patologias_cv using btree(nif,fecha,medico_id) tablespace tb_index;
drop index if exists rem_sp_h04_base.m_patologias_cv_i_07;create index m_patologias_cv_i_07 on rem_sp_h04_base.m_patologias_cv using btree(dm,hta,dl) tablespace tb_index;
drop index if exists rem_sp_h04_base.m_patologias_cv_i_08;create index m_patologias_cv_i_08 on rem_sp_h04_base.m_patologias_cv using btree(nif,fecha) tablespace tb_index;

----------------------------------------------------------------------------------
-- contactos con pacientes agenda, farmacos, rescates
----------------------------------------------------------------------------------

drop table if exists med_enf_nutri;
create temp table med_enf_nutri as
select distinct id as medico_id
	,case when med_estamento_desc ='MEDICO' 		then 1::smallint else 0 end as age_medico
	,case when med_estamento_desc ='ENFERMERA' 		then 1::smallint else 0 end as age_enfermera
	,case when med_estamento_desc ='NUTRICIONISTA' 		then 1::smallint else 0 end as age_nutricionista
	,case when med_estamento_desc ='KINESIOLOGO' 		then 1::smallint else 0 end as age_kine
	,case when med_estamento_desc ='PSICOLOGO/A' 		then 1::smallint else 0 end as age_psicologo
	,case when med_estamento_desc ='ASISTENTE SOCIAL'	then 1::smallint else 0 end as age_tsocial
  from admomi.iddmed
 where med_estamento_desc in ('MEDICO','ENFERMERA','NUTRICIONISTA','KINESIOLOGO','PSICOLOGO/A','ASISTENTE SOCIAL');
create index med_enf_nutri_i_01 on med_enf_nutri using btree(medico_id);
create index med_enf_nutri_i_02 on med_enf_nutri using btree(age_medico);
create index med_enf_nutri_i_03 on med_enf_nutri using btree(age_enfermera);
create index med_enf_nutri_i_04 on med_enf_nutri using btree(age_nutricionista);
create index med_enf_nutri_i_05 on med_enf_nutri using btree(medico_id,age_medico);
create index med_enf_nutri_i_06 on med_enf_nutri using btree(medico_id,age_enfermera);
create index med_enf_nutri_i_07 on med_enf_nutri using btree(medico_id,age_nutricionista);
create index med_enf_nutri_i_08 on med_enf_nutri using btree(medico_id,age_medico,age_enfermera,age_nutricionista);

create index med_enf_nutri_i_09 on med_enf_nutri using btree(medico_id,age_kine);
create index med_enf_nutri_i_10 on med_enf_nutri using btree(medico_id,age_psicologo);
create index med_enf_nutri_i_11 on med_enf_nutri using btree(medico_id,age_tsocial);

delete from med_enf_nutri where medico_id is null;
delete from med_enf_nutri where age_medico is null and age_enfermera is null and age_nutricionista is null;

drop table if exists age_med_enf_nut01;
create temp table age_med_enf_nut01 as
select   distinct
	 a.pac_nif as nif
	--,a.fecha
	,a.fecha2 as fecha
	,case when reasignado_med_id is not null then a.reasignado_med_id else a.med_id end as medico_id
  from biancora.cubo_agenda as a,med_enf_nutri as b
 where a.vino=1
   and pac_nif is not null
   and fecha2 is not null
   and (b.medico_id = a.med_id or a.reasignado_med_id = b.medico_id);
create index age_med_enf_nut01_i_01 on age_med_enf_nut01 using btree(medico_id);

drop table if exists rem_sp_h04_base.m_atenciones_cv;
create table rem_sp_h04_base.m_atenciones_cv as
select   distinct
	 a.nif
	,a.fecha
	,a.medico_id
	,sum(b.age_medico) 		as age_medico
	,sum(b.age_enfermera)		as age_enfermera
	,sum(b.age_nutricionista)	as age_nutricionista
	,sum(b.age_kine)		as age_kine
	,sum(b.age_psicologo)		as age_psicologo
	,sum(b.age_tsocial)		as age_tsocial
  from age_med_enf_nut01 as a left join med_enf_nutri as b on b.medico_id=a.medico_id
  group by 1,2,3;
create index m_atenciones_cv_i_01 on rem_sp_h04_base.m_atenciones_cv using btree(nif) tablespace tb_index;
create index m_atenciones_cv_i_02 on rem_sp_h04_base.m_atenciones_cv using btree(nif,fecha) tablespace tb_index;
create index m_atenciones_cv_i_04 on rem_sp_h04_base.m_atenciones_cv using btree(age_medico) tablespace tb_index;
create index m_atenciones_cv_i_05 on rem_sp_h04_base.m_atenciones_cv using btree(age_enfermera) tablespace tb_index;
create index m_atenciones_cv_i_06 on rem_sp_h04_base.m_atenciones_cv using btree(age_nutricionista) tablespace tb_index;
create index m_atenciones_cv_i_07 on rem_sp_h04_base.m_atenciones_cv using btree(medico_id) tablespace tb_index;
create index m_atenciones_cv_i_08 on rem_sp_h04_base.m_atenciones_cv using btree(nif,fecha,medico_id) tablespace tb_index;
create index m_atenciones_cv_i_09 on rem_sp_h04_base.m_atenciones_cv using btree(age_kine) tablespace tb_index;
create index m_atenciones_cv_i_10 on rem_sp_h04_base.m_atenciones_cv using btree(age_psicologo) tablespace tb_index;
create index m_atenciones_cv_i_11 on rem_sp_h04_base.m_atenciones_cv using btree(age_tsocial) tablespace tb_index;
delete from rem_sp_h04_base.m_atenciones_cv where nif is null or fecha is null;
----------------------------------------------------------------------------------
--Farmacos
----------------------------------------------------------------------------------

drop table if exists farma_cv;
create temp table farma_cv as
select distinct btrim(material) as codigo
  from admomi.iddmat
 where material in (
			--Sacarina (DM)
			 'G/557'
			--ACIDO ACETIL SALICILICO (ASPIRINA). (DM,HTA)
			,'F-1069'
			,'F-1297'
			,'689836'
			--Estatinas (atorvastatina, lovastatina, paravastatina). (DM, HTA, DL)
			,'F11909'  --atorvastatina
			,'F-7144'  --atorvastatina
			,'F-8493' --lovastatina
			--Antagonista del receptor angiotensina II (DM,HTA)
			,'F3021'		--LOSARTAN + HCT (SIMPERTEN - D 50 / 12,5 )
			--IECA (enalapril). (DM,HTA)
			,'F-8105'	--ENALAPRIL CM RANURADO 10 MG
			--Insulina. (DM)
			--,'153000'	--JERINGA INSULINA 1 ML
			--,'1U2713'	--JERINGA DES. INSULINA 1 ML (SSMSO) C/AGUJA 28G X 1/2
			--,'225-4542-0'	--NV-JERINGA DE INSULINA
			,'4383-B'	--INSULINA LENTA 100 UI/ML (FCO 10 ML)
			,'772459'	--INSULINA RETARDADA HORM 400UI
			,'772467'	--INSULINA HORM 400UI VIAL 10ML
			,'885665'	--INSULINA NPH BERNA 400UI 1
			--,'951178'	--INSULINA NOVO LENTE MC 40UI/ML
			,'B-1615'	--INSULINA CRISTALINA 100 UI/ML
			--,'EJ7009'	--NV-JERINGA DES. INSULINA 1ML AGUJA 28Gx1/2
			--Metformina. (DM)
			,'F-8438'	--METFORMINA 850MG (HIPOGLUCIN)
			--Sulfonilureas: Tolbutamina,Glibenclamida. (DM)
			,'F-7876'	--TOLBUTAMIDA 500MG
			,'F-2414'	--GLIBENCLAMIDA 5MG
			--Furosemida (HTA)
			,'F-1258'	--NV-FUROSEMIDA 20MG/2ML	N
			,'F-1261'	--FUROSEMIDA 40 MG CM	S
			,'F-6156'	--FUROSEMIDA 20MG/1ML AMP
			--Hidroclorotiazida (HTA)
			,'F-1491'	--HIDROCLOROTIAZIDA 50 MG
			-- Propanolol (HTA)
			,'F7224'	--PROPANOLOL 40 MG
			,'F-3357'	--PROPANOLOL CLORHIDRATO 1MG/1ML	S
			--Atenolol (HTA)
			,'672139'	--ATENOLOL NORMON 100MG 30 COMPR	N
			,'672303'	--ATENOLOL NORMON 100MG 60 COMPR	N
			,'672311'	--ATENOLOL NORMON 50MG 60 COMPR	N
			,'672329'	--ATENOLOL NORMON 50MG 30 COMPR	N
			,'903591'	--ATENOLOL MERCK 100MG 60	N
			,'904631'	--ATENOLOL MERCK 50MG 60 COMPRIM	N
			,'974790'	--ATENOLOL ALTER 100MG 30	N
			,'974808'	--ATENOLOL ALTER 100MG 60	N
			,'976050'	--ATENOLOL LEO 100MG 30	N
			,'976068'	--ATENOLOL LEO 100MG 60	N
			,'976092'	--ATENOLOL BOI 100MG 30	N
			,'976100'	--ATENOLOL BOI 100MG 60	N
			,'988063'	--ATENOLOL RATIOPHARM 100 100MG	N
			,'988071'	--ATENOLOL RATIOPHARM 100 100MG	N
			,'F-8329'	--ATENOLOL 50 MG	S
			--Espironolactona (HTA)
			,'F-8843'	--ESPIRONOLACTONA 25 MG	S
			,'FF5008'	--NV-ESPIRONOLACTONA
			--Nifedipino (HTA)
			,'F12851'
			,'665778'	--NIFEDIPINO RETARD RATIOPHAR	N
			,'665786'	--NIFEDIPINO RETARD RATIOPHAR	N
			,'665901'	--NIFEDIPINO RETARD BAYVIT 20MG	S
			,'973339'	--NIFEDIPINO RATIOPHARM 10MG 50	N
			,'974287'	--NIFEDIPINO JUSTE 10MG 50	N
			--metildopa
			,'22046'
			--valsartan (HTA) ARA
			,'F22214','F22215','F22216','F22217','F22218','F22219'
			);

create index farma_cv_i_01 on farma_cv using btree(codigo);

drop table if exists rem_sp_h04_base.farmacos_cv;
create table rem_sp_h04_base.farmacos_cv as
select distinct c.nif,a.ufecha::date as fecha
		,sum(case when a.codigo in ('G/557','F-1069','F-1297','689836','F11909','F-7144','F-8493','F3021','F-8105','153000'
				,'1U2713','225-4542-0','4383-B','772459','772467','885665','951178','B-1615','EJ7009'
				,'F-8438','F-7876','F-2414') then 1::smallint else 0::smallint 						end) as dm
		,sum(case when a.codigo in ('F-1069','F-1297','689836','F11909','F-7144','F-8493','F3021','F-8105','F-1258'
				,'F-1261','F-6156','F-1491','F7224','F-3357','672139','672303','672311','672329'
				,'903591','904631','974790','974808','976050','976068','976092','976100','988063'
				,'988071','F-8329','F-8843','FF5008','F12851','22046'
				,'F22214','F22215','F22216','F22217','F22218','F22219'
				,'665778','665786','665901','973339','974287'
				) then 1::smallint else 0::smallint 									end) as hta
		,sum(case when a.codigo in ('F11909','F-7144','F-8493') then 1::smallint else 0::smallint 				end) as dl
		,sum(case when a.codigo in ('F11909','F-7144','F-8493') then 1::smallint else 0::smallint				end) as estatina
		,sum(case when a.codigo in ('F-1069','F-1297','689836') then 1::smallint else 0::smallint				end) as aspirina
		,sum(case when a.codigo in ('976985','977694','979773','979880','981837','982843','F-8105'
					,'663237','663245','663252','664466','664474','664482','669499'
					,'669507','669846','669853','F-8493','F3021'
					,'F22214','F22215','F22216','F22217','F22218','F22219')	 then 1::smallint else 0::smallint	end) as ieca_ara
		,sum(case when a.codigo in ('153000','1U2713','225-4542-0','4383-B','772459'
					,'772467','885665'
					--,'951178' --NO VA ESTO SON GOTAS
					,'B-1615','EJ7009') then 1::smallint else 0::smallint		end) as insulina
from admomi.iddfm2 as a,farma_cv as b,rem_sp_h04_base.m_solo_cv as c
 where a.nif2 = c.nif
   and a.nif2 is not null
   and a.ufecha is not null
   and a.situacion='1'
   and a.codigo=b.codigo
   group by 1,2;
update rem_sp_h04_base.farmacos_cv set dm=1  		where dm>1;
update rem_sp_h04_base.farmacos_cv set hta=1 		where hta>1;
update rem_sp_h04_base.farmacos_cv set dl=1  		where dl>1;
update rem_sp_h04_base.farmacos_cv set estatina=1 	where estatina>1;
update rem_sp_h04_base.farmacos_cv set aspirina=1 	where aspirina>1;
update rem_sp_h04_base.farmacos_cv set ieca_ara=1 	where ieca_ara>1;
update rem_sp_h04_base.farmacos_cv set insulina=1 	where insulina>1;

delete from rem_sp_h04_base.farmacos_cv where dm=0 and hta=0 and dl=0 and estatina=0 and aspirina=0 and ieca_ara=0 and insulina=0;

create index farmacos_cv_i_01 on rem_sp_h04_base.farmacos_cv using btree (nif) tablespace tb_index;
create index farmacos_cv_i_02 on rem_sp_h04_base.farmacos_cv using btree (hta) tablespace tb_index;
create index farmacos_cv_i_03 on rem_sp_h04_base.farmacos_cv using btree (fecha) tablespace tb_index;
create index farmacos_cv_i_04 on rem_sp_h04_base.farmacos_cv using btree (nif,fecha,hta) tablespace tb_index;
create index farmacos_cv_i_06 on rem_sp_h04_base.farmacos_cv using btree (fecha) tablespace tb_index;
create index farmacos_cv_i_07 on rem_sp_h04_base.farmacos_cv using btree (nif,fecha,hta) tablespace tb_index;
create index farmacos_cv_i_08 on rem_sp_h04_base.farmacos_cv using btree (nif,fecha,dm) tablespace tb_index;
create index farmacos_cv_i_09 on rem_sp_h04_base.farmacos_cv using btree (nif,fecha,dl) tablespace tb_index;
create index farmacos_cv_i_10 on rem_sp_h04_base.farmacos_cv using btree (nif,fecha,aspirina) tablespace tb_index;
create index farmacos_cv_i_11 on rem_sp_h04_base.farmacos_cv using btree (nif,fecha,estatina) tablespace tb_index;
create index farmacos_cv_i_12 on rem_sp_h04_base.farmacos_cv using btree (nif,fecha,ieca_ara) tablespace tb_index;
create index farmacos_cv_i_13 on rem_sp_h04_base.farmacos_cv using btree (nif,fecha) tablespace tb_index;
create index farmacos_cv_i_14 on rem_sp_h04_base.farmacos_cv using btree (dm) tablespace tb_index;
create index farmacos_cv_i_15 on rem_sp_h04_base.farmacos_cv using btree (dl) tablespace tb_index;
create index farmacos_cv_i_16 on rem_sp_h04_base.farmacos_cv using btree (aspirina) tablespace tb_index;
create index farmacos_cv_i_17 on rem_sp_h04_base.farmacos_cv using btree (estatina) tablespace tb_index;
create index farmacos_cv_i_18 on rem_sp_h04_base.farmacos_cv using btree (ieca_ara) tablespace tb_index;
delete from rem_sp_h04_base.farmacos_cv where nif is null or fecha is null;

--select * from rem_sp_h04_base.farmacos_cv

------------------------------------------------------------------------------------------------------------------------------------------
--@@@ examenes
------------------------------------------------------------------------------------------------------------------------------------------



------------------------------------------------------------------------------------------------------------------------------------------
--@@@ PACIENTES EN CONTROL
------------------------------------------------------------------------------------------------------------------------------------------
--01 Genera episodios

drop table if exists pcv_01;
create temp table pcv_01 as
select distinct nif,fecha2 as fecha,clinico_id as medico_id
  from rem_sp.patologias_cronicas_all_registro where nif is not null and fecha2 is not null;
/*

select b.nif
	,a.especiali
	,c.id as medico_id,a.fecha
  from admomi.iddncu as a,admomi.pacientes_validos as b,admomi.iddmed as c
 where
	---a.ciap in ('K86','K87','T89','T90','T93')

   and a.nif = b.nif2 and a.medico=c.medico;
*/
create index pcv_01_i_01 on pcv_01 using btree(nif);


--02 Genera fechas de control cronico cardiovascular otros
drop table if exists pcv_02;
create temp table pcv_02 as
with consulta as
		(
		select nif2 as nif,clinico_id as medico_id,fecha2 as fecha,dgp from admomi.iddpag as a
		 where dgp in ('*P120','*P130','*P140','*P209','*P220','*P278','*P329','*P355','*P357' --CV
				'*P391','*P388','*P357','*P355','*P398' --cdi
				'*P326','*P345','*P346','*P374','*P375' --salud mental
				'*P118','*P119','*P194','*P290','*P373','*P370','*P328','*P364','*P224','*P145','*P141','*P337','*P371','*P161','*P264','*P333','*P222','*P210') --resp --334988
	        union
	        select nif2 as nif,medico_id,fecha2 as fecha,'*P'||wproto::text from admomi.iddcop as a
		 where wproto in (120,130,140,209,220,278,329,355,357
				,391,38,357,355,398 --cdi
				,326,345,346,374,375 --salud mental
				,118,119,194,290,373,370,328,364,224,145,141,337,371,161,264,333,222,210) --resp
		union
	        select nif2 as nif,medico_id,fecha2 as fecha,'*P'||wproto::text from admomi.iddcoe as a
		 where wproto in (120,130,140,209,220,278,329,355,357
				,391,38,357,355,398 --cdi
				,326,345,346,374,375 --salud mental
				,118,119,194,290,373,370,328,364,224,145,141,337,371,161,264,333,222,210) --resp
		)
		select distinct a.nif,a.medico_id,a.fecha
			,case
				when a.dgp in ('*P120','*P130','*P140','*P209','*P220','*P278','*P329') then 'CV'::Text
				when a.dgp in ('*P391','*P388','*P357','*P355','*P398') then 'MULTIM'::text
				when a.dgp in ('*P326','*P345','*P346','*P374','*P375') then 'SM'::text
				when a.dgp in ('*P118','*P119','*P194','*P290','*P373','*P370','*P328','*P364','*P224','*P145','*P141','*P337','*P371','*P161','*P264','*P333','*P222','*P210') then 'RESP'::text
				end as protocolo
			,case
				when a.dgp in ('*P120','*P130','*P140','*P209','*P220','*P278','*P329') then 1::smallint --'CV'::Text
				when a.dgp in ('*P391','*P388','*P357','*P355','*P398') then 0::smallint --then 'MULTIM'::text
				when a.dgp in ('*P326','*P345','*P346','*P374','*P375') then 2::smallint --'SM'::text
				when a.dgp in ('*P118','*P119','*P194','*P290','*P373','*P370','*P328','*P364','*P224','*P145','*P141','*P337','*P371','*P161','*P264','*P333','*P222','*P210') then 3::smallint --'RESP'::text
				end as programa
		from consulta as a where a.nif is not null and a.fecha is not null;
create index pcv_02_i_01 on pcv_02 using btree(medico_id);

--03 Generar fechas de control cardiovascular unicas.

drop table if exists pcv_03;
create temp table pcv_03 as
select distinct a.nif,a.medico_id,a.fecha,protocolo,programa
  from pcv_02 as a where a.nif is not null and a.fecha is not null;
create index pcv_03_i_01 on pcv_03 using btree(medico_id);

drop table if exists rem_sp_h04_base.cv_fecha_control_con_protocolo_04;
create  table 	     rem_sp_h04_base.cv_fecha_control_con_protocolo_04 as
select distinct a.nif,b.id AS medico_id,a.fecha::Date as fecha
	,protocolo,programa
  from pcv_03 as a,admomi.iddmed as b
  where a.medico_id = b.id
   AND (a.nif IS NOT NULL OR a.fecha IS NOT NULL);
   --and b.med_estamento_desc in ('MEDICO','ENFERMERA','NUTRICIONISTA','KINESIOLOGO','PSICOLOGO/A','ASISTENTE SOCIAL');

   delete from rem_sp_h04_base.cv_fecha_control_con_protocolo_04 where fecha is null or nif is null;


create index pcv_04_i_01 on rem_sp_h04_base.cv_fecha_control_con_protocolo_04 using btree(nif);
create index pcv_04_i_04 on rem_sp_h04_base.cv_fecha_control_con_protocolo_04 using btree(fecha);
create index pcv_04_i_05 on rem_sp_h04_base.cv_fecha_control_con_protocolo_04 using btree(nif,fecha);
create index pcv_04_i_06 on rem_sp_h04_base.cv_fecha_control_con_protocolo_04 using btree(nif,fecha,medico_id);
create index pcv_04_i_07 on rem_sp_h04_base.cv_fecha_control_con_protocolo_04 using btree(medico_id);
create index pcv_04_i_08 on rem_sp_h04_base.cv_fecha_control_con_protocolo_04 using btree(nif,fecha,protocolo);
create index pcv_04_i_09 on rem_sp_h04_base.cv_fecha_control_con_protocolo_04 using btree(nif,fecha,medico_id,protocolo);
create index pcv_04_i_10 on rem_sp_h04_base.cv_fecha_control_con_protocolo_04 using btree(protocolo);

create index pcv_04_i_11 on rem_sp_h04_base.cv_fecha_control_con_protocolo_04 using btree(programa);
create index pcv_04_i_12 on rem_sp_h04_base.cv_fecha_control_con_protocolo_04 using btree(nif,fecha,medico_id,programa);
create index pcv_04_i_13 on rem_sp_h04_base.cv_fecha_control_con_protocolo_04 using btree(nif,fecha,medico_id);

----------------------------------------------------------------------------------
--PIE DIABETICO
----------------------------------------------------------------------------------

drop table if exists cubo_cv_01_pie_02;
create temp table cubo_cv_01_pie_02 as --select * from admomi.iddpag limit 10
select b.nif,fecha::date as fecha,c.id as medico_id,1::smallint as hizo_eval_pie
  from admomi.iddpag as a,admomi.pacientes_validos as b,admomi.iddmed as c
 where a.dgp in ('*P135','PJEPIEDM','PCVEPD') and a.nif =b.nif2
   and a.medico=c.medico
 union
select nif2 as nif,fecha2 as fecha,medico_id,1::smallint as hizo_eval_pie from admomi.iddcop where wproto=329 and wnumero=228 and campo='S'	union
select nif2 as nif,fecha2 as fecha,medico_id,1::smallint as hizo_eval_pie from admomi.iddcop where wproto=278 and wnumero=228 and campo='S'	union
select nif2 as nif,fecha2 as fecha,medico_id,1::smallint as hizo_eval_pie from admomi.iddcop where wproto=209 and wnumero=493 and campo<>''	union
select nif2 as nif,fecha2 as fecha,medico_id,1::smallint as hizo_eval_pie from admomi.iddcop where wproto=302 and wnumero=228 and campo='S';

delete from cubo_cv_01_pie_02 where nif is null or fecha is null;

drop table if exists cubo_cv_01_pie_03;
create temp table cubo_cv_01_pie_03 as
select   distinct
	 nif
	,fecha
	,medico_id
	,case
		when valor::smallint >= 50 		then 3::smallint
		when valor::smallint between 25 and 49  then 2::smallint
		when valor::smallint < 25 		then 1::smallint
		else null::smallint
		end as valor
	,case
		when valor < 25			then 'Riesgo Moderado'
		when valor between 25 and 49 	then 'Riesgo Alto'
		when valor >=50 		then 'Riesgo Maximo'
		end as riesgo
  from
	(
	select b.nif as nif --,a.especiali,a.secuen
		,a.fecha::date fecha,c.id as medico_id,
		sum(case
			when a.wnumero=44 and a.campo<>'0' then 50::smallint
			when a.wnumero=45 and a.campo<>'0' then 25::smallint
			when a.wnumero=46 and a.campo<>'0' then 25::smallint
			when a.wnumero=47 and a.campo<>'0' then 15::smallint
			when a.wnumero=48 and a.campo<>'0' then  5::smallint
			when a.wnumero=49 and a.campo<>'0' then 25::smallint
			when a.wnumero=50 and a.campo<>'0' then 10::smallint
			when a.wnumero=51 and a.campo<>'0' then 10::smallint
			when a.wnumero=52 and a.campo<>'0' then  5::smallint
			when a.wnumero=53 and a.campo<>'0' then  5::smallint
			when a.wnumero=54 and a.campo<>'0' then  5::smallint
			when a.wnumero=55 and a.campo<>'0' then  6::smallint
			else 0::smallint end) as valor
	  from admomi.iddcop as a,admomi.pacientes_validos as b,admomi.iddmed as c
	 where a.wproto=135 and a.medico=c.medico
	   and a.wnumero in (44,45,46,47,48,49,50,51,52,53,54,55)
	   and (a.campo<>'' or a.campo is not null)
	   and (a.nif<>'' or a.nif is not null)
	   and a.nif=b.nif2
	  group by 1,2,3
	) as a;

insert into cubo_cv_01_pie_03
select   distinct b.nif
	,a.fecha::Date as fecha,c.id as medico_id
	,case
		when valor::smallint >= 50 		then 3::smallint
		when valor::smallint between 25 and 49  then 2::smallint
		when valor::smallint < 25 		then 1::smallint
		else null::smallint
		end as valor
	,case
		when a.texto::integer < 25			then 'Riesgo Moderado'
		when a.texto::integer between 25 and 49 	then 'Riesgo Alto'
		when a.texto::integer >=50 			then 'Riesgo Maximo'
		end as riesgo
  from admomi.iddpag as a,admomi.pacientes_validos as b,admomi.iddmed as c
 where a.dgp='PJEPIEDM' and a.medico=c.medico
   and a.nif=b.nif2
   and (texto<>'' or a.texto is not null)
   and (btrim(a.nif)<>''   or a.nif is not null);

insert into cubo_cv_01_pie_03
select   distinct
	 b.nif
	,a.fecha::date as fecha
	,c.id as medico_id
	,null::smallint as valor
	,case
		when wnumero=64 and campo='1' then 'Riesgo Moderado'
		when wnumero=61 and campo='1' then 'Riesgo Alto'
		when wnumero=57 and campo='1' then 'Riesgo Maximo'
		end as riesgo
  from admomi.iddcop as a,admomi.pacientes_validos as b,admomi.iddmed as c
 where a.wproto=135  and c.medico=a.medico
   and a.wnumero in (57,61,64)
   and (a.campo<>'' or a.campo is not null)
   and (a.nif<>'' or a.nif is not null)
   and a.nif=b.nif2;

insert into cubo_cv_01_pie_03
select distinct
	 a.nif
	,a.fecha
	,a.medico_id
	,a.valor
	,case
		when a.valor=0 then 'Riesgo Bajo'
		when a.valor=1 then 'Riesgo Moderado'
		when a.valor=2 then 'Riesgo Alto'
		when a.valor=3 then 'Riesgo Maximo'
		end
  from
	(
	  select b.nif,a.fecha2 as fecha,a.medico_id
		,case
			when a.wnumero=206 and a.campo='1' then 0::smallint
			when a.wnumero=207 and a.campo='1' then 1::smallint
			when a.wnumero=208 and a.campo='1' then 2::smallint
			when a.wnumero=209 and a.campo='1' then 3::smallint
			else null end as valor
	    from admomi.iddcop as a , admomi.pacientes_validos as b
	   where a.wproto in (278,329,302)
	     and a.wnumero in (206,207,208,209)
	     and (a.campo<>'' or a.campo is not null)
	   and (a.nif<>'' or a.nif is not null)
	   and a.nif=b.nif2
	) as a order by 2;
create index cubo_cv_01_pie_03_i_01 on cubo_cv_01_pie_03 using btree(nif);
create index cubo_cv_01_pie_03_i_02 on cubo_cv_01_pie_03 using btree(nif,fecha);
create index cubo_cv_01_pie_03_i_03 on cubo_cv_01_pie_03 using btree(fecha);
create index cubo_cv_01_pie_03_i_04 on cubo_cv_01_pie_03 using btree(nif,fecha,medico_id);
create index cubo_cv_01_pie_03_i_05 on cubo_cv_01_pie_03 using btree(medico_id);
delete from cubo_cv_01_pie_03 where nif is null or fecha is null;

drop table if exists cubo_cv_01_pie_04;
create temp table cubo_cv_01_pie_04 as
select nif,fecha,medico_id,1::smallint as 	hizo_eval_pie from cubo_cv_01_pie_03 union
select nif,fecha,medico_id,1::smallint as 	hizo_eval_pie from cubo_cv_01_pie_02;
create index cubo_cv_01_pie_04_i_01 on cubo_cv_01_pie_04 using btree (nif);
create index cubo_cv_01_pie_04_i_02 on cubo_cv_01_pie_04 using btree (nif,fecha);
create index cubo_cv_01_pie_04_i_03 on cubo_cv_01_pie_04 using btree (fecha);
create index cubo_cv_01_pie_04_i_04 on cubo_cv_01_pie_04 using btree (nif,fecha,medico_id);
create index cubo_cv_01_pie_04_i_05 on cubo_cv_01_pie_04 using btree (medico_id);

drop table if exists rem_sp_h04_base.cubo_cv_01_pie_final; --select * from rem_sp_h04_base.cubo_cv_01_pie_final where riesgo_id is null limit 100
create  table rem_sp_h04_base.cubo_cv_01_pie_final as
select distinct a.*,b.valor,b.riesgo
	,case
		when b.riesgo ='Riesgo Bajo' then 1::smallint
		when b.riesgo ='Riesgo Moderado' then 2::smallint
		when b.riesgo ='Riesgo Alto' then 3::smallint
		when b.riesgo ='Riesgo Maximo' then 4::smallint end as riesgo_id
  from cubo_cv_01_pie_04 as a
  left join cubo_cv_01_pie_03 as b on a.nif=b.nif and a.fecha=b.fecha
  where a.nif is not null and a.fecha is not null
union --select nif,fecha from admomi.iddpag where dgp='CLASEPD'
select nif2,fecha2,medico_id,1,campo::smallint as valor
	,case
		when campo='0' then 'Riesgo Bajo'
		when campo='1' then 'Riesgo Moderado'
		when campo='2' then 'Riesgo Alto'
		when campo='3' then 'Riesgo Maximo'

		end as riesgo
	,case
		when campo='0' then 1
		when campo='1' then 2
		when campo='2' then 3
		when campo='3' then 4
		 end as riesgo_id
  from admomi.iddcop
 where wproto=398
   and wnumero=374 --select nif,fecha from admomi.iddpag where dgp='CLASEPD'
   and campo is not null
union
   select nif2,fecha2,clinico_id,1
	,case
		when texto='0. Bajo'		then 1
		when texto='1. Moderado'	then 2
		when texto='2. Alto'		then 3
		when texto='3. Máximo'		then 4
	end as valor
	,case
		when texto='0. Bajo'		then 'Riesgo Bajo'
		when texto='1. Moderado'	then 'Riesgo Moderado'
		when texto='2. Alto'		then 'Riesgo Alto'
		when texto='3. Máximo'		then 'Riesgo Maximo'
	end as riesgo
	,case
		when texto='0. Bajo'		then 1
		when texto='1. Moderado'	then 2
		when texto='2. Alto'		then 3
		when texto='3. Máximo'		then 4
	end as riesgo_id
     from admomi.iddpag where dgp='CLASEPD'
  ;
  delete from rem_sp_h04_base.cubo_cv_01_pie_final where riesgo_id is null;
create index cubo_cv_01_pie_final_i_01 on rem_sp_h04_base.cubo_cv_01_pie_final using btree(nif) tablespace tb_index;
create index cubo_cv_01_pie_final_i_02 on rem_sp_h04_base.cubo_cv_01_pie_final using btree(nif,fecha) tablespace tb_index;
create index cubo_cv_01_pie_final_i_03 on rem_sp_h04_base.cubo_cv_01_pie_final using btree(nif,fecha,riesgo) tablespace tb_index;
create index cubo_cv_01_pie_final_i_04 on rem_sp_h04_base.cubo_cv_01_pie_final using btree(riesgo) tablespace tb_index;
create index cubo_cv_01_pie_final_i_05 on rem_sp_h04_base.cubo_cv_01_pie_final using btree(medico_id) tablespace tb_index;
create index cubo_cv_01_pie_final_i_06 on rem_sp_h04_base.cubo_cv_01_pie_final using btree(nif,fecha,medico_id) tablespace tb_index;
create index cubo_cv_01_pie_final_i_07 on rem_sp_h04_base.cubo_cv_01_pie_final using btree(nif,fecha,medico_id,riesgo) tablespace tb_index;
delete from rem_sp_h04_base.cubo_cv_01_pie_final where nif is null or fecha is null;

--select aym(fecha),count(*) from rem_sp_h04_base.cubo_cv_01_pie_final where fecha <='20190430' group by 1 order by 1 desc;

--------------------------------------------------------------
-- RIEGO CV
--------------------------------------------------------------

drop table if exists rem_sp_h04_base.cubo_cv_01_riesgo_cv; --select riesgo_cv,count(*) from biancora.cubo_cronicos where riesgo_cv is not null group by rollup(1) order by 1
create  table    rem_sp_h04_base.cubo_cv_01_riesgo_cv as
select
     b.nif2 as nif
	,a.fecha2 as fecha
	,a.medico_id
	,case
		when a.wproto in (398,391,388,329) 											then a.campo::integer --3 alto, 2 moderado, 1 bajo
		when a.wproto in (378)		 	   				and  a.campo::integer=4 	then 3
		when a.wproto in (130) and a.wnumero=326  	   	and  a.campo::integer=4 	then 3
		when a.wproto in (140) and a.wnumero=42			and  a.campo::integer=4 	then 3
		when a.wproto in (378)			   				and  a.campo::integer<>4	then a.campo::integer --1 bajo, 2 moderado, 3 alto, 4 muy alto
		when a.wproto in (130) and a.wnumero=326  	   	and  a.campo::integer<>4 	then a.campo::integer --1 bajo, 2 moderado, 3 alto, 4 muy alto
		when a.wproto in (140) and a.wnumero=42			and  a.campo::integer<>4 	then a.campo::integer --1 bajo, 2 moderado, 3 alto, 4 muy alto
		when
			(
				(a.wproto=120 and a.wnumero= 92) or
				(a.wproto=130 and a.wnumero=332) or
				(a.wproto=140 and a.wnumero= 13) or
				(a.wproto=209 and a.wnumero=332) or
				(a.wproto=220 and a.wnumero=332) or
				(a.wproto=278 and a.wnumero=244)
			)
			and a.campo::integer<10													then 1::integer --<10% bajo, 10 a 20% moderado, 20% maximo
		when
			(
				(a.wproto=120 and wnumero= 92) or
				(a.wproto=130 and wnumero=332) or
				(a.wproto=140 and wnumero= 13) or
				(a.wproto=209 and wnumero=332) or
				(a.wproto=220 and wnumero=332) or
				(a.wproto=278 and wnumero=244)
			)
			and a.campo::integer between 10 and 20									then 2::integer --<10% bajo, 10 a 20% moderado, 20% maximo
		when
			(
				(a.wproto=120 and wnumero= 92) or
				(a.wproto=130 and wnumero=332) or
				(a.wproto=140 and wnumero= 13) or
				(a.wproto=209 and wnumero=332) or
				(a.wproto=220 and wnumero=332) or
				(a.wproto=278 and wnumero=244)
			)
			and a.campo::integer>20													then 3::integer --<10% bajo, 10 a 20% moderado, 20% maximo
			end valor
  from admomi.iddcop as a,admomi.iddpacpa3 as b
 where
	(
	a.wproto=398 and a.wnumero in (	253) or
	a.wproto=391 and a.wnumero in (	271) or
	a.wproto=388 and a.wnumero in (	271) or
	a.wproto=329 and a.wnumero in (	86)  or
	a.wproto=378 and a.wnumero in (	86)  or --
	a.wproto=130 and a.wnumero in (326)  or
	a.wproto=140 and a.wnumero in (	42)  or
	a.wproto=120 and a.wnumero in ( 92)  or --
	a.wproto=130 and a.wnumero in (332)  or
	a.wproto=140 and a.wnumero in (	13)  or
	a.wproto=209 and a.wnumero in (332)  or
	a.wproto=220 and a.wnumero in (332)  or
	a.wproto=278 and a.wnumero in (244)
	)
   and a.nif=b.nif
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null;
/*
union
select nif2,fecha2,c.id
		,campo::integer as valor
  from admomi.iddcop as a,admomi.iddcol as b,admomi.iddmed as c
 where a.wproto		= b.wproto
   and a.wnumero	= b.wnumero
   and b.wcodigodgp	= 'RESULRCV'
   and a.medico		= c.medico
union
select cor.nif, --76542
 				   cor.fecha2,
 				   med.id,
					case
						when pag.texto='1. Bajo' 		or pag.texto='Bajo' 		 					then 1::integer
						when pag.texto='2. Moderado'	or pag.texto='Moderado' 						then 2::integer
						when pag.texto='3. Alto' 		or pag.texto='Alto' 	or pag.texto='Muy Alto'	then 3::integer
						end as valor
 			  from admomi.iddpag pag , admomi.iddcor cor,admomi.iddmed as med --select * from admomi.iddpag pag where paG.dgp='RESULRCV'
 			 where paG.dgp='RESULRCV'
 			   and (pag.texto is not null or pag.texto<>'')
 			   and pag.nif2=cor.nif
 			   and cor.especiali=pag.especiali
 			   and cor.secuen=pag.secuen
 			   and cor.medico=med.medico
 			   and cor.numero=1;
 			   */


create index cubo_cv_01_riesgo_cv_i_01 on rem_sp_h04_base.cubo_cv_01_riesgo_cv using btree(nif) tablespace tb_index;
create index cubo_cv_01_riesgo_cv_i_02 on rem_sp_h04_base.cubo_cv_01_riesgo_cv using btree(nif,fecha) tablespace tb_index;
create index cubo_cv_01_riesgo_cv_i_03 on rem_sp_h04_base.cubo_cv_01_riesgo_cv using btree(nif,fecha,valor) tablespace tb_index;
create index cubo_cv_01_riesgo_cv_i_04 on rem_sp_h04_base.cubo_cv_01_riesgo_cv using btree(valor) tablespace tb_index;
create index cubo_cv_01_riesgo_cv_i_05 on rem_sp_h04_base.cubo_cv_01_riesgo_cv using btree(nif,fecha,medico_id) tablespace tb_index;
create index cubo_cv_01_riesgo_cv_i_06 on rem_sp_h04_base.cubo_cv_01_riesgo_cv using btree(medico_id) tablespace tb_index;
delete from rem_sp_h04_base.cubo_cv_01_riesgo_cv where nif is null or fecha is null;
/*
homologa tablas
proto 278, tabla lisrcv rcv 01=bajo, 02=Moderado,03=Alto, 04=Muy alto
proto 329, tabla altomodba alto/moderador/bajo, 1=alto, 2=moderado, 3Bajo
*/

/*
corregir riesgo
select * from rem_sp_h04.cubo_cardiovascular limit 10
select riesgo_cv,count(*) from rem_sp_h04.cubo_cardiovascular group by 1 order by 1
"riesgo_cv";"count"
1;171734
2;133592
3;46780
4;25509
;3943254

update rem_sp_h04.cubo_cardiovascular
   set riesgo_cv = rem_sp_h04_base.cubo_cv_01_riesgo_cv.valor
  from rem_sp_h04_base.cubo_cv_01_riesgo_cv
 where rem_sp_h04_base.cubo_cv_01_riesgo_cv.nif 	= rem_sp_h04.cubo_cardiovascular.nif
   and rem_sp_h04_base.cubo_cv_01_riesgo_cv.fecha2 	= rem_sp_h04.cubo_cardiovascular.fecha

select riesgo_cv,count(*) from rem_sp_h04.cubo_cardiovascular group by 1 order by 1

"riesgo_cv";"count"
1;42447
2;195256
3;113983
4;25929
;3943254

*/


---------------------------------------
/*dimension ERC etapa*/
---------------------------------------
drop table if exists  rem_sp_h04_base.cubo_cv_01_etapa_erc;
create table     rem_sp_h04_base.cubo_cv_01_etapa_erc as
select   A.nif2 as nif
	,a.fecha2 as fecha
	,b.id as medico_id
	,a.campo::integer as valor
	from admomi.iddcop as a,admomi.iddmed as b,admomi.iddcol as c--select * from admomi.iddcol
  where c.wcodigodgp in ('CLASIERC','ETAPAERC')
    and a.wproto=c.wproto
    and a.wnumero=c.wnumero
    and a.medico=b.medico
    and (a.campo<>'' or a.campo is not null)
    and a.fecha is not null --order by fecha desc
union
select b.nif2 as nif,a.fecha2 as fecha,a.medico_id,a.campo::integer as valor
  from admomi.iddcop as a,admomi.iddpacpa3 as b
 where a.wproto IN (391,388) and a.wnumero=273 --cdi
   and a.nif=b.nif
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null
union

select b.nif2 as nif,a.fecha2 as fecha,a.medico_id,a.campo::integer as valor
  from admomi.iddcop as a,admomi.iddpacpa3 as b
 where a.wproto=329 and a.wnumero=80
   and a.nif=b.nif
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null
union
select b.nif2 as nif,a.fecha2 as fecha,a.medico_id,a.campo::integer as valor
  from admomi.iddcop as a,admomi.iddpacpa3 as b
 where a.wproto=278 and a.wnumero=80
   and a.nif=b.nif
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null
union
select b.nif2 as nif,a.fecha2 as fecha,a.medico_id,a.campo::integer as valor
  from admomi.iddcop as a,admomi.iddpacpa3 as b
 where a.wproto=220 and a.wnumero=702
   and a.nif=b.nif
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null
   ;
create index cubo_cv_01_etapa_erc_i_01 on rem_sp_h04_base.cubo_cv_01_etapa_erc using btree(nif);
create index cubo_cv_01_etapa_erc_i_02 on rem_sp_h04_base.cubo_cv_01_etapa_erc using btree(nif,fecha);
create index cubo_cv_01_etapa_erc_i_04 on rem_sp_h04_base.cubo_cv_01_etapa_erc using btree(nif,fecha,valor);
create index cubo_cv_01_etapa_erc_i_05 on rem_sp_h04_base.cubo_cv_01_etapa_erc using btree(valor);
create index cubo_cv_01_etapa_erc_i_06 on rem_sp_h04_base.cubo_cv_01_etapa_erc using btree(nif,fecha,medico_id);
create index cubo_cv_01_etapa_erc_i_07 on rem_sp_h04_base.cubo_cv_01_etapa_erc using btree(medico_id);
delete from rem_sp_h04_base.cubo_cv_01_etapa_erc where nif is null and fecha is null;

-------------------------------------------
/*dimension curacion avanzada*/
-------------------------------------------

drop table if exists  rem_sp_h04_base.cubo_cv_01_curacion_avanzada;
create table          rem_sp_h04_base.cubo_cv_01_curacion_avanzada as
select distinct a.nif2 as nif,a.fecha2 as fecha,a.medico_id,1::integer as valor
  from admomi.iddcop as a
 where a.wproto=316
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null
   and (a.nif2 is not null and a.fecha2 is not null);
create index cubo_cv_01_curacion_avanzada_i_01 on rem_sp_h04_base.cubo_cv_01_curacion_avanzada using btree(nif);
create index cubo_cv_01_curacion_avanzada_i_02 on rem_sp_h04_base.cubo_cv_01_curacion_avanzada using btree(nif,fecha);
create index cubo_cv_01_curacion_avanzada_i_04 on rem_sp_h04_base.cubo_cv_01_curacion_avanzada using btree(nif,fecha,valor);
create index cubo_cv_01_curacion_avanzada_i_05 on rem_sp_h04_base.cubo_cv_01_curacion_avanzada using btree(valor);
create index cubo_cv_01_curacion_avanzada_i_06 on rem_sp_h04_base.cubo_cv_01_curacion_avanzada using btree(nif,fecha,medico_id);
create index cubo_cv_01_curacion_avanzada_i_07 on rem_sp_h04_base.cubo_cv_01_curacion_avanzada using btree(medico_id);

-----------------------------------------
/*SOLITUD DE FONDO DE OJOS*/
-----------------------------------------

drop table if exists rem_sp_h04_base.cubo_cv_01_fondo_de_ojos;
create table rem_sp_h04_base.cubo_cv_01_fondo_de_ojos as
            select c.nif2 as nif,a.fecha::date as fecha,b.id as medico_id,1::smallint as valor,null::smallint as resultado_fondo_de_ojos
              from admomi.iddome as a,admomi.iddmed as b,admomi.iddpacpa3 as c
             where a.liniacor like '%fondo de ojo%'
               and a.solicita = b.medico
               and a.nif<>''
               and c.nif=a.nif --order by fecha desc
            union
            select a.nif2 as nif,fecha2 as fecha,a.medico_id,1::smallint as valor,null::smallint as resultado
              from admomi.iddcoe as a
             where a.wproto in (252,332) --IC electrónica fondo de ojos
               and lower(a.campo) like '%fondo de ojo%'
               and a.nif<>''
            union
            select a.nif2 as nif,fecha2 as fecha,a.medico_id,1::smallint as valor
			,case
				when a.campo='01' then 1::smallint --normal
				when a.campo='02' then 2::smallint --anormal
				when a.wproto=209 and a.wnumero=407 and a.campo='S' then 1::smallint --normal
				when a.wproto=220 and a.wnumero=407 and a.campo='S' then 1::smallint --anormal
				end as resultado

              from admomi.iddcop as a
             where
			(
				(a.wproto in (391,388)  and a.wnumero in (265) )
				or
				(a.wproto in (329) 	and a.wnumero in (84) )
				or
				(a.wproto in (278,329) 	and a.wnumero in (84) )
				or
				(a.wproto in (209,220) 	and a.wnumero in (407) )
				/*
				...CARDIOVASCULAR (B20130527)	Fondo de Ojo Alterado		220	407	2	S	FORESUL
				...CARDIOVASCULAR (B20100625)	Fondo de Ojo Normal		209	407	2	S	FORESUL*/
			)
               and a.campo<>''
               and a.nif<>''

               ;
create index cubo_cv_01_fondo_de_ojos_i_01 on rem_sp_h04_base.cubo_cv_01_fondo_de_ojos using btree(nif);
create index cubo_cv_01_fondo_de_ojos_i_02 on rem_sp_h04_base.cubo_cv_01_fondo_de_ojos using btree(nif,fecha);
create index cubo_cv_01_fondo_de_ojos_i_04 on rem_sp_h04_base.cubo_cv_01_fondo_de_ojos using btree(nif,fecha,valor);
create index cubo_cv_01_fondo_de_ojos_i_05 on rem_sp_h04_base.cubo_cv_01_fondo_de_ojos using btree(valor);
create index cubo_cv_01_fondo_de_ojos_i_06 on rem_sp_h04_base.cubo_cv_01_fondo_de_ojos using btree(nif,fecha,medico_id);
create index cubo_cv_01_fondo_de_ojos_i_07 on rem_sp_h04_base.cubo_cv_01_fondo_de_ojos using btree(medico_id);
delete from rem_sp_h04_base.cubo_cv_01_fondo_de_ojos where nif is null or  fecha is null;

drop table if exists  rem_sp_h04_base.cubo_cv_01_vfg;
create table     rem_sp_h04_base.cubo_cv_01_vfg as
select a.nif2 as nif,a.fecha2 as fecha,a.medico_id,a.campo::numeric as valor
  from admomi.iddcop as a,admomi.iddpacpa3 as b
 where
	(
		(a.wproto=329 and a.wnumero=75) --HOMBRE
		or
		(a.wproto=329 and a.wnumero=76)  --MUJER
		OR
		(a.wproto=278 and a.wnumero=75) --HOMBRE
		or
		(a.wproto=278 and a.wnumero=76)  --MUJER
		OR
		(a.wproto=93 and a.wnumero=43) --HOMBRE
		or
		(a.wproto=92 and a.wnumero=39)  --MUJER
	)
   and a.nif2=b.nif2 and b.sexo='M'
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null
union
select a.nif2 as nif,a.fecha2 as fecha,a.medico_id,replace(a.campo,',','.')::numeric*0.85 as valor
  from admomi.iddcop as a,admomi.iddpacpa3 as b
 where a.wproto in (388,391) and a.wnumero in (402) and b.sexo='F'
   and a.nif=b.nif;
create index cubo_cv_01_vfg_i_01 on rem_sp_h04_base.cubo_cv_01_vfg using btree(nif);
create index cubo_cv_01_vfg_i_02 on rem_sp_h04_base.cubo_cv_01_vfg using btree(nif,fecha);
create index cubo_cv_01_vfg_i_03 on rem_sp_h04_base.cubo_cv_01_vfg using btree(nif,fecha,valor);
create index cubo_cv_01_vfg_i_04 on rem_sp_h04_base.cubo_cv_01_vfg using btree(valor);
create index cubo_cv_01_vfg_i_05 on rem_sp_h04_base.cubo_cv_01_vfg using btree(nif,fecha,medico_id);
create index cubo_cv_01_vfg_i_06 on rem_sp_h04_base.cubo_cv_01_vfg using btree(medico_id);
delete from rem_sp_h04_base.cubo_cv_01_vfg where nif is null or fecha is null;

-----------------------------------
/*RAC*/
-----------------------------------

drop table if exists  rem_sp_h04_base.cubo_cv_01_rac;
create table     rem_sp_h04_base.cubo_cv_01_rac as
select b.nif2 as nif,fecha_toma_muestra::date as fecha,1::smallint as valor
 from examenes.utm as a,admomi.iddpacpa3 as b
 where a.COD_FLEX='1950'
   AND a.VAL like '%mg/gr%'
   and a.rut=b.tis
union
select a.nif2 as nif,a.fecha,valor
  from rem_sp_h04_base.examenes_de_laboratorio as a
 where examen='RAC'
   ;
create index cubo_cv_01_rac_i_01 on rem_sp_h04_base.cubo_cv_01_rac using btree(nif);
create index cubo_cv_01_rac_i_02 on rem_sp_h04_base.cubo_cv_01_rac using btree(nif,fecha);
create index cubo_cv_01_rac_i_03 on rem_sp_h04_base.cubo_cv_01_rac using btree(nif,fecha,valor);
create index cubo_cv_01_rac_i_04 on rem_sp_h04_base.cubo_cv_01_rac using btree(valor);
delete from rem_sp_h04_base.cubo_cv_01_rac where nif is null or fecha is null;
-------------------------------
/*CONTROL CON Y CONTROL EN*/
-------------------------------
drop table if exists control_con;create temp table control_con as
SELECT a.nif2 as nif,a.fecha2 as fecha,a.clinico_id as medico_id,a.tabla_descripcion_id as valor,a.tabla_campo_descripcion,a.wproto,campo as campo,tabla_descripcion_id as campo2
  from biancora.cubo_formularios as a
 where
	(
		(a.wproto=398 and a.wnumero=328)	or --ok MULTIM
		(a.wproto=398 and a.wnumero=415)	or --ok MULTIM
		(a.wproto=398 and a.wnumero=158)	or --ok MULTIM
		(a.wproto=391 and a.wnumero=333)	or --ok MULTIM
		(a.wproto=388 and a.wnumero=333)	or --ok MULTIM
		(a.wproto=355 and a.wnumero=257)	or --ok MULTIM
		(a.wproto=357 and a.wnumero=257)	or --ok MULTIM (398,391,388,355,355,357)
		(a.wproto=220 and a.wnumero=622)	or --OK CV
		(a.wproto=329 and a.wnumero=100)	or --ok CV
		(a.wproto=278 and a.wnumero=100)	   --ok CV
		/*
		(a.wproto=371 and a.wnumero=170)	or --OK RESP Protocolo ERA
		(a.wproto=371 and a.wnumero=284)	or --OK RESP Protocolo ERA
		(a.wproto=371 and a.wnumero=419)	or --OK RESP Protocolo ERA
		(a.wproto=337 and a.wnumero=419)	or --OK RESP Protocolo ERA
		(a.wproto=333 and a.wnumero=170)	or --OK RESP Protocolo ERA
		(a.wproto=333 and a.wnumero=419)	or --OK RESP Protocolo ERA
		(a.wproto=333 and a.wnumero=284)	or --OK RESP Protocolo ERA
		(a.wproto=370 and a.wnumero=364)	or --OK RESP Protocolo Control IRA
		(a.wproto=328 and a.wnumero=364)	or --OK RESP Protocolo Control IRA
		(a.wproto=367 and a.wnumero=157)	or --OK Adulto Mayor
		(a.wproto=366 and a.wnumero=157)	or --OK Adulto Mayor
		(a.wproto=354 and a.wnumero=157)	or --OK Adulto Mayor
		(a.wproto=374 and a.wnumero=157)	or --OK Adulto Mayor 	OJO
		(a.wproto=374 and a.wnumero=169)	or --OK SM   			OJO REVISAR IDEM ARRIBA
		(a.wproto=346 and a.wnumero=169)	or --OK SM
		(a.wproto=345 and a.wnumero=169)	or --OK SM
		(a.wproto=326 and a.wnumero=242)	or --OK SM
		(a.wproto=326 and a.wnumero=348)	or --OK SM
		(a.wproto=326 and a.wnumero= 60)	or --OK SM
		(a.wproto=326 and a.wnumero=131)	or --OK SM
		(a.wproto=326 and a.wnumero=432)    or --OK SM
		(a.wproto=326 and a.wnumero=506)	or --OK SM
		(a.wproto=320 and a.wnumero= 34)	or --OK Postrado
		(a.wproto=319 and a.wnumero=325)	   --OK Postrado
		*/
	)
   and a.campo<>''
   and a.campo is not null
   and a.fecha is not null;

--control en
drop table if exists control_en;create temp table control_en as
SELECT distinct a.nif2 as nif,a.fecha2 as fecha,a.clinico_id as medico_id,a.tabla_descripcion_id as valor,a.tabla_campo_descripcion,a.wproto,campo,campo2
  from biancora.cubo_formularios as a
 where
	(
		(a.wproto=398 and a.wnumero=330) or --ok Multimorbilidad
		(a.wproto=391 and a.wnumero=335) or --ok Multimorbilidad
		(a.wproto=388 and a.wnumero=335) or --ok Multimorbilidad
		(a.wproto=357 and a.wnumero=256) or --ok Multimorbilidad
		(a.wproto=355 and a.wnumero=256) or --ok Multimorbilidad
		(a.wproto=329 and a.wnumero=231) or --OK Cardiovascular
		(a.wproto=278 and a.wnumero=231) or --OK Cardiovascular
		(a.wproto=220 and a.wnumero=624)  --OK Cardiovascular
		/*
		(a.wproto=384 and a.wnumero=362) or --OK CLAP
		(a.wproto=255 and a.wnumero=454) or --OK CLAP
		(a.wproto=375 and a.wnumero=240) or --OK Salud Mental
		(a.wproto=345 and a.wnumero=240) or --OK Salud Mental
		(a.wproto=370ea
		 and a.wnumero=362) or --OK Nino ingreso control Ira
		(a.wproto=328 and a.wnumero=362) or --OK Nino ingreso control Ira
		(a.wproto=320 and a.wnumero=32) or --OK Postrado
		(a.wproto=319 and a.wnumero=327) --OK Postrado
		*/
	)
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null;

   drop table if exists control_con_en01;create temp table control_con_en01 as--select tabla_campo_descripcion,aym(fecha),count(*) from (
   select nif,fecha,medico_id,wproto from control_con union
   select nif,fecha,medico_id,wproto from control_en;

   drop table if exists control_con_en02;create temp table control_con_en02 as--select tabla_campo_descripcion,aym(fecha),count(*) from (
   select a.*
    ,CASE WHEN A.WPROTO IN (398,391,388,355,355,357) THEN 'MULTIM'::text else 'CV' end as Protocolo
   	,a01.tabla_campo_descripcion control_con
	,a01.campo2		as control_con_id
	,upper(a02.tabla_campo_descripcion) control_en
	,a02.campo::integer     as control_en_id
	--,a01.campo as control_con_campo
     from control_con_en01 as a
	 left join control_con as a01 using (nif,fecha,medico_id,wproto)
	 left join control_en  as a02 using (nif,fecha,medico_id,wproto)
	 order by fecha desc;

delete from control_con_en02 where control_con is null;
delete from control_con_en02 where control_en is null;

drop table if exists  rem_sp_h04_base.cubo_cv_01_control_con_en; --SELECT * from rem_sp_h04_base.cubo_cv_01_control_con_en limit 100
create table     rem_sp_h04_base.cubo_cv_01_control_con_en as
select distinct *,null::date as fecha_proximo_control from control_con_en02;
--select * from rem_sp_h04_base.cubo_cv_01_control_con_en limit 10

update rem_sp_h04_base.cubo_cv_01_control_con_en set fecha_proximo_control=case
				when ((((date_part('year',fecha)::text||'01'::Text||'01'::text)::Date+(date_part('day',fecha)::Text||' days')::interval)::date
					+(control_en_id::Text||' mons')::interval)::Date-'1 mons 1 days'::interval)::Date > fecha
				then ((((date_part('year',fecha)::text||'01'::Text||'01'::text)::Date+(date_part('day',fecha)::Text||' days')::interval)::date
					+(control_en_id::Text||' mons')::interval)::Date-'1 mons 1 days'::interval)::Date
				else ((((date_part('year',fecha)::text||'01'::Text||'01'::text)::Date+(date_part('day',fecha)::Text||' days')::interval)::date
					+(control_en_id::Text||' mons')::interval)::Date-'1 mons 1 days'::interval+'1 year'::interval)::Date
				end where control_en_id is not null;

create index cubo_cv_01_control_con_en_i_01 on rem_sp_h04_base.cubo_cv_01_control_con_en using btree(nif);
create index cubo_cv_01_control_con_en_i_02 on rem_sp_h04_base.cubo_cv_01_control_con_en using btree(nif,fecha);
create index cubo_cv_01_control_con_en_i_04 on rem_sp_h04_base.cubo_cv_01_control_con_en using btree(nif,fecha,control_en_id);
create index cubo_cv_01_control_con_en_i_05 on rem_sp_h04_base.cubo_cv_01_control_con_en using btree(control_en_id);
create index cubo_cv_01_control_con_en_i_06 on rem_sp_h04_base.cubo_cv_01_control_con_en using btree(control_con_id);
create index cubo_cv_01_control_con_en_i_07 on rem_sp_h04_base.cubo_cv_01_control_con_en using btree(nif,fecha,medico_id);
create index cubo_cv_01_control_con_en_i_08 on rem_sp_h04_base.cubo_cv_01_control_con_en using btree(medico_id);
create index cubo_cv_01_control_con_en_i_09 on rem_sp_h04_base.cubo_cv_01_control_con_en using btree(protocolo);
create index cubo_cv_01_control_con_en_i_10 on rem_sp_h04_base.cubo_cv_01_control_con_en using btree(nif,fecha,medico_id,protocolo);
delete from rem_sp_h04_base.cubo_cv_01_control_con_en where nif is null or fecha is null;
--SELECT * from rem_sp_h04_base.cubo_cv_01_control_con_en limit 100

/*
drop table if exists  rem_sp_h04_base.cubo_cv_01_control_con; --SELECT COUNT(*) FROM rem_sp_h04_base.cubo_cv_01_control_con --112952
create table     rem_sp_h04_base.cubo_cv_01_control_con as
SELECT a.nif2 as nif,a.fecha2 as fecha,a.medico_id,a.campo::integer as valor
	,'CV'::TEXT AS PROTOCOLO
  from admomi.iddcop as a
 where
	(
		(a.wproto=220 and a.wnumero=622)
		or
		(a.wproto in (278,329) and a.wnumero=100)
	)
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null
UNION
SELECT a.nif2 as nif,a.fecha2 as fecha,a.medico_id,a.campo::integer as valor
	,'MULTIM'::TEXT AS PROTOCOLO
  from admomi.iddcop as a
 where
	(
		(a.wproto in (391,338) and a.wnumero=333)
		or
		(a.wproto in (355,357) and a.wnumero=257)
		or
		(a.wproto in (398) and a.wnumero=415)
	)
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null
;
create index cubo_cv_01_control_con_i_01 on rem_sp_h04_base.cubo_cv_01_control_con using btree(nif);
create index cubo_cv_01_control_con_i_02 on rem_sp_h04_base.cubo_cv_01_control_con using btree(nif,fecha);
create index cubo_cv_01_control_con_i_04 on rem_sp_h04_base.cubo_cv_01_control_con using btree(nif,fecha,valor);
create index cubo_cv_01_control_con_i_05 on rem_sp_h04_base.cubo_cv_01_control_con using btree(valor);
create index cubo_cv_01_control_con_i_06 on rem_sp_h04_base.cubo_cv_01_control_con using btree(nif,fecha,medico_id);
create index cubo_cv_01_control_con_i_07 on rem_sp_h04_base.cubo_cv_01_control_con using btree(medico_id);
create index cubo_cv_01_control_con_i_08 on rem_sp_h04_base.cubo_cv_01_control_con using btree(PROTOCOLO);
create index cubo_cv_01_control_con_i_09 on rem_sp_h04_base.cubo_cv_01_control_con using btree(nif,fecha,medico_id,protocolo);

--------------------------
--CONTROL EN
--------------------------

drop table if exists  rem_sp_h04_base.cubo_cv_01_control_en;
create table     rem_sp_h04_base.cubo_cv_01_control_en as
select a.nif2 as nif,a.fecha2 as fecha,a.medico_id,a.campo::integer as valor
	,'CV'::text as protocolo
  from admomi.iddcop as a
 where
	(
		(a.wproto=220 and a.wnumero=624)
		or
		(a.wproto IN (278,329) and a.wnumero=231)
	)
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null
 union
select a.nif2 as nif,a.fecha2 as fecha,a.medico_id,a.campo::integer as valor
	,'MULTIM'::text as protocolo
  from admomi.iddcop as a
 where
	(
		(a.wproto=357 and a.wnumero=256)
		or
		(a.wproto=391 and a.wnumero=355)
				or
		(a.wproto in (398) and a.wnumero=330)
	)
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null;

delete from rem_sp_h04_base.cubo_cv_01_control_en where nif is null or fecha is null or valor is null;
create index cubo_cv_01_control_en_i_01 on rem_sp_h04_base.cubo_cv_01_control_en using btree(nif);
create index cubo_cv_01_control_en_i_02 on rem_sp_h04_base.cubo_cv_01_control_en using btree(nif,fecha);
create index cubo_cv_01_control_en_i_04 on rem_sp_h04_base.cubo_cv_01_control_en using btree(nif,fecha,valor);
create index cubo_cv_01_control_en_i_05 on rem_sp_h04_base.cubo_cv_01_control_en using btree(valor);
create index cubo_cv_01_control_en_i_06 on rem_sp_h04_base.cubo_cv_01_control_en using btree(nif,fecha,medico_id);
create index cubo_cv_01_control_en_i_07 on rem_sp_h04_base.cubo_cv_01_control_en using btree(medico_id);
create index cubo_cv_01_control_en_i_08 on rem_sp_h04_base.cubo_cv_01_control_en using btree(protocolo);
create index cubo_cv_01_control_en_i_09 on rem_sp_h04_base.cubo_cv_01_control_en using btree(nif,fecha,medico_id,protocolo);
*/
--------------------------
/*CON EXAMENES S/N*/
--------------------------
drop table if exists  rem_sp_h04_base.cubo_cv_01_con_examenes_s_n;
create table     rem_sp_h04_base.cubo_cv_01_con_examenes_s_n as
select b.nif2 as nif,a.fecha::date as fecha,c.id as medico_id
	,case when a.texto = 'S' then 1::smallint when a.texto = 'N' then 0::smallint else null end as valor
  from admomi.iddpag as a,admomi.iddpacpa3 as b ,admomi.iddmed as c
 where a.nif=b.nif and a.medico=c.medico
   and a.dgp='CONEXAME'
union
select a.nif2 as nif,a.fecha2 as fecha,a.medico_id
	,case when a.campo = 'S' then 1::smallint when a.campo = 'N' then 0::smallint else null end as valor
  from admomi.iddcop as a
 where
	(
		(a.wproto=329 and a.wnumero=102)
		or
		(a.wproto=278 and a.wnumero=102)
		or
		(a.wproto=220 and a.wnumero=626)
		or
		(a.wproto=357 and a.wnumero=347)
		or
		(a.wproto in (391,388) and a.wnumero=306)
		or
		(a.wproto=398 and a.wnumero=332)
	)
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null;
create index cubo_cv_01_con_examenes_s_n_i_01 on rem_sp_h04_base.cubo_cv_01_con_examenes_s_n using btree(nif);
create index cubo_cv_01_con_examenes_s_n_i_03 on rem_sp_h04_base.cubo_cv_01_con_examenes_s_n using btree(nif,fecha);
create index cubo_cv_01_con_examenes_s_n_i_04 on rem_sp_h04_base.cubo_cv_01_con_examenes_s_n using btree(nif,fecha,valor);
create index cubo_cv_01_con_examenes_s_n_i_05 on rem_sp_h04_base.cubo_cv_01_con_examenes_s_n using btree(valor);
create index cubo_cv_01_con_examenes_s_n_i_06 on rem_sp_h04_base.cubo_cv_01_con_examenes_s_n using btree(nif,fecha,medico_id);
create index cubo_cv_01_con_examenes_s_n_i_07 on rem_sp_h04_base.cubo_cv_01_con_examenes_s_n using btree(medico_id);
delete from rem_sp_h04_base.cubo_cv_01_con_examenes_s_n where nif is null or fecha is null;

drop table if exists  	rem_sp_h04_base.cubo_cv_01_consejeia_telefonica_automanejo;
create table     	rem_sp_h04_base.cubo_cv_01_consejeia_telefonica_automanejo as
select distinct *
  from
	(
	  select a.nif2 as nif,a.fecha2 as fecha,a.medico_id
		,case when a.campo = 'S' then 1::smallint when a.campo = 'N' then 0::smallint else null end as valor
		,(select b.campo::int
		    from admomi.iddcop as b
		   where a.nif2=b.nif2
		     and a.fecha2=b.fecha2
		     and a.medico_id=b.medico_id
		     and
			(
			(b.wproto=391 and b.wnumero=291)
			or
			(b.wproto=388 and b.wnumero=291)
			--or
			--(b.wproto=398 and b.wnumero=305)
			)
			limit 1
		) as mes
	  from admomi.iddcop as a
	 where
		(
			(a.wproto=391 and a.wnumero=289)
			or
			(a.wproto=388 and a.wnumero=298)
			--or
			--(a.wproto=398 and a.wnumero=305)
		)

	) as a where (coalesce(valor,null,0)+coalesce(mes,null,0))>=1 order by 4 nulls last;
create index cubo_cv_01_consejeia_telefonica_automanejo_i_01 on rem_sp_h04_base.cubo_cv_01_consejeia_telefonica_automanejo using btree(nif);
create index cubo_cv_01_consejeia_telefonica_automanejo_i_03 on rem_sp_h04_base.cubo_cv_01_consejeia_telefonica_automanejo using btree(nif,fecha);
create index cubo_cv_01_consejeia_telefonica_automanejo_i_04 on rem_sp_h04_base.cubo_cv_01_consejeia_telefonica_automanejo using btree(nif,fecha,valor);
create index cubo_cv_01_consejeia_telefonica_automanejo_i_05 on rem_sp_h04_base.cubo_cv_01_consejeia_telefonica_automanejo using btree(valor);
create index cubo_cv_01_consejeia_telefonica_automanejo_i_06 on rem_sp_h04_base.cubo_cv_01_consejeia_telefonica_automanejo using btree(nif,fecha,medico_id);
create index cubo_cv_01_consejeia_telefonica_automanejo_i_07 on rem_sp_h04_base.cubo_cv_01_consejeia_telefonica_automanejo using btree(medico_id);
create index cubo_cv_01_consejeia_telefonica_automanejo_i_08 on rem_sp_h04_base.cubo_cv_01_consejeia_telefonica_automanejo using btree(nif,fecha,valor,mes);
create index cubo_cv_01_consejeia_telefonica_automanejo_i_09 on rem_sp_h04_base.cubo_cv_01_consejeia_telefonica_automanejo using btree(mes);
delete from rem_sp_h04_base.cubo_cv_01_consejeia_telefonica_automanejo where nif is null or fecha is null;
------------
-- ACG 2015
------------
/*
drop materialized view if exists acg.acg_rub_ancora_2015;
create materialized view acg.acg_rub_ancora_2015 as
select patient_id::numeric as patient_id		,'20141231'::date as fecha,6626830::integer as clinico_id,resource_utilization_band::integer 	as rub,replace(unscaled_acg_concurrent_risk,',','.')::numeric 	peso_acg from acg.acg_sabana_ssmso_2014 			union
select run::numeric					,'20151231'::date as fecha,6626830::integer as clinico_id,"Banda Utilizacion Recurso"::integer 	as rub,replace("Peso Local Concurrente",',','.')::numeric 	peso_acg from acg.acg_mis_ssmso__2015 as a 			union
select "Patient Id"::numeric				,'20161231'::date as fecha,6626830::integer as clinico_id,resource_utilization_band::integer 	as rub,replace("Local Concurrent Weight",',','.')::numeric 	peso_acg from acg.mis_acg_2016_cdi 				union
select patient_id::numeric				,'20131231'::date as fecha,6626830::integer as clinico_id,resource_utilization_band::integer 	as rub,replace(local_concurrent_weight,',','.')::numeric 	peso_acg from acg_etl.acg_output_ancora_2016_2017_05112018 where periodo='2013' union
select patient_id::numeric				,'20141231'::date as fecha,6626830::integer as clinico_id,resource_utilization_band::integer 	as rub,replace(local_concurrent_weight,',','.')::numeric 	peso_acg from acg_etl.acg_output_ancora_2016_2017_05112018 where periodo='2014' union
select patient_id::numeric				,'20151231'::date as fecha,6626830::integer as clinico_id,resource_utilization_band::integer 	as rub,replace(local_concurrent_weight,',','.')::numeric 	peso_acg from acg_etl.acg_output_ancora_2016_2017_05112018 where periodo='2015' union
select patient_id::numeric				,'20161231'::date as fecha,6626830::integer as clinico_id,resource_utilization_band::integer 	as rub,replace(local_concurrent_weight,',','.')::numeric 	peso_acg from acg_etl.acg_output_ancora_2016_2017_05112018 where periodo='2016' union
select patient_id::numeric				,'20171231'::date as fecha,6626830::integer as clinico_id,resource_utilization_band::integer 	as rub,replace(local_concurrent_weight,',','.')::numeric 	peso_acg from acg_etl.acg_output_ancora_2016_2017_05112018 where periodo='2017'
;
create index acg_rub_ancora_2015_i_01 on acg.acg_rub_ancora_2015 using btree(patient_id);
create index acg_rub_ancora_2015_i_02 on acg.acg_rub_ancora_2015 using btree(patient_id,fecha,clinico_id);

--select * from acg.acg_rub_ancora_2015 limit 10
--select count(*) from acg.acg_rub_ancora_2015 --"count" 2827697
--select count(*) from (select distinct patient_id,fecha from acg.acg_rub_ancora_2015) as a --"count" 2.569.767

drop table if exists rem_sp.acg_rub;
create table rem_sp.acg_rub as
select   patient_id
	,fecha
	,clinico_id
	,null::text nif
	,null::numeric nif2
	,max(rub) as rub
	,max(peso_acg) peso_acg
  from acg.acg_rub_ancora_2015 group by 1,2,3,4,5;

create index acg_rub_i_01 on rem_sp.acg_rub using btree(nif);
create index acg_rub_i_02 on rem_sp.acg_rub using btree(nif2);
create index acg_rub_i_03 on rem_sp.acg_rub using btree(fecha);
create index acg_rub_i_04 on rem_sp.acg_rub using btree(clinico_id);
create index acg_rub_i_05 on rem_sp.acg_rub using btree(nif,fecha,clinico_id);
create index acg_rub_i_06 on rem_sp.acg_rub using btree(nif2,fecha,clinico_id);
*/

update rem_sp.acg_rub
   set nif=admomi.iddpacpa3.nif,nif2=admomi.iddpacpa3.nif2
  from admomi.iddpacpa3
 where admomi.iddpacpa3.run=rem_sp.acg_rub.patient_id;




  --
  --patologias cronicas
  --
drop table if exists  rem_sp_h04_base.cubo_cv_01_all_patologicas_cronicas;
create table     rem_sp_h04_base.cubo_cv_01_all_patologicas_cronicas as
with consulta as
		(
		select distinct nif2 from rem_sp.patologias_cronicas_all_long where nif2 is not null
		)
		select a.nif2 as nif
			,array(select patologia_cronica from rem_sp.patologias_cronicas_all_long as b where a.nif2=b.nif2) as patologias_cronicas
		  from consulta as a;
create index cubo_cv_01_all_patologicas_cronicas_i_01 on rem_sp_h04_base.cubo_cv_01_all_patologicas_cronicas using btree(nif);

--
-- ETAPA DE CAMBIO
--
drop table if exists  	rem_sp_h04_base.cubo_cv_01_etapa_de_cambio;
create table     	rem_sp_h04_base.cubo_cv_01_etapa_de_cambio as
select distinct a.nif2 as nif,a.fecha2 as fecha,a.medico_id,a.campo::integer as valor
  from admomi.iddcop as a
 where
	(
		(a.wproto=391 and a.wnumero in (326) ) -- CDI 1 Accion,2 Mantencion,3 Recaida,4 Precontemplativo ,5 Preparacion
		or
		(a.wproto=338 and a.wnumero in (326) ) -- CDI idem
		--or
		--(a.wproto=371 and a.wnumero in (374) ) -- Protocolo ERA

	)
   and (a.campo<>'' or a.campo is not null)
   and a.nif is not null and a.fecha2 is not null;
create index cubo_cv_01_etapa_de_cambio_i_01 on rem_sp_h04_base.cubo_cv_01_etapa_de_cambio using btree(nif);
create index cubo_cv_01_etapa_de_cambio_i_02 on rem_sp_h04_base.cubo_cv_01_etapa_de_cambio using btree(nif,fecha);
create index cubo_cv_01_etapa_de_cambio_i_03 on rem_sp_h04_base.cubo_cv_01_etapa_de_cambio using btree(nif,fecha,valor);
create index cubo_cv_01_etapa_de_cambio_i_04 on rem_sp_h04_base.cubo_cv_01_etapa_de_cambio using btree(valor);
create index cubo_cv_01_etapa_de_cambio_i_05 on rem_sp_h04_base.cubo_cv_01_etapa_de_cambio using btree(nif,fecha,medico_id);

--
-- Apoyo Automanejo
--
drop table if exists  rem_sp_h04_base.cubo_cv_01_apoyo_automanejo;
create table     rem_sp_h04_base.cubo_cv_01_apoyo_automanejo as
select distinct a.nif2 as nif,a.fecha2 as fecha,a.medico_id,b.nombremed,b.med_Estamento_desc
	,case
		when wproto in (388,391) and wnumero = 283 then 'SMS'::text
		when wproto in (388,391) and wnumero = 281 then 'TALLER'::text
		when wproto in (388,391) and wnumero = 285 then 'AUTOMONITORE'::text
		when wproto in (388,391) and wnumero = 287 then 'FORO'::text
		END AS tipo_apoyo_automanejo
	,case
		when a.campo='S' then 1::smallint
		when a.campo='N' then 0::smallint
		else null
		 end as valor
  from admomi.iddcop as a,admomi.iddmed as b
 where b.id=a.medico_id and
	(
		(a.wproto in (388,391) and a.wnumero in (281,283,285,287) ) --283 sms, --281 taller,--285 automonitoreo, --287 foros
	)
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null;
delete from rem_sp_h04_base.cubo_cv_01_apoyo_automanejo where nif is null or fecha is null or valor is null;
create index cubo_cv_01_apoyo_automanejo_i_01 on rem_sp_h04_base.cubo_cv_01_apoyo_automanejo using btree(nif) 										tablespace tb_index;
create index cubo_cv_01_apoyo_automanejo_i_02 on rem_sp_h04_base.cubo_cv_01_apoyo_automanejo using btree(nif,fecha) 								tablespace tb_index;
create index cubo_cv_01_apoyo_automanejo_i_04 on rem_sp_h04_base.cubo_cv_01_apoyo_automanejo using btree(nif,fecha,valor) 							tablespace tb_index;
create index cubo_cv_01_apoyo_automanejo_i_05 on rem_sp_h04_base.cubo_cv_01_apoyo_automanejo using btree(valor) 									tablespace tb_index;
create index cubo_cv_01_apoyo_automanejo_i_06 on rem_sp_h04_base.cubo_cv_01_apoyo_automanejo using btree(nif,fecha,medico_id) 						tablespace tb_index;
create index cubo_cv_01_apoyo_automanejo_i_07 on rem_sp_h04_base.cubo_cv_01_apoyo_automanejo using btree(medico_id) 								tablespace tb_index;
create index cubo_cv_01_apoyo_automanejo_i_09 on rem_sp_h04_base.cubo_cv_01_apoyo_automanejo using btree(tipo_apoyo_automanejo) 					tablespace tb_index;
create index cubo_cv_01_apoyo_automanejo_i_10 on rem_sp_h04_base.cubo_cv_01_apoyo_automanejo using btree(nif,fecha,medico_id,tipo_apoyo_automanejo) tablespace tb_index;

--EPOC
drop table if exists  rem_sp_h04_base.cubo_cv_01_epoc_tipo;
create table          rem_sp_h04_base.cubo_cv_01_epoc_tipo as
select distinct a.nif2 as nif,a.fecha2 as fecha,a.medico_id,campo::int as epoc_etapa
  from admomi.iddcop as a
 where (a.campo<>'')
   and 	(
	(a.wproto in (371,337) and a.wproto in (432,436))
	)
union
select a.nif2,a.fecha2 as fecha,a.medico_id,1::int
 from admomi.iddcop as a
where
	(
		(a.wproto in (333,337,371) and a.wnumero in (189,331) )
	)
;
-- eN TEORÍA TODO PACIENTE EPOC QUE NO SEA EPOC TIPO b, POR DEFECTO ES A. fuente: protocolo ERA,
create index cubo_cv_01_epoc_tipo_i_01 on rem_sp_h04_base.cubo_cv_01_epoc_tipo(nif,fecha,medico_id) tablespace tb_index;
delete from rem_sp_h04_base.cubo_cv_01_epoc_tipo where nif is null or fecha is null;

--nivel control asma epoc
drop table if exists	rem_sp_h04_base.cubo_cv_01_asma_sbor_epoc_nivel_control; --rem_sp_h04_base.asma_epoc_nivel_control --1 asma,2 sbor, 3 epoc
create table    	rem_sp_h04_base.cubo_cv_01_asma_sbor_epoc_nivel_control as --select * from rem_sp_h03_temp_asma_epoc_nivel_control limit 100
select distinct nif2 as nif,fecha2 as fecha,medico_id
/*	,case
		when campo='1' then 'CONTROLADO'
		when campo='2' then 'PARCIALMENTE CONTROLADO'
		when campo='3' then 'NO CONTROLADO'
		when campo='4' then 'NO EVALUADO'
		end as nivel_control */
	,case
		when 		(wproto in (290,373) and wnumero=108)
			or
				(wproto in (371,337) and wnumero in (438,442))
			or
				(wproto in (370,328) and wnumero=376)
			or
				(wproto in (371,337) and wnumero=442)

			 then 1::smallint --'ASMA'
		when wproto in (370,328)	and wnumero=377 then 2::smallint --'SBOR'
		when wproto in (290,373) 	and wnumero=109 then 3::smallint --'EPOC'
		end as patologia
		,campo::smallint as nivel_control
  from admomi.iddcop
 where
	(
		(wproto in (290,373) and wnumero in (108,109))
		or
		(wproto in (371,337) and wnumero in (438,442))
		or
		(wproto in (370,328) and wnumero in (376,377))

	)
   and (campo<>'' or campo is not null)
   and nif is not null and fecha is not null
   ;
create index cubo_cv_01_asma_sbor_epoc_nivel_control_i_01 on rem_sp_h04_base.cubo_cv_01_asma_sbor_epoc_nivel_control(nif,fecha,medico_id) tablespace tb_index;
create index cubo_cv_01_asma_sbor_epoc_nivel_control_i_02 on rem_sp_h04_base.cubo_cv_01_asma_sbor_epoc_nivel_control(nif,fecha,medico_id,patologia) tablespace tb_index;
delete from rem_sp_h04_base.cubo_cv_01_asma_sbor_epoc_nivel_control where nif is null or fecha is null;

--NIVEL DE PATOLOGIA ASMA SBOR
drop table if exists 	rem_sp_h04_base.cubo_cv_01_asma_sbor_leve_moderada_severa;
create table 		rem_sp_h04_base.cubo_cv_01_asma_sbor_leve_moderada_severa as
select distinct nif2 as nif,fecha2 as fecha,medico_id
			,case
				when (wproto in (141,224) 	and wnumero=51 			and campo='1')	THEN 1::SMALLINT 	--sbor leve
				when (wproto in (370,264,328) 	and wnumero=194 		and campo='1')	THEN 1::SMALLINT	--sbor leve
				when (wproto in (145) 		and wnumero=9			and campo='1')	THEN 1::SMALLINT 	--sbor leve
				when (wproto in (264,328,370) 	and wnumero=195 		and campo='1')	THEN 1::SMALLINT 	--sbor moderada
				when (wproto in (141,224) 	and wnumero=52 			and campo='1')	THEN 1::SMALLINT 	--sbor moderada
				when (wproto in (145) 		and wnumero=10			and campo='1')	THEN 1::SMALLINT 	--sbor moderada
				when (wproto in (264,328,370) 	and wnumero=196			and campo='1')	THEN 1::SMALLINT 	--sbor severa
				when (wproto in (141,224) 	and wnumero=53 			and campo='1')	THEN 1::SMALLINT 	--sbor severa
				when (wproto in (145) 		and wnumero=11			and campo='1')	THEN 2::SMALLINT 	--sbor severa
				when (wproto in (141,224) 	and wnumero=54 			and campo='1')	THEN 2::SMALLINT 	--asma leve
				when (wproto in (145) 		and wnumero=6			and campo='1')	THEN 2::SMALLINT 	--asma leve				when (wproto in (264,328,370) 	and wnumero=190			and campo='1')	or 	--asma leve
				when (wproto IN (371,337,333)   and wnumero=5			and campo='1')	THEN 2::SMALLINT 	--asma leve
				when (wproto in (264,328,370) 	and wnumero=191			and campo='1')	THEN 2::SMALLINT 	--asma moderada
				when (wproto in (141) 		and wnumero in (55,126,127)	and campo='1')	THEN 2::SMALLINT 	--asma moderada
				when (wproto in (224) 		and wnumero in (55)		and campo='1')	THEN 2::SMALLINT 	--asma moderada
				when (wproto in (145) 		and wnumero in (7,72,73)	and campo='1')	THEN 2::SMALLINT 	--asma moderada
				when (wproto IN (371,337,333) 	and wnumero IN (5)   		and campo='2')  THEN 2::SMALLINT	--asma moderada
				when (wproto IN (141,224) 	and wnumero IN (56)  		and campo='1')  THEN 2::SMALLINT	--asma severa
				when (wproto IN (264,370,328) 	and wnumero IN (192)  	 	and campo='1')  THEN 2::SMALLINT	--asma severa
				when (wproto in (145) 		and wnumero in (8)		and campo='1')	THEN 2::SMALLINT 	--asma severa
				when (wproto IN (371,337,333) 	and wnumero=5   		and campo='3')	THEN 2::SMALLINT	--asma severa
				END AS patologia
			,case
				when (wproto in (141,224) 	and wnumero=51 			and campo='1')	THEN 1::SMALLINT 	--sbor leve
				when (wproto in (370,264,328) 	and wnumero=194 		and campo='1')	THEN 1::SMALLINT	--sbor leve
				when (wproto in (145) 		and wnumero=9			and campo='1')	THEN 1::SMALLINT 	--sbor leve
				when (wproto in (264,328,370) 	and wnumero=195 		and campo='1')	THEN 2::SMALLINT 	--sbor moderada
				when (wproto in (141,224) 	and wnumero=52 			and campo='1')	THEN 2::SMALLINT 	--sbor moderada
				when (wproto in (145) 		and wnumero=10			and campo='1')	THEN 2::SMALLINT 	--sbor moderada
				when (wproto in (264,328,370) 	and wnumero=196			and campo='1')	THEN 3::SMALLINT 	--sbor severa
				when (wproto in (141,224) 	and wnumero=53 			and campo='1')	THEN 3::SMALLINT 	--sbor severa
				when (wproto in (145) 		and wnumero=11			and campo='1')	THEN 3::SMALLINT 	--sbor severa
				when (wproto in (141,224) 	and wnumero=54 			and campo='1')	THEN 1::SMALLINT 	--asma leve
				when (wproto in (145) 		and wnumero=6			and campo='1')	THEN 1::SMALLINT 	--asma leve				when (wproto in (264,328,370) 	and wnumero=190			and campo='1')	or 	--asma leve
				when (wproto IN (371,337,333)   and wnumero=5			and campo='1')	THEN 1::SMALLINT 	--asma leve
				when (wproto in (264,328,370) 	and wnumero=191			and campo='1')	THEN 2::SMALLINT 	--asma moderada
				when (wproto in (141) 		and wnumero in (55,126,127)	and campo='1')	THEN 2::SMALLINT 	--asma moderada
				when (wproto in (224) 		and wnumero in (55)		and campo='1')	THEN 2::SMALLINT 	--asma moderada
				when (wproto in (145) 		and wnumero in (7,72,73)	and campo='1')	THEN 2::SMALLINT 	--asma moderada
				when (wproto IN (371,337,333) 	and wnumero IN (5)   		and campo='2')  THEN 2::SMALLINT	--asma moderada
				when (wproto IN (141,224) 	and wnumero IN (56)  		and campo='1')  THEN 3::SMALLINT	--asma severa
				when (wproto IN (264,370,328) 	and wnumero IN (192)  	 	and campo='1')  THEN 3::SMALLINT	--asma severa
				when (wproto in (145) 		and wnumero in (8)		and campo='1')	THEN 3::SMALLINT 	--asma severa
				when (wproto IN (371,337,333) 	and wnumero=5   		and campo='3')	THEN 3::SMALLINT	--asma severa
				END AS patologia_leve_moderada_severa
		  from admomi.iddcop --select * from admomi.iddcop limit 10
		 where
			(
			(wproto in (141,224) 		and wnumero=51 			and campo='1')	or 	--sbor leve
			(wproto in (370,264,328) 	and wnumero=194 		and campo='1')	or 	--sbor leve
			(wproto in (145) 		and wnumero=9			and campo='1')	or 	--sbor leve
			(wproto in (264,328,370) 	and wnumero=195 		and campo='1')	or 	--sbor moderada
			(wproto in (141,224) 		and wnumero=52 			and campo='1')	or 	--sbor moderada
			(wproto in (145) 		and wnumero=10			and campo='1')	or 	--sbor moderada
			(wproto in (264,328,370) 	and wnumero=196			and campo='1')	or 	--sbor severa
			(wproto in (141,224) 		and wnumero=53 			and campo='1')	or 	--sbor severa
			(wproto in (145) 		and wnumero=11			and campo='1')	or 	--sbor severa
			(wproto in (141,224) 		and wnumero=54 			and campo='1')	or 	--asma leve
			(wproto in (145) 		and wnumero=6			and campo='1')	or 	--asma leve
			(wproto in (264,328,370) 	and wnumero=190			and campo='1')	or 	--asma leve
			(wproto IN (371,337,333)        and wnumero=5			and campo='1')	or	--asma leve
			(wproto in (264,328,370) 	and wnumero=191			and campo='1')	or 	--asma moderada
			(wproto in (141) 		and wnumero in (55,126,127)	and campo='1')	or 	--asma moderada
			(wproto in (224) 		and wnumero in (55)		and campo='1')	or 	--asma moderada
			(wproto in (145) 		and wnumero in (7,72,73)	and campo='1')	or 	--asma moderada
			(wproto IN (371,337,333) 	and wnumero IN (5)   		and campo='2')  or	--asma moderada
			(wproto IN (141,224) 		and wnumero IN (56)  	 	and campo='1')  or	--asma severa
			(wproto IN (264,370,328) 	and wnumero IN (192)  	 	and campo='1')  or	--asma severa
			(wproto in (145) 		and wnumero in (8)		and campo='1')	or 	--asma severa
			(wproto IN (371,337,333) 	and wnumero=5   		and campo='3')		--asma severa
			);
create index cubo_cv_01_asma_sbor_leve_moderada_severa_i_01 on rem_sp_h04_base.cubo_cv_01_asma_sbor_leve_moderada_severa(nif,fecha,medico_id) tablespace tb_index;
create index cubo_cv_01_asma_sbor_leve_moderada_severa_i_02 on rem_sp_h04_base.cubo_cv_01_asma_sbor_leve_moderada_severa(nif,fecha,medico_id,patologia,patologia_leve_moderada_severa) tablespace tb_index;
create index cubo_cv_01_asma_sbor_leve_moderada_severa_i_03 on rem_sp_h04_base.cubo_cv_01_asma_sbor_leve_moderada_severa(nif,fecha,medico_id,patologia) tablespace tb_index;

delete from rem_sp_h04_base.cubo_cv_01_asma_sbor_leve_moderada_severa where nif is null or fecha is null;

--DATO ANTIGUO DE CONTROL PATOLOGIA CRÓNICA
drop table if exists 	rem_sp_h04_base.cubo_cv_01_control_patologia_cronica; --select * from rem_sp_h03_base.paso0_asma_sbor_complemento_full where dx='EPOC' limit 100
create table 		rem_sp_h04_base.cubo_cv_01_control_patologia_cronica as
select nif2,fecha2,clinico_id,1::smallint as q from admomi.iddpag where dgp='CRONICOS' and nif2 is not null and fecha2 is not null and clinico_id is not null
union
select nif2,fecha2,medico_id,1::smallint as q from admomi.iddcop
 where nif2 is not null and fecha2 is not null and medico_id is not null
   and
	(
		(wproto in (298,195,157,107) and wnumero=8)
		or
		(wproto in (329,278) and wnumero=232)
	)
;
create index cubo_cv_01_control_patologia_cronica_i_01 on rem_sp_h04_base.cubo_cv_01_control_patologia_cronica(nif2,fecha2,clinico_id) tablespace tb_index;

delete from rem_sp_h04_base.cubo_cv_01_control_patologia_cronica where nif2 is null or fecha2 is null;

--salufam
drop table if exists 	rem_sp_h04_base.cubo_cv_01_salufam; --select * from rem_sp_h03_base.paso0_asma_sbor_complemento_full where dx='EPOC' limit 100
create table 		rem_sp_h04_base.cubo_cv_01_salufam as
with consulta as
		(
		select nif2,fecha2,clinico_id
			,btrim(replace(texto,',','.')) as texto
			,(replace(btrim(texto),',','.')::varchar ~ '^-?[0-9]*.?[0-9]*$') is_numeric
		    from admomi.iddpag where dgp='VALFAMSA' and nif2 is not null and fecha2 is not null
		)
		select distinct a.nif2,a.fecha2,a.clinico_id,round(texto::numeric,2) as salufam
			,b.responsable_de_familia_nif as familia_id
			,b.familia_tipo
		  from consulta as a
		  left join familia.cubo_familias as b on a.nif2=b.pac_nif
		 where a.is_numeric is true;
update rem_sp_h04_base.cubo_cv_01_salufam set familia_id=nif2, familia_tipo='UNITARIO' where familia_id is null;

create index cubo_cv_01_salufam_i_01 on rem_sp_h04_base.cubo_cv_01_salufam(nif2,fecha2,clinico_id) tablespace tb_index;
create index cubo_cv_01_salufam_i_02 on rem_sp_h04_base.cubo_cv_01_salufam(familia_id,fecha2) tablespace tb_index;

delete from rem_sp_h04_base.cubo_cv_01_salufam where nif2 is null or fecha2 is null;

--episodio -63
drop table if exists 	rem_sp_h04_base.cubo_cv_01_visita_Seguimiento_ciap_menos_63; --select * from rem_sp_h03_base.paso0_asma_sbor_complemento_full where dx='EPOC' limit 100
create table 		rem_sp_h04_base.cubo_cv_01_visita_Seguimiento_ciap_menos_63 as
select distinct nif2,fecha2,clinico_id from admomi.iddncu where ciap='-63'; --select * from admomi.iddncu where ciap='-63' limit 1000
create index cubo_cv_01_visita_Seguimiento_ciap_menos_63_i_01 on rem_sp_h04_base.cubo_cv_01_visita_Seguimiento_ciap_menos_63(nif2,fecha2,clinico_id) tablespace tb_index;

delete from rem_sp_h04_base.cubo_cv_01_visita_Seguimiento_ciap_menos_63 where nif2 is null or fecha2 is null;

--canales de comunicación
drop table if exists 	rem_sp_h04_base.cubo_cv_01_desea_ser_contatado_x_whatsapp; --select * from rem_sp_h03_base.paso0_asma_sbor_complemento_full where dx='EPOC' limit 100
create table 		rem_sp_h04_base.cubo_cv_01_desea_ser_contatado_x_whatsapp as
select distinct nif2,fecha2,medico_id clinico_id,1::smallint desea_ser_contatado_x_whatsapp from admomi.iddcop where wproto=398 and wnumero=32 and campo='1'; --whatsapp
create index cubo_cv_01_desea_ser_contatado_x_whatsapp_i_01 on rem_sp_h04_base.cubo_cv_01_desea_ser_contatado_x_whatsapp(nif2,fecha2,clinico_id) tablespace tb_index;

delete from rem_sp_h04_base.cubo_cv_01_desea_ser_contatado_x_whatsapp where nif2 is null or fecha2 is null;

---------------------------------
--INGRESO CDI X ENFERMERA
---------------------------------
drop table if exists 	rem_sp_h04_base.cubo_cv_01_ingreso_control_cdi; --select * from rem_sp_h04_base.cubo_cv_01_ingreso_control_cdi limit 100
create table 		rem_sp_h04_base.cubo_cv_01_ingreso_control_cdi as
select nif2 nif,clinico_id,fecha2 fecha,1::smallint as estado
  from admomi.iddpag
 where
	(
	dgp in ('*P355','*P388','*P398')
	or
		( dgp = 'INCONEGR' and texto='1'--campo desplegable de protocolo nuevo 1= ingreso
		)
	)
union
select nif2,clinico_id,fecha2,2::smallint as ingreso_control
  from admomi.iddpag
 where
	(
	dgp in ('*P357','*P391')
	or
		( dgp = 'INCONEGR' and texto='2'--campo desplegable de protocolo nuevo, 2 control
		)
	)
union
select nif2,clinico_id,fecha2,2::smallint as ingreso_control
  from admomi.iddpag
 where dgp = 'INCONEGR' and texto='3'--campo desplegable de protocolo nuevo, 2 control
	;
CREATE INDEX cubo_cv_01_ingreso_control_i_01 on rem_sp_h04_base.cubo_cv_01_ingreso_control_cdi using btree(nif,fecha,clinico_id) tablespace tb_index;
CREATE INDEX cubo_cv_01_ingreso_control_i_02 on rem_sp_h04_base.cubo_cv_01_ingreso_control_cdi using btree(nif,fecha,clinico_id,estado) tablespace tb_index;

delete from rem_sp_h04_base.cubo_cv_01_ingreso_control_cdi where nif is null or fecha is null;

--------------------------
-- INGRESO CDI ALTO RIESGO
--------------------------
drop table if exists 	rem_sp_h04_base.cubo_cv_01_ingreso_cdi_alto_riesgo; --select * from rem_sp_h04_base.cubo_cv_01_ingreso_cdi_alto_riesgo limit 100
create table 		rem_sp_h04_base.cubo_cv_01_ingreso_cdi_alto_riesgo as
with consulta as
		(
		select nif2 as nif,fecha2 as fecha,clinico_id from biancora.cubo_formularios where wproto in (398) and wnumero in (350) and campo='1' union
		select nif2,fecha2,clinico_id from biancora.cubo_formularios where wproto in (355,388)
		)
		select a.*,b.med_nombre from consulta as a,admomi.iddmed as b --select * from admomi.iddmed as b limit 10
		 where  a.clinico_id = b.id
		    and b.id is not null
		    and b.med_estamento_desc in ('ENFERMERA','MEDICO')
		    and length(b.id::text)>=7
		    and b.numcolegia like '%-%'
		    and a.nif is not null
		    and a.clinico_id is not null;
CREATE INDEX cubo_cv_01_ingreso_cdi_alto_riesgo_i_01 on rem_sp_h04_base.cubo_cv_01_ingreso_cdi_alto_riesgo using btree(nif,fecha,clinico_id) tablespace tb_index;

--------------------------
-- OTROS_EXAMENES
--------------------------
drop table if exists 	rem_sp_h04_base.cubo_cv_01_cdi_otros_examenes; --select * from rem_sp_h04_base.cubo_cv_01_ingreso_cdi_alto_riesgo limit 100
create table 		rem_sp_h04_base.cubo_cv_01_cdi_otros_examenes as
select distinct nif2 nif,fecha2 fecha,medico_id clinico_id,campo valor from admomi.iddcoe where wproto=398 and wnumero=249 and campo is not null;
CREATE INDEX cubo_cv_01_cdi_otros_examenes_i_01 on rem_sp_h04_base.cubo_cv_01_cdi_otros_examenes using btree(nif,fecha,clinico_id) tablespace tb_index;

delete from rem_sp_h04_base.cubo_cv_01_cdi_otros_examenes where nif is null or fecha is null;

-----------------------------------------------------------------------------
-- AREA PRINCIPAL DE ATENCION ENTREGADA CV RESP SM OTRO
-----------------------------------------------------------------------------
drop table if exists 	rem_sp_h04_base.cubo_cv_01_area_atencion_entregada; --select * from rem_sp_h04_base.cubo_cv_01_ingreso_cdi_alto_riesgo limit 100
create table 			rem_sp_h04_base.cubo_cv_01_area_atencion_entregada as
SELECT distinct nif2 nif,fecha2 fecha,medico_id clinico_id
		,case 	when 	(wproto=398 and wnumero=438 and campo='1') or
						(wproto=398 and wnumero=297 and campo='1') or
						(wproto=391 and wnumero=328 and campo='1') or
						(wproto=388 and wnumero=328 and campo='1') then 1::integer else 0::integer end area_atencion_entregada_CV
		,case 	when 	(wproto=398 and wnumero=438 and campo='3') or
						(wproto=398 and wnumero=298 and campo='1') or
						(wproto=391 and wnumero=329 and campo='1') or
						(wproto=388 and wnumero=329 and campo='1') then 1::integer else 0::integer end area_atencion_entregada_SM
		,case 	when 	(wproto=398 and wnumero=438 and campo='2') or
						(wproto=398 and wnumero=299 and campo='1') or
						(wproto=391 and wnumero=330 and campo='1') or
						(wproto=388 and wnumero=330 and campo='1') then 1::integer else 0::integer end area_atencion_entregada_RESP
		,case 	when 	(wproto=398 and wnumero=438 and campo='4') or
						(wproto=398 and wnumero=300 and campo='1') or
						(wproto=391 and wnumero=331 and campo='1') or
						(wproto=388 and wnumero=331 and campo='1') then 1::integer else 0::integer end area_atencion_entregada_OTRO
  from admomi.iddcop
 where
 		(
 			(wproto=398 AND wnumero in (438	 --todas,1_cv, 2_respiratorio, 3_salud_mental,4_Otros
 										,297 --cv
 										,298 --SM
 										,299 --Respirat
 										,300 --otros
 										) AND campo IS NOT null) or
			 (wproto=391 AND wnumero in (328 --cv
			 							,329 --sm
			 							,330 --RESP
			 							,331 --otros
 										) AND campo IS NOT null) or
 			(wproto=388 AND wnumero in (328  --cv
 										,329 --sm
			 							,330 --RESP
			 							,331 --otros
 										) AND campo IS NOT null)
 		);
CREATE INDEX cubo_cv_01_area_atencion_entregada_i_01 on rem_sp_h04_base.cubo_cv_01_area_atencion_entregada using btree(nif,fecha,clinico_id) tablespace tb_index;


---------------------------------------------------------------------------------------------------
-- TIPO DE RIESGO CUALITATIVO, riesgo q pone el clínico al momento de atender al paciente
---------------------------------------------------------------------------------------------------
drop table if exists 	rem_sp_h04_base.cubo_cv_01_tipo_riesgo_cualitativo;
create table 			rem_sp_h04_base.cubo_cv_01_tipo_riesgo_cualitativo as
select distinct nif2 nif,fecha2 fecha,medico_id clinico_id
		,campo::integer as tipo_riesgo_cualitativo
  from admomi.iddcop
 where wproto=398
   and wnumero=2
   and campo is not null
   union
select distinct nif2 nif,fecha2 fecha,medico_id clinico_id
		,campo::integer as tipo_riesgo
  from admomi.iddcop
 where wproto=398
   and wnumero=426
   and campo is not null;
CREATE INDEX cubo_cv_01_tipo_riesgo_cualitativo_i_01 on rem_sp_h04_base.cubo_cv_01_tipo_riesgo_cualitativo using btree(nif,fecha,clinico_id) tablespace tb_index;

---------------------------------------------------------------------------------------------------
-- MODALIDAD DE CONTACTO
---------------------------------------------------------------------------------------------------
drop table if exists 	rem_sp_h04_base.cubo_cv_01_modalidad_de_contacto_proximo_control;
create table 			rem_sp_h04_base.cubo_cv_01_modalidad_de_contacto_proximo_control as
select distinct nif2 nif,fecha2 fecha,medico_id clinico_id
		,campo::integer as modalidad_de_contacto_proximo_control --solicitud_de_examenes_s_n
  from admomi.iddcop
 where wproto=398
   and wnumero=447
   and campo is not null
   and campo<>'';
CREATE INDEX cubo_cv_01_modalidad_de_contacto_proximo_control_i_01 on rem_sp_h04_base.cubo_cv_01_modalidad_de_contacto_proximo_control using btree(nif,fecha,clinico_id) tablespace tb_index;

---------------------------------------------------------------------------------------------------
-- MODALIDAD DE CONTACTO
---------------------------------------------------------------------------------------------------
drop table if exists 	rem_sp_h04_base.cubo_cv_01_solicitud_de_examenes_s_n; --modalidad_de_contacto_proximo_control --solicitud_de_examenes_s_n
create table 			rem_sp_h04_base.cubo_cv_01_solicitud_de_examenes_s_n as
select distinct nif2 nif,fecha2 fecha,medico_id clinico_id
		,case when campo='S' then 1 else 0 end as solicitud_de_examenes_s_n
  from admomi.iddcop
 where wproto=398
   and wnumero=332
   and campo is not null
   and campo<>'';
CREATE INDEX cubo_cv_01_solicitud_de_examenes_s_n_i_01 on rem_sp_h04_base.cubo_cv_01_solicitud_de_examenes_s_n using btree(nif,fecha,clinico_id) tablespace tb_index;


----------------------------------------------------------------------------------
--crea tabla de hechos CV
----------------------------------------------------------------------------------

drop 		table if exists h_hechos_cardiocascular00;
create temp 	table 		h_hechos_cardiocascular00 as	select nif,fecha2 as fecha,clinico_id as medico_id    	from mapping.all_presion_arterial where nif is not null and fecha2 is not null; --select * from mapping.all_presion_arterial  limit 10
insert into h_hechos_cardiocascular00				select nif,fecha,clinico_id										from mapping.all_peso_talla_imc where nif is not null and fecha is not null; --select * from mapping.all_peso_talla_imc limit 10
insert into h_hechos_cardiocascular00				select nif,fecha,clinico_id										from mapping.all_circunferencia_cintura where nif is not null and fecha is not null; --select * from mapping.all_circunferencia_cintura limit 10
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id										from rem_sp_h04_base.m_patologias_cv where nif is not null and fecha is not null; --select * from rem_sp_h04_base.m_patologias_cv limit 100;
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id 										from rem_sp_h04_base.m_atenciones_cv where nif is not null and fecha is not null; --select * from rem_sp_h04_base.m_atenciones_cv limit 10
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id										from rem_sp_h04_base.cv_fecha_control_con_protocolo_04 where nif is not null and fecha is not null; --select * from rem_sp_h04_base.cv_fecha_control_con_protocolo_04 limit 100
insert into h_hechos_cardiocascular00				select nif2,fecha2,null as medico_id							from rem_sp_h04_base.examenes_de_laboratorio where nif is not null and fecha2 is not null; --select * from rem_sp_h04_base.examenes_de_laboratorio limit 100
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id		 								from rem_sp_h04_base.cubo_cv_01_pie_final where nif is not null and fecha is not null; --select * from rem_sp_h04_base.cubo_cv_01_pie_final limit 100
insert into h_hechos_cardiocascular00				select nif,fecha::date,null as medico_id 						from rem_sp_h04_base.farmacos_cv where nif is not null and fecha is not null; --select * from rem_sp_h04_base.farmacos_cv limit 100
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id				from rem_sp_h04_base.cubo_cv_01_riesgo_cv where nif is not null and fecha is not null; --select * from rem_sp_h04_base.cubo_cv_01_riesgo_cv
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id				from rem_sp_h04_base.cubo_cv_01_etapa_erc where nif is not null and fecha is not null; --select * from rem_sp_h04_base.cubo_cv_01_etapa_erc limit 10 --
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id				from rem_sp_h04_base.cubo_cv_01_curacion_avanzada where nif is not null and fecha is not null; --select * from rem_sp_h04_base.cubo_cv_01_curacion_avanzada limit 10
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id				from rem_sp_h04_base.cubo_cv_01_fondo_de_ojos where nif is not null and fecha is not null; --select * from rem_sp_h04_base.cubo_cv_01_fondo_de_ojos limit 10
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id				from rem_sp_h04_base.cubo_cv_01_vfg where nif is not null and fecha is not null; --select * from rem_sp_h04_base.cubo_cv_01_vfg limit 100
insert into h_hechos_cardiocascular00				select nif,fecha,null as medico_id			from rem_sp_h04_base.cubo_cv_01_rac where nif is not null and fecha is not null; --select * from rem_sp_h04_base.cubo_cv_01_rac limit 10
--insert into h_hechos_cardiocascular00				select nif,fecha,medico_id				from rem_sp_h04_base.cubo_cv_01_control_con; --select * from rem_sp_h04_base.cubo_cv_01_control_con limit 10
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id				from rem_sp_h04_base.cubo_cv_01_control_con_en where nif is not null and fecha is not null; --select * from rem_sp_h04_base.cubo_cv_01_control_en limit 10
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id       			from rem_sp_h04_base.cubo_cv_01_con_examenes_s_n where nif is not null and fecha is not null; --select * from rem_sp_h04_base.cubo_cv_01_con_examenes_s_n limit 10
insert into h_hechos_cardiocascular00				select nif,fecha2,clinico_id       			from rem_sp.patologias_cronicas_all_registro where nif is not null and fecha2 is not null; --select * from rem_sp_h04_base.cubo_cv_01_con_examenes_s_n limit 10
insert into h_hechos_cardiocascular00				select nif2,fecha2,clinico_id 				from rem_sp.patologias_cronicas_all_long where nif2 is not null and fecha is not null;
insert into h_hechos_cardiocascular00				select nif2,fecha,clinico_id				from rem_sp.acg_rub as a where nif2 is not null and fecha is not null;
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id				from rem_sp_h04_base.cubo_cv_01_consejeia_telefonica_automanejo where nif is not null and fecha is not null;
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id				from rem_sp_h04_base.cubo_cv_01_etapa_de_cambio where nif is not null and fecha is not null;
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id				from rem_sp_h04_base.cubo_cv_01_apoyo_automanejo where nif is not null and fecha is not null;
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id				from rem_sp_h04_base.cubo_cv_01_epoc_tipo where nif is not null and fecha is not null;
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id 				from rem_sp_h04_base.cubo_cv_01_asma_sbor_epoc_nivel_control where nif is not null and fecha is not null; --1 asma,2 sbor, 3 epoc
insert into h_hechos_cardiocascular00				select nif,fecha,medico_id				from rem_sp_h04_base.cubo_cv_01_asma_sbor_leve_moderada_severa where nif is not null and fecha is not null;
insert into h_hechos_cardiocascular00				select nif2,fecha2,clinico_id 			from rem_sp_h04_base.cubo_cv_01_control_patologia_cronica where nif2 is not null and fecha2 is not null;
insert into h_hechos_cardiocascular00				select nif2,fecha2,clinico_id 			from rem_sp_h04_base.cubo_cv_01_salufam where nif2 is not null and fecha2 is not null;
insert into h_hechos_cardiocascular00				select nif2,fecha2,clinico_id 			from rem_sp_h04_base.cubo_cv_01_visita_Seguimiento_ciap_menos_63 where nif2 is not null and fecha2 is not null;
insert into h_hechos_cardiocascular00				select nif2,fecha2,clinico_id 			from rem_sp.patologias_cronicas_all_long where nif2 is not null and fecha2 is not null;
insert into h_hechos_cardiocascular00				select nif2,fecha2,clinico_id 			from rem_sp_h04_base.cubo_cv_01_desea_ser_contatado_x_whatsapp where nif2 is not null and fecha2 is not null;
insert into h_hechos_cardiocascular00				select nif,fecha,clinico_id				from rem_sp_h04_base.cubo_cv_01_ingreso_control_cdi where nif is not null and fecha is not null;
insert into h_hechos_cardiocascular00				select nif,fecha,clinico_id				from rem_sp_h04_base.cubo_cv_01_ingreso_cdi_alto_riesgo where nif is not null and fecha is not null;
insert into h_hechos_cardiocascular00				select nif,fecha,clinico_id				from rem_sp_h04_base.cubo_cv_01_cdi_otros_examenes where nif is not null and fecha is not null;
insert into h_hechos_cardiocascular00				select nif,fecha,clinico_id				from rem_sp_h04_base.cubo_cv_01_area_atencion_entregada where nif is not null and fecha is not null;
insert into h_hechos_cardiocascular00				select nif,fecha,clinico_id				from rem_sp_h04_base.cubo_cv_01_tipo_riesgo_cualitativo where nif is not null and fecha is not null;
insert into h_hechos_cardiocascular00				select nif,fecha,clinico_id				from rem_sp_h04_base.cubo_cv_01_modalidad_de_contacto_proximo_control 	where nif is not null and fecha is not null;
insert into h_hechos_cardiocascular00				select nif,fecha,clinico_id				from rem_sp_h04_base.cubo_cv_01_solicitud_de_examenes_s_n 				where nif is not null and fecha is not null;
--select count(*) from (select *,row_number() over (partition by nif,fecha,medico_id) rownum from h_hechos_cardiocascular00 ) as a where rownum=1

drop table if exists h_hechos_cardiocascular;
create temp table h_hechos_cardiocascular as
select distinct a.*
	,b.responsable_de_familia_nif as familia_id
 from h_hechos_cardiocascular00 as a
  left join familia.cubo_familias as b on a.nif=b.pac_nif;

delete from h_hechos_cardiocascular where nif is null or fecha is null;

update h_hechos_cardiocascular set familia_id=nif where familia_id is null;

----------------------------------------------------------------------------------
-- CREA TABLA hechos
----------------------------------------------------------------------------------
--drop table if exists rem_sp_h04_base.h_hechos_cardiocascular2; --select * from rem_sp_h04_base.h_hechos_cardiocascular2 limit 10;
--create table rem_sp_h04_base.h_hechos_cardiocascular2 as
drop table if exists h_hechos_cardiocascular2_temp;
create temp table h_hechos_cardiocascular2_temp as
select  distinct
	a.nif
	,a.familia_id
	,a.fecha
	,a.medico_id
	,f.nacimiento::date as pac_fecha_nacimiento
	,b.valor	 as pas
	,c.valor	 as pad
	,case
		when b.valor>=36 and b.valor<140 and c.valor>=18.4 and c.valor<90  then 1::smallint
		when b.valor>=140 or c.valor>=90 then 0::smallint
		else null::smallint
		end as hta_compensacion
	,g.valor as examen_hba1c
	,n.valor as examen_colesterol_ldl
	,s.valor as examen_colesterol_hdl
	,t.valor as examen_colesterol_total
	,u.valor as examen_creatinina
	,v.valor as examen_trigliceridos
	,y.valor as examen_orina_completa_solicitada
	,z.valor as examen_orina_completa_firmada
	,aa.valor as examen_microalbuminuria
	,a28.valor	as examen_tsh
	,case
		when g.valor is not null and g.valor between 0 and 6.999 then 1::smallint
		when g.valor is not null and g.valor >=7 then 0::smallint
		else null::smallint end as dm_compensacion
	,h.hizo_eval_pie dm_pie_hizo_eval_pie
	,h.valor as dm_pie_valor
	,h.riesgo_id as dm_pie_riesgo
	,d.dm
	,d.hta
	,d.dl
	,d.ob
	,d.tb
	,d.erc
	,d.inf
	,d.ecv
	,i.dm as farmacos_retira_dm
	,i.hta as farmacos_retira_hta
	,i.dl as farmacos_retira_dl
	,i.estatina as farmacos_estatina
	,i.aspirina as farmacos_aspirina
	,i.ieca_ara as farmacos_ieca_ara
	,i.insulina as farmacos_insulina
	,e.age_medico
	,e.age_enfermera
	,e.age_nutricionista
	,j.fecha 				as fecha_atencion_con_protocolo_cv
	,j01.fecha 				as fecha_atencion_con_protocolo_prog_cdi
	,j02.fecha 				as fecha_atencion_con_protocolo_prog_cv
	,j03.fecha 				as fecha_atencion_con_protocolo_prog_sm
	,j04.fecha 				as fecha_atencion_con_protocolo_prog_resp
	,k.fecha				as fecha_1er_ingreso_cv
	,l.valor				as riesgo_cv
	,m.valor				as etapa_erc
	,o.valor				as curacion_avanzada
	,p.valor				as fondo_de_ojos
	,p.resultado_fondo_de_ojos
	,q.valor				as vfg
	,r.valor				as rac
	,w.control_con_id					as control_con
	,w.control_en_id					as control_en
	,w.fecha_proximo_control			as control_en_fecha_proximo_control
	,w02.control_con_id					as control_con_multim
	,w02.control_en_id					as control_en_multim
	,w02.fecha_proximo_control			as control_en_multim_fecha_proximo_control
	,a01.valor							as con_examenes_s_n
	,a02.peso							as peso
	,a02.talla							as talla
	,a02.imc							as imc
	,a03.valor							as circunferencia_cintura
	,a04.rub							as acg_rub_2015
	,a04.peso_acg						as acg_peso_2015
	,a05.patologias_cronicas
	,a06.protocolo						as tipo_atencion_cronica
	,a07.valor							as consejeria_automanejo_s_n
	,a07.mes							as consejeria_automanejo_mes_cuando
	,a08.valor							as etapa_de_cambio
	,a09.valor							as apoyo_automanejo_sms
	,a10.valor							as apoyo_automanejo_taller
	,a11.valor							as apoyo_automanejo_automonitoreo
	,a12.valor							as apoyo_automanejo_foro
	,a13.epoc_etapa						as epoc_etapa
	,a14.nivel_control					as asma_nivel_control
	,a15.nivel_control					as sbor_nivel_control
	,a16.nivel_control					as epoc_nivel_control
	,a17.patologia_leve_moderada_severa	as sbor_leve_moderada_severa
	,a18.patologia_leve_moderada_severa	as asma_leve_moderada_severa
	,a19.fecha2 						as control_patologia_cronica_date
	,a20.salufam
	,a21.fecha2 						as fecha_apertura_episodio_menos_63
	,null::date							as fecha_proximo_control
	,a22.desea_ser_contatado_x_whatsapp
	,a23.fecha 							as cdi_ingreso_control_fecha
	,a23.clinico_id						as cdi_ingreso_control_clinico_id
	,a23.estado							as cdi_ingreso_control_estado
	,a24.fecha 							as cdi_ingreso_alto_riesgo_fecha
	,a24.clinico_id						as cdi_ingreso_alto_riesgo_clinico_id
	,a25.valor							as cdi_otros_examenes
	,a26.area_atencion_entregada_cv
	,a26.area_atencion_entregada_sm
	,a26.area_atencion_entregada_resp
	,a26.area_atencion_entregada_otro
	,a27.tipo_riesgo_cualitativo
	,a29.modalidad_de_contacto_proximo_control
	,a30.solicitud_de_examenes_s_n
--1769-1671
	/*
	ALTER TABLE biancora.cubo_cronicos ADD COLUMN cdi_ingreso_alto_riesgo_fecha date;
	ALTER TABLE biancora.cubo_cronicos ADD COLUMN cdi_ingreso_alto_riesgo_clinico_id integer;
	ALTER TABLE biancora.cubo_cronicos ADD COLUMN cdi_ingreso_control_estado smallint;
	ALTER TABLE biancora.cubo_cronicos ADD COLUMN cdi_otros_examenes text;
	*/
  from h_hechos_cardiocascular as a
  	left join 	(
  				select nif,valor,fecha2,clinico_id,row_number() over (partition by nif,fecha2 order by fecha desc) rownum
  				 from mapping.all_presion_arterial 	--select distinct * from mapping.all_presion_arterial where nif=110 order by fecha2
  				 where tipo='PAS' and urgencia_s_n=0
  				) as b on a.nif=b.nif	and a.fecha=b.fecha2 and a.medico_id=b.clinico_id and b.rownum=1
  	left join 	(
  				select nif,valor,fecha2,clinico_id,row_number() over (partition by nif,fecha2 order by fecha desc) rownum
  				 from mapping.all_presion_arterial
  				 where tipo='PAD' and urgencia_s_n=0
			  	) as c on a.nif=c.nif	and a.fecha=c.fecha2 and a.medico_id=c.clinico_id and c.rownum=1
	left join 	(
				select medico_id,nif,fecha,hta,dm,dl,ob,tb,erc,inf,ecv,row_number() over (partition by nif,fecha order by fecha) rownum
				  from rem_sp_h04_base.m_patologias_cv
				 where hta=1 or dm=1 or dl=1 or ob=1 or tb=1 or erc=1 or inf=1 or ecv=1
				)	as d on a.nif=d.nif 	and a.fecha=d.fecha and d.rownum=1 and a.medico_id = d.medico_id
	left join 	(
  				select nif,fecha,medico_id,age_medico,age_enfermera,age_nutricionista,row_number() over (partition by nif,fecha order by fecha) rownum
  				  from rem_sp_h04_base.m_atenciones_cv
  				 where age_medico=1 or age_enfermera=1 or age_nutricionista=1
  	) as e on a.nif=e.nif and a.fecha=e.fecha    and a.medico_id=e.medico_id and e.rownum=1
	left join 	(
				select nif2,nacimiento,row_number() over (partition by nif2 order by fechactis) rownum
				  from admomi.iddpacpa3
				 where nif2 is not null and nacimiento is not null
	) as f on a.nif=f.nif2 and f.rownum=1
	left join	(
				select nif2,valor,fecha2,row_number() over (partition by nif2,fecha2 order by fecha) rownum
				  from rem_sp_h04_base.examenes_de_laboratorio
				 where examen='HEMOGLOBINA'
				   and valor is not null
	) as g on a.nif=g.nif2 and a.fecha=g.fecha2 and g.rownum=1
	left join	(
				select *,row_number() over (partition by nif,fecha order by fecha) rownum
				  from rem_sp_h04_base.cubo_cv_01_pie_final
	) as h on a.nif=h.nif 	and a.fecha=h.fecha     and a.medico_id=h.medico_id and h.rownum=1
  	left join	(
  				select *,row_number() over (partition by nif,fecha order by fecha) rownum
  				  from rem_sp_h04_base.farmacos_cv
  				  where dm=1 or hta=1 or dl=1 or estatina=1 or aspirina=1 or ieca_ara=1 or insulina=1
  	) as i on a.nif=i.nif and a.fecha=i.fecha and i.rownum=1
  	left join	(
				select nif2,valor,fecha2,row_number() over (partition by nif2,fecha2 order by fecha) rownum
				  from rem_sp_h04_base.examenes_de_laboratorio
				 where examen='COLESTEROL-LDL'
				   and valor is not null
	) as n on a.nif=n.nif2 and a.fecha=n.fecha2 and n.rownum=1
  	left join	(
				select nif2,valor,fecha2,row_number() over (partition by nif2,fecha2 order by fecha) rownum
				  from rem_sp_h04_base.examenes_de_laboratorio
				 where examen='COLESTEROL-HDL'
				   and valor is not null
	) as s on a.nif=s.nif2 and a.fecha=s.fecha2 and s.rownum=1
  	left join	(
				select nif2,valor,fecha2,row_number() over (partition by nif2,fecha2 order by fecha) rownum
				  from rem_sp_h04_base.examenes_de_laboratorio
				 where examen='COLESTEROL-TOTAL'
				   and valor is not null
	) as t on a.nif=t.nif2 and a.fecha=t.fecha2 and t.rownum=1
  	left join	(
				select nif2,valor,fecha2,row_number() over (partition by nif2,fecha2 order by fecha) rownum
				  from rem_sp_h04_base.examenes_de_laboratorio
				 where examen='TRIGLICERIDOS'
				   and valor is not null
	) as u on a.nif=u.nif2 and a.fecha=u.fecha2 and u.rownum=1
  	left join	(
				select nif2,valor,fecha2,row_number() over (partition by nif2,fecha2 order by fecha) rownum
				  from rem_sp_h04_base.examenes_de_laboratorio
				 where examen='COLESTEROL-LDL'
				   and valor is not null
	) as v on a.nif=v.nif2 and a.fecha=v.fecha2 and v.rownum=1
  	left join	(
				select nif2,valor,fecha2,row_number() over (partition by nif2,fecha2 order by fecha) rownum
				  from rem_sp_h04_base.examenes_de_laboratorio
				 where examen='ORINA COMPLETA' and origen = 'OMI AP' --solicitada
				   and valor is not null
	) as y on a.nif=y.nif2 and a.fecha=y.fecha2 and y.rownum=1
	left join	(
				select nif2,valor,fecha2,row_number() over (partition by nif2,fecha2 order by fecha) rownum
				  from rem_sp_h04_base.examenes_de_laboratorio
				 where examen='ORINA COMPLETA' and origen = 'LAB UC' --informada
				   and valor is not null
	) as z on a.nif=z.nif2 and a.fecha=z.fecha2 and z.rownum=1
	left join	(
				select nif2,valor,fecha2,row_number() over (partition by nif2,fecha2 order by fecha) rownum
				  from rem_sp_h04_base.examenes_de_laboratorio
				 where examen='MICROALBUMINURIA'
				   and valor is not null
	) as aa on a.nif=aa.nif2 and a.fecha=aa.fecha2 and aa.rownum=1
	left join 	(
				select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cv_fecha_control_con_protocolo_04
	) as j on a.nif=j.nif and a.fecha=j.fecha and a.medico_id=j.medico_id and j.rownum=1
	left join 	(
				select nif,medico_id,fecha,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cv_fecha_control_con_protocolo_04 where programa=0 --cdi
	) as j01 on a.nif=j01.nif and a.fecha=j01.fecha and a.medico_id=j01.medico_id and j01.rownum=1
	left join 	(
				select nif,medico_id,fecha,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cv_fecha_control_con_protocolo_04 where programa=1 --cv
	) as j02 on a.nif=j02.nif and a.fecha=j02.fecha and a.medico_id=j02.medico_id and j02.rownum=1
	left join 	(
				select nif,medico_id,fecha,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cv_fecha_control_con_protocolo_04 where programa=2 --csm
	) as j03 on a.nif=j03.nif and a.fecha=j03.fecha and a.medico_id=j03.medico_id and j03.rownum=1
	left join 	(
				select nif,medico_id,fecha,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cv_fecha_control_con_protocolo_04 where programa=3 --resp
	) as j04 on a.nif=j04.nif and a.fecha=j04.fecha and a.medico_id=j04.medico_id and j04.rownum=1
	left join	(
				select nif,fecha,row_number() over (partition by nif order by fecha) rownum
				  from rem_sp_h04_base.m_patologias_cv
				 where hta=1 or dm=1 or dl=1 or erc=1 or inf=1 or ecv=1
	) as k on a.nif=k.nif and a.fecha=k.fecha and k.rownum=1 --35seg
	left join	(
				select nif,fecha,medico_id,valor,row_number() over (partition by nif,fecha order by fecha) rownum
				  from rem_sp_h04_base.cubo_cv_01_riesgo_cv
				 where valor is not null
	) as l on a.nif=l.nif and a.fecha=l.fecha and a.medico_id=l.medico_id
	left join 	(
				select *,row_number() over (partition by nif,fecha order by fecha) rownum
				  from rem_sp_h04_base.cubo_cv_01_etapa_erc
				 where valor is not null
	)as m on a.nif=m.nif and a.fecha=m.fecha and a.medico_id=m.medico_id and m.rownum=1 --40seg
	left join 	(
				select *,row_number() over (partition by nif,fecha order by fecha) rownum
				  from rem_sp_h04_base.cubo_cv_01_curacion_avanzada
				 where valor is not null
	)as o on a.nif=o.nif and a.fecha=o.fecha and a.medico_id=o.medico_id and o.rownum=1 --40seg
	left join 	(
				select *,row_number() over (partition by nif,fecha order by fecha) rownum
				  from rem_sp_h04_base.cubo_cv_01_fondo_de_ojos
				 where valor is not null
	)as p on a.nif=p.nif and a.fecha=p.fecha and a.medico_id=p.medico_id and p.rownum=1 --40seg
	left join 	(
				select *,row_number() over (partition by nif,fecha order by fecha) rownum
				  from rem_sp_h04_base.cubo_cv_01_vfg
				 where valor is not null
	)as q on a.nif=q.nif and a.fecha=q.fecha and a.medico_id=q.medico_id and q.rownum=1 --40seg
	left join 	(
				select *,row_number() over (partition by nif,fecha order by fecha) rownum
				  from rem_sp_h04_base.cubo_cv_01_rac
				 where valor is not null
	)as r on a.nif=r.nif and a.fecha=r.fecha and r.rownum=1 --40seg
	left join	(
				select nif,fecha,medico_id,control_con_id,control_en_id,fecha_proximo_control
						,row_number() over (partition by nif,fecha order by fecha) rownum
				  from rem_sp_h04_base.cubo_cv_01_control_con_en
				where protocolo='CV'
	) as w on a.nif=w.nif and a.fecha=w.fecha and a.medico_id=w.medico_id and w.rownum=1
	left join	(
				select nif,fecha,medico_id,control_con_id,control_en_id,fecha_proximo_control
						,row_number() over (partition by nif,fecha order by fecha) rownum
				  from rem_sp_h04_base.cubo_cv_01_control_con_en
				where protocolo='MULTIM'
	) as w02 on a.nif=w02.nif and a.fecha=w02.fecha and a.medico_id=w02.medico_id and w02.rownum=1
	left join	(
				select *,row_number() over (partition by nif,fecha order by fecha) rownum
				  from rem_sp_h04_base.cubo_cv_01_con_examenes_s_n
				 where valor=1
	) as a01 on a.nif=a01.nif and a.fecha=a01.fecha	and a.medico_id=a01.medico_id and a01.rownum=1
  	left join 	(
  				select nif,fecha,clinico_id,peso,talla,imc
  						,row_number() over (partition by nif,fecha order by fecha) rownum
  				  from mapping.all_peso_talla_imc
  	) as a02 on a.nif=a02.nif and a.fecha=a02.fecha 	and a.medico_id=a02.clinico_id and a01.rownum=1 --51seg
	left join 	(select * ,row_number() over (partition by nif,fecha order by fecha) rownum from mapping.all_circunferencia_cintura where valor is not null 													) as a03 on a.nif=a03.nif  	and a.fecha=a03.fecha 	and a.medico_id=a03.clinico_id 	and a03.rownum=1
  	left join 	(select * ,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp.acg_rub where rub is not null																			) as a04 on a.nif=a04.nif2 	and a.fecha=a04.fecha 	and a.medico_id=a04.clinico_id 	and a04.rownum=1
  	left join 	(select nif,patologias_cronicas ,row_number() over (partition by nif) rownum from rem_sp_h04_base.cubo_cv_01_all_patologicas_cronicas where patologias_cronicas is not null 					) as a05 on a.nif=a05.nif	and a05.rownum=1
  	left join 	(select *,row_number() over (partition by nif) rownum from rem_sp_h04_base.cv_fecha_control_con_protocolo_04 where protocolo is not null														)as a06 on a.nif=a06.nif 	and a.fecha=a06.fecha   and a.medico_id=a06.medico_id  	and a06.rownum=1--select * from rem_sp_h04_base.cv_fecha_control_con_protocolo_04	 limit 100
  	left join	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_consejeia_telefonica_automanejo where valor is not null								) as a07 on a.nif=a07.nif 	and a.fecha=a07.fecha   and a.medico_id=a07.medico_id 	and a07.rownum=1
  	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_etapa_de_cambio where  valor is not null												) as a08 on a.nif=a08.nif 	and a.fecha=a08.fecha   and a.medico_id=a08.medico_id	and a08.rownum=1
  	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_apoyo_automanejo where VALOR is not null and tipo_apoyo_automanejo='SMS'				) as a09 on a.nif=a09.nif 	and a.fecha=a09.fecha   and a.medico_id=a09.medico_id 	and a09.rownum=1
  	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_apoyo_automanejo where VALOR is not null and tipo_apoyo_automanejo='TALLER'			) as a10 on a.nif=a10.nif 	and a.fecha=a10.fecha   and a.medico_id=a10.medico_id 	and a10.rownum=1
 	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_apoyo_automanejo where VALOR is not null and tipo_apoyo_automanejo='AUTOMONITOREO'	) as a11 on a.nif=a11.nif 	and a.fecha=a11.fecha   and a.medico_id=a11.medico_id 	and a11.rownum=1
  	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_apoyo_automanejo where VALOR is not null and tipo_apoyo_automanejo='FORO'			) as a12 on a.nif=a12.nif 	and a.fecha=a12.fecha   and a.medico_id=a12.medico_id 	and a12.rownum=1
	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_epoc_tipo where epoc_etapa is not null												) as a13 on a.nif=a13.nif 	and a.fecha=a13.fecha   and a.medico_id=a13.medico_id
  	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_asma_sbor_epoc_nivel_control	where patologia=1										) as a14 on a.nif=a14.nif 	and a.fecha=a14.fecha   and a.medico_id=a14.medico_id 	and a14.rownum=1
  	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_asma_sbor_epoc_nivel_control	where patologia=2										) as a15 on a.nif=a15.nif 	and a.fecha=a15.fecha   and a.medico_id=a15.medico_id 	and a15.rownum=1
  	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_asma_sbor_epoc_nivel_control	where patologia=3										) as a16 on a.nif=a16.nif 	and a.fecha=a16.fecha   and a.medico_id=a16.medico_id 	and a16.rownum=1
  	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_asma_sbor_leve_moderada_severa	where patologia=1									) as a17 on a.nif=a17.nif 	and a.fecha=a17.fecha   and a.medico_id=a17.medico_id 	and a17.rownum=1
	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_asma_sbor_leve_moderada_severa	where patologia=2									) as a18 on a.nif=a18.nif 	and a.fecha=a18.fecha   and a.medico_id=a18.medico_id 	and a18.rownum=1
  	left join 	(select *,row_number() over (partition by nif2,fecha2 order by fecha2) rownum from rem_sp_h04_base.cubo_cv_01_control_patologia_cronica															) as a19 on a.nif=a19.nif2 and a.fecha=a19.fecha2 and a.medico_id=a19.clinico_id 	and	a19.rownum=1
  	left join 	(select *,row_number() over (partition by nif2,fecha2 order by fecha2) rownum from rem_sp_h04_base.cubo_cv_01_salufam where salufam is not null													) as a20 on a.familia_id=a20.familia_id 	      and a.fecha=a20.fecha2			and	a20.rownum=1
  	left join 	(select *,row_number() over (partition by nif2,fecha2 order by fecha2) rownum from rem_sp_h04_base.cubo_cv_01_visita_Seguimiento_ciap_menos_63													) as a21 on a.nif=a21.nif2 and a.fecha=a21.fecha2 and a.medico_id=a21.clinico_id 	and	a21.rownum=1
  	left join 	(select *,row_number() over (partition by nif2,fecha2 order by fecha2) rownum from rem_sp_h04_base.cubo_cv_01_desea_ser_contatado_x_whatsapp													) as a22 on a.nif=a22.nif2 and a.fecha=a22.fecha2 and a.medico_id=a22.clinico_id 	and	a22.rownum=1
  	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_ingreso_control_cdi																	) as a23 on a.nif=a23.nif  and a.fecha=a23.fecha  and a.medico_id=a23.clinico_id 	and	a23.rownum=1
  	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_ingreso_cdi_alto_riesgo																) as a24 on a.nif=a24.nif  and a.fecha=a24.fecha  and a.medico_id=a24.clinico_id 	and	a24.rownum=1
 	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_cdi_otros_examenes																	) as a25 on a.nif=a25.nif  and a.fecha=a25.fecha  and a.medico_id=a25.clinico_id 	and	a25.rownum=1
 	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_area_atencion_entregada																) as a26 on a.nif=a26.nif  and a.fecha=a26.fecha  and a.medico_id=a26.clinico_id 	and	a26.rownum=1
 	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_tipo_riesgo_cualitativo																) as a27 on a.nif=a27.nif  and a.fecha=a27.fecha  and a.medico_id=a27.clinico_id 	and	a27.rownum=1
	left join	(
				select nif2,valor,fecha2,row_number() over (partition by nif2,fecha2 order by fecha) rownum
				  from rem_sp_h04_base.examenes_de_laboratorio --select * from rem_sp_h04_base.examenes_de_laboratorio where examen like '%TSH%' limit 100
				 where examen='TSH'
				   and valor is not null
				) as a28
				  on a.nif=a28.nif2
				 and a.fecha=a28.fecha2
				 and aa.rownum=1
	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_modalidad_de_contacto_proximo_control												) as a29 on a.nif=a29.nif  and a.fecha=a29.fecha  and a.medico_id=a29.clinico_id 	and	a29.rownum=1
	left join 	(select *,row_number() over (partition by nif,fecha order by fecha) rownum from rem_sp_h04_base.cubo_cv_01_solicitud_de_examenes_s_n															) as a30 on a.nif=a30.nif  and a.fecha=a30.fecha  and a.medico_id=a30.clinico_id 	and	a30.rownum=1

	--rem_sp_h04_base.cubo_cv_01_solicitud_de_examenes_s_n; --modalidad_de_contacto_proximo_control --solicitud_de_examenes_s_n
;


--select distinct nif from rem_sp_h04_base.h_hechos_cardiocascular2 where fecha between '20180101' and '20181231' and farmacos_insulina=1

/* --se hace mas arriba en la temporal de dimension
update rem_sp_h04_base.h_hechos_cardiocascular2 set fecha_proximo_control=case
				when ((((date_part('year',fecha)::text||'01'::Text||'01'::text)::Date+(date_part('day',fecha)::Text||' days')::interval)::date
					+(control_en::Text||' mons')::interval)::Date-'1 mons 1 days'::interval)::Date > fecha
				then ((((date_part('year',fecha)::text||'01'::Text||'01'::text)::Date+(date_part('day',fecha)::Text||' days')::interval)::date
					+(control_en::Text||' mons')::interval)::Date-'1 mons 1 days'::interval)::Date
				else ((((date_part('year',fecha)::text||'01'::Text||'01'::text)::Date+(date_part('day',fecha)::Text||' days')::interval)::date
					+(control_en::Text||' mons')::interval)::Date-'1 mons 1 days'::interval+'1 year'::interval)::Date
				end where control_en is not null;
*/
--DROP TABLE IF EXISTS rem_sp_h04.cubo_cardiovascular;
--create table rem_sp_h04.cubo_cardiovascular as
--DROP TABLE IF EXISTS h_hechos_cardiocascular3_temp;
--create temp table h_hechos_cardiocascular3_temp as
--select distinct * from h_hechos_cardiocascular2_temp; --select * from rem_sp_h04_base.h_hechos_cardiocascular2 where control_en is not null  limit 10;

--select distinct nif from rem_sp_h04.cubo_cardiovascular where fecha between '20180101' and '20181231' and farmacos_insulina=1

--delete from rem_sp_h04.cubo_cardiovascular where nif is null or fecha is null;

/*
update rem_sp_h04.cubo_cardiovascular set tipo_atencion_cronica=null;
update rem_sp_h04.cubo_cardiovascular set tipo_atencion_cronica=rem_sp_h04_base.cv_fecha_control_con_protocolo_04.programa
  from rem_sp_h04_base.cv_fecha_control_con_protocolo_04
 where rem_sp_h04_base.cv_fecha_control_con_protocolo_04.nif		= rem_sp_h04.cubo_cardiovascular.nif
   and rem_sp_h04_base.cv_fecha_control_con_protocolo_04.medico_id	= rem_sp_h04.cubo_cardiovascular.medico_id
   and rem_sp_h04_base.cv_fecha_control_con_protocolo_04.fecha		= rem_sp_h04.cubo_cardiovascular.fecha  --select * from rem_sp_h04_base.cv_fecha_control_con_protocolo_04 limit 10
*/

--SELECT control_con from rem_sp_h04_base.h_hechos_cardiocascular2 where control_con is not null group by 1 order by 1
--SELECT control_con_multim from rem_sp_h04_base.h_hechos_cardiocascular2 where control_con_multim is not null group by 1 order by 1


/*
update biancora.cubo_cronicos set fecha_proximo_control=case
				when ((((date_part('year',fecha)::text||'01'::Text||'01'::text)::Date+(date_part('day',fecha)::Text||' days')::interval)::date
					+(control_en::Text||' mons')::interval)::Date-'1 mons 1 days'::interval)::Date > fecha
				then ((((date_part('year',fecha)::text||'01'::Text||'01'::text)::Date+(date_part('day',fecha)::Text||' days')::interval)::date
					+(control_en::Text||' mons')::interval)::Date-'1 mons 1 days'::interval)::Date
				else ((((date_part('year',fecha)::text||'01'::Text||'01'::text)::Date+(date_part('day',fecha)::Text||' days')::interval)::date
					+(control_en::Text||' mons')::interval)::Date-'1 mons 1 days'::interval+'1 year'::interval)::Date
				end where control_en is not null

*/

--select * from rem_sp_h04.cubo_cardiovascular limit 100

truncate table biancora.cubo_cronicos; --select * from biancora.cubo_cronicos

drop index if exists biancora.cubo_cronicos_i_01;
drop index if exists biancora.cubo_cronicos_i_02;
drop index if exists biancora.cubo_cronicos_i_03;
drop index if exists biancora.cubo_cronicos_i_04;
drop index if exists biancora.cubo_cronicos_i_05;
drop index if exists biancora.cubo_cronicos_i_06;
drop index if exists biancora.cubo_cronicos_i_07;
drop index if exists biancora.cubo_cronicos_i_08;

--ALTER TABLE biancora.cubo_cronicos add column cdi_otros_examenes text;-
--drop table biancora.cubo_cronicos;create table biancora.cubo_cronicos as select * from h_hechos_cardiocascular2_temp;
--drop table biancora.cubo_cronicos;create table biancora.cubo_cronicos as select * from h_hechos_cardiocascular2_temp;
--insert into biancora.cubo_cronicos select * from rem_sp_h04.cubo_cardiovascular;
--create table usr_edox.h_hechos_cardiocascular2_temp as select * from h_hechos_cardiocascular2_temp limit 1000
insert into biancora.cubo_cronicos select * from h_hechos_cardiocascular2_temp; --select count(*) from rem_sp_h04.cubo_cardiovascular;
drop table if exists rem_sp_h04.cubo_cardiovascular; create table rem_sp_h04.cubo_cardiovascular as select * from h_hechos_cardiocascular2_temp;
--alter table biancora.cubo_CRONICOS ADD COLUMN desea_ser_contatado_x_whatsapp SMALLINT
/*
ALTER TABLE biancora.cubo_cronicos
  ADD CONSTRAINT cubo_cronicos_fk_control_con FOREIGN KEY (control_con)
      REFERENCES biancora.cubo_cronicos_dim_proximo_control_con (id) MATCH SIMPLE --select * from biancora.cubo_cronicos_dim_proximo_control_con limit 100
      ON UPDATE NO ACTION ON DELETE NO ACTION;
*/

create index cubo_cronicos_i_01 on biancora.cubo_cronicos using btree(nif,fecha) tablespace tb_index;
create index cubo_cronicos_i_02 on biancora.cubo_cronicos using btree(nif,fecha,control_con,control_en) tablespace tb_index;
create index cubo_cronicos_i_03 on biancora.cubo_cronicos using btree(fecha_apertura_episodio_menos_63) tablespace tb_index;
create index cubo_cronicos_i_04 on biancora.cubo_cronicos using btree(nif) tablespace tb_index;
create index cubo_cronicos_i_05 on biancora.cubo_cronicos using btree(fecha_proximo_control) tablespace tb_index;
create index cubo_cronicos_i_06 on biancora.cubo_cronicos using btree(nif,fecha_proximo_control) tablespace tb_index;
create index cubo_cronicos_i_07 on biancora.cubo_cronicos using btree(desea_ser_contatado_x_whatsapp) tablespace tb_index;
create index cubo_cronicos_i_08 on biancora.cubo_cronicos using btree(tipo_riesgo_cualitativo) tablespace tb_index;

--select nif, from biancora.cubo_cronicos group by 1,2 order by 2 desc

drop index if exists rem_sp_h04.cubo_cardiovascular_i_001; CREATE INDEX cubo_cardiovascular_i_001  ON rem_sp_h04.cubo_cardiovascular USING btree(nif) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_002; CREATE INDEX cubo_cardiovascular_i_002  ON rem_sp_h04.cubo_cardiovascular USING btree(fecha) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_003; CREATE INDEX cubo_cardiovascular_i_003  ON rem_sp_h04.cubo_cardiovascular USING btree(medico_id) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_004; CREATE INDEX cubo_cardiovascular_i_004  ON rem_sp_h04.cubo_cardiovascular USING btree(pac_fecha_nacimiento) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_005; CREATE INDEX cubo_cardiovascular_i_005  ON rem_sp_h04.cubo_cardiovascular USING btree(pas) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_006; CREATE INDEX cubo_cardiovascular_i_006  ON rem_sp_h04.cubo_cardiovascular USING btree(pad) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_007; CREATE INDEX cubo_cardiovascular_i_007  ON rem_sp_h04.cubo_cardiovascular USING btree(hta_compensacion) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_008; CREATE INDEX cubo_cardiovascular_i_008  ON rem_sp_h04.cubo_cardiovascular USING btree(examen_hba1c) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_009; CREATE INDEX cubo_cardiovascular_i_009  ON rem_sp_h04.cubo_cardiovascular USING btree(examen_colesterol_ldl) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_010; CREATE INDEX cubo_cardiovascular_i_010  ON rem_sp_h04.cubo_cardiovascular USING btree(examen_colesterol_hdl) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_011; CREATE INDEX cubo_cardiovascular_i_011  ON rem_sp_h04.cubo_cardiovascular USING btree(examen_colesterol_total) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_012; CREATE INDEX cubo_cardiovascular_i_012  ON rem_sp_h04.cubo_cardiovascular USING btree(examen_creatinina) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_013; CREATE INDEX cubo_cardiovascular_i_013  ON rem_sp_h04.cubo_cardiovascular USING btree(examen_trigliceridos) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_014; CREATE INDEX cubo_cardiovascular_i_014  ON rem_sp_h04.cubo_cardiovascular USING btree(examen_orina_completa_solicitada) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_015; CREATE INDEX cubo_cardiovascular_i_015  ON rem_sp_h04.cubo_cardiovascular USING btree(examen_orina_completa_firmada) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_016; CREATE INDEX cubo_cardiovascular_i_016  ON rem_sp_h04.cubo_cardiovascular USING btree(examen_microalbuminuria) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_017; CREATE INDEX cubo_cardiovascular_i_017  ON rem_sp_h04.cubo_cardiovascular USING btree(dm_compensacion) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_018; CREATE INDEX cubo_cardiovascular_i_018  ON rem_sp_h04.cubo_cardiovascular USING btree(dm_pie_hizo_eval_pie) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_019; CREATE INDEX cubo_cardiovascular_i_019  ON rem_sp_h04.cubo_cardiovascular USING btree(dm_pie_valor) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_020; CREATE INDEX cubo_cardiovascular_i_020  ON rem_sp_h04.cubo_cardiovascular USING btree(dm_pie_riesgo) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_021; CREATE INDEX cubo_cardiovascular_i_021  ON rem_sp_h04.cubo_cardiovascular USING btree(dm) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_022; CREATE INDEX cubo_cardiovascular_i_022  ON rem_sp_h04.cubo_cardiovascular USING btree(hta) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_023; CREATE INDEX cubo_cardiovascular_i_023  ON rem_sp_h04.cubo_cardiovascular USING btree(dl) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_024; CREATE INDEX cubo_cardiovascular_i_024  ON rem_sp_h04.cubo_cardiovascular USING btree(ob) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_025; CREATE INDEX cubo_cardiovascular_i_025  ON rem_sp_h04.cubo_cardiovascular USING btree(tb) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_026; CREATE INDEX cubo_cardiovascular_i_026  ON rem_sp_h04.cubo_cardiovascular USING btree(erc) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_027; CREATE INDEX cubo_cardiovascular_i_027  ON rem_sp_h04.cubo_cardiovascular USING btree(inf) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_028; CREATE INDEX cubo_cardiovascular_i_028  ON rem_sp_h04.cubo_cardiovascular USING btree(ecv) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_029; CREATE INDEX cubo_cardiovascular_i_029  ON rem_sp_h04.cubo_cardiovascular USING btree(farmacos_retira_dm) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_030; CREATE INDEX cubo_cardiovascular_i_030  ON rem_sp_h04.cubo_cardiovascular USING btree(farmacos_retira_hta) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_031; CREATE INDEX cubo_cardiovascular_i_031  ON rem_sp_h04.cubo_cardiovascular USING btree(farmacos_retira_dl) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_032; CREATE INDEX cubo_cardiovascular_i_032  ON rem_sp_h04.cubo_cardiovascular USING btree(farmacos_estatina) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_033; CREATE INDEX cubo_cardiovascular_i_033  ON rem_sp_h04.cubo_cardiovascular USING btree(farmacos_aspirina) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_034; CREATE INDEX cubo_cardiovascular_i_034  ON rem_sp_h04.cubo_cardiovascular USING btree(farmacos_ieca_ara) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_035; CREATE INDEX cubo_cardiovascular_i_035  ON rem_sp_h04.cubo_cardiovascular USING btree(farmacos_insulina) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_036; CREATE INDEX cubo_cardiovascular_i_036  ON rem_sp_h04.cubo_cardiovascular USING btree(age_medico) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_037; CREATE INDEX cubo_cardiovascular_i_037  ON rem_sp_h04.cubo_cardiovascular USING btree(age_enfermera) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_038; CREATE INDEX cubo_cardiovascular_i_038  ON rem_sp_h04.cubo_cardiovascular USING btree(age_nutricionista) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_039; CREATE INDEX cubo_cardiovascular_i_039  ON rem_sp_h04.cubo_cardiovascular USING btree(fecha_atencion_con_protocolo_cv) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_040; CREATE INDEX cubo_cardiovascular_i_040  ON rem_sp_h04.cubo_cardiovascular USING btree(fecha_1er_ingreso_cv) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_041; CREATE INDEX cubo_cardiovascular_i_041  ON rem_sp_h04.cubo_cardiovascular USING btree(riesgo_cv) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_042; CREATE INDEX cubo_cardiovascular_i_042  ON rem_sp_h04.cubo_cardiovascular USING btree(etapa_erc) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_043; CREATE INDEX cubo_cardiovascular_i_043  ON rem_sp_h04.cubo_cardiovascular USING btree(curacion_avanzada) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_044; CREATE INDEX cubo_cardiovascular_i_044  ON rem_sp_h04.cubo_cardiovascular USING btree(fondo_de_ojos) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_045; CREATE INDEX cubo_cardiovascular_i_045  ON rem_sp_h04.cubo_cardiovascular USING btree(vfg) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_046; CREATE INDEX cubo_cardiovascular_i_046  ON rem_sp_h04.cubo_cardiovascular USING btree(rac) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_047; CREATE INDEX cubo_cardiovascular_i_047  ON rem_sp_h04.cubo_cardiovascular USING btree(control_con) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_048; CREATE INDEX cubo_cardiovascular_i_048  ON rem_sp_h04.cubo_cardiovascular USING btree(control_en) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_049; CREATE INDEX cubo_cardiovascular_i_049  ON rem_sp_h04.cubo_cardiovascular USING btree(con_examenes_s_n) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_050; CREATE INDEX cubo_cardiovascular_i_050  ON rem_sp_h04.cubo_cardiovascular USING btree(peso) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_051; CREATE INDEX cubo_cardiovascular_i_051  ON rem_sp_h04.cubo_cardiovascular USING btree(talla) tablespace tb_index;
drop index if exists rem_sp_h04.cubo_cardiovascular_i_052; CREATE INDEX cubo_cardiovascular_i_052  ON rem_sp_h04.cubo_cardiovascular USING btree(imc);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_053; CREATE INDEX cubo_cardiovascular_i_053  ON rem_sp_h04.cubo_cardiovascular USING btree(circunferencia_cintura);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_054; CREATE INDEX cubo_cardiovascular_i_054  ON rem_sp_h04.cubo_cardiovascular USING btree(acg_rub_2015);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_055; CREATE INDEX cubo_cardiovascular_i_055  ON rem_sp_h04.cubo_cardiovascular USING btree(acg_peso_2015);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_056; CREATE INDEX cubo_cardiovascular_i_056  ON rem_sp_h04.cubo_cardiovascular USING gin(patologias_cronicas);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_056; CREATE INDEX cubo_cardiovascular_i_056  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,pac_fecha_nacimiento);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_057; CREATE INDEX cubo_cardiovascular_i_057  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,pas);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_058; CREATE INDEX cubo_cardiovascular_i_058  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,pad);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_059; CREATE INDEX cubo_cardiovascular_i_059  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,hta_compensacion);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_060; CREATE INDEX cubo_cardiovascular_i_060  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,examen_hba1c);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_061; CREATE INDEX cubo_cardiovascular_i_061  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,examen_colesterol_ldl);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_062; CREATE INDEX cubo_cardiovascular_i_062  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,examen_colesterol_hdl);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_063; CREATE INDEX cubo_cardiovascular_i_063  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,examen_colesterol_total);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_064; CREATE INDEX cubo_cardiovascular_i_064  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,examen_creatinina);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_065; CREATE INDEX cubo_cardiovascular_i_065  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,examen_trigliceridos);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_066; CREATE INDEX cubo_cardiovascular_i_066  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,examen_orina_completa_solicitada);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_067; CREATE INDEX cubo_cardiovascular_i_067  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,examen_orina_completa_firmada);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_068; CREATE INDEX cubo_cardiovascular_i_068  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,examen_microalbuminuria);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_069; CREATE INDEX cubo_cardiovascular_i_069  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,dm_compensacion);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_070; CREATE INDEX cubo_cardiovascular_i_070  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,dm_pie_hizo_eval_pie);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_071; CREATE INDEX cubo_cardiovascular_i_071  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,dm_pie_valor);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_072; CREATE INDEX cubo_cardiovascular_i_072  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,dm_pie_riesgo);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_073; CREATE INDEX cubo_cardiovascular_i_073  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,dm);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_074; CREATE INDEX cubo_cardiovascular_i_074  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,hta);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_075; CREATE INDEX cubo_cardiovascular_i_075  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,dl);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_076; CREATE INDEX cubo_cardiovascular_i_076  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,ob);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_077; CREATE INDEX cubo_cardiovascular_i_077  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,tb);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_078; CREATE INDEX cubo_cardiovascular_i_078  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,erc);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_079; CREATE INDEX cubo_cardiovascular_i_079  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,inf);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_080; CREATE INDEX cubo_cardiovascular_i_080  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,ecv);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_081; CREATE INDEX cubo_cardiovascular_i_081  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,farmacos_retira_dm);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_082; CREATE INDEX cubo_cardiovascular_i_082  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,farmacos_retira_hta);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_083; CREATE INDEX cubo_cardiovascular_i_083  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,farmacos_retira_dl);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_084; CREATE INDEX cubo_cardiovascular_i_084  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,farmacos_estatina);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_085; CREATE INDEX cubo_cardiovascular_i_085  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,farmacos_aspirina);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_086; CREATE INDEX cubo_cardiovascular_i_086  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,farmacos_ieca_ara);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_087; CREATE INDEX cubo_cardiovascular_i_087  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,farmacos_insulina);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_088; CREATE INDEX cubo_cardiovascular_i_088  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,age_medico);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_089; CREATE INDEX cubo_cardiovascular_i_089  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,age_enfermera);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_090; CREATE INDEX cubo_cardiovascular_i_090  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,age_nutricionista);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_091; CREATE INDEX cubo_cardiovascular_i_091  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,fecha_atencion_con_protocolo_cv);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_092; CREATE INDEX cubo_cardiovascular_i_092  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,fecha_1er_ingreso_cv);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_093; CREATE INDEX cubo_cardiovascular_i_093  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,riesgo_cv);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_094; CREATE INDEX cubo_cardiovascular_i_094  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,etapa_erc);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_095; CREATE INDEX cubo_cardiovascular_i_095  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,curacion_avanzada);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_096; CREATE INDEX cubo_cardiovascular_i_096  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,fondo_de_ojos);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_097; CREATE INDEX cubo_cardiovascular_i_097  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,vfg);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_098; CREATE INDEX cubo_cardiovascular_i_098  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,rac);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_099; CREATE INDEX cubo_cardiovascular_i_099  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,control_con);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_100; CREATE INDEX cubo_cardiovascular_i_100  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,control_en);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_101; CREATE INDEX cubo_cardiovascular_i_101  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,con_examenes_s_n);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_102; CREATE INDEX cubo_cardiovascular_i_102  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,peso);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_103; CREATE INDEX cubo_cardiovascular_i_103  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,talla);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_104; CREATE INDEX cubo_cardiovascular_i_104  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,imc);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_105; CREATE INDEX cubo_cardiovascular_i_105  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,circunferencia_cintura);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_106; CREATE INDEX cubo_cardiovascular_i_106  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,acg_rub_2015);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_107; CREATE INDEX cubo_cardiovascular_i_107  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,acg_peso_2015);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_108; CREATE INDEX cubo_cardiovascular_i_108  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha,pad,pas);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_109; CREATE INDEX cubo_cardiovascular_i_109  ON rem_sp_h04.cubo_cardiovascular USING btree(fecha,pad,pas);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_110; CREATE INDEX cubo_cardiovascular_i_110  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,medico_id,fecha_atencion_con_protocolo_cv);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_111; CREATE INDEX cubo_cardiovascular_i_111  ON rem_sp_h04.cubo_cardiovascular USING btree(tipo_atencion_cronica);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_112; CREATE INDEX cubo_cardiovascular_i_112  ON rem_sp_h04.cubo_cardiovascular USING btree(consejeria_automanejo_s_n);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_113; CREATE INDEX cubo_cardiovascular_i_113  ON rem_sp_h04.cubo_cardiovascular USING btree(consejeria_automanejo_mes_cuando);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_114; CREATE INDEX cubo_cardiovascular_i_114  ON rem_sp_h04.cubo_cardiovascular USING btree(etapa_de_cambio);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_115; CREATE INDEX cubo_cardiovascular_i_115  ON rem_sp_h04.cubo_cardiovascular USING btree(apoyo_automanejo_sms);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_116; CREATE INDEX cubo_cardiovascular_i_116  ON rem_sp_h04.cubo_cardiovascular USING btree(apoyo_automanejo_taller);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_117; CREATE INDEX cubo_cardiovascular_i_117  ON rem_sp_h04.cubo_cardiovascular USING btree(apoyo_automanejo_automonitoreo);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_118; CREATE INDEX cubo_cardiovascular_i_118  ON rem_sp_h04.cubo_cardiovascular USING btree(apoyo_automanejo_foro);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_119; CREATE INDEX cubo_cardiovascular_i_119  ON rem_sp_h04.cubo_cardiovascular USING btree(fecha_atencion_con_protocolo_prog_cdi);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_120; CREATE INDEX cubo_cardiovascular_i_120  ON rem_sp_h04.cubo_cardiovascular USING btree(fecha_atencion_con_protocolo_prog_cv);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_121; CREATE INDEX cubo_cardiovascular_i_121  ON rem_sp_h04.cubo_cardiovascular USING btree(fecha_atencion_con_protocolo_prog_sm);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_122; CREATE INDEX cubo_cardiovascular_i_122  ON rem_sp_h04.cubo_cardiovascular USING btree(fecha_atencion_con_protocolo_prog_resp);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_123; CREATE INDEX cubo_cardiovascular_i_123  ON rem_sp_h04.cubo_cardiovascular USING btree(epoc_etapa);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_124; CREATE INDEX cubo_cardiovascular_i_124  ON rem_sp_h04.cubo_cardiovascular USING btree(asma_nivel_control);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_125; CREATE INDEX cubo_cardiovascular_i_125  ON rem_sp_h04.cubo_cardiovascular USING btree(sbor_nivel_control);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_126; CREATE INDEX cubo_cardiovascular_i_126  ON rem_sp_h04.cubo_cardiovascular USING btree(epoc_nivel_control);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_127; CREATE INDEX cubo_cardiovascular_i_127  ON rem_sp_h04.cubo_cardiovascular USING btree(sbor_leve_moderada_severa);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_128; CREATE INDEX cubo_cardiovascular_i_128  ON rem_sp_h04.cubo_cardiovascular USING btree(asma_leve_moderada_severa);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_129; CREATE INDEX cubo_cardiovascular_i_129  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha_atencion_con_protocolo_prog_cdi);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_130; CREATE INDEX cubo_cardiovascular_i_130  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha_atencion_con_protocolo_prog_cv);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_131; CREATE INDEX cubo_cardiovascular_i_131  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha_atencion_con_protocolo_prog_sm);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_132; CREATE INDEX cubo_cardiovascular_i_132  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,fecha_atencion_con_protocolo_prog_resp);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_133; CREATE INDEX cubo_cardiovascular_i_133  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,epoc_etapa);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_134; CREATE INDEX cubo_cardiovascular_i_134  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,asma_nivel_control);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_135; CREATE INDEX cubo_cardiovascular_i_135  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,sbor_nivel_control);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_136; CREATE INDEX cubo_cardiovascular_i_136  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,epoc_nivel_control);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_137; CREATE INDEX cubo_cardiovascular_i_137  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,sbor_leve_moderada_severa);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_138; CREATE INDEX cubo_cardiovascular_i_138  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,asma_leve_moderada_severa);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_139; CREATE INDEX cubo_cardiovascular_i_139  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,control_patologia_cronica_date);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_140; CREATE INDEX cubo_cardiovascular_i_140  ON rem_sp_h04.cubo_cardiovascular USING btree(control_patologia_cronica_date);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_142; CREATE INDEX cubo_cardiovascular_i_142  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,salufam);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_143; CREATE INDEX cubo_cardiovascular_i_143  ON rem_sp_h04.cubo_cardiovascular USING btree(salufam);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_144; CREATE INDEX cubo_cardiovascular_i_144  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,cdi_ingreso_control_estado);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_145; CREATE INDEX cubo_cardiovascular_i_145  ON rem_sp_h04.cubo_cardiovascular USING btree(cdi_ingreso_control_estado);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_146; CREATE INDEX cubo_cardiovascular_i_146  ON rem_sp_h04.cubo_cardiovascular USING btree(medico_id,cdi_ingreso_control_estado);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_147; CREATE INDEX cubo_cardiovascular_i_147  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,cdi_ingreso_alto_riesgo_fecha);
drop index if exists rem_sp_h04.cubo_cardiovascular_i_148; CREATE INDEX cubo_cardiovascular_i_148  ON rem_sp_h04.cubo_cardiovascular USING btree(nif,cdi_ingreso_alto_riesgo_clinico_id);


GRANT SELECT ON TABLE rem_sp_h04.cubo_cardiovascular TO jesoto;
GRANT SELECT ON TABLE rem_sp_h04.cubo_cardiovascular TO consultaomi;
GRANT SELECT ON TABLE rem_sp_h04.cubo_cardiovascular TO consulta;
GRANT SELECT ON TABLE rem_sp_h04.cubo_cardiovascular TO ugina;
GRANT SELECT ON TABLE rem_sp_h04.cubo_cardiovascular TO ehenriquez;


-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- PASO 3 INFORME GESTION DE CASOS
--
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- INFORME GESTION DE CASOS
/*proximo_ctcv

-20170525 v02 :
		- Se corrigen calculo de indicadores para mayores de 80 años
		- Outlier en Hba1c, PAS , PAD
		- Se agregan pacientes del centro de Innovacion
		- Se Agregan Ordenes clínicas analíticas pendientes.

-20170622 v03 :
		- agregar DL para poder desplegar Población en Control.
		- Se agrega PIE DM.
-20180105 v04
		- se corrige ultimo control "con protocolo"
		-se agrega persona que hizo el último pie dm para ranking de pie dm sah
- 20180424 v28
		- Se hace diferencia con algunos datos que se registran en protocol
 		   Cardiovascular vs Multimorbilidad crónica.
- 20180507 v29 - Varios cambios, separar att cdi vs cardiovascular.

- 20180903 v30 -  se agrega fecha de apertura de episodios -63 cdi
		- se agrega salufam x familia
- 20200226 v42 - earenas, agrega riesgo cualitativo
*/

--ANALÍTICAS PENDIENTES
drop table if exists analiticas_pendientes; --select * from analiticas_pendientes limit 10
create temp table analiticas_pendientes as
		select distinct b.nif,a.fecha::date as fecha
		  from admomi.iddome as a,admomi.pacientes_validos as b --select * from admomi.pacientes_validos as b limit 10
		 where a.prueba like 'ANA%'
		   and a.fechar is null
		   and a.fecha between now()::date-'365 days'::interval and now()::date
		   and a.fecha is not null and a.nif is not null
		   and a.nif=b.nif2;
create index analiticas_pendientes_i_01 on analiticas_pendientes using btree(nif);
create index analiticas_pendientes_i_02 on analiticas_pendientes using btree(fecha);
create index analiticas_pendientes_i_03 on analiticas_pendientes using btree(nif,fecha);

--EVAL PIE
drop table if exists eval_pie00;create temp table eval_pie00 as
select case when campo2< 25			then 'Riesgo Moderado'
		when campo2 between 25 and 49 	then 'Riesgo Alto'
		when campo2 >=50 			then 'Riesgo Maximo' end as riesgo_pie_dm,* from biancora.cubo_formularios where wproto=135 and wnumero=56
union
select case
			when (a.wproto in (278,329,302) and a.wnumero=206 and campo='1')
			  OR (a.wproto in (402) 		and a.wnumero=418 and campo='1')	then 'Riesgo Bajo'
			when (a.wproto in (278,329,302) and a.wnumero=207 and campo='1')
			  OR (a.wproto in (402) 		and a.wnumero=419 and campo='1') 	then 'Riesgo Moderado'
			when (a.wproto in (278,329,302) and a.wnumero=208 and campo='1')
			  OR (a.wproto in (402) 		and a.wnumero=420 and campo='1') 	then 'Riesgo Alto'
			when (a.wproto in (278,329,302)	and a.wnumero=209 and campo='1')
			  OR (a.wproto in (402) 		and a.wnumero=421 and campo='1') 	then 'Riesgo Maximo' end
		,*
 from biancora.cubo_formularios as a
where a.campo<>'' and a.nif<>'' and a.fecha is not null
  and		(
				(a.wproto in (278,329,302) 	and a.wnumero in (206,207,208,209)	)
				OR
				(a.wproto in (402) 			and a.wnumero in (418,419)	)
			)

union
select 'Riesgo Bajo',*    	from biancora.cubo_formularios as a where a.wproto in (402,398) and a.wnumero=374 and a.campo='0' union
select 'Riesgo Moderado',* 	from biancora.cubo_formularios as a where a.wproto in (402,398) and a.wnumero=374 and a.campo='1' union
select 'Riesgo Alto',* 		from biancora.cubo_formularios as a where a.wproto in (402,398) and a.wnumero=374 and a.campo='2' union
select 'Riesgo Maximo',* 	from biancora.cubo_formularios as a where a.wproto in (402,398) and a.wnumero=374 and a.campo='3';

drop table if exists eval_pie;create temp table eval_pie as
select a.nif2 as nif,a.fecha2 as fecha,a.clinico_id
	,(select b.med_nombre from admomi.iddmed as b where b.id=a.clinico_id limit 1)
	,a.riesgo_pie_dm
  from eval_pie00 as a where a.nif2 is not null and a.fecha2 is not null;
create index eval_pie_i_01 on eval_pie using btree(nif);
create index eval_pie_i_02 on eval_pie using btree(fecha);
create index eval_pie_i_03 on eval_pie using btree(nif,fecha);


--CENTRO DE INNOVACION
drop table if exists centro_innovacion;
create temp table centro_innovacion as
--select nif2 as nif,min(fecha::date) as fecha from admomi.iddpag
with consulta as (
		select nif2,fecha2 from admomi.iddpag where dgp in ('*P391','*P388','*P357','*P355','*P398')	union
		select nif2,fecha2 from admomi.iddcop where wproto in (355,357,388,391,398) union
		select nif2,fecha2 from admomi.iddcoe where wproto in (355,357,388,391,398)
		)
		select nif2 as nif,min(fecha2) as fecha from consulta group by 1 order by 1;
create index centro_innovacion_i_01 on centro_innovacion using btree(nif);
create index centro_innovacion_i_02 on centro_innovacion using btree(fecha);
create index centro_innovacion_i_03 on centro_innovacion using btree(nif,fecha);

--PROBLACION EN CONTROL
drop table if exists pcv;
create temp table pcv  as
WITH consulta as
	(
	select a.nif
	  from rem_sp_h04.cubo_cardiovascular as a --select * from rem_sp_h04.cubo_cardiovascular as a where consejeria_automanejo_s_n=1 limit 10
	 where a.dm=1 or a.hta=1 or dl=1 group by 1
	UNION
	SELECT NIF
	  FROM CENTRO_INNOVACION
	UNION
	select a.nif
	  from rem_sp_h04.cubo_cardiovascular as a
	 where patologias_cronicas is not null GROUP BY 1 --no solo cronicos, se incluyen para priorizar post estado de emergencia chile
	)
	select distinct  a.nif,a.nif::varchar as nif2 from consulta as a,admomi.iddpacpa3 as b where a.nif=b.nif2 and b.estado2<>'HISTORICO' and a.nif is not null;
 create index pcv_i_01 on pcv using btree(nif);
 create index pcv_i_02 on pcv using btree(nif2);
 create index pcv_i_03 on pcv using btree(nif,nif2);

--DM
drop table if exists dm ; create temp table dm  as select nif,1::smallint as dm  from rem_sp_h04.cubo_cardiovascular where dm=1 and nif is not null group by 1,2;create index dm_i_01 on dm using btree(nif);

--HTA
drop table if exists hta; create temp table hta as select nif,1::smallint as hta from rem_sp_h04.cubo_cardiovascular where hta=1 and nif is not null group by 1,2;create index hta_i_01 on hta using btree(nif);;

--DL
drop table if exists dl; create temp table dl as select nif,1::smallint as dl from rem_sp_h04.cubo_cardiovascular where dl=1 and nif is not null group by 1,2;create index dl_i_01 on dl using btree(nif);;

--RIESGO CV
drop table if exists riesgo_cv; --select fecha,count(*) from riesgo_cv group by 1 order by 1 desc
create temp table riesgo_cv as
with consulta as 	(
			select nif,max(fecha) as fecha from rem_sp_h04.cubo_cardiovascular where riesgo_cv is not null group by 1
			)
			select a.*,b.riesgo_cv from consulta as a left join rem_sp_h04.cubo_cardiovascular as b on a.nif=b.nif and a.fecha=b.fecha and b.riesgo_cv is not null
			where a.nif is not null and a.fecha is not null;
 create index riesgo_cv_i_01 on riesgo_cv using btree(nif);
 create index riesgo_cv_i_02 on riesgo_cv using btree(nif,fecha);
 create index riesgo_cv_i_03 on riesgo_cv using btree(fecha);

--PAS
drop table if exists pas;
create temp table pas as
with consulta as 	(
			select nif,max(fecha) as fecha from rem_sp_h04.cubo_cardiovascular where pas is not null and pas>=36 group by 1
			)
			select a.*,b.pas::integer as pas from consulta as a left join rem_sp_h04.cubo_cardiovascular as b on a.nif=b.nif and a.fecha=b.fecha and a.nif is not null and a.fecha is not null  and b.pas is not null ;
 create index pas_i_01 on pas using btree(nif);
 create index pas_i_02 on pas using btree(nif,fecha);
 create index pas_i_03 on pas using btree(nif,fecha,pas);
 create index pas_i_04 on pas using btree(fecha);

--PAD
drop table if exists pad;
create temp table pad as
with consulta as 	(
			select nif,max(fecha) as fecha from rem_sp_h04.cubo_cardiovascular where pad is not null and pad>=18.4 group by 1 order by 1
			)
			select a.*,b.pad::integer as pad from consulta as a left join rem_sp_h04.cubo_cardiovascular as b on a.nif=b.nif and a.fecha=b.fecha and b.pad is not null and a.nif is not null and a.fecha is not null;
 create index pad_i_01 on pad using btree(nif);
 create index pad_i_02 on pad using btree(nif,fecha);
 create index pad_i_03 on pad using btree(nif,fecha,pad);
 create index pad_i_04 on pad using btree(fecha);

--HEMOGLOBINA GLUCOSILADA
drop table if exists hba1c;
create temp table hba1c as
with consulta as 	(
			select nif,max(fecha) as fecha from rem_sp_h04.cubo_cardiovascular where examen_hba1c is not null and examen_hba1c between 2 and 13.2 group by 1
			)
			select a.*,b.examen_hba1c::numeric examen_hba1c from consulta as a left join rem_sp_h04.cubo_cardiovascular as b on a.nif=b.nif and a.fecha=b.fecha and b.examen_hba1c  is not null ;
 create index hba1c_i_01 on hba1c using btree(nif);

--COLESTEROL LDL
drop table if exists colesterol_ldl;
create temp table colesterol_ldl as
with consulta as 	(
			select nif,max(fecha) as fecha from rem_sp_h04.cubo_cardiovascular where examen_colesterol_ldl is not null group by 1
			)
			select a.*,b.examen_colesterol_ldl::numeric as examen_colesterol_ldl from consulta as a left join rem_sp_h04.cubo_cardiovascular as b on a.nif=b.nif and a.fecha=b.fecha and b.examen_colesterol_ldl  is not null ;
 create index colesterol_ldl_i_01 on colesterol_ldl using btree(nif);

--ULTIMA FECHA ATT CON PROTOCOLO
drop table if exists fecha_atencion_con_protocolo_cv;
create temp table fecha_atencion_con_protocolo_cv as
select nif
	,tipo_atencion_cronica
	,max(fecha_atencion_con_protocolo_cv) as fecha_atencion_con_protocolo_cv
  from rem_sp_h04.cubo_cardiovascular
 where fecha_atencion_con_protocolo_cv is not null
   and (tipo_atencion_cronica is not null or tipo_atencion_cronica<>'')
 group by 1,2;
create index fecha_atencion_con_protocolo_cv_i_01 on fecha_atencion_con_protocolo_cv using btree(nif);
create index fecha_atencion_con_protocolo_cv_i_02 on fecha_atencion_con_protocolo_cv using btree(fecha_atencion_con_protocolo_cv);
create index fecha_atencion_con_protocolo_cv_i_03 on fecha_atencion_con_protocolo_cv using btree(tipo_atencion_cronica);
create index fecha_atencion_con_protocolo_cv_i_04 on fecha_atencion_con_protocolo_cv using btree(nif,tipo_atencion_cronica);
create index fecha_atencion_con_protocolo_cv_i_05 on fecha_atencion_con_protocolo_cv using btree(nif,fecha_atencion_con_protocolo_cv,tipo_atencion_cronica);
create index fecha_atencion_con_protocolo_cv_i_06 on fecha_atencion_con_protocolo_cv using btree(nif,fecha_atencion_con_protocolo_cv);

drop table if exists ultimo_control; --select count(*) from ultimo_control order by fecha desc limit 100
create temp table ultimo_control as
select a.nif
	,tipo_atencion_cronica
	,b.id as medico_id
	,max(a.fecha) as fecha
  from rem_sp_h04.cubo_cardiovascular as a,admomi.iddmed as b --select * from rem_sp_h04.cubo_cardiovascular limit 100
 where a.medico_id = b.id
   and b.id is not null
   and b.med_estamento_desc in ('MEDICO','ENFERMERA','NUTRICIONISTA','KINESIOLOGO','PSICOLOGO/A','ASISTENTE SOCIAL')
   and length(b.id::text)>=7
   and b.numcolegia like '%-%'
   and b.segtipo in ('M','T','E')
   and a.fecha_atencion_con_protocolo_cv is not null
   and a.nif is not null
   and a.medico_id is not null
 group by 1,2,3;
 create index ultimo_control_i_01 on ultimo_control using btree(nif);
 create index ultimo_control_i_02 on ultimo_control using btree(medico_id);
 create index ultimo_control_i_03 on ultimo_control using btree(nif,medico_id);
 create index ultimo_control_i_04 on ultimo_control using btree(nif,fecha);
 create index ultimo_control_i_05 on ultimo_control using btree(nif,medico_id,fecha);
 create index ultimo_control_i_06 on ultimo_control using btree(tipo_atencion_cronica);
 create index ultimo_control_i_07 on ultimo_control using btree(nif,medico_id,tipo_atencion_cronica);
 create index ultimo_control_i_08 on ultimo_control using btree(nif,fecha,tipo_atencion_cronica);
 create index ultimo_control_i_09 on ultimo_control using btree(nif,medico_id,fecha,tipo_atencion_cronica);

 --select * from rem_sp_h04.informe_gestion_casos_00 limit 100
 --select id,nombremed,med_estamento_desc from admomi.iddmed where med_estamento_desc in ('MEDICO','ENFERMERA','NUTRICIONISTA') and length(id::text)>=7 and numcolegia like '%-%' and segtipo in ('M','T','E') order by 2

--PROXIMO CONTROL CON y EN
drop table if exists control_con;create temp table control_con as
SELECT a.nif2 as nif,a.fecha2 as fecha,a.clinico_id as medico_id,a.tabla_descripcion_id as valor,a.tabla_campo_descripcion,a.wproto,campo as campo
  from biancora.cubo_formularios as a
 where
	(
		(a.wproto=398 and a.wnumero=328)	or --ok MULTIM --ingreso 398 350,
		(a.wproto=398 and a.wnumero=415)	or --ok MULTIM
		(a.wproto=398 and a.wnumero=158)	or --ok MULTIM
		(a.wproto=391 and a.wnumero=333)	or --ok MULTIM
		(a.wproto=388 and a.wnumero=333)	or --ok MULTIM
		(a.wproto=355 and a.wnumero=257)	or --ok MULTIM --ingreso
		(a.wproto=357 and a.wnumero=257)	or --ok MULTIM (398,391,388,355,355,357)
		(a.wproto=220 and a.wnumero=622)	or --OK CV
		(a.wproto=329 and a.wnumero=100)	or --ok CV
		(a.wproto=278 and a.wnumero=100)	   --ok CV

	)
   and a.campo<>''
   and a.campo is not null
   and a.fecha is not null;

--control en
drop table if exists control_en;create temp table control_en as
SELECT distinct a.nif2 as nif,a.fecha2 as fecha,a.clinico_id as medico_id,a.tabla_descripcion_id as valor,a.tabla_campo_descripcion,a.wproto,campo as campo
  from biancora.cubo_formularios as a
 where
	(
		(a.wproto=398 and a.wnumero=330) or --ok Multimorbilidad
		(a.wproto=391 and a.wnumero=335) or --ok Multimorbilidad
		(a.wproto=388 and a.wnumero=335) or --ok Multimorbilidad
		(a.wproto=357 and a.wnumero=256) or --ok Multimorbilidad
		(a.wproto=355 and a.wnumero=256) or --ok Multimorbilidad
		(a.wproto=329 and a.wnumero=231) or --OK Cardiovascular
		(a.wproto=278 and a.wnumero=231) or --OK Cardiovascular
		(a.wproto=220 and a.wnumero=624)  --OK Cardiovascular
	)
   and (a.campo<>'' or a.campo is not null)
   and a.fecha is not null;



   drop table if exists control_con_en01;create temp table control_con_en01 as--select tabla_campo_descripcion,aym(fecha),count(*) from (
   select nif,fecha,medico_id,wproto from control_con union
   select nif,fecha,medico_id,wproto from control_en;

   drop table if exists control_con_en02;create temp table control_con_en02 as--select tabla_campo_descripcion,aym(fecha),count(*) from (
   select a.*
    ,CASE WHEN A.WPROTO IN (398,391,388,355,355,357) THEN 'MULTIM'::text else 'CV' end as Protocolo
   	,a01.tabla_campo_descripcion control_con
	,upper(a02.tabla_campo_descripcion) control_en
	--,a01.campo as control_con_campo
	,a02.campo::integer as control_en_id
     from control_con_en01 as a
	 left join control_con as a01 using (nif,fecha,medico_id,wproto)
	 left join control_en  as a02 using (nif,fecha,medico_id,wproto)
	 order by fecha desc;

delete from control_con_en02 where control_con is null;
delete from control_con_en02 where control_en is null;
create index control_con_en02_i_01 on control_con_en02(nif,protocolo);

--ingreso x enfermera
drop table if exists ingreso_multimorbilidad_x_enfermera01;
create temp table ingreso_multimorbilidad_x_enfermera01 as
with consulta as
		(
		select nif2 as nif,fecha2 as fecha,clinico_id from biancora.cubo_formularios where wproto in (398) and wnumero in (350) and campo='1' union
		select nif2,fecha2,clinico_id from biancora.cubo_formularios where wproto in (355,388)
		)
		select a.*,b.med_nombre from consulta as a,admomi.iddmed as b --select * from admomi.iddmed as b limit 10
		 where  a.clinico_id = b.id
		    and b.id is not null
		    and b.med_estamento_desc in ('ENFERMERA','MEDICO')
		    and length(b.id::text)>=7
		    and b.numcolegia like '%-%'
		    and b.segtipo in ('M','T','E')
		    and a.nif is not null
		    and a.clinico_id is not null;
		    create index ingreso_multimorbilidad_x_enfermera01_i_01 on ingreso_multimorbilidad_x_enfermera01(nif,fecha);

		    --select * from ingreso_multimorbilidad_x_enfermera01 limit 10


--PROXIMO CTCV
drop table if exists proximo_ctcv;
create temp table proximo_ctcv as
with consulta as
		(
		select a.nif,a.fecha as fecha
		   from admomi.iddage as a
		  where a.acto='CTCV'
		    and a.fecha>=now()::date-'1 day'::interval
		    and (nif<>'' or nif is not null)
		    and asignada='S'
		   group by 1,2
		 )
		select b.nif,a.fecha as fecha
		  from consulta as a
			,admomi.pacientes_validos as b
			,pcv as c
		  where a.nif=b.nif2
		    and c.nif = c.nif
		    and a.nif is not null and a.fecha is not null
		  group by 1,2;
create index proximo_ctcv_i_01 on proximo_ctcv using btree(nif);
create index proximo_ctcv_i_02 on proximo_ctcv using btree(nif,fecha);
create index proximo_ctcv_i_03 on proximo_ctcv using btree(fecha);

--EXAMENES_ULTIMA_TOMA
drop table if exists examenes_ultima_toma;
create temp table examenes_ultima_toma as
with consulta as
		(
		select b.nif,fecha,asignada,horae,vino
		   from admomi.iddage as a , pcv as b--select extract('hours' from horae),* from admomi.iddage as a limit 1
		  where a.acto='EXTRA'
		    and a.nif=b.nif2

		 )
		select a.nif,max(a.fecha) as fecha
		  from consulta as a
		  where a.asignada='S'
		    and (vino='S' or horae<now())
		    and a.nif is not null and a.fecha is not null
		  group by 1
		  ;
create index examenes_ultima_toma_i_01 on examenes_ultima_toma using btree(nif);
create index examenes_ultima_toma_i_02 on examenes_ultima_toma using btree(nif,fecha);
create index examenes_ultima_toma_i_03 on examenes_ultima_toma using btree(fecha);
--EXAMENES_PROXIMA_TOMA
drop table if exists examenes_proxima_toma;
create temp table examenes_proxima_toma as
with consulta as
		(
		select a.nif,a.fecha as fecha
		   from admomi.iddage as a
		  where a.acto='EXTRA'
		    and a.fecha>=now()::date-'1 day'::interval
		    and (nif<>'' or nif is not null)
		    and asignada='S'
		   group by 1,2
		 )
		select b.nif,a.fecha as fecha
		  from consulta as a
			,admomi.pacientes_validos as b
			,pcv as c
		  where a.nif=b.nif2
		    and c.nif = c.nif
		    and a.nif is not null and a.fecha is not null
		  group by 1,2;
create index examenes_proxima_toma_i_01 on examenes_proxima_toma using btree(nif);
create index examenes_proxima_toma_i_02 on examenes_proxima_toma using btree(nif,fecha);
create index examenes_proxima_toma_i_03 on examenes_proxima_toma using btree(fecha);
--DIAGNOSTICOS
DROP TABLE IF EXISTS diagnostico;
CREATE TEMP TABLE diagnostico AS
with consulta as
		(
		select nif,campo,fecha from admomi.iddcoe as b where b.wproto=220 and b.wnumero=620 and b.campo<>'' union
		select nif,campo,fecha from admomi.iddcoe as b where b.wproto=278 and b.wnumero=236 and b.campo<>'' union
		select nif,campo,fecha from admomi.iddcoe as b where b.wproto=329 and b.wnumero=236 and b.campo<>''
		)
		select c.nif,a.fecha
			,(select b.campo from consulta as b where a.nif=b.nif and a.fecha=b.fecha order by fecha desc limit 1) as diagnostico
		  from
			(
			select nif,max(fecha) as fecha
			  from consulta as a
			 group by 1
			) as a,admomi.pacientes_validos as c
		  where a.nif=c.nif2
		    and a.nif is not null and a.fecha is not null;
create index diagnostico_i_01 on diagnostico using btree(nif);
create index diagnostico_i_02 on diagnostico using btree(nif,fecha);
create index diagnostico_i_03 on diagnostico using btree(fecha);

--FONASA
DROP TABLE IF EXISTS fonasa;
CREATE TEMP TABLE fonasa as
select distinct nif2 as nif,1::smallint as fonasa from admomi.iddpacpa3 where coberturap in ('00000001','00000002','00000003','00000004','00000006') and nif2 is not null;
create index fonasa_i_01 on fonasa using btree(nif);

--ACTIVO(2= activo)
DROP TABLE IF EXISTS estado;
CREATE TEMP TABLE estado as
with consulta as
		(
		select nif,fecha,campo
		  from admomi.iddcop as b
		 where b.wproto=317
		   and b.wnumero=55
		   and b.campo <>''
		 )
		 select c.nif,a.fecha
			,(select b.campo from consulta as b where a.nif=b.nif order by fecha desc limit 1) as estado
		   from
			(
			 select a.nif,max(a.fecha) as fecha
			   from consulta as a
			   group by 1
			) as a,admomi.pacientes_validos as c
		  where a.nif=c.nif2 and a.nif is not null and a.fecha is not null;
create index estado_i_01 on estado using btree(nif);
create index estado_i_02 on estado using btree(nif,fecha);
create index estado_i_03 on estado using btree(fecha);

--examenes_s_n
DROP TABLE IF EXISTS examenes_s_n; --select * from examenes_s_n
CREATE TEMP TABLE examenes_s_n as
with consulta as
		(
		select nif,fecha,con_examenes_s_n from rem_sp_h04.cubo_cardiovascular where con_examenes_s_n is not null
		)
		select a.*
			,(select b.con_examenes_s_n from rem_sp_h04.cubo_cardiovascular as b where a.nif=b.nif and a.fecha=b.fecha and con_examenes_s_n is not null order by fecha desc limit 1)
		  from
			(
			select nif,max(fecha) as fecha
			  from consulta
			 group by 1
			) as a
			where a.nif is not null and a.fecha is not null;
create index examenes_s_n_i_01 on examenes_s_n using btree(nif);
create index examenes_s_n_i_02 on examenes_s_n using btree(nif,fecha);
create index examenes_s_n_i_03 on examenes_s_n using btree(fecha);

drop table if exists clearence;
create temp table clearence as
with consulta as
		(
		select nif,fecha,texto from admomi.iddpag where dgp='CLCRCALC' and texto<>''
		)
		select c.nif,a.fecha
			,(select b.texto::numeric from consulta as b where a.nif=b.nif order by fecha desc limit 1) as clearence
		  from
			(
			select nif,max(fecha) as fecha
			  from consulta
			 group by 1
			) as a,admomi.pacientes_validos as c
		  where a.nif=c.nif2 and a.nif is not null and a.fecha is not null;
create index clearence_i_01 on clearence using btree(nif);
create index clearence_i_02 on clearence using btree(nif,fecha);
create index clearence_i_03 on clearence using btree(fecha);

--acg rub
drop table if exists acg_rub01;
create table acg_rub01 as
select nif,max(fecha) as fecha
  from rem_sp_h04.cubo_cardiovascular
 where acg_rub_2015 is not null
 group by 1; --127315
--create index acg_rub_2015_i_01 on acg_rub_2015 using btree(nif);

drop table if exists acg_rub02;
create table acg_rub02 as
select a.nif,a.fecha
	,a01.acg_rub_2015 as acg_rub
	,replace(round(a02.acg_peso_2015,6)::text,'.',',') as acg_peso
  from acg_rub01 as a
  left join rem_sp_h04.cubo_cardiovascular as a01 on a01.nif=a.nif and a01.fecha=a.fecha and a01.acg_rub_2015 is not null
  left join rem_sp_h04.cubo_cardiovascular as a02 on a02.nif=a.nif and a02.fecha=a.fecha and a02.acg_rub_2015 is not null;
create index acg_rub02_i_01 on acg_rub02 using btree(nif);


--patologias crónicas
drop table if exists patologias_cronicas;
create table patologias_cronicas as
select distinct nif,patologias_cronicas from rem_sp_h04.cubo_cardiovascular where patologias_cronicas is not null;
create index patologias_cronicas_i_01 on patologias_cronicas using btree(nif);

--consejeria telefonica automanejo
drop table if exists consejeria_automanejo_s_n_cuando;
create table consejeria_automanejo_s_n_cuando as
with consulta as (select distinct nif2,fecha2
		    from biancora.cubo_formularios
		   where
			(
				(wproto=398 and wnumero=305 ) or
				(wproto in (388,391) and wnumero=289)
			)
			and campo is not null
		)
		select a.*
			,(
			SELECT b.campo as consejeria_telefonica_automanejo_s_n--distinct a.nif2 as nif,a.fecha2 as fecha,a.clinico_id as medico_id,a.tabla_descripcion_id as valor,a.tabla_campo_descripcion,a.wproto,campo as campo
				--,*
			  from biancora.cubo_formularios as b
			 where
				(
					(b.wproto=398 and b.wnumero=305) or --ok Multimorbilidad
					(b.wproto=388 and b.wnumero=289) or --ok Multimorbilidad
					(b.wproto=391 and b.wnumero=289)    --ok Multimorbilidad

				)
			   and b.campo in ('S','N')
			   and b.fecha is not null
			   and a.nif2=b.nif2 and a.fecha2=b.fecha2 limit 1
			  )
			  ,(
			SELECT b.tabla_campo_descripcion as consejeria_telefonica_automanejo_mes--distinct a.nif2 as nif,a.fecha2 as fecha,a.clinico_id as medico_id,a.tabla_descripcion_id as valor,a.tabla_campo_descripcion,a.wproto,campo as campo
				--,*
			  from biancora.cubo_formularios as b
			 where
				(
					(b.wproto=398 and b.wnumero=307) or --ok Multimorbilidad
					(b.wproto=388 and b.wnumero=291) or --ok Multimorbilidad
					(b.wproto=391 and b.wnumero=291)    --ok Multimorbilidad

				)
			   and b.campo is not null --limit 1000
			   and b.fecha is not null
			   and a.nif2=b.nif2 and a.fecha2=b.fecha2 limit 1
			  )
			  from consulta as a;
create index consejeria_automanejo_s_n_cuando_i_01 on consejeria_automanejo_s_n_cuando using btree(nif2);
--mes consejeria
/*
drop table if exists consejeria_automanejo_mes_cuando;
create table consejeria_automanejo_mes_cuando as
with consulta as
		(
		select nif,max(fecha) as fecha
		  from rem_sp_h04.cubo_cardiovascular as a
		 where consejeria_automanejo_mes_cuando is not null
		 group by 1
		)
		select a.*
		       ,(select b.consejeria_automanejo_mes_cuando from rem_sp_h04.cubo_cardiovascular as b where b.fecha=a.fecha and a.nif=b.nif and b.consejeria_automanejo_mes_cuando is not null limit 1)
		  from consulta as a;
create index consejeria_automanejo_mes_cuando_i_01 on consejeria_automanejo_mes_cuando using btree(nif);
*/
--familia SALUFAM
drop table if exists cdi_salufam;
create temp table cdi_salufam as
			with consulta as
					(
					select nif,max(fecha) as fecha
					  from rem_sp_h04.cubo_cardiovascular where salufam is not null group by 1
					)
					select a.*
						,(select salufam from rem_sp_h04.cubo_cardiovascular as b where b.nif=a.nif and a.fecha=b.fecha order by fecha desc limit 1)
					  from consulta as a;
create index cdi_salufam_i_01 on cdi_salufam using btree(nif);

--CDI fecha apertura episodio -63
drop table if exists cdi_episodio_menos_63;
create temp table cdi_episodio_menos_63 as
with consulta as
		(select nif,min(fecha_apertura_episodio_menos_63) fecha_apertura_episodio_menos_63
		   from rem_sp_h04.cubo_cardiovascular where fecha_apertura_episodio_menos_63 is not null group by 1)
		   select a.*
			,(select (select c.med_nombre from admomi.iddmed as c where c.id=b.medico_id limit 1)  from rem_sp_h04.cubo_cardiovascular as b
			  where b.nif=a.nif and b.fecha_apertura_episodio_menos_63 is not null order by b.fecha_apertura_episodio_menos_63 limit 1 )
			from consulta as a;
create index cdi_episodio_menos_63_i_01 on cdi_episodio_menos_63 using btree(nif);

--ingreso control en protocolo CDI
drop table if exists ingreso_cdi001;create temp table ingreso_cdi001 as
select nif,min(fecha) as fecha
  from rem_sp_h04.cubo_cardiovascular
 where cdi_ingreso_control_estado=1
 group by 1;

drop table if exists ingreso_cdi002;create temp table ingreso_cdi002 as
select distinct a.nif,a.fecha
	,(select b.cdi_ingreso_control_clinico_id
	   from  rem_sp_h04.cubo_cardiovascular as b
	  where  b.nif=a.nif
	    and b.fecha=a.fecha
	    and b.cdi_ingreso_control_estado=1 limit 1) as clinico_id
  from ingreso_cdi001 as a;

drop table if exists ingreso_cdi003;create temp table ingreso_cdi003 as
select b.nif,b.fecha,b.clinico_id,(select c.med_nombre from admomi.iddmed as c where c.id=b.clinico_id limit 1)
  from ingreso_cdi002 as b;
create index ingreso_cdi003_i_01 on ingreso_cdi003 using btree(nif);

--ingreso multimorbilidad x enfermera
--RIESGO CUALITATIVO
drop table if exists riesgo_cualitativo;
create temp table riesgo_cualitativo as
with consulta as
					(
					select nif,fecha,tipo_riesgo_cualitativo,medico_id
							,LAG (tipo_riesgo_cualitativo, 1) OVER (
					      PARTITION BY nif
					      ORDER BY
					         fecha
					   		) AS tipo_riesgo_cualitativo_anterior
					  from biancora.cubo_cronicos
					 where tipo_riesgo_cualitativo is not null
					 order by nif,fecha
					)
select *,tipo_riesgo_cualitativo-tipo_riesgo_cualitativo_anterior as dif
		,row_number() over (partition by nif)
  from consulta
 where (tipo_riesgo_cualitativo-tipo_riesgo_cualitativo_anterior<>0 or tipo_riesgo_cualitativo-tipo_riesgo_cualitativo_anterior is null)
   and nif not in (select nif2 from admomi.iddpacpa3 where estado2 is null)
   --and nif in 	(33131,104869,33131,104869,27512,27684,27923,28799,29394,30009)
order by nif,fecha;

drop table if exists riesgo_cualitativo02;
create temp table    riesgo_cualitativo02 as
select a.nif,a01.riesgo_actual,a.riesgo_historico
  from
  		(
		select a.nif,array_agg(riesgo_cambios) as riesgo_historico
		  from
		  		(
				select nif,(select row_to_json(_) from (select t.fecha as f,t.riesgo as r) as _) riesgo_cambios
				  from
				  		(
						select nif,fecha
								,case
									when tipo_riesgo_cualitativo=1 then 'ALTO '
									when tipo_riesgo_cualitativo=2 then 'MEDIO'
									when tipo_riesgo_cualitativo=3 then 'BAJO '
									end
								as riesgo
						  from riesgo_cualitativo
						 order by nif,fecha desc
						) as t
				) as a
			group by 1
			) as a
	  left join	( --ultimo riesgo cualitativo registrdo
  			 	select   nif
  			 			,case
							when tipo_riesgo_cualitativo=1 then 'ALTO '
							when tipo_riesgo_cualitativo=2 then 'MEDIO'
							when tipo_riesgo_cualitativo=3 then 'BAJO '
							end
							as riesgo_actual
						,fecha,row_number() over (partition by nif order by fecha desc) rownum
				  from riesgo_cualitativo
			  	) as a01 on a.nif=a01.nif and a01.rownum=1;
create index riesgo_cualitativo02_i_01 on riesgo_cualitativo02(nif);


--INFORME V01

drop        table if exists	informe_gestion_casos_00; --select * from informe_gestion_casos_00 where nif=45332
create temp table 		informe_gestion_casos_00 as
select a.*
	,(select b.riesgo_cv 						from riesgo_cv as b 				where b.nif=a.nif order by b.fecha desc limit 1)
	,(select b.pas 							from pas as b 					where b.nif=a.nif and b.pas>=36 order by b.fecha desc limit 1)
	,(select b.pad 							from pad as b 					where b.nif=a.nif and b.pad>=18.4 order by b.fecha desc limit 1)
	,(select b.examen_hba1c 					from hba1c as b 				where b.nif=a.nif and b.examen_hba1c between 2 and 13.2 order by b.fecha desc limit 1)
	,(select b.examen_colesterol_ldl 				from colesterol_ldl as b 			where b.nif=a.nif order by b.fecha desc limit 1)
	,(select b.fecha_atencion_con_protocolo_cv 			from fecha_atencion_con_protocolo_cv as b 	where b.nif=a.nif order by b.fecha_atencion_con_protocolo_cv desc limit 1)
	,(select b.dm							from dm as b 					where b.nif=a.nif limit 1)
	,(select b.hta							from hta as b 					where b.nif=a.nif limit 1)
	,(select b.dl							from dl as b 					where b.nif=a.nif limit 1)
	,(select b.fecha as ultimo_control_fecha			from ultimo_control as b 			where b.nif=a.nif order by b.fecha desc limit 1) --cardiovasculiar
	,(select b.medico_id as ultimo_control_clinico			from ultimo_control as b 			where b.nif=a.nif order by b.fecha desc limit 1)
	,(select b.control_con as proximo_control_con			from control_con_en02 as b			where b.nif=a.nif and b.protocolo='CV' is not null order by b.fecha desc limit 1)
	,(select control_en_id as proximo_control_en			from control_con_en02 as b			where b.nif=a.nif and b.protocolo='CV' is not null order by b.fecha desc limit 1)
	,(select b.fecha as proximo_ctcv				from proximo_ctcv as b				where b.nif=a.nif order by b.fecha limit 1)
	,(select b.fecha as examenes_ultima_toma			from examenes_ultima_toma as b			where b.nif=a.nif order by b.fecha desc limit 1)
	,(select b.fecha as examenes_proxima_toma			from examenes_proxima_toma as b			where b.nif=a.nif order by b.fecha limit 1)
	,(select b.con_examenes_s_n 					from examenes_s_n as b				where b.nif=a.nif order by b.fecha limit 1)
	,(select b.fonasa						from fonasa as b				where b.nif=a.nif limit 1)
	,(select b.estado						from estado as b				where b.nif=a.nif order by b.fecha limit 1)
	,(select b.clearence						from clearence as b				where b.nif=a.nif order by b.fecha limit 1)
	,(select b.diagnostico						from diagnostico as b				where b.nif=a.nif order by b.fecha limit 1)
	,(select b.fecha as ingreso_centro_innovacion  			from centro_innovacion as b 			where a.nif=b.nif limit 1)
	,array(select b.fecha 						from analiticas_pendientes as b 		where a.nif=b.nif) as analitica_pendiente
	,(select b.fecha as ultimo_control_fecha_multim			from ultimo_control as b 			where b.nif=a.nif order by b.fecha desc limit 1)
	,(select b.medico_id as ultimo_control_clinico_multim		from control_con_en02 as b			where b.nif=a.nif and b.protocolo='MULTIM' is not null order by b.fecha desc limit 1)
	,(select b.control_con   as proximo_control_con_multim		from control_con_en02 as b			where b.nif=a.nif and b.protocolo='MULTIM' is not null order by b.fecha desc limit 1)
	,(select b.control_en    as proximo_control_en_multim		from control_con_en02 as b			where b.nif=a.nif and b.protocolo='MULTIM' is not null order by b.fecha desc limit 1)
	,(select b.control_en_id as proximo_control_en_num_multim	from control_con_en02 as b			where b.nif=a.nif and b.protocolo='MULTIM' is not null order by b.fecha desc limit 1)
	,(select b.consejeria_telefonica_automanejo_s_n			from consejeria_automanejo_s_n_cuando as b		where a.nif=b.nif2 order by b.fecha2 	desc limit 1)
	,(select b.consejeria_telefonica_automanejo_mes			from consejeria_automanejo_s_n_cuando as b	where a.nif=b.nif2 order by b.fecha2 		desc limit 1)
	,(select b.med_nombre 	ingreso_cdi_enfermera			from ingreso_multimorbilidad_x_enfermera01 as b	where a.nif=b.nif order by b.fecha limit 1)
	,(select b.fecha 	ingreso_cdi_enfermera_fecha		from ingreso_multimorbilidad_x_enfermera01 as b	where a.nif=b.nif order by b.fecha limit 1)
  from pcv as a ;

drop table if exists 	rem_sp_h04.informe_gestion_casos_00 ;  --select * from rem_sp_h04.informe_gestion_casos_00 limit 100
create table 		rem_sp_h04.informe_gestion_casos_00 as
select
	 case

		when a.dm=1  and edad_en_agnios(now()::date,(select b.nacimiento from admomi.iddpacpa3 as b where b.nif2=a.nif limit 1)) between 15 and 79 and (a.pas >=140 or a.pas is null or a.pad >=90 or a.pad is null or a.examen_hba1c>=7 or a.examen_hba1c is null or a.clearence<=45 or a.clearence is null or a.examen_colesterol_ldl>=190 or a.examen_colesterol_ldl is null) then 'DESCOMPENSADO'
		when a.dm=1  and edad_en_agnios(now()::date,(select b.nacimiento from admomi.iddpacpa3 as b where b.nif2=a.nif limit 1)) >= 80             and (a.pas >=150 or a.pas is null or a.pad >=90 or a.pad is null or a.examen_hba1c>=8 or a.examen_hba1c is null or a.clearence<=45 or a.clearence is null or a.examen_colesterol_ldl>=190 or a.examen_colesterol_ldl is null) then 'DESCOMPENSADO'

		when a.hta=1 and edad_en_agnios(now()::date,(select b.nacimiento from admomi.iddpacpa3 as b where b.nif2=a.nif limit 1)) between 15 and 79 and (a.pas >=140 or a.pas is null or a.pad >=90 or a.pad is null or a.clearence<=45 or a.clearence is null or a.examen_colesterol_ldl>=190 or a.examen_colesterol_ldl is null) then 'DESCOMPENSADO'
		when a.hta=1 and edad_en_agnios(now()::date,(select b.nacimiento from admomi.iddpacpa3 as b where b.nif2=a.nif limit 1)) >=80              and (a.pas >=150 or a.pas is null or a.pad >=90 or a.pad is null or a.clearence<=45 or a.clearence is null or a.examen_colesterol_ldl>=190 or a.examen_colesterol_ldl is null) then 'DESCOMPENSADO'
		else 'COMPENSADO'

	     end as compensacion
	,(select btrim((btrim(coalesce(b.nombre,' '))||' '||btrim(coalesce(b.apellido1,' '))||' '||btrim(coalesce(b.apellido2,' ')))) from admomi.iddpacpa3 as b where b.nif2=a.nif limit 1) 	as pac_nombre
	,(select b.telpart		from admomi.iddpacpa3 as b where b.nif2=a.nif limit 1) 													as telefono1
	,(select b.teldesp 		from admomi.iddpacpa3 as b where b.nif2=a.nif limit 1) 													as telefono2
	,case
		when a.riesgo_cv=1 then 'BAJO'
		when a.riesgo_cv=2 then 'MODERADO'
		when a.riesgo_cv=3 then 'ALTO'
		when a.riesgo_cv=4 then 'MUY ALTO'
		end				 																		as riesgo_cv
	,a.pas																							as pas
	,a.pad																							as pad
	,a.examen_hba1c																						as hba1c
	,a.clearence																						as clearence
	,a.examen_colesterol_ldl																				as ldl
	,a.fecha_atencion_con_protocolo_cv																			as fecha_ultimo_control
	,(select btrim((replace(replace(replace(b.med_nombre,'(A)',''),'(V)',''),'(R)','')))
	    from admomi.iddmed as b where b.id=a.ultimo_control_clinico limit 1)														as profesional_ultimo_contorl
	,a.diagnostico																						as diagnostico
	,a.examenes_ultima_toma																					as fecha_ultimo_examen_tomado
	,a.proximo_control_en_num_multim																			as mes_proximo_control_num
	,a.proximo_control_en_multim																				as mes_proximo_control
	,a.proximo_control_con_multim																				as profesional_proximo_control
	,a.con_examenes_s_n																					as solicitud_de_examenes
	,a.proximo_ctcv																						as agenda_proximo_ctcv
	,a.examenes_proxima_toma																				as agenda_proxima_hora_examen
	,a.hta																							as hta
	,a.dm																							as dm
	,a.dl as dl
	,a.fonasa																						as fonasa
	,case when a.estado='2' then 'INACTIVO' else 'ACTIVO' END 																as estado
	,(select b.centro 		from admomi.iddpacpa3 as b where b.nif2=a.nif limit 1) 													as centro
	,(select c.med_nombre 		from admomi.iddpacpa3 as b,admomi.iddmed as c where c.med_estamento_desc='ENFERMERA' and c.codigo=b.medicoha and b.nif2=a.nif limit 1) 							as enfermera
	--select * from admomi.iddmed as c where c.med_estamento_desc='ENFERMERA' limit 10
	,a.nif																							as nif
	,(select b.tis 			from admomi.iddpacpa3 as b where b.nif2=a.nif limit 1) 													as rut
	,a.ingreso_centro_innovacion
	,a.analitica_pendiente
	,case when now()::Date-a.fecha_atencion_con_protocolo_cv<=365 then 1::smallint else 0::smallint end as en_control_cardiovascular
	,(select b.fecha from eval_pie as b where b.nif=a.nif and b.fecha is not null order by b.fecha desc limit 1) as ultimo_eval_pie
        ,(select b.med_nombre from eval_pie as b where b.nif=a.nif and b.fecha is not null order by b.fecha desc limit 1) as ultimo_eval_pie_clinico
        ,(select b.riesgo_pie_dm from eval_pie as b where b.nif=a.nif and b.fecha is not null order by b.fecha desc limit 1) as ultimo_eval_pie_resultado --select * from eval_pie limit 10
        ,(select b.acg_rub from acg_rub02 as b where a.nif=b.nif limit 1) as acg_rub_2015
        ,(select b.acg_peso from acg_rub02 as b where a.nif=b.nif limit 1) as acg_peso_2015
        ,(select b.patologias_cronicas from patologias_cronicas as b where a.nif=b.nif limit 1) as patologias_cronicas
	,a.ultimo_control_fecha_multim
	,(select btrim((replace(replace(replace(b.med_nombre,'(A)',''),'(V)',''),'(R)','')))
	    from admomi.iddmed as b where b.id=a.ultimo_control_clinico_multim limit 1) 	as ultimo_control_clinico_multim
	,a.proximo_control_en									as proximo_control_con_multim
	,a.consejeria_telefonica_automanejo_s_n
	,a.consejeria_telefonica_automanejo_mes
	,(select b.salufam from cdi_salufam as b where b.nif=a.nif limit 1)
	,(select b.fecha_apertura_episodio_menos_63 from cdi_episodio_menos_63 as b where b.nif=a.nif order by fecha_apertura_episodio_menos_63 limit 1)
	,(select b.med_nombre as fecha_apertura_episodio_menos_63_clinico_id from cdi_episodio_menos_63 as b where b.nif=a.nif order by fecha_apertura_episodio_menos_63 limit 1)
	,(select min(b.fecha) from ingreso_cdi003 as b where b.nif=a.nif limit 1) as ingreso_cdi_fecha
	,(select b.med_nombre from ingreso_cdi003 as b where b.nif=a.nif limit 1) as ingreso_cdi_clinico_nombre
	,a.ingreso_cdi_enfermera
	,a.ingreso_cdi_enfermera_fecha
	,(select edad_en_agnios(now(),b.nacimiento) from admomi.iddpacpa3 as b where b.nif2=a.nif limit 1) as edad
	,(select riesgo_actual from riesgo_cualitativo02 as b where b.nif=a.nif limit 1) as riesgo_cualitativo_actual
	,(select riesgo_historico from riesgo_cualitativo02 as b where b.nif=a.nif limit 1) as riesgo_cualitativo_historico
	,a00.examen_tsh
	,a01.modalidad_de_contacto_proximo_control
	,a02.solicitud_de_examenes_s_n
	,a03.fecha_diag_dm
	,a04.fecha_diag_hta
	,a05.examen_trigliceridos
  from informe_gestion_casos_00 as a
 left join
			(
			select nif,fecha,examen_tsh
					,row_number() over (partition by nif order by fecha desc) rownum
			  from biancora.cubo_cronicos
			 where examen_tsh is not null
			) as a00
		      on a00.nif=a.nif
		     and a00.rownum=1
left join
			(
			select nif,fecha
					,case
						when modalidad_de_contacto_proximo_control=1 then 'Presencial'
						when modalidad_de_contacto_proximo_control=2 then 'Distancia'
						end modalidad_de_contacto_proximo_control
					,row_number() over (partition by nif order by fecha desc) rownum
			  from biancora.cubo_cronicos
			 where modalidad_de_contacto_proximo_control is not null
			) as a01
		      on a01.nif=a.nif
		     and a01.rownum=1
left join
			(
			select nif,fecha
				,case
					when solicitud_de_examenes_s_n=1 then 'SI'
					when solicitud_de_examenes_s_n=0 then 'NO'
					end solicitud_de_examenes_s_n
					,row_number() over (partition by nif order by fecha desc) rownum
			  from biancora.cubo_cronicos
			 where solicitud_de_examenes_s_n is not null
			) as a02
		      on a02.nif=a.nif
		     and a02.rownum=1
left join 	(
			select nif,fecha fecha_diag_dm
					,row_number() over (partition by nif order by fecha) rownum
			  from biancora.cubo_cronicos
			 where dm is not null
			) as a03
		      on a03.nif=a.nif
		     and a03.rownum=1
left join 	(
			select nif,fecha fecha_diag_hta
					,row_number() over (partition by nif order by fecha) rownum
			  from biancora.cubo_cronicos
			 where hta is not null
			) as a04
		      on a04.nif=a.nif
		     and a04.rownum=1
left join 	(
			select nif,fecha,examen_trigliceridos
					,row_number() over (partition by nif order by fecha desc) rownum
			  from biancora.cubo_cronicos
			 where examen_trigliceridos is not null
			) as a05
		      on a05.nif=a.nif
		     and a05.rownum=1;
--,a29.modalidad_de_contacto_proximo_control
--	,a30.solicitud_de_examenes_s_n
--select nif,fecha,examen_tsh from biancora.cubo_cronicos where examen_tsh is not null limit 100
--select * from rem_sp_h04.informe_gestion_casos_00
--select count(*) from rem_sp_h04.informe_gestion_casos_00 --33716
--INCLUYE A TODOS LOS CRONICOS !!!
--delete from rem_sp_h04.informe_gestion_casos_00 where nif in (select nif2 from admomi.iddpacpa3 where edad_en_agnios(now(),nacimiento)<=14 );

update rem_sp_h04.informe_gestion_casos_00 set mes_proximo_control_num=0,mes_proximo_control='O. SIN MES' where mes_proximo_control_num is null;

update rem_sp_h04.informe_gestion_casos_00 set enfermera='.SIN ENFERMERA ASIGNADAS' where enfermera is null;

update rem_sp_h04.informe_gestion_casos_00 set hta=0 where hta is null; --select * from rem_sp_h04.informe_gestion_casos_00 --30494
update rem_sp_h04.informe_gestion_casos_00 set dm=0  where dm  is null;
update rem_sp_h04.informe_gestion_casos_00 set dl=0  where dl  is null;

create index informe_gestion_casos_00_i_01 on rem_sp_h04.informe_gestion_casos_00 using btree(centro) tablespace tb_index;
create index informe_gestion_casos_00_i_02 on rem_sp_h04.informe_gestion_casos_00 using btree(hta) tablespace tb_index;
create index informe_gestion_casos_00_i_03 on rem_sp_h04.informe_gestion_casos_00 using btree(dm) tablespace tb_index;
create index informe_gestion_casos_00_i_04 on rem_sp_h04.informe_gestion_casos_00 using btree(dl) tablespace tb_index;
create index informe_gestion_casos_00_i_05 on rem_sp_h04.informe_gestion_casos_00 using btree(hta,dm,dl) tablespace tb_index;
create index informe_gestion_casos_00_i_06 on rem_sp_h04.informe_gestion_casos_00 using btree(centro,hta,dm,dl) tablespace tb_index;
create index informe_gestion_casos_00_i_07 on rem_sp_h04.informe_gestion_casos_00 using btree(nif,ingreso_centro_innovacion) tablespace tb_index;
create index informe_gestion_casos_00_i_08 on rem_sp_h04.informe_gestion_casos_00 using btree(nif) tablespace tb_index;
create index informe_gestion_casos_00_i_09 on rem_sp_h04.informe_gestion_casos_00 using btree(centro,enfermera,mes_proximo_control_num) tablespace tb_index;

GRANT SELECT ON TABLE rem_sp_h04.informe_gestion_casos_00 TO ugina;
GRANT SELECT ON TABLE rem_sp_h04.informe_gestion_casos_00 TO jesoto;
GRANT SELECT ON TABLE rem_sp_h04.informe_gestion_casos_00 TO ehenriquez;
GRANT SELECT ON TABLE rem_sp_h04.informe_gestion_casos_00 TO consultaomi;
GRANT SELECT ON TABLE rem_sp_h04.informe_gestion_casos_00 TO consulta;

--select * from rem_sp_h04.informe_gestion_casos_00
