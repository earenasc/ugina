#14 sidra3.edox ssmso carga diaria4.pl
use DBI;
use strict;
use LWP::UserAgent;
use HTTP::Request;
use DBI;

# connect
my $dbh = DBI->connect("DBI:Pg:dbname=ancorabi;host=192.168.150.39", "postgres", "cb400sf"
						, {'RaiseError' => 1});
my $sth = $dbh->prepare("
select cod_consumidor,folio_interno,folio_sigges,mensaje_xml2
  from sidra_base.ic_para_enviar
 where enviar_a_sidra=1
   and (pertinencia is null or pertinencia = 1)
   and folio_interno not in (select numorden::varchar 
                               from sidra_base.ic_enviada)
    and (
            (ges='1' and folio_sigges<>'')
             or
             ges='2'
        )
   and 
        (
		(
		    cod_consumidor = 'CJPII' 
		and fecha_entrada between (now()::date-'187 days'::interval)::date and (now()::date-'2 days'::interval)::date
		)
		or
		(      
		    cod_consumidor in ('CSAH','CMTC') 
		and fecha_entrada between (now()::date-'195 days'::interval)::date and (now()::date)::date
		) 
		or
		(
		    (tipo_profesional='2' or especialidad_destino_homologado='07-301-0')
                and fecha_entrada between (now()::date-'195 days'::interval)::date and (now()::date)::date
		)
	)
and run in (
'8219125',
'24317760',
'12900005',
'6946236',
'12062187',
'7117014',
'16695280',
'17065461',
'22902085',
'24769859',
'6885466',
'6946236',
'7448287',
'21413480',
'4785389',
'7666980',
'8013675',
'13485135',
'8989219',
'16131980',
'7814359',
'23901537',
'11318839',
'6387392',
'15843195',
'25870609',
'8774583',
'2697949',
'6114093',
'3582915',
'5719898',
'6266797',
'3475092',
'21984754',
'9395058',
'24229351',
'5461486',
'5776289',
'5113376',
'7618615',
'7473929',
'3226900',
'13786370',
'7571035',
'24738302',
'15470435',
'5834435',
'7816573',
'25987377',
'25781673',
'22453024',
'7000869',
'7193411',
'4619187',
'4858730',
'4994179',
'4865980',
'2895363',
'5809043',
'7054449',
'25390142',
'19515932',
'12814206',
'22001778',
'3320170',
'7529392',
'23635980',
'11875222',
'15756725',
'11878638',
'4814479',
'7346960',
'8297993',
'10739115',
'7266852',
'4141666',
'13249776',
'3356633',
'16128508',
'6848501',
'16440406',
'12647252',
'13079414',
'22540851',
'10499482',
'5323198',
'19219734',
'16959857',
'12814206',
'14595059',
'12065402',
'11557060',
'21745777',
'6783305',
'6135935',
'7593845',
'22755982',
'13079546',
'12674891',
'7299822',
'6948947',
'16032573',
'20640809',
'25869555',
'25869580',
'24624654',
'18057801',
'13092128',
'18880934',
'19443571',
'21834621',
'21834632',
'4130387',
'12507379',
'10566866',
'7561421',
'19704774',
'25925429',
'24081645',
'19999950',
'7733681',
'8537322',
'4804095',
'5801033',
'13283848',
'13786370'
)


order by fecha_entrada desc
");
$sth->execute();
my ($cod_consumidor,$folio_interno,$folio_sigges,$mensaje_xml);
$sth->bind_columns(\$cod_consumidor,\$folio_interno,\$folio_sigges,\$mensaje_xml);

while ($sth->fetch()) {
	my $message = $mensaje_xml;
	my $userAgent = LWP::UserAgent ->new();
	my $request = HTTP::Request->new(POST =>'http://sidra.ssmso.cl/ServicioInterconsultaV3.asmx');
#	my $request = HTTP::Request->new(POST =>'http://163.247.80.132/SIDRAPRUEBAS/ServicioInterconsultaV3.asmx');

	$request->header(SOAPAction =>'"SSMSO/EnviarInterconsulta"');
	$request->content($message);
	$request->content_type("text/xml;character=utf-8");
	my $response = $userAgent->request($request);
	my $Estado = $response->as_string;
	my $rows = $dbh->do("INSERT INTO sidra_base.respuesta_sidra_ssmso 
							VALUES ('$cod_consumidor','$Estado',now(),'$folio_interno'
        									,'$mensaje_xml','$folio_sigges',11,0)");
	print "Folio_interno=\n";
	print $folio_interno;
	print "\n";
        print $cod_consumidor;
	print "\n";
	print $response->as_string;
	};
$dbh->disconnect();

