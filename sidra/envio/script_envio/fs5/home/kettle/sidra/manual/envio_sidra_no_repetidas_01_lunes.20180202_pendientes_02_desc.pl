#14 sidra3.edox ssmso carga diaria4.pl
use DBI;
use strict;
use LWP::UserAgent;
use HTTP::Request;
use DBI;

# connect
my $dbh = DBI->connect("DBI:Pg:dbname=ancorabi;host=192.168.150.39", "postgres", "cb400sf"
						, {'RaiseError' => 1});
my $sth = $dbh->prepare("
select cod_consumidor,folio_interno,folio_sigges,mensaje_xml2
  from sidra_base.ic_para_enviar
 where enviar_a_sidra=1
   and (pertinencia is null or pertinencia = 1)
   and folio_interno not in (select numorden::varchar 
                               from sidra_base.ic_enviada)
   and extract(dow from fecha_entrada)=1
    and (
            (ges='1' and folio_sigges<>'')
             or
             ges='2'
        )
   and 
        (
		(
		    cod_consumidor = 'CJPII' 
		and fecha_entrada between (now()::date-'60 days'::interval)::date and (now()::date-'2 days'::interval)::date
		)
		or
		(      
		    cod_consumidor in ('CSAH','CMTC') 
		and fecha_entrada between (now()::date-'60 days'::interval)::date and (now()::date)::date
		) 
		or
		(
		    (tipo_profesional='2' or especialidad_destino_homologado='07-301-0')
                and fecha_entrada between (now()::date-'60 days'::interval)::date and (now()::date)::date
		)
	)
and run in (
'13679482',
'2853518',
'3286408',
'3310046',
'3342386',
'3783883',
'3801870',
'3909820',
'3927024',
'3941250',
'3979270',
'3989875',
'4087279',
'4188952',
'4277904',
'4409067',
'4506924',
'4506924',
'4508815',
'4640905',
'4650471',
'4679234',
'4697951',
'4724212',
'4724212',
'4971735',
'5196144',
'5208047',
'5212084',
'5229593',
'5713926',
'5722607',
'5748990',
'5759164',
'5767874',
'5817191',
'5861868',
'5861868',
'6002655',
'6167929',
'6189070',
'6194873',
'6369057',
'6405794',
'6490114',
'6532182',
'6597601',
'6597601',
'6719068',
'6728771',
'6851661',
'6873752',
'6990118',
'7023697',
'7072951',
'7192132',
'7343795',
'7353255',
'7464848',
'7740264',
'7820570',
'8114451',
'8147637',
'8469272',
'8673105',
'9065559',
'9247859',
'9260132',
'9316587',
'9344957',
'9486679',
'9787207',
'9830372',
'9926746',
'10162456',
'10406814',
'10490566',
'10495062',
'11336210',
'12030040',
'12274030',
'12288512',
'12574350',
'12673029',
'12677346',
'12677346',
'12910219',
'13081353',
'13148868',
'13278343',
'13548569',
'13808057',
'14186269',
'15121749',
'15253178',
'15323438',
'15414251',
'16268126',
'17007567',
'17007567',
'17302908',
'17386145',
'17416067',
'17496066',
'17578266',
'18661525',
'18675268',
'18895859',
'18990520',
'19382463',
'19733819',
'20812180',
'21271895',
'21724281',
'22208500',
'22701298',
'22735411',
'22937536',
'23600564',
'23608189',
'23680744',
'23841224',
'24113574',
'24125846',
'24386889',
'24853975',
'24942415',
'25921775',
'25928711',
'25934933',
'25940947',
'26043793',
'49877162',
'6281564',
'6281564',
'6633929',
'4042132',
'16547388',
'6366358',
'7659505',
'6053915',
'6860638',
'6024845',
'9531436',
'6873783',
'13836846')
order by fecha_entrada desc 
");
$sth->execute();
my ($cod_consumidor,$folio_interno,$folio_sigges,$mensaje_xml);
$sth->bind_columns(\$cod_consumidor,\$folio_interno,\$folio_sigges,\$mensaje_xml);

while ($sth->fetch()) {
	my $message = $mensaje_xml;
	my $userAgent = LWP::UserAgent ->new();
	my $request = HTTP::Request->new(POST =>'http://sidra.ssmso.cl/ServicioInterconsultaV3.asmx');
#	my $request = HTTP::Request->new(POST =>'http://163.247.80.132/SIDRAPRUEBAS/ServicioInterconsultaV3.asmx');

	$request->header(SOAPAction =>'"SSMSO/EnviarInterconsulta"');
	$request->content($message);
	$request->content_type("text/xml;character=utf-8");
	my $response = $userAgent->request($request);
	my $Estado = $response->as_string;
	my $rows = $dbh->do("INSERT INTO sidra_base.respuesta_sidra_ssmso 
							VALUES ('$cod_consumidor','$Estado',now(),'$folio_interno'
        									,'$mensaje_xml','$folio_sigges',11,0)");
	print "Folio_interno=\n";
	print $folio_interno;
	print "\n";
        print $cod_consumidor;
	print "\n";
	print $response->as_string;
	};
$dbh->disconnect();

