import http.client
import mimetypes
conn = http.client.HTTPSConnection("api-pocurgencias.cens.cl")
payload = "{\n    \"fecha_nacimiento\": \"1978-03-28T12:00:00-04\",\n    \"fecha_toma_muestra\": \"2020-05-12T12:00:00-04\",\n    \"fecha_recepcion_muestra\": \"2020-05-16T12:00:00-04\",\n    \"fecha_validacion\": \"2020-05-16T12:00:00-04\",\n    \"numero_identificacion\": \"1-9\",\n    \"primer_nombre\": \"EDUARDO ANDRES\",\n    \"primer_apellido\": \"ARENAS\",\n    \"segundo_apellido\": \"CASTILLO\",\n    \"institucion_codigo\": \"005\",\n    \"orden_numero\": \"005SP1006566055_03\",\n    \"codigo_tipo_identificacion\": \"787300\",\n    \"sexo\": \"01\",\n    \"tipo_muestra\": \"3\",\n    \"otra_institucion\": \"CESFAM San Alberto Hurtado\",\n    \"codigo_test\": \"94309-2\",\n    \"resultado_test\": \"LA11883-8\"\n}"
headers = {
  'Content-Type': 'application/json'
}
conn.request("POST", "/poclisfhir/fhir", payload, headers)
res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))
