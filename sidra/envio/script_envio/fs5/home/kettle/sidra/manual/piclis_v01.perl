my $uri = 'https://api-pocurgencias.cens.cl/poclisfhir/fhir';
my $json = '{
    "fecha_nacimiento": "1978-03-28T12:00:00-04", 
    "fecha_toma_muestra": "2020-05-12T12:00:00-04",
    "fecha_recepcion_muestra": "2020-05-16T12:00:00-04",
    "fecha_validacion": "2020-05-16T12:00:00-04",
    "numero_identificacion": "1-9",
    "primer_nombre": "EDUARDO ANDRES",
    "primer_apellido": "ARENAS",
    "segundo_apellido": "CASTILLO",
    "institucion_codigo": "005",
    "orden_numero": "005SP1006566055_02",
    "codigo_tipo_identificacion": "787300",
    "sexo": "01",
    "tipo_muestra": "3",
    "otra_institucion": "CESFAM San Alberto Hurtado",
    "codigo_test": "94309-2",
    "resultado_test": "LA11883-8"
}';
my $req = HTTP::Request->new( 'POST', $uri );
$req->header( 'Content-Type' => 'application/json' );
$req->content( $json );
