#14 sidra3.edox ssmso carga diaria4.pl
use DBI;
use strict;
use LWP::UserAgent;
use HTTP::Request;
use DBI;

# connect
my $dbh = DBI->connect("DBI:Pg:dbname=ancorabi;host=192.168.150.39", "postgres", "cb400sf"
						, {'RaiseError' => 1});
my $sth = $dbh->prepare("
select cod_consumidor,folio_interno,folio_sigges,mensaje_xml2
  from sidra_base.ic_para_enviar
 where enviar_a_sidra=1
   and (pertinencia is null or pertinencia = 1)
   and extract(dow from fecha_entrada)=1
    and (
            (ges='1' and folio_sigges<>'')
             or
             ges='2'
        )
   and 
        (
		(
		    cod_consumidor = 'CJPII' 
		and fecha_entrada between (now()::date-'30 days'::interval)::date and (now()::date-'2 days'::interval)::date
		)
		or
		(      
		    cod_consumidor in ('CSAH','CMTC') 
		and fecha_entrada between (now()::date-'30 days'::interval)::date and (now()::date)::date
		) 
		or
		(
		    (tipo_profesional='2' or especialidad_destino_homologado='07-301-0')
                and fecha_entrada between (now()::date-'30 days'::interval)::date and (now()::date)::date
		)
	)
order by fecha_entrada desc
");
$sth->execute();
my ($cod_consumidor,$folio_interno,$folio_sigges,$mensaje_xml);
$sth->bind_columns(\$cod_consumidor,\$folio_interno,\$folio_sigges,\$mensaje_xml);

while ($sth->fetch()) {
	my $message = $mensaje_xml;
	my $userAgent = LWP::UserAgent ->new();
	my $request = HTTP::Request->new(POST =>'http://sidra.ssmso.cl/ServicioInterconsultaV3.asmx');
#	my $request = HTTP::Request->new(POST =>'http://163.247.80.132/SIDRAPRUEBAS/ServicioInterconsultaV3.asmx');

	$request->header(SOAPAction =>'"SSMSO/EnviarInterconsulta"');
	$request->content($message);
	$request->content_type("text/xml;character=utf-8");
	my $response = $userAgent->request($request);
	my $Estado = $response->as_string;
	my $rows = $dbh->do("INSERT INTO sidra_base.respuesta_sidra_ssmso 
							VALUES ('$cod_consumidor','$Estado',now(),'$folio_interno'
        									,'$mensaje_xml','$folio_sigges',11,0)");
	print "Folio_interno=\n";
	print $folio_interno;
	print "\n";
        print $cod_consumidor;
	print "\n";
	print $response->as_string;
	};
$dbh->disconnect();

