use strict;
use LWP::UserAgent;
use HTTP::Request;
use DBI;

my $message = '  
<soap:Envelope
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsd="http://www.w3.rg/2001/XMLSchema"
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <EnviarInterconsulta
            xmlns="SSMSO">
            <agente>@Ancora2012@</agente>
            <InterConsulta>
                <CodConsumidor>CJPII</CodConsumidor>
                <FolioSigges></FolioSigges>
                <FolioInterno>1005667</FolioInterno>
                <FolioInternoAGEG>-1</FolioInternoAGEG>
                <TipoAtencion>1</TipoAtencion>
                <Rutdigitador>15428829</Rutdigitador>
                <DVdigitador>5</DVdigitador>
                <CodigoEspecialidadOrigen>07-900-1</CodigoEspecialidadOrigen>
                <CodigoEspecialidadDestino>07-301-0</CodigoEspecialidadDestino>
                <Ges>2</Ges>
                <MotivoSolicitud></MotivoSolicitud>
                <ContRefIndicaciDIAG></ContRefIndicaciDIAG>
                <TramoFonasa>A</TramoFonasa>
                <UnidadOrigen>1</UnidadOrigen>
                <ProblemaSaludRama></ProblemaSaludRama>
                <FundamentoDiagnostico>FO: 10/02/00/2. Emb de 33+4. Ant de CCA , Sobre peso . Evaluacion curva de crecimiento fetal . Obs PHA, AU: 30  LCF: 140lpm MF (+) DU (-) </FundamentoDiagnostico>
                <ExamenRealizado>Envio para evaluacion de curva de crecimiento fetal .</ExamenRealizado>
                <DerivadoPara>1</DerivadoPara>
                <TipoPaciente>1</TipoPaciente>
                <CodigoSospechaAuge></CodigoSospechaAuge>
                <DescSospechaAuge></DescSospechaAuge>
                <IdentificaciondelUsuario>
                    <ServicioSalud>14010</ServicioSalud>
                    <RUN>18365649</RUN>
                    <DV>K</DV>
                    <Nombres>ROMINA DEL PILAR</Nombres>
                    <PrimerApellido>TAPIA</PrimerApellido>
                    <SegundoApellido>CARIMAN</SegundoApellido>
                    <FechaNacimiento>13/07/1990</FechaNacimiento>
                    <Sexo>1</Sexo>
                    <Prevision>1</Prevision>
                    <Prais>2</Prais>
                    <Region>13</Region>
                    <Cuidad>Santiago</Cuidad>
                    <ComunaResidencia>13112</ComunaResidencia>
                    <CondicRuralidadComunaResi>1</CondicRuralidadComunaResi>
                    <ClaseviaSectorDomiResi>-1</ClaseviaSectorDomiResi>
                    <NombrecalleDirecc>PASAJE SATURNINO RETAMALES 13987</NombrecalleDirecc>
                    <NumeroDirecc>13987</NumeroDirecc>
                    <RestoDirecc></RestoDirecc>
                    <Telefonofijo>65260036</Telefonofijo>
                    <Telefonomovil></Telefonomovil>
                    <Correoelectronico></Correoelectronico>
                    <Sospechadiagnostica></Sospechadiagnostica>
                    <Confirmaciondiagnostica></Confirmaciondiagnostica>
                </IdentificaciondelUsuario>
                <PrestSolicitadaLugar>
                    <FechaEntrada>15/06/2016</FechaEntrada>
                    <FechaCitacion></FechaCitacion>
                    <RunProfesionalSolicitante>15428829</RunProfesionalSolicitante>
                    <DvProfesionalSolicitante>5</DvProfesionalSolicitante>
                    <EstablecimientoOrigen>14327</EstablecimientoOrigen>
                    <EstablecimientoDestino>14103</EstablecimientoDestino>
                    <TipoPrestacion>2</TipoPrestacion>
                    <PrestacionMinsal>07-100-1</PrestacionMinsal>
                    <PrestacionEstablecimiento>-1</PrestacionEstablecimiento>
                </PrestSolicitadaLugar>
                <PrestRealizadaLugarResuelve>
                    <RunProfesionalResolvio>1</RunProfesionalResolvio>
                    <DvProfesionalResolvio>9</DvProfesionalResolvio>
                    <CausalSalida>-1</CausalSalida>
                    <FechaSalida></FechaSalida>
                    <EstableOtorgaAten>-1</EstableOtorgaAten>
                </PrestRealizadaLugarResuelve>
            </InterConsulta>
        </EnviarInterconsulta>
    </soap:Body>
</soap:Envelope>';
my $userAgent = LWP::UserAgent ->new();
my $request = HTTP::Request->new(POST =>'http://sidra.ssmso.cl/SIDRA_ANCORA/ServicioInterconsultaV3.ASMX');
$request->header(SOAPAction =>'"SSMSO/EnviarInterconsulta"');
$request->content($message);
$request->content_type("text/xml;character=utf-8");
my $response = $userAgent->request($request);
print $response->as_string;
