drop table if exists log_pentaho;create temp table log_pentaho as
SELECT distinct replace(substr(btrim(field_000),1,10),'/','')::date as fecha2
		--,field_000 as fecha
		--, field_001, field_002
		--,field_003
		,  field_003 informe--, field_004
		--, field_005
		, field_005 usuario
		,b.centro
		--, field_006, field_007, field_008, field_009, field_010
	FROM usr_edox.pentaho_log20200815 as a
	left join pentaho.usuarios as b on b.usuario=btrim(a.field_005);

update log_pentaho set centro='JPII' where usuario='aalbina';
update log_pentaho set centro='JPII' where usuario='aalvina';
update log_pentaho set centro='JPII' where usuario='acastillo';
update log_pentaho set centro='SAH' where usuario='cacevedo';
update log_pentaho set centro='MTC' where usuario='cmena';
update log_pentaho set centro='JPII' where usuario='csermini';
update log_pentaho set centro='MTC' where usuario='dnunez';
update log_pentaho set centro='UGI' where usuario='fbrito';
update log_pentaho set centro='MTC' where usuario='gschwartz';
update log_pentaho set centro='UGI' where usuario='jesoto';
update log_pentaho set centro='SAH' where usuario='jfortuno';
update log_pentaho set centro='MTC' where usuario='jmolina';
update log_pentaho set centro='SAH' where usuario='mbrugerolles';
update log_pentaho set centro='JPII' where usuario='mgalarce';
update log_pentaho set centro='JPII' where usuario='nsepulveda';
update log_pentaho set centro='UGI' where usuario='ugiancora';
update log_pentaho set centro='SAH' where usuario='vaguilera';
update log_pentaho set centro='SAH' where usuario='vlabbe';
update log_pentaho set centro='JPII' where usuario='vorellana';
update log_pentaho set centro='MTC' where usuario='vrengifo';
update log_pentaho set centro='SAH' where usuario='xrojas';
update log_pentaho set centro='MTC' where usuario='yahumada';

delete from log_pentaho where usuario in (
'system session'
,'scheduler.xaction'
,'joe'
,'backupScheduler.xaction'
,'suzy');

delete from log_pentaho where informe not like '%ncora%';

select distinct informe from log_pentaho order by 1;

select distinct * from log_pentaho;
select distinct usuario from log_pentaho where centro is null



select   a01.centro
    	,a.nif
		,a01.tis as rut
		,a.ciap 
		,a.descripcio ciap_descripcion
		,a.fecha ciap_fecha
		,a01.nombre
		,a01.apellido1
		,a01.apellido2
		,a01.domicilio
		,a01.tiscab
        ,edad_en_agnios(now()::Date,a01.nacimie	nto) as edad_en_agnios
  from admomi.iddncu as a
  	left join
  				(
  				select nif,centro,nacimiento,sexo,domicilio,tiscab,tis,nombre,apellido1,apellido2,
  					row_number() over (partition by nif order by fechalta) rownum
  					from admomi.iddpacpa3
  				) as a01
  				  on a01.nif=a.nif
  				 and a01.rownum=1
 where a.ciap in ('G12','G13')
   and a.nif not in (select nif from admomi.iddpacpa3 where estado2 is null);