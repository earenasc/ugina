drop view if exists public.vista_planilla_controles_matronas;
create view public.vista_planilla_controles_matronas as
select distinct a.nif2
			, b.pac_nombre_completo
			, edad_en_agnios (a.fecha_ingreso,b.nacimiento ) as edad 
			, b.telpart as telefono1
			, b.teldesp as telefono2
			, b.consultorio as sector
			, b.centro
			, a.medico_id 
			, (select b.med_nombre from admomi.iddmed as b where a.medico_id=b.id limit 1 ) as med_nombre
			, (select b.med_estamento_desc from admomi.iddmed as b where a.medico_id=b.id limit 1) as med_estamento
			, a.fecha_ingreso 
			, aym (a.fecha_ingreso) as aym
			, a.embarazsos_en_el_perioro
			,a01.numero_factores_de_riesgo
			,a02.fecha_ultima_regla
			,a03.fecha_probable_parto
			,a04.fecha_controles
			,a04.numeros_controles_cronologicos
  from 		(--drop table if exists ingresos_prenatal;create temp table ingresos_prenatal as
			select nif as nif2, clinico_id as medico_id , fecha2 as fecha_ingreso, especiali
				,row_number () over (partition by nif order by fecha2 asc) as embarazsos_en_el_perioro
			from admomi.iddcor 
			where wproto =347
			and numero=1
			and nif is not null 
			and fecha2 is not null 
			and fecha2 >='20200101'
			) as a
left join 	(--drop table if exists num_fact_riesgos;create temp table num_fact_riesgos as 
			select nif2, medico_id , fecha2, campo::int as numero_factores_de_riesgo, especiali
			from admomi.iddcop
			where wproto =347
			and wnumero=291
			and campo <>''
			and nif is not null 
			and fecha2 is not null 
			and fecha2 >='20200101'
			) as a01 on a.nif2=a01.nif2 
			 and a.fecha_ingreso=a01.fecha2 
			 and a.medico_id=a01.medico_id 
			 and a.especiali=a01.especiali
left join (--drop table if exists Fecha_Ultima_Regla;create temp table Fecha_Ultima_Regla	 as 
			select a.nif2, a.medico_id , a.fecha2, a.campo, especiali
					,(agnio||'-'||mes||'-'||dia)::date as fecha_ultima_regla
			  from 	( 
					select nif2, medico_id , fecha2, campo,especiali
					,length (campo) as num_caracteres
					,case when length (campo) = 10 then SUBSTRING ( campo, 7, 4 ) 
							when length (campo) = 9 then SUBSTRING ( campo, 6, 4 ) end agnio
					,case when length (campo) = 10 then SUBSTRING ( campo, 4, 2 ) 
							when length (campo) = 9 then SUBSTRING ( campo, 3, 2 ) end mes
					,case when length (campo) = 10 then SUBSTRING ( campo, 1, 2 ) 
							when length (campo) = 9 then 0||''||SUBSTRING ( campo, 1, 1 ) end dia
					from admomi.iddcop
					where wproto =347
					and wnumero=8
					and campo <>''
					and nif is not null 
					and fecha2 is not null 
					and fecha2 >='20200101'
					) as a
			) as a02 
			  on a.nif2=a02.nif2 
			 and a.fecha_ingreso=a02.fecha2 
			 and a.medico_id=a02.medico_id 
			 and a.especiali=a02.especiali
left join (--drop table if exists Fecha_Probable_Parto;create temp table Fecha_Probable_Parto	 as 
			select a.nif2, a.medico_id , a.fecha2,a.campo, especiali
				,(agnio||'-'||mes||'-'||dia)::date as fecha_probable_parto
			from 	( 
					select nif2, medico_id , fecha2,campo 	,especiali
					,length (campo) as num_caracteres
					,case when length (campo) = 10 then SUBSTRING ( campo, 7, 4 ) 
							when length (campo) = 9 then SUBSTRING ( campo, 6, 4 ) end agnio
					,case when length (campo) = 10 then SUBSTRING ( campo, 4, 2 ) 
							when length (campo) = 9 then SUBSTRING ( campo, 3, 2 ) end mes
					,case when length (campo) = 10 then SUBSTRING ( campo, 1, 2 ) 
							when length (campo) = 9 then 0||''||SUBSTRING ( campo, 1, 1 ) end dia
					from admomi.iddcop
					where wproto =347
					and wnumero=12
					and campo <>''
					and nif is not null 
					and fecha2 is not null 
					and fecha2 >='20200101'
					) as a
			) as a03 
			  on a.nif2=a03.nif2 
			 and a.fecha_ingreso=a03.fecha2 
			 and a.medico_id=a03.medico_id 
			 and a.especiali=a03.especiali
left join 	(--drop table if exists controles_prenatal;create temp table controles_prenatal as 
			select nif as nif2, clinico_id as medico_id , fecha2 as fecha_controles, especiali
				,row_number () over (partition by nif order by fecha2 asc) as numeros_controles_cronologicos
			from admomi.iddcor 
			where wproto =348
			and numero=1
			and nif is not null 
			and fecha2 is not null 
			and fecha2 >='20200101'			
			) as a04 
			  on a.nif2=a04.nif2 
			 and a.fecha_ingreso<=a04.fecha_controles 
			 and a.especiali=a04.especiali
left join admomi.iddpacpa3 as b on a.nif2=b.nif2
where a.nif2 not in (select nif2 from admomi.iddpacpa3 where estado2 is null)			
;

	select * from public.vista_planilla_controles_matronas;
	
grant select on public.vista_planilla_controles_matronas to consultaomi;
grant select on public.vista_planilla_controles_matronas to consulta;