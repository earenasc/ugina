 DROP MATERIALIZED VIEW rem_sd.cubo_rem_leche_02;

CREATE MATERIALIZED VIEW rem_sd.cubo_rem_leche_02
TABLESPACE tb_ancorabi2
AS
 SELECT replace(a.centro, 'JPII'::text, 'JP2'::text) AS centro2,
    a.centro,
    a.nif,
    a.rut,
    a.codigo,
    a.presenta,
    a.posologia,
    a.fenvases AS cantidad,
    a.codrem,
    a.tipo_dato,
    a.usuario,
    a.nombre,
    a.apellido1,
    a.apellido2,
    a.despachador,
    a.peso,
    a.fecha_digitacion,
    a.beneficiario_no_beneficiario,
    a.tipo_seccion,
    a.codrem_desc,
    a.edad_exacta,
    a.rango_edad,
    a.rango_cod,
    a.aym,
    b.denomina AS producto,
    a.peso * a.fenvases::double precision AS kilos,
    a.prevision
   FROM ( SELECT a_1.centro,
            a_1.nif,
            a_1.rut,
            a_1.codigo,
            a_1.presenta,
            a_1.posologia,
            a_1.fenvases,
            a_1.codrem,
            a_1.tipo_dato,
            a_1.usuario,
            btrim((((btrim(COALESCE(c.nombre, ' '::character varying)::text) || ' '::text) || btrim(COALESCE(c.apellido1, ' '::character varying)::text)) || ' '::text) || btrim(COALESCE(c.apellido2, ' '::character varying)::text)) AS nombre,
            c.apellido1,
            c.apellido2,
            a_1.usuario AS despachador,
                CASE
                    WHEN a_1.codigo::text = 'R599'::text OR a_1.codigo::text = '001862'::text OR a_1.codigo::text = 'R 598'::text OR a_1.codigo::text = 'R16400'::text OR a_1.codigo::text = 'R278'::text OR a_1.codigo::text = 'R6445'::text OR a_1.codigo::text = 'R540'::text THEN 1::double precision
                    WHEN a_1.codigo::text = '342914'::text OR a_1.codigo::text = 'R10940'::text OR a_1.codigo::text = 'R57748'::text THEN 0.4::double precision
                    WHEN a_1.codigo::text = '308700'::text OR a_1.codigo::text = 'R16408'::text THEN 0.5::double precision
                    WHEN a_1.codigo::text = '780005'::text THEN 0.9::double precision
                    WHEN a_1.codigo::text = 'R10942'::text THEN 1.0::double precision
                    
                    
                    ELSE NULL::double precision
                END AS peso,
            fechaomi(a_1.ufecha::text) AS fecha_digitacion,
                CASE
                    WHEN c.coberturap::text = ANY (ARRAY['00000001'::character varying::text, '00000002'::character varying::text, '00000003'::character varying::text, '00000004'::character varying::text, '00000005'::character varying::text, '00000007'::character varying::text]) THEN 'Beneficiario'::text
                    ELSE 'No Beneficiario'::text
                END AS beneficiario_no_beneficiario,
                CASE
                    WHEN c.coberturap::text = '00000000'::text THEN 'SIN PREVISION'::text
                    WHEN c.coberturap::text = '00000001'::text THEN 'FONASA A'::text
                    WHEN c.coberturap::text = '00000002'::text THEN 'FONASA B'::text
                    WHEN c.coberturap::text = '00000003'::text THEN 'FONASA C'::text
                    WHEN c.coberturap::text = '00000004'::text THEN 'FONASA D'::text
                    WHEN c.coberturap::text = '00000005'::text THEN 'PRAIS'::text
                    WHEN c.coberturap::text = '00000006'::text THEN 'ISAPRE'::text
                    WHEN c.coberturap::text = '00000007'::text THEN 'CAPREDENA'::text
                    ELSE 'ISAPRE'::text
                END AS prevision,
                CASE
                    WHEN a_1.codrem = '1'::text OR a_1.codrem = '4'::text OR a_1.codrem = '5'::text OR a_1.codrem = '7'::text OR a_1.codrem = '8'::text OR a_1.codrem = '18'::text OR a_1.codrem = '22'::text OR a_1.codrem = '23'::text THEN 'BASICO'::text
                    WHEN a_1.codrem = '2'::text OR a_1.codrem = '3'::text OR a_1.codrem = '6'::text OR a_1.codrem = '9'::text OR a_1.codrem = '19'::text OR a_1.codrem = '20'::text OR a_1.codrem = '21'::text THEN 'REFUERZO'::text
                    ELSE NULL::text
                END AS tipo_seccion,
                CASE
                    WHEN a_1.codrem = '0'::text THEN 'No Clasif. en Disp. Directa.'::text
                    WHEN a_1.codrem = '1'::text THEN 'Nino basico (Normal, sobrepeso u obeso)'::text
                    WHEN a_1.codrem = '2'::text THEN 'Nino refuerzo (Desnutrido)'::text
                    WHEN a_1.codrem = '3'::text THEN 'Nino Refuerzo (Con riesgo de desnutrir)'::text
                    WHEN a_1.codrem = '4'::text THEN 'Embarazada basico Normal, Obesa o Sobrepeso'::text
                    WHEN a_1.codrem = '5'::text THEN 'Embarazada basico Bajo Peso Recuperada'::text
                    WHEN a_1.codrem = '6'::text THEN 'Embarazada Refuerzo Bajo Peso'::text
                    WHEN a_1.codrem = '7'::text THEN 'Nodriza hijo basico LME (lactancia Materna Exclusiva)'::text
                    WHEN a_1.codrem = '8'::text THEN 'Nodriza hijo basico LMP (lactancia Materna predominante)'::text
                    WHEN a_1.codrem = '9'::text THEN 'Nodriza Refuerzo Bajo Peso'::text
                    WHEN a_1.codrem = '10'::text THEN 'PACAM - 70 anos y mas '::text
                    WHEN a_1.codrem = '11'::text THEN 'PACAM - Chile Solidario (65 y más años)'::text
                    WHEN a_1.codrem = '12'::text THEN 'PACAM - 65 y mas anios  -  con TBC'::text
                    WHEN a_1.codrem = '13'::text THEN 'PACAM - 60 y mas anios (Hogar de Cristo)'::text
                    WHEN a_1.codrem = '14'::text THEN 'PACAM - Otros convenios'::text
                    WHEN a_1.codrem = '15'::text THEN 'Prematuro - Leche Prematuro'::text
                    WHEN a_1.codrem = '16'::text THEN 'Prematuro - Leche Continuacion'::text
                    WHEN a_1.codrem = '17'::text THEN 'Prematuro - Leche Purita Fortificada'::text
                    WHEN a_1.codrem = '18'::text THEN 'Nodriza Básico de Niño Termino LME'::text
                    WHEN a_1.codrem = '19'::text THEN 'Nodriza Refuerzo LME'::text
                    WHEN a_1.codrem = '20'::text THEN 'Nodriza Refuerzo LMP'::text
                    WHEN a_1.codrem = '21'::text THEN 'Nodriza Ant. Bajo Peso c/formula Predomiante'::text
                    WHEN a_1.codrem = '22'::text THEN 'Nodriza basico LME'::text
                    WHEN a_1.codrem = '23'::text THEN 'Nodriza basico LMP'::text
                    WHEN a_1.codrem = '24'::text THEN 'Nodriza basico FP'::text
                    WHEN a_1.codrem = '25'::text THEN 'PACAM - 65 a 69 agnos Con alteracion de funcionalidad (EFAM alterado)'::text
                    WHEN a_1.codrem = '26'::text THEN 'PACAM - 65 a 69 agnos Programa vinculos'::text
                    WHEN a_1.codrem = '27'::text THEN 'PACAM - 60 a 69 agnos Con TBC'::text
                    WHEN a_1.codrem = '28'::text THEN 'PACAM - 60 a 69 agnos ELEAM autorizado'::text
                    ELSE NULL::text
                END AS codrem_desc,
            age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) AS edad_exacta,
                CASE
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '1 day'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '3 mons'::interval THEN '00-02m'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '3 mons'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '6 mons'::interval THEN '03-05m'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '6 mons'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '1 year'::interval THEN '06-11m'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '1 year'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '1 year 6 mons'::interval THEN '12-17m'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '1 year 6 mons'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '2 years'::interval THEN '18-23m'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '2 years'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '4 years'::interval THEN '24-47m'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '4 years'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '6 years'::interval THEN '48-71m'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '60 years'::interval THEN '60-y+a'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '65 years'::interval THEN '65-y+a'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '70 years'::interval THEN '70-y+a'::text
                    ELSE 'Otros'::text
                END AS rango_edad,
                CASE
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '00:00:00'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '2 mons'::interval THEN '0'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '3 mons'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '6 mons'::interval THEN '1'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '6 mons'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '1 year'::interval THEN '2'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '1 year'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '1 year 6 mons'::interval THEN '3'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '1 year 6 mons'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '2 years'::interval THEN '4'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '2 years'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '4 years'::interval THEN '5'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '4 years'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '6 years'::interval THEN '6'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '60 years'::interval THEN '7'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '65 years'::interval THEN '8'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '70 years'::interval THEN '9'::text
                    ELSE '10'::text
                END AS rango_cod,
                CASE
                    WHEN date_part('month'::text, fechaomi(a_1.ufecha::text)) < 10::double precision THEN (substr(fechaomi(a_1.ufecha::text)::text, 1, 4) || '-0'::text) || date_part('month'::text, fechaomi(a_1.ufecha::text))::text
                    ELSE (substr(fechaomi(a_1.ufecha::text)::text, 1, 4) || '-'::text) || btrim(date_part('month'::text, fechaomi(a_1.ufecha::text))::text)
                END AS aym
           FROM ( SELECT replace(btrim(a_2.centro::text), '00000000'::text, 'MTC'::text) AS centro,
                    a_2.ufecha,
                    a_2.nif,
                    a_2.rut,
                    a_2.codigo,
                    a_2.presenta,
                    a_2.posologia,
                    a_2.fenvases::integer AS fenvases,
                    round(a_2.cantidad)::text AS codrem,
                    'DISPENSACION'::text AS tipo_dato,
                    upper(btrim(a_2.usuario::text)) AS usuario
                   FROM rem_sd_base.iddfm2 a_2
                  WHERE a_2.situacion::text = 1::text
                UNION ALL
                 SELECT replace(b_1.centro::text, '00000000'::text, 'MTC'::text) AS centro,
                    a_2.fecha::text AS fecha,
                    b_1.nif,
                    b_1.tis AS rut,
                    a_2.material AS codigo,
                    ''::text AS presentacion,
                    a_2.comentario AS posologia,
                    a_2.cantidad::integer * '-1'::integer AS fenvases,
                    substr(a_2.comentario::text, 0, "position"(a_2.comentario::text, '/'::text)) AS codrem,
                    'CORRECCION'::text AS tipo_dato,
                    upper(btrim(a_2.usuario::text)) AS usuario
                   FROM rem_sd_base.iddcos a_2,
                    admomi.iddpacpa3 b_1,
                    admomi.iddmat c_1
                  WHERE NOT (a_2.autonumber IN ( SELECT error_de_correccion.autonumber
                           FROM rem_sd_base.error_de_correccion)) AND a_2.material::text = c_1.material::text AND btrim(a_2.comentario::text) ~~ '%/%'::text AND btrim(b_1.nif::text) = btrim(substr(a_2.comentario::text, "position"(a_2.comentario::text, '/'::text) + 1, length(a_2.comentario::text)))) a_1
             LEFT JOIN admomi.iddpacpa3 c ON a_1.nif::text = c.nif::text) a
     LEFT JOIN admomi.iddmat b ON btrim(a.codigo::text) = btrim(b.material::text)
WITH DATA;

ALTER TABLE rem_sd.cubo_rem_leche_02
    OWNER TO postgres;

GRANT ALL ON TABLE rem_sd.cubo_rem_leche_02 TO postgres;
GRANT SELECT ON TABLE rem_sd.cubo_rem_leche_02 TO PUBLIC;
GRANT SELECT ON TABLE rem_sd.cubo_rem_leche_02 TO consulta;
GRANT SELECT ON TABLE rem_sd.cubo_rem_leche_02 TO consultaomi;

DROP MATERIALIZED VIEW rem_sd.cubo_rem_leche_03;

CREATE MATERIALIZED VIEW rem_sd.cubo_rem_leche_03
TABLESPACE tb_ancorabi2
AS
 SELECT replace(a.centro, 'JPII'::text, 'JP2'::text) AS centro2,
    a.centro,
    a.nif,
    a.rut,
    a.codigo,
    a.presenta,
    a.posologia,
    a.fenvases AS cantidad,
    a.codrem,
    a.tipo_dato,
    a.usuario,
    a.nombre,
    a.apellido1,
    a.apellido2,
    a.despachador,
    a.peso,
    a.fecha_digitacion,
    a.beneficiario_no_beneficiario,
    a.tipo_seccion,
    a.codrem_desc,
    a.edad_exacta,
    a.rango_edad,
    a.rango_cod,
    a.aym,
    b.denomina AS producto,
    a.peso * a.fenvases::double precision AS kilos,
    a.prevision
   FROM ( SELECT a_1.centro,
            a_1.nif,
            a_1.rut,
            a_1.codigo,
            a_1.presenta,
            a_1.posologia,
            a_1.fenvases,
            a_1.codrem,
            a_1.tipo_dato,
            a_1.usuario,
            btrim((((btrim(COALESCE(c.nombre, ' '::character varying)::text) || ' '::text) || btrim(COALESCE(c.apellido1, ' '::character varying)::text)) || ' '::text) || btrim(COALESCE(c.apellido2, ' '::character varying)::text)) AS nombre,
            c.apellido1,
            c.apellido2,
            a_1.usuario AS despachador,
                CASE
                    WHEN a_1.codigo::text = 'R599'::text OR a_1.codigo::text = '001862'::text OR a_1.codigo::text = 'R 598'::text OR a_1.codigo::text = 'R16400'::text OR a_1.codigo::text = 'R278'::text OR a_1.codigo::text = 'R6445'::text OR a_1.codigo::text = 'R540'::text OR a_1.codigo::text = 'R2790'::text THEN 1::double precision
                    WHEN a_1.codigo::text = '342914'::text OR a_1.codigo::text = 'R10940'::text OR a_1.codigo::text = 'R57748'::text OR a_1.codigo::text = 'R2789'::text THEN 0.4::double precision
                    WHEN a_1.codigo::text = '308700'::text OR a_1.codigo::text = 'R16408'::text THEN 0.5::double precision
                    WHEN a_1.codigo::text = '780005'::text THEN 0.9::double precision
                    WHEN a_1.codigo::text = 'R10942'::text THEN 1.0::double precision
                    ELSE NULL::double precision
                END AS peso,
            fechaomi(a_1.ufecha::text) AS fecha_digitacion,
                CASE
                    WHEN c.coberturap::text = ANY (ARRAY['00000001'::character varying::text, '00000002'::character varying::text, '00000003'::character varying::text, '00000004'::character varying::text, '00000005'::character varying::text, '00000007'::character varying::text]) THEN 'Beneficiario'::text
                    ELSE 'No Beneficiario'::text
                END AS beneficiario_no_beneficiario,
                CASE
                    WHEN c.coberturap::text = '00000000'::text THEN 'SIN PREVISION'::text
                    WHEN c.coberturap::text = '00000001'::text THEN 'FONASA A'::text
                    WHEN c.coberturap::text = '00000002'::text THEN 'FONASA B'::text
                    WHEN c.coberturap::text = '00000003'::text THEN 'FONASA C'::text
                    WHEN c.coberturap::text = '00000004'::text THEN 'FONASA D'::text
                    WHEN c.coberturap::text = '00000005'::text THEN 'PRAIS'::text
                    WHEN c.coberturap::text = '00000006'::text THEN 'ISAPRE'::text
                    WHEN c.coberturap::text = '00000007'::text THEN 'CAPREDENA'::text
                    ELSE 'ISAPRE'::text
                END AS prevision,
                CASE
                    WHEN a_1.codrem = '1'::text OR a_1.codrem = '4'::text OR a_1.codrem = '5'::text OR a_1.codrem = '7'::text OR a_1.codrem = '8'::text OR a_1.codrem = '18'::text OR a_1.codrem = '22'::text OR a_1.codrem = '23'::text THEN 'BASICO'::text
                    WHEN a_1.codrem = '2'::text OR a_1.codrem = '3'::text OR a_1.codrem = '6'::text OR a_1.codrem = '9'::text OR a_1.codrem = '19'::text OR a_1.codrem = '20'::text OR a_1.codrem = '21'::text THEN 'REFUERZO'::text
                    WHEN a_1.codrem = '29'::text OR a_1.codrem = '30'::text THEN 'ALERGIA A LA PROTEINA DE LA LECHE DE VACA'::text
                    ELSE NULL::text
                END AS tipo_seccion,
                CASE
                    WHEN a_1.codrem = '0'::text THEN 'No Clasif. en Disp. Directa.'::text
                    WHEN a_1.codrem = '1'::text THEN 'Nino basico (Normal, sobrepeso u obeso)'::text
                    WHEN a_1.codrem = '2'::text THEN 'Nino refuerzo (Desnutrido)'::text
                    WHEN a_1.codrem = '3'::text THEN 'Nino Refuerzo (Con riesgo de desnutrir)'::text
                    WHEN a_1.codrem = '4'::text THEN 'Embarazada basico Normal, Obesa o Sobrepeso'::text
                    WHEN a_1.codrem = '5'::text THEN 'Embarazada basico Bajo Peso Recuperada'::text
                    WHEN a_1.codrem = '6'::text THEN 'Embarazada Refuerzo Bajo Peso'::text
                    WHEN a_1.codrem = '7'::text THEN 'Nodriza hijo basico LME (lactancia Materna Exclusiva)'::text
                    WHEN a_1.codrem = '8'::text THEN 'Nodriza hijo basico LMP (lactancia Materna predominante)'::text
                    WHEN a_1.codrem = '9'::text THEN 'Nodriza Refuerzo Bajo Peso'::text
                    WHEN a_1.codrem = '10'::text THEN 'PACAM - 70 anos y mas '::text
                    WHEN a_1.codrem = '11'::text THEN 'PACAM - Chile Solidario (65 y más años)'::text
                    WHEN a_1.codrem = '12'::text THEN 'PACAM - 65 y mas anios  -  con TBC'::text
                    WHEN a_1.codrem = '13'::text THEN 'PACAM - 60 y mas anios (Hogar de Cristo)'::text
                    WHEN a_1.codrem = '14'::text THEN 'PACAM - Otros convenios'::text
                    WHEN a_1.codrem = '15'::text THEN 'Prematuro - Leche Prematuro'::text
                    WHEN a_1.codrem = '16'::text THEN 'Prematuro - Leche Continuacion'::text
                    WHEN a_1.codrem = '17'::text THEN 'Prematuro - Leche Purita Fortificada'::text
                    WHEN a_1.codrem = '18'::text THEN 'Nodriza Básico de Niño Termino LME'::text
                    WHEN a_1.codrem = '19'::text THEN 'Nodriza Refuerzo LME'::text
                    WHEN a_1.codrem = '20'::text THEN 'Nodriza Refuerzo LMP'::text
                    WHEN a_1.codrem = '21'::text THEN 'Nodriza Ant. Bajo Peso c/formula Predomiante'::text
                    WHEN a_1.codrem = '22'::text THEN 'Nodriza basico LME'::text
                    WHEN a_1.codrem = '23'::text THEN 'Nodriza basico LMP'::text
                    WHEN a_1.codrem = '24'::text THEN 'Nodriza basico FP'::text
                    WHEN a_1.codrem = '25'::text THEN 'PACAM - 65 a 69 agnos Con alteracion de funcionalidad (EFAM alterado)'::text
                    WHEN a_1.codrem = '26'::text THEN 'PACAM - 65 a 69 agnos Programa vinculos'::text
                    WHEN a_1.codrem = '27'::text THEN 'PACAM - 60 a 69 agnos Con TBC'::text
                    WHEN a_1.codrem = '28'::text THEN 'PACAM - 60 a 69 agnos ELEAM autorizado'::text
                    WHEN a_1.codrem = '29'::text THEN 'Alergia a la proteína de la leche de vaca'::text
                    WHEN a_1.codrem = '30'::text THEN 'Alergia a la proteína de la leche de vaca'::text
                    ELSE NULL::text
                END AS codrem_desc,
            age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) AS edad_exacta,
                CASE
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '1 day'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '3 mons'::interval THEN '00-02m'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '3 mons'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '6 mons'::interval THEN '03-05m'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '6 mons'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '1 year'::interval THEN '06-11m'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '1 year'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '1 year 6 mons'::interval THEN '12-17m'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '1 year 6 mons'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '2 years'::interval THEN '18-23m'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '2 years'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '4 years'::interval THEN '24-47m'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '4 years'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '6 years'::interval THEN '48-71m'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '60 years'::interval THEN '60-y+a'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '65 years'::interval THEN '65-y+a'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '70 years'::interval THEN '70-y+a'::text
                    ELSE 'Otros'::text
                END AS rango_edad,
                CASE
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '00:00:00'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '2 mons'::interval THEN '0'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '3 mons'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '6 mons'::interval THEN '1'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '6 mons'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '1 year'::interval THEN '2'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '1 year'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '1 year 6 mons'::interval THEN '3'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '1 year 6 mons'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '2 years'::interval THEN '4'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '2 years'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '4 years'::interval THEN '5'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '4 years'::interval AND age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) < '6 years'::interval THEN '6'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '60 years'::interval THEN '7'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '65 years'::interval THEN '8'::text
                    WHEN age(fechaomi(a_1.ufecha::text)::timestamp without time zone, c.nacimiento::timestamp without time zone) >= '70 years'::interval THEN '9'::text
                    ELSE '10'::text
                END AS rango_cod,
                CASE
                    WHEN date_part('month'::text, fechaomi(a_1.ufecha::text)) < 10::double precision THEN (substr(fechaomi(a_1.ufecha::text)::text, 1, 4) || '-0'::text) || date_part('month'::text, fechaomi(a_1.ufecha::text))::text
                    ELSE (substr(fechaomi(a_1.ufecha::text)::text, 1, 4) || '-'::text) || btrim(date_part('month'::text, fechaomi(a_1.ufecha::text))::text)
                END AS aym
           FROM ( SELECT replace(btrim(a_2.centro::text), '00000000'::text, 'MTC'::text) AS centro,
                    a_2.ufecha,
                    a_2.nif,
                    a_2.rut,
                    a_2.codigo,
                    a_2.presenta,
                    a_2.posologia,
                    a_2.fenvases::integer AS fenvases,
                    round(a_2.cantidad)::text AS codrem,
                    'DISPENSACION'::text AS tipo_dato,
                    upper(btrim(a_2.usuario::text)) AS usuario
                   FROM rem_sd_base.iddfm2 a_2
                  WHERE a_2.situacion::text = 1::text
                UNION ALL
                 SELECT replace(b_1.centro::text, '00000000'::text, 'MTC'::text) AS centro,
                    a_2.fecha::text AS fecha,
                    b_1.nif,
                    b_1.tis AS rut,
                    a_2.material AS codigo,
                    ''::text AS presentacion,
                    a_2.comentario AS posologia,
                    a_2.cantidad::integer * '-1'::integer AS fenvases,
                    substr(a_2.comentario::text, 0, "position"(a_2.comentario::text, '/'::text)) AS codrem,
                    'CORRECCION'::text AS tipo_dato,
                    upper(btrim(a_2.usuario::text)) AS usuario
                   FROM rem_sd_base.iddcos a_2,
                    admomi.iddpacpa3 b_1,
                    admomi.iddmat c_1
                  WHERE NOT (a_2.autonumber IN ( SELECT error_de_correccion.autonumber
                           FROM rem_sd_base.error_de_correccion)) AND a_2.material::text = c_1.material::text AND btrim(a_2.comentario::text) ~~ '%/%'::text AND btrim(b_1.nif::text) = btrim(substr(a_2.comentario::text, "position"(a_2.comentario::text, '/'::text) + 1, length(a_2.comentario::text)))) a_1
             LEFT JOIN admomi.iddpacpa3 c ON a_1.nif::text = c.nif::text) a
     LEFT JOIN admomi.iddmat b ON btrim(a.codigo::text) = btrim(b.material::text)
WITH DATA;

ALTER TABLE rem_sd.cubo_rem_leche_03
    OWNER TO postgres;

GRANT SELECT ON TABLE rem_sd.cubo_rem_leche_03 TO ugina;
GRANT ALL ON TABLE rem_sd.cubo_rem_leche_03 TO postgres;
GRANT SELECT ON TABLE rem_sd.cubo_rem_leche_03 TO PUBLIC;
GRANT SELECT ON TABLE rem_sd.cubo_rem_leche_03 TO consulta;
GRANT SELECT ON TABLE rem_sd.cubo_rem_leche_03 TO consultaomi;

select * from rem_sd.cubo_rem_leche_03 where CODIGO='R10942';