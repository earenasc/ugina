drop table if exists seguimientos_totales01;create temp table seguimientos_totales01 as 
with consulta as	(
					select a.nif2,a.fecha2,a.campo 
					  from protocolos_omi_covid_base.iddcop as a
					 where a.wproto=409 and campo<>'' and campo is not null
					)
					select a01.rut as rut_caso,a.fecha2,a.campo 
					  from consulta as a --select * from protocolos_omi_covid_base.iddcop limit 100
					  	left join	(
					  				select nif2,tis as rut 
					  						,row_number() over (partition by tis order by fechalta) rownum
					  				  from admomi.iddpacpa3 
					  				 where estado2 is not null 
					  				   and tis<>'' 
					  				   and tis is not null 
					  				   and tis like '%-%'
					  				) as a01 
					  				  on a01.nif2=a.nif2 
					  				 and a01.rownum=1
					; 

update seguimientos_totales01 set rut_caso=replace(rut_caso,'.','')				;
update seguimientos_totales01 set rut_caso=replace(rut_caso,',','')				;
update seguimientos_totales01 set rut_caso=replace(rut_caso,'--','-')				;
update seguimientos_totales01 set rut_caso=replace(rut_caso,':','')				;
--update seguimientos_totales01 set rut_caso=replace(rut_caso,'G10T','')			;
--update seguimientos_totales01 set rut_caso=replace(rut_caso,'G12T','')			;
--update seguimientos_totales01 set rut_caso=replace(rut_caso,'G13T','')			;
update seguimientos_totales01 set rut_caso=replace(rut_caso,'*','')				;
update seguimientos_totales01 set rut_caso=replace(rut_caso,'/','')				;
update seguimientos_totales01 set rut_caso=replace(rut_caso,'+','')				;
update seguimientos_totales01 set rut_caso=replace(rut_caso,' ','')				;


delete from seguimientos_totales01 where rut_caso is null or rut_caso='';

drop table if exists 	covid19_kpi.insumos_contactos_2_o_mas_seguimientos01;
create table 			covid19_kpi.insumos_contactos_2_o_mas_seguimientos01 as
select distinct rut_caso
  from
  		(
		select a.*
				,lag(a.semana,1) over (partition by rut_caso order by semana) dif_semana
		  from 
				(
				select distinct rut_caso,extract(week from fecha2) as semana from seguimientos_totales01 order by 1,2
				) as a
		) as a			
 where semana-dif_semana=1;
