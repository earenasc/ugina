drop table if exists total_casos_identificados01;
create temp table total_casos_identificados01 as
select   a.patient_rut --NULLIF(regexp_replace(a01.nif, '\D','','g'), '')::numeric as nif2
		,a.orderdate::timestamp as fecha 
  from examenes_etl.mg_vw_ancora2 as a 
 where a.profilecode = '2798H' 
   and a.interpretedresult='Positivo'
union
select a01.rut--,NULLIF(regexp_replace(a.nif, '\D','','g'), '')::numeric as nif2
	,a02.stk_fecha::timestamp 
  from protocolos_omi_covid_base.examenes_pcr_omi as a
  left join	(
  			select nif2,tis as rut,row_number() over (partition by nif order by fechalta) rownum
    		  from admomi.iddpacpa3
    		 where tis is not null and tis<>'' and tis like '%-%'
    		) as a01 on a01.nif2=a.nif2 and a01.rownum=1
  left join	(
  			select * 
  					,row_number() over (partition by nif2,numorden order by stk_fecha) rownum
  			   from protocolos_omi_covid_base.hisa_iddome as a 
  			  where stk_tipo_operacion='A'
  			) as a02 on a02.nif2=a.nif2 and a02.rownum=1 and a02.numorden=a.numorden
 where a.resultado like '%POSITIV%'
 union
 select   a01.rut
		,a02.stk_fecha::timestamp as fecha--a.fecha
  from protocolos_omi_covid_base.iddncu as a --select * from protocolos_omi_covid_base.hisa_iddncu as a
  left join	(
  			select nif2,tis as rut,row_number() over (partition by nif order by fechalta) rownum
    		  from admomi.iddpacpa3
    		 where tis is not null and tis<>'' and tis like '%-%'
    		) as a01 on a01.nif2=a.nif2 and a01.rownum=1
  left join	(
  			select * 
  					,row_number() over (partition by nif2,especiali order by stk_fecha) rownum
  			   from protocolos_omi_covid_base.hisa_iddncu as a 
  			  where stk_tipo_operacion='A'
  			) as a02 on a02.nif2=a.nif2 and a02.rownum=1 and a02.especiali=a.especiali
  where a.ciap in ('G12','G13')
; 

delete from total_casos_identificados01 where patient_rut is null;
--select distinct patient_rut from total_casos_identificados01;
--select * from total_casos_identificados01 order by 1 nulls first;

drop table if exists total_casos_identificados02;
create temp table    total_casos_identificados02 as
select   upper(btrim(a.patient_rut)) as rut
		--a01.nif2
		,min(a.fecha) as fecha 
  from total_casos_identificados01 as a
  left join
  			(
  			select nif2,tis as rut 
  					,row_number() over (partition by nif2 order by fechalta) rownum
  			  from admomi.iddpacpa3
  			 where tis is not null and tis<>'' and tis like '%-%'
  			) as a01 on a01.rut=a.patient_rut
 group by 1 order by 1 nulls first; --2842 --2815 --2836

delete from  total_casos_identificados02 where rut is null;

--select *,row_number() over (partition by fecha) from total_casos_identificados02 order by 3 desc,2

drop table if exists casos_contactos01;
create temp table    casos_contactos01 as
select  distinct a01.rut as rut_caso,valor as rut_contacto
  from protocolos_omi_covid_base.datos_contactos01 as a
  left join	(
  			select nif2,tis as rut 
  					,row_number() over (partition by nif2 order by fechalta) rownum
  			  from admomi.iddpacpa3
  			 where tis is not null and tis<>'' and tis like '%-%'
  			) as a01 on a01.nif2=a.nif
 where a.dato like 'RUT%' 
   and a.valor<>''
 order by 1,2 nulls last; --7293

drop table if exists pacientes01;
create temp table    pacientes01 as 
with consulta as 	(
					select btrim(upper(rut_caso)) as rut_paciente 	from casos_contactos01 union
					select btrim(upper(rut_caso)) as rut_contacto 	from casos_contactos01 union
					select btrim(upper(rut))	  as rut			from total_casos_identificados02
					)
					select distinct rut_paciente 
					  from consulta 
					 where rut_paciente is not null order by 1;

drop table if exists pacientes02;
create temp table    pacientes02 as 
select a.rut_paciente,rut_caso 
  from pacientes01 as a
  left join casos_contactos01 as a01 on a01.rut_contacto=a.rut_paciente;
  
drop table if exists pacientes03;create temp table    pacientes03 as 
select distinct a.rut_paciente
		,substr(rut_paciente,1,position('-' in rut_paciente)-1) as rut_pac2
		,a.rut_caso
		,substr(rut_caso,1,position('-' in rut_caso)-1) as rut_caso2
		,a01.fecha 
  from pacientes02 as a
  left join total_casos_identificados02 as a01 on a01.rut=a.rut_caso
order by 2,3;

--select distinct substr(rut_paciente,1,position('-' in rut_paciente)-1) from pacientes03 where rut_paciente like '%-%' and rut_caso like '%-%';
--select * from total_casos_identificados02

drop table if exists pacientes04;create temp table    pacientes04 as
select  distinct row_number() over (partition by rut_caso) repetido_caso
		,rut_caso
		,row_number() over (partition by rut_paciente) repetido_contacto
		,rut_paciente as rut_contacto
		,fecha as fecha_caso
		,min(fecha) over (partition by rut_paciente) as min_fecha_caso
  from pacientes03 
 where fecha is not null
   and rut_paciente 
  	  in (
'15939383-6'
'16646722-5',
'18609435-2',
'26153173-9',
'7073503-2',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'26153173-9',
'26153173-9',
'26153173-9',
'26153173-9',
'26153173-9',
'15939383-6',
'17672873-6',
'18609435-2',
'23010471-9',
'26153173-9',
'7073503-2',
'9253244-5') or rut_caso in (
'15939383-6'
'16646722-5',
'18609435-2',
'26153173-9',
'7073503-2',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'9253244-5',
'26153173-9',
'26153173-9',
'26153173-9',
'26153173-9',
'26153173-9',
'15939383-6',
'17672873-6',
'18609435-2',
'23010471-9',
'26153173-9',
'7073503-2',
'9253244-5')
  order by 5; 
  --desc
  --26153173-9

drop table if exists pacientes04;create temp table    pacientes04 as
select  distinct row_number() over (partition by rut_caso) repetido_caso
		,rut_caso
		,row_number() over (partition by rut_paciente) repetido_contacto
		,rut_paciente as rut_contacto
		,fecha as fecha_caso
		,min(fecha) over (partition by rut_paciente) as min_fecha_caso
  from pacientes03;

drop table if exists pacientes05;create temp table pacientes05 as 
 select distinct a.*,a01.rut_caso as rut_caso_primario
   from pacientes04 as a
   left join 	(
   				select rut_caso,fecha_caso
   						,row_number() over (partition by rut_caso order by fecha_caso) rownum
   				  from pacientes04 where fecha_caso is not null
   )as a01 on a.min_fecha_caso=a01.fecha_caso and a01.rownum=1;

delete 
  from pacientes05 
 where rut_caso in 
						(
						select tis from admomi.iddpacpa3 where estado2 is null and tis is not null and tis<>'' and tis like '%-%'
						)
    or rut_contacto in 
						(
						select tis from admomi.iddpacpa3 where estado2 is null and tis is not null and tis<>'' and tis like '%-%'
						);
					
select * from pacientes05