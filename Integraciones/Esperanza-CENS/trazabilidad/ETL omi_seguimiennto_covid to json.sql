--ingreso
drop table if exists 	poc_epivigila_casos01;
create temp table 		poc_epivigila_casos01 as
select distinct nif,especiali,ciap   
  FROM admomi.iddncu as a 
 WHERE a.ciap IN ('G12','G13');

drop table if exists 	poc_epivigila_cmd_ingreso01;
create temp table 		poc_epivigila_cmd_ingreso01 as
SELECT   a02.numero_identificacion
		,a01.fecha_ingreso
		,case 
			when a.ciap='G10' then '840544004' 		--Caso Sospechoso
			when a.ciap='G12' then '840539006' 		--Caso Confirmado
			when a.ciap='G13' then '64781000122109' 	--Caso Probable
		end as codigo_diagnostico
		,a.* 
  FROM poc_epivigila_casos01 as a
	left join	(
				select stk_fecha as fecha_ingreso,*
						,row_number() over (partition by nif,ciap,especiali order by stk_fecha) as rownum
				  from protocolos_omi_covid_base.hisa_iddncu --select * from admomi.hisa_iddncu
				 where stk_tipo_operacion = 'A'
				) as a01
			      on a01.nif=a.nif
			     and a01.especiali=a.especiali
			     and a01.ciap=a.ciap
			     and a01.rownum=1
	left join	(
	  			select centro,nif,tis rut
	  					,case when tis like '%-%' and length(tis)>=3
							then NULLIF(regexp_replace(substr(tis,1,position('-' in tis)-1), '\D','','g'), '')::integer 
							end as numero_identificacion
	  					,nombre as primer_nombre
	  					,apellido1 as  primer_apellido
	  					,apellido2 as segundo_apellido
	  					,nacimiento as fecha_nacimiento
	  					,case 
	  						when sexo='F' then '02'
	  						when sexo='M' then '01'
	  						else '99' end sexo --desconocido
	  					,row_number() over (partition by nif2 order by fechalta) rownum
	  			  from admomi.iddpacpa3
	  			 where tis is not null and tis like '%-%'
	  			) as a02
	  			  on a02.nif 	= a.nif
	  			 and a02.rownum	= 1;

			    
select * from poc_epivigila_cmd_ingreso01 order by 1 nulls first;			    
--fecha_ingreso
--	787300 as tipo_documento --rut