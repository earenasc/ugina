drop table if exists usr_edox.poc_lis_cens;
create table usr_edox.poc_lis_cens as
select a.*,row_to_json(a) as cmd_json
		,jsonb_pretty(row_to_json(a)::jsonb) as cmd_json2
  from
  	(
SELECT   787300::integer as codigo_tipo_identificacion
		,case when a.paciente_rut like '%-%' and length(a.paciente_rut)>=3
			then NULLIF(regexp_replace(substr(a.paciente_rut,1,position('-' in a.paciente_rut)-1), '\D','','g'), '')::integer
			end as numero_identificacion 
		,primer_nombre
		, primer_apellido
		,segundo_apellido
		,fecha_nacimiento
		,sexo
		,3 as tipo_muestra
		,orden_fecha_toma::timestamptz as fecha_toma_muestra
		,case 
				when a00.centro='MTC'  	then '005' --'323'
				when a00.centro='JPII'	then '005' --'327'
				when a00.centro='SAH'  	then '005' --'329'
				end as institucion_codigo
		,case 
				when a00.centro='MTC'  	then 'CESFAM Madre Teresa de Calcuta'
				when a00.centro='JPII'	then 'CESFAM Juan Pablo II'
				when a00.centro='SAH'  	then 'CESFAM San Alberto Hurtado'
				end as otra_institucion
		,'94309-2' as CODIGO_TEST
		,case 
			when examen_resultado_interpretado='Positivo' then 'LA11882-0'
			when examen_resultado_interpretado='Negativo' then 'LA11883-8'
			else 'LA9663-1'
			end resultado_test
			,orden_fecha_recepcion as fecha_validacion
			,case 
				when a00.centro='MTC'  	then '005'||orden_numero --'323'||orden_numero
				when a00.centro='JPII'	then '005'||orden_numero --'327'||orden_numero
				when a00.centro='SAH'  	then '005'||orden_numero --'329'||orden_numero
				end orden_numero	--,* --NULLIF(regexp_replace(admomi.iddpacpa3.telpart, '\D','','g'), '')::numeric
  FROM examenes_etl.mg_vw_ancora_final2 as a --select *   FROM examenes_etl.mg_vw_ancora_final2 as a  where a.examen_codigo = '2798H'
  left join 
  			(
  			select centro,nif2,tis rut
  					,nombre as primer_nombre
  					,apellido1 as  primer_apellido
  					,apellido2 as segundo_apellido
  					,nacimiento as fecha_nacimiento
  					,case 
  						when sexo='F' then '02'
  						when sexo='M' then '01'
  						else '99' end sexo --desconocido
  					,row_number() over (partition by nif2 order by fechalta) rownum
  			  from admomi.iddpacpa3
  			 where tis is not null and tis like '%-%'
  			) as a00
  			  on a00.rut 	= a.paciente_rut
  			 and a00.rownum	= 1 
 where a.examen_codigo = '2798H'
 
 --select FROM examenes_etl.mg_vw_ancora_final2 as a
 ) as a;
 -- order by a.orden_fecha_solicitud desc nulls last
 
select * from usr_edox.poc_lis_cens;


with consulta as 
					(
					select 1 as q,orden_numero,examen_resultado_interpretado,orden_fecha_solicitud
					  FROM examenes_etl.mg_vw_ancora_final2
					 where examen_codigo = '2798H' 
					   and orden_fecha_solicitud between '20200501 00:00:00' and '20200705 23:59:59' order by 3 desc
 					)
					select *
							--,case when examen_resultado_interpretado='Positivo' then count(*) over (partition by q)  
					  from consulta;
					  
					  select 28::numeric/130::numeric
					  select 81::numeric/303::numeric
					  select 398::numeric/1055::numeric
					  select 1068::numeric/2571::numeric
					  
					  
					  select * from usr_edox.poc_lis_cens where numero_identificacion=13524894;

{"codigo_tipo_identificacion":787300,"numero_identificacion":13524894,"primer_nombre":"EDUARDO ANDRES","primer_apellido":"ARENAS","segundo_apellido":"CASTILLO","fecha_nacimiento":"1978-03-28","sexo":"01","tipo_muestra":3,"fecha_toma_muestra":"2020-05-12","institucion_codigo":"005","otra_institucion":"CESFAM San Alberto Hurtado","codigo_test":"94309-2","resultado_test":"LA11883-8","fecha_validacion":"2020-05-12","orden_numero":"005SP1006566055"}

{
    "sexo": "01",
    "codigo_test": "94309-2",
    "orden_numero": "005SP1006566055",
    "tipo_muestra": 3,
    "primer_nombre": "EDUARDO ANDRES",
    "resultado_test": "LA11883-8",
    "primer_apellido": "ARENAS",
    "fecha_nacimiento": "1978-03-28",
    "fecha_validacion": "2020-05-12",
    "otra_institucion": "CESFAM San Alberto Hurtado",
    "segundo_apellido": "CASTILLO",
    "fecha_toma_muestra": "2020-05-12",
    "institucion_codigo": "005",
    "numero_identificacion": 13524894,
    "codigo_tipo_identificacion": 787300
}